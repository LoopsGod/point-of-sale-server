import { spawnSync } from "child_process";
import { defineConfig, ProxyOptions } from "vite";
import vue from "@vitejs/plugin-vue";
import Components from "unplugin-vue-components/vite";
import { AntDesignVueResolver } from "unplugin-vue-components/resolvers";
import checker from "vite-plugin-checker";

const runningInDocker = !!spawnSync("which", ["docker"]).status;
const proxyHost: ProxyOptions = {
  target: `http://${runningInDocker ? "backend" : "localhost"}:5000`,
  xfwd: true,
};

export default ({ mode }) => {
  const production = mode === "production";

  return defineConfig({
    root: "./src",
    define: {
      __SENTRY_DSN__: JSON.stringify(process.env["FRONTEND_SENTRY_DSN"] || ""),
      __APP_VERSION__: JSON.stringify(process.env["CI_COMMIT_SHORT_SHA"] || ""),
      // The vue.esm-bundler.js shows warnings when these variables aren't defined.
      // The option API can be disabled, when all components are converted to Composition API.
      __VUE_PROD_DEVTOOLS__: !production,
    },
    build: {
      target: "es2018",
      outDir: "build",
      sourcemap: true,
    },
    resolve: {
      alias: [{ find: "vue", replacement: "vue/dist/vue.esm-bundler.js" }],
    },
    plugins: [
      Components({
        // TODO: We can also use unplugin-vue to automatically import our own components when they are used..
        resolvers: [
          AntDesignVueResolver({
            resolveIcons: true,
            importStyle: "less",
          }),
        ],
      }),
      vue(),
      checker({ typescript: true }),
    ],
    server: {
      host: "0.0.0.0",
      port: 8080,
      proxy: {
        "/api": proxyHost,
        "/static": proxyHost,
        "/redoc": proxyHost,
        "/docs": proxyHost,
      },
    },
    css: {
      preprocessorOptions: {
        sass: {
          javascriptEnabled: true,
          indentedSyntax: true,
        },
        less: {
          modifyVars: {
            "primary-color": "#304ba3",
          },
          javascriptEnabled: true,
        },
      },
    },
  });
};
