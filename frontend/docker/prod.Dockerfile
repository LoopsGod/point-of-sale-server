FROM node:18-alpine AS build-deps
WORKDIR /app

RUN npm install -g npm

COPY package.json package-lock.json tsconfig.json vite.config.ts ./

RUN npm ci --no-progress --omit=optional

COPY src /app/src
ARG FRONTEND_SENTRY_DSN
ENV FRONTEND_SENTRY_DSN=$FRONTEND_SENTRY_DSN
ARG CI_COMMIT_SHORT_SHA
ENV CI_COMMIT_SHORT_SHA=$CI_COMMIT_SHORT_SHA
RUN npm run build

FROM nginx:alpine

COPY --from=build-deps /app/src/build /usr/share/nginx/html
COPY src/img /usr/share/nginx/html/img
COPY nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
