// (Mostly) from https://github.com/weekdone/GoogleChartStyles
// This makes Google Charts look significantly better.
export const defaultChartOptions = {
  vAxis: {
    minValue: 0,
    baselineColor: "transparent",
  },
  annotations: {
    textStyle: {
      auraColor: "white",
    },
  },
  legend: {
    position: "top",
    alignment: "center",
    textStyle: { color: "#607d8b", fontName: "Roboto", fontSize: "12" },
  },
  colors: [
    "#3766CB",
    "#DB3C18",
    "#FF9800",
    "#19951E",
    "#990099",
    "#0099c6",
    "#dd4477",
    "#66aa00",
    "#b82e2e",
    "#316395",
  ],
  backgroundColor: "transparent",
  areaOpacity: 0.24,
  lineWidth: 1,
  chartArea: {
    width: "100%",
    height: "80%",
  },
  pieSliceBorderColor: "#263238",
  pieSliceTextStyle: { color: "#607d8b" },
  pieHole: 0.9,
  bar: { groupWidth: "40" },
  colorAxis: { colors: ["#3f51b5", "#2196f3", "#03a9f4", "#00bcd4"] },
  datalessRegionColor: "#37474f",
  displayMode: "regions",
} as const;

export interface AreaChartYSeries {
  title: string;
  values: unknown[];
  type?: string;
}

export interface BarChartDataEntry {
  label: unknown;
  value: unknown;
  annotation?: unknown;
  tooltipHtml?: string;
}
