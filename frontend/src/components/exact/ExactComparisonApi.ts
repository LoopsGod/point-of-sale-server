import { Api, urlWithSearchParams } from "../../scripts/api/api";
import type { ApiResponse } from "../../scripts/api/api";
import { Account } from "../../scripts/types";

export interface ExactComparisonResult {
  account: Account;

  dailyOrders: {
    date: string;
    amount: number;
    count: number;
  }[];

  dailyTransactions: {
    date: string;
    amount: number;
  }[];
}

export class ExactComparisonApi extends Api {
  public async getComparison(
    dateFilter: string,
    accountFilter?: string
  ): Promise<ApiResponse<ExactComparisonResult>> {
    const params = {
      dateFilter,
    };

    if (accountFilter) {
      params["accountFilter"] = accountFilter;
    }

    return this.get(
      urlWithSearchParams("/api/admin/exact/comparison/", params)
    );
  }
}

export const exactComparisonApi = new ExactComparisonApi();
