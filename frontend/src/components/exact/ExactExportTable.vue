<template lang="pug">
DataTable(
  :disabled="true",
  endpoint="/api/admin/exact/export/",
  rowKey="date",
  size="small",
  searchPlaceholder="Search for booking identifiers",
  :columnMapping="columnMapping",
  :scroll="{ x: true }",
  :filterDefinition="filterDefinition"
)
  template(#prop-date="{ record }")
    a-tag {{ isoDateStringToLocale(record.date) }}

  template(#prop-status="{ record, loading }")
    ExactExportStatusDisplay(v-model="record.status", :loading="loading", :edit="false")
    a-popover(v-if="record.messages.length && record.status === 'error'")
      template(#content)
          p(style="padding: 0;" v-for="message of record.messages") {{ message.description }}
      ExclamationCircleOutlined

  template(#prop-data="{ record }")
    span(v-if="record.data") {{ record.data }}
    em(v-else) N/A

  template(#prop-orderCount="{ record }")
    span {{ record.orderCount }}

  template(#row-buttons="{ record, context, loading }")
    a-button-group(size="small", @click.stop.prevent="() => {}")
      a-button(
        type="primary",
        :loading="loading",
        :disabled="!canExportRecord(record)",
        @click="() => onExport(record, context)"
      ) Export
      a-popconfirm(
        placement="left",
        @confirm="() => onDelete(record, context)",
        :disabled="!canDeleteRecord(record)"
      )
        template(#title)
          div(style="max-width: 300px;")
            | This will delete the export record from this system, it will not delete the booking in Exact.
            | To delete this booking, it must be deleted from Exact first.
        a-button(:loading="loading", :disabled="!canDeleteRecord(record)") Delete
</template>

<style lang="sass" scoped>
@import '../../styles/admin.sass'
</style>

<script setup lang="ts">
import { markRaw } from "vue";
import DataTable from "../datatable/DataTable.vue";
import type {
  ColumnMapping,
  DataTableContext,
  FilterDefinition,
} from "../../scripts/DataTableTypes";
import MonthFilter from "../common/MonthFilter.vue";
import NumberFilter from "../common/NumberFilter.vue";

import ExactExportStatusDisplay from "./ExactExportStatusDisplay.vue";
import ExactExportStatusFilter from "./ExactExportStatusFilter.vue";
import ExactDivisionFilter from "./ExactDivisionFilter.vue";
import ExactDivisionDisplay from "./ExactDivisionDisplay.vue";
import { useExactStore } from "../../scripts/store/exact";
import { type ExactExport } from "../../scripts/api/exact";
import { exactApi } from "../../scripts/api/exact";
import { isApiResponse } from "../../scripts/api/api";
import { notification } from "ant-design-vue";
import { usePollUntil } from "../../scripts/composables/pollUntil";

interface ExactExportTableProps {
  canExport: boolean;
}

const props = withDefaults(defineProps<ExactExportTableProps>(), { canExport: true });

const columnMapping: ColumnMapping = {
  date: {
    title: "Date",
    align: "left",
    sort: "date",
  },
  status: {
    sort: "status",
  },
  orderCount: {
    title: "Order #",
    sort: "orderCount",
    align: "right",
  },
  division: {
    title: "Administration",
    sort: "division",
    component: markRaw(ExactDivisionDisplay),
  },
  data: {
    title: "Booking",
    sort: "data",
    align: "right",
  },

} as const;

const filterDefinition: FilterDefinition[] = [
  {
    title: "Month",
    query: "dateFilter",
    component: markRaw(MonthFilter),
  },
  {
    title: "Administration",
    query: "divisionFilter",
    component: markRaw(ExactDivisionFilter),
  },
  {
    title: "Order #",
    query: "countFilter",
    component: markRaw(NumberFilter),
  },
  {
    title: "Status",
    query: "statusFilter",
    component: markRaw(ExactExportStatusFilter),
  },
];

const onExport = async (record: ExactExport, context: DataTableContext) => {
  if (!canExportRecord(record)) {
    return;
  }

  context.setIsLoading(record, true);

  const exportDate = new Date(record.date);

  try {
    const response = await exactApi.export(
      exportDate.getFullYear(),
      exportDate.getMonth() + 1,
      exportDate.getDate()
    );

    const exportId = response.result.exportId;

    usePollUntil(2500, () => exactApi.getExport(exportId), (response) => {
      const updatedExport = response.result;
      const isPending = updatedExport.status === "pending";

      if (!isPending) {
        // Backend doesn't preserve order count
        updatedExport.orderCount = record.orderCount;
        context.saveEdit(updatedExport);
        context.setIsLoading(record, false);
      }

      return !isPending;
    });
  } catch (e) {
    const message = "Error exporting to Exact Online"
    let description;
    if (!isApiResponse(e)) {
      description = "An unknown error occurred";
    } else if (e.errorIdentifier === "setup_not_finished" || e.errorIdentifier === "exact_incomplete_setup") {
      description = "Setup not finished."
    } else if (e.errorIdentifier === "end_date_in_future") {
      description = "Cannot export a date in the future."
    } else if (e.errorIdentifier === "exact_journal_pos_code_not_set" || e.errorIdentifier === "exact_journal_pos_description_not_set") {
      description = "Point of Sale journal account in Exact not selected."
    } else if (e.errorIdentifier === "exact_income_ledgers_not_set") {
      description = "Not all income ledgers have an associated ledger account in Exact."
    }

    notification.error({
        message,
        description
    });
    context.setIsLoading(record, false);
  }
}


const onDelete = async (record: ExactExport, context: DataTableContext) => {
  if (!canDeleteRecord(record) || !record.id) {
    return;
  }

  context.setIsLoading(record, true);

  try {
    const response = await exactApi.deleteExport(
      record.id
    );
    const exactExport = response.result;
    exactExport.orderCount = record.orderCount;
    context.saveEdit(exactExport);
  } catch (e) {
    const message = "Error deleting Exact export record"
    let description;
    if (!isApiResponse(e)) {
      description = "An unknown error occurred";
    } else if (e.errorIdentifier === "batch_not_exported" || e.errorIdentifier === "unknown_booking_id") {
      description = "This date was not exported."
    } else if (e.errorIdentifier === "exact_booking_exists") {
      description = `Booking '${record.data}' should be deleted in Exact Online manually first.`;
    }
    notification.error({
        message,
        description
    })
  } finally {
    context.setIsLoading(record, false);
  }

}

const isoDateStringToLocale = (date: string) => {
  return new Date(date).toLocaleString("nl-NL", {
    year: "numeric",
    month: "long",
    day: "numeric",
  });
};

const canExportRecord = (record: ExactExport) => {
  return props.canExport && (record.status !== "exported" && record.status !== "pending") && record.orderCount > 0
};

const canDeleteRecord = (record: ExactExport) => {
  return record.status === "exported" && record.id;
};

const exactStore = useExactStore();

await exactStore.loadAll();
</script>
