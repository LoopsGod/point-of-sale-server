<template>
  <a-skeleton v-if="isLoading && !props.edit" size="small" :paragraph="false" />
  <slot v-else-if="!props.edit" name="display" :values="modelValueRecordArray">
    {{ modelValueRecordArray?.map((slv) => slv[props.labelField]).join(", ") }}
  </slot>
  <a-select
    v-else
    style="width: 100%"
    :value="modelValue"
    :size="size"
    :placeholder="placeholder"
    :filter-option="false"
    :not-found-content="isLoading ? undefined : null"
    :options="selectOptions"
    :showSearch="true"
    :allow-clear="true"
    :mode="mode"
    :getPopupContainer="getPopupContainer"
    @search="onSearch"
    @change="onChange"
  >
    <template v-if="isLoading" #notFoundContent>
      <a-spin size="small" />
    </template>
  </a-select>
</template>
<script setup lang="ts">
import { computed, ref } from "@vue/reactivity";
import { debounce } from "../../scripts/util";
import { useSchemaStore } from "../../scripts/store/schema";
import SchemaParser from "../../scripts/schema/parser";
import { storeToRefs } from "pinia";
import { nextTick, watchEffect } from "vue";
import { usePaginatedSearchQuery } from "../../scripts/query/pagination";
import { useQueryClient } from "@tanstack/vue-query";

// We are unable to retrieve more than 50 items from the pagination API, thus when
// in non-edit mode, we cannot retrieve the full list anymore. To make this more predictable
// we also limit the selector to 50 items.
const MAX_SELECTABLE_ITEMS = 50;

type ItemKey = number | string;

interface LabeledValue {
  key: ItemKey;
  label: string;
  value: ItemKey;
}

interface SchemaSelectFieldProps {
  /**
   * Backend url that returns a `PaginatedResponse[Record<string, unknown>]` and takes `PageSearchParameters` as query.
   * Must have an `idFilter` LiteralEqualsFilter.
   */
  endpoint: string;

  /**
   * Fields of the returned api object.
   */
  labelField: string;
  /**
   * Key is usually 'id'
   */
  keyField: string;

  /**
   * (List of) keys that identify the resource uniquely.
   */
  modelValue: ItemKey | ItemKey[] | null;

  /**
   * Initial values that can be rendered without hitting the endpoint. These records
   * have to at least include the key field, label field, and any other properties that are
   * used in the 'display' slot.
   */
  initialValue?: Record<string, unknown> | Record<string, unknown>[];

  edit?: boolean;
  mode?: "multiple" | "tags";

  loading?: boolean;

  /**
   * Text shown in search field
   */
  placeholder?: string;
  size?: "small" | "middle" | "large";

  /**
   * Parent node which the selector should be rendered to. Issues can be
   * caused by rendering a dropdown out-of-tree. Try to adjust this to a
   * closer node when that happens.
   */
  getPopupContainer?: () => HTMLElement;

  requestParameters?: Record<string, string>;

  // This defines the emits of this component, see BaseNumberFilter for explanation.
  "onUpdate:modelValue": (value: ItemKey | ItemKey[] | null) => void;
  // This prop is a workaround for https://github.com/vuejs/core/issues/8635
  thisPropIsIntentionallyLeftBlank?: void;
  "onUpdate:record": (
    value: Record<string, unknown> | Record<string, unknown>[] | null
  ) => void;
}

const props = withDefaults(defineProps<SchemaSelectFieldProps>(), {
  edit: true,
  size: "middle",
  placeholder: "Enter text to search for more",
  getPopupContainer: () => document.body,
});

interface SchemaSelectFieldEmits {
  (e: "update:modelValue", value: ItemKey | ItemKey[] | null): void;
  /**
   * Emits the full api object when it is added to the selected list.
   */
  (
    e: "update:record",
    value: Record<string, unknown> | Record<string, unknown>[] | null
  ): void;
}

const emit = defineEmits<SchemaSelectFieldEmits>();

function emitModelValueUpdate(value: ItemKey | ItemKey[] | null) {
  emit("update:modelValue", value);

  nextTick(() => {
    // The first emit will reactively update our computeds to
    // modelValueRecordArrayOrSingle will be up-to-date.
    emit("update:record", modelValueRecordArrayOrSingle.value ?? null);
  });
}

// We are in multiple mode if props.mode is set
const inMultipleMode = !!props.mode;
const inMultipleOrEditMode = inMultipleMode || !props.edit;

if (
  !inMultipleOrEditMode &&
  typeof props.initialValue !== "undefined" &&
  Array.isArray(props.initialValue)
) {
  throw new TypeError("Initial value cannot be an array in single mode.");
}

if (
  props.mode &&
  !props.edit &&
  typeof props.initialValue !== "undefined" &&
  !Array.isArray(props.initialValue)
) {
  // Note that in non-edit mode, we do not care if a array or singular item is used.
  throw new TypeError("Initial value must be array when in multiple mode.");
}

const schemaStore = useSchemaStore();
const { schema } = storeToRefs(schemaStore);
const searchQuery = ref("");

const initialValueArray = computed<Record<string, unknown>[] | undefined>(
  () => {
    if (!props.initialValue) {
      return;
    }

    // Normalise initial value to a (empty) array
    if (Array.isArray(props.initialValue)) {
      return props.initialValue;
    } else {
      return [props.initialValue];
    }
  }
);

const initialValueLabels = computed<LabeledValue[] | undefined>(() => {
  if (!initialValueArray.value) {
    return;
  }

  const labeledValues: LabeledValue[] = [];

  for (const item of initialValueArray.value) {
    const key = item[props.keyField];
    if (!(typeof key === "string" || typeof key === "number")) {
      // Simply ignore invalid initial values
      continue;
    }

    const label = item[props.labelField];
    if (typeof label === "undefined") {
      continue;
    }

    labeledValues.push({
      key,
      label: label + "", // cast to string
      value: key,
    });
  }

  return labeledValues;
});

const initialValueKeys = computed<ItemKey[] | undefined>(() => {
  if (!initialValueLabels.value) {
    return;
  }

  return initialValueLabels.value.map((iv) => iv.key);
});

const { data, isLoading: primaryIsLoading, queryKey } = usePaginatedSearchQuery<
  Record<string, unknown>
>(
  props.endpoint,
  {
    page: 1,
    perPage: 50,
  },
  {
    ...props.requestParameters,
    search: searchQuery,
  }
);

const secondaryQueryEnabled = computed(() => {
  return (
    (Array.isArray(props.modelValue)
      ? !!props.modelValue.length
      : !!props.modelValue) &&
    !!data.value &&
    // When an initial value is given and the display is not being edited, we
    // can already display the item without querying.
    (props.edit || !initialValueArray.value?.length)
  );
});

// If the modelValue is set to a specific id, it is not guaranteed that the primary
// query includes this data. So we always get it seperately. This is unnecessary
// when the primary query turns out to already have this data, but we can only know
// this when the primary is done, we do not want to wait for that.
const {
  data: secondaryData,
  isLoading: secondaryIsLoading,
} = usePaginatedSearchQuery<Record<string, unknown>>(
  props.endpoint,
  {
    page: 1,
    perPage: MAX_SELECTABLE_ITEMS,
  },
  {
    ...props.requestParameters,
    idFilter: computed(() => JSON.stringify(props.modelValue)),
  },
  {
    queryClient: useQueryClient(),
    cacheQueryKey: queryKey,
    enabled: secondaryQueryEnabled,
  }
);

const isLoading = computed(() => {
  // If we are in edit mode, we need to wait until the select options are loaded. If we
  // are not in edit mode, we are loading as long as we do not have anything to show.
  // Or we are forced in a loading state by one of our props.
  return (
    (primaryIsLoading.value && props.edit) ||
    (secondaryIsLoading.value && secondaryQueryEnabled.value && !props.edit) ||
    props.loading
  );
});

const selectOptions = computed(function searchResultsToLabeledValues() {
  const map = new Map<ItemKey, LabeledValue>();
  // Computes select options based on api-retrieved data and initial data. Will only use
  // initial data when api data is not available.

  for (const v of [
    ...(data.value?.items ?? []),
    ...(secondaryData.value?.items ?? []),
  ]) {
    const key = v[props.keyField] as ItemKey;

    if (!map.has(key)) {
      map.set(key, {
        key,
        label: v[props.labelField] + "",
        value: key,
      });
    }
  }
  return [...map.values()];
});

watchEffect(function syncChangedInitialValueToModelValue() {
  if (typeof initialValueKeys.value === "undefined" || !props.edit) {
    return;
  }

  if (inMultipleMode) {
    emitModelValueUpdate(initialValueKeys.value);
  } else if (initialValueKeys.value.length) {
    emitModelValueUpdate(initialValueKeys.value[0]);
  }
});

const modelValueRecordArray = computed(function keysToRecordArray() {
  const keys: Set<ItemKey> = props.modelValue
    ? Array.isArray(props.modelValue)
      ? new Set(props.modelValue)
      : new Set([props.modelValue])
    : new Set();

  const records: Record<string, unknown>[] = [];

  for (const v of [
    ...(data.value?.items ?? []),
    ...(secondaryData.value?.items ?? []),
    ...(initialValueArray.value ?? []),
  ]) {
    const key = v[props.keyField] as ItemKey;
    if (keys.has(key)) {
      keys.delete(key);
      records.push(v);
    }
  }

  return records;
});

const modelValueRecordArrayOrSingle = computed(
  function recordArrayToArrayOrSingle() {
    if (inMultipleOrEditMode) {
      return modelValueRecordArray.value;
    } else if (modelValueRecordArray.value.length) {
      return modelValueRecordArray.value[0];
    }
  }
);

// var not const so it is properly available in itself.
var unwatchSchemaValidation = watchEffect(() => {
  // This effect simply checks if this endpoint is suitable to be a SchemaSelectDisplay.
  if (!schema.value) {
    return;
  } else {
    // Only run once
    unwatchSchemaValidation?.();

    const parser = new SchemaParser(schema.value);
    const parameterSchema = parser.getParameterSchemaForPath(
      "get",
      props.endpoint,
      "idFilter"
    );

    if (parameterSchema?.title !== "LiteralEqualsFilter") {
      throw new TypeError(
        `Endpoint '${props.endpoint}' does not have 'idFilter' of type 'LiteralEqualsFilter'.`
      );
    }
  }
});

const onSearch = debounce((searchText: string) => {
  searchQuery.value = searchText;
}, 200);

const onChange = (itemValue?: ItemKey | ItemKey[]) => {
  if (Array.isArray(itemValue)) {
    // Multiple mode
    if (!itemValue.length) {
      // Clear button pressed
      searchQuery.value = "";
      emitModelValueUpdate([]);
    } else {
      // Truncate the list to maximum selectable items
      emitModelValueUpdate(itemValue.splice(0, MAX_SELECTABLE_ITEMS));
    }
  } else {
    // Single mode

    if (itemValue === undefined) {
      // Clear button pressed
      searchQuery.value = "";
      emitModelValueUpdate(null);
    } else {
      emitModelValueUpdate(itemValue);
    }
  }
};
</script>

<style lang="sass" scoped></style>
