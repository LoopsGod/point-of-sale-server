<template lang="pug">
div
  SearchBar(v-model="searchQuery", :placeholder="searchPlaceholder")
  DataTableFilters(
    v-if="filterDefinition?.length"
    :column-mapping="props.columnMapping",
    :filter-definition="props.filterDefinition"
    :filter-query="filterQuery")

  //- v-bind and v-on forward all properties and listener on `DataTable` to Ant's a-table.
  //- This way, we keep Ant's customization
  a-table.data-table(
    :rowClassName="(record, idx) => (idx % 2 ? 'data-row striped' : 'data-row')",
    v-bind="{ ...$props, ...$attrs }",
    :row-key="(record) => record[rowKey] + ''",
    :columns="columns",
    :data-source="tableData",
    :pagination="{ ...pagination, showSizeChanger: false }",
    :loading="requestsActive || loading",
    @change="onTableChange",

    :defaultExpandAllRows="false",
    :showExpandColumn="true",
    :expandedRowKeys="expandedRowKeys",
    @expand="onExpand"
  )
    //- Forward all slots to a-table, e.g. #expandedRowRender
    template(v-for="(_, slotName) of $slots", v-slot:[slotName]="slotScope")
      slot(v-if="!(slotName + '').startsWith(PROP_SLOT_PREFIX) && slotName !== 'row-buttons'" :name="slotName", v-bind="slotScope")

    template(#bodyCell="{ column, record }")
      //- Buttons/other content inside each row (last column)
      slot(
        v-if="column?.key == 'row-buttons'"
        name="row-buttons",
        v-bind="{ record, context: getContext(), loading: isLoading(record) }"
      )

      //- Component defined in column mapping
      component(
        v-else-if="column && getComponentByColumnKey(column.key)"
        :is="getComponentByColumnKey(column.key)",
        v-model="record[getColumnDataKeyByColumnKey(column.key)]",
        :edit="isEditing(record)",
        :loading="isLoading(record)",
        size="small"
      )

      //- Allows a 'DataTable' user to override any of the sub-components by using slot
      //- `template(#prop-<propertyName>="{ record, context }")`
      slot(v-else-if="column" :name="PROP_SLOT_PREFIX + column.key", v-bind="{ record, context: getContext(), loading: isLoading(record) }")

  //- Add buttons below the table, on the same level as the pagination component.
  div(:class="{ 'buttons-under': tableData.length > 0 }")
    slot(name="buttons", v-bind="{ context: getContext() }")
</template>

<script setup lang="ts">
/**
 * This is a general DataTable that uses the backend's OpenAPI spec and `repositories.util.
 * pagination` to create a customizable, filterable, and paginated way of displaying data.
 * Additionally, it provides built-in support for row-level editing and adding new rows.
 *
 * The type parameter in this class represents the returned data from the paginated endpoint.
 *
 * This component forwards all props, attributes, and listeners to Ant's <Table>.
 *
 * Rendering of column data can be overridden using slot syntax:
 *   ```
 *   template(#prop-price="{ record, context }")
 *     span €'{{record.price}}' ({{context.isEditing(record)}})
 *   ```
 * Alternatively, the `column-mapping` prop can be used.
 *
 * Other slots:
 * - `#row-buttons="{ record, context, loading }"`:
 *     when this is defined the last column will contain this slot.
 * - `#buttons="{ context }"`: slot below the component.
 *
 * In the various slots `record` represents the current row data. `context` is an isntance to this
 * class. It allows you to use public functions such as `isEditing`, `isLoading`, `startEdit`, etc.
 *
 * Listeners:
 * - `@search="onSearch"` called when the displayed data has changed.
 * - All listeners from Ant's table are supported.
 */

import type { OpenAPIV3 } from "openapi-types";

import SchemaParser from "../../scripts/schema/parser";
import SearchBar from "../common/searchbar.vue";

import type { PaginationProps } from "ant-design-vue";

import { NEW_ITEM_KEY } from "../../scripts/DataTableTypes";
import type {
  ColumnMapping,
  DataTableContext,
  SorterProps,
  FilterDefinition,
  ColumnProps,
} from "../../scripts/DataTableTypes";
import { computed, reactive, readonly, ref, useSlots, watchEffect } from "vue";
import { useSearch } from "../../scripts/composables/search";

import DataTableFilters from "./DataTableFilters.vue";
import { useSchemaStore } from "../../scripts/store/schema";

const PROP_SLOT_PREFIX = "prop-" as const;

interface DataTableProps {
  /**
   * Backend url that returns a `PaginatedResponse[Record<string, unknown>]` and takes `PageSearchParameters` as query.
   */
  endpoint: string;
  /**
   * Mapping of columnNames to Ant column information, and additionally a component to render.
   * This allows the `DataTable` user to override any settings defined in the Ant Table
   * documentation (E.g. width, align, title). A `component` key can be specified to define a
   * component that should be rendered for this column type. These are the
   * `[String/Number/etc.]Display` components. The order of the keys in this object is used to
   * determine the order of columns.
   *
   * The keys are the names of the columns on a record (as returned from the backend).
   *
   * An alternative to using the `component` property is using slots. (See example at the top
   * of this component)
   */
  columnMapping: ColumnMapping;
  /**
   * Unique key to identify a row. Needed for Vue's row rendering. Usually this will be `id`.
   *
   * (A table entry is of type Record<string, unknown> so the key is always string)
   */
  rowKey: string;
  /**
   * Set the table to loading.
   */
  loading?: boolean;

  /**
   * Placeholder shown in searchbar.
   */
  searchPlaceholder?: string;

  /**
   * Query parameters that are sent but not shown to the user.
   */
  hiddenQueryParameters?: Record<string, string>;

  /**
   * Mapping from filter query parameters to components.
   */
  filterDefinition?: FilterDefinition[];
}

const props = withDefaults(defineProps<DataTableProps>(), {
  loading: false,
  filterDefinition: () => [],
});

const schemaStore = useSchemaStore();

// `unknown` here is actually `ColumnProps`, but properly typing it gives a "Type
// instantiation is excessively deep and possibly infinite" error which might be
// caused by some circular reference. Not sure.
const columns = computed<unknown[]>(() => {
  if (!schemaStore.schema) {
    return [];
  } else {
    const parser = new SchemaParser(schemaStore.schema);
    const paginatedResponseGeneric = parser.getSuccessResponseForPath(
      "get",
      props.endpoint
    );
    const columnDefinition = parser.getPaginatedResponseGenericChild(
      paginatedResponseGeneric
    );

    const sortableKeys: string[] | undefined = parser.getParameterSchemaForPath(
      "get",
      props.endpoint,
      "sort"
    )?.["allowed_columns"];

    if (!columnDefinition?.properties) {
      throw new TypeError("Got invalid schema.");
    }

    const properties = parser.getResolvedProperties(columnDefinition);

    return columnsFromSchema(properties, sortableKeys);
  }
});

const filterNames = computed(() => {
  return props.filterDefinition?.map((f) => f.query) ?? [];
});

const {
  data: singlePageData,
  pagination: pagination,
  isLoading: requestsActive,
  searchQuery,
  extraQueryParameters: filterQuery,
  sortQuery,
  changePage: search,
} = useSearch<Record<string, unknown>>(props.endpoint, {
  onChangePage(_page: number) {
    cancelEdit();

    if (latchedPageSize.value) {
      pagination.value = {
        ...pagination.value,
        pageSize: latchedPageSize.value,
      };
      latchedPageSize.value = null;
    }
  },
  extraQueryParameters: filterNames.value,
  hiddenQueryParameters: props.hiddenQueryParameters,
});

const tableData = ref<Record<string, unknown>[]>([]);

watchEffect(() => {
  // Deeply copy the page data when it changes so we can make edits in our local copy.
  if (singlePageData.value) {
    tableData.value = JSON.parse(JSON.stringify(singlePageData.value.items));
  }
});

// values before editing, used to reset editing process.
const latchedRecord = ref<Record<string, unknown> | null>(null);
const latchedPageSize = ref<number | null>(null);
const editingKey = ref<unknown | null>(null);
const loadingKeys = reactive<unknown[]>([]);

const slots = useSlots();

const columnComponents = computed(() => {
  return Object.entries(props.columnMapping).map(([name, column]) => {
    return {
      columnName: column.columnDataKey || name,
      slot: PROP_SLOT_PREFIX + name,
      component: column.component,
    };
  });
});

const getComponentByColumnKey = (key: string) => {
  if (!(key in props.columnMapping)) {
    return;
  }

  return props.columnMapping[key].component;
};

const getColumnDataKeyByColumnKey = (key: string) => {
  if (!(key in props.columnMapping)) {
    return key;
  }

  return props.columnMapping[key].columnDataKey ?? key;
};

/**
 * Used to provide users of DataTable with the `this` context within their slots.
 * This allows for communication between parent (slots) and child (this).
 */
const getContext = (): DataTableContext => {
  return {
    isEditing,
    isLoading,
    startEdit,
    cancelEdit,
    saveEdit,
    addItem,
    deleteItem,
    setIsLoading,
    data: readonly(tableData.value),
  };
};

// We only support having one expanded item at a time
const expandedRowItemKey = ref<string | null>(null);
const expandedRowKeys = computed(() =>
  expandedRowItemKey.value ? [expandedRowItemKey.value] : []
);

const onExpand = (shouldExpand: boolean, record: Record<string, unknown>) => {
  if (record[props.rowKey] === NEW_ITEM_KEY) {
    // Cannot expand the item we are newly creating
    return;
  }

  if (record[props.rowKey] == editingKey.value) {
    // Cannot open row we are editing
    return;
  }

  expandedRowItemKey.value = shouldExpand ? record[props.rowKey] + "" : null;
};

function onTableChange(
  newPager: PaginationProps,
  _filters: unknown,
  sorter: SorterProps
) {
  if (sorter.field && sorter.order) {
    const sortTarget = props.columnMapping[sorter.field].sort ?? sorter.field;

    sortQuery.value = `${sorter.order == "ascend" ? "" : "-"}${sortTarget}`;
  }

  search(newPager.current);
}

function getRowIndexByKey(key: unknown): number {
  return tableData.value.findIndex((record) => record[props.rowKey] === key);
}

const addItem = (item: Record<string, unknown>) => {
  if (editingKey.value === NEW_ITEM_KEY) {
    // Already adding a new item.
    return;
  }

  item[props.rowKey] = NEW_ITEM_KEY;
  cancelEdit();
  editingKey.value = NEW_ITEM_KEY;
  tableData.value.push(item);

  if (!latchedPageSize.value) {
    latchedPageSize.value = pagination.value.pageSize;
  }

  pagination.value = {
    ...pagination.value,
    // Increase pageSize so our new item fits at the bottom.
    pageSize: pagination.value.pageSize + 1,
  };
};

const startEdit = (record: Record<string, unknown>) => {
  cancelEdit();
  editingKey.value = record[props.rowKey];

  // Note: use only double equals because we convert keys to strings.
  if (expandedRowItemKey.value == record[props.rowKey]) {
    // Close expandend renderer
    expandedRowItemKey.value = null;
  }

  // Makes a (deep) copy of the current object
  latchedRecord.value = JSON.parse(JSON.stringify(record));
};

const isEditing = (record: Record<string, unknown>) => {
  return editingKey.value === record[props.rowKey];
};

const isLoading = (record: Record<string, unknown>) => {
  for (const k of loadingKeys) {
    if (k === record[props.rowKey]) {
      return true;
    }
  }

  return false;
};

const setIsLoading = (record: Record<string, unknown>, flag: boolean) => {
  const idx = loadingKeys.findIndex((k) => k == record[props.rowKey]);

  if (flag) {
    if (idx === -1) {
      loadingKeys.push(record[props.rowKey]);
    } // else: already loading
  } else {
    if (idx >= 0) {
      loadingKeys.splice(idx, 1);
    } // else: wasn't loading
  }
};

const cancelEdit = () => {
  if (!editingKey.value) {
    return;
  }

  const idx = getRowIndexByKey(editingKey.value);
  if (idx >= 0) {
    if (latchedRecord.value) {
      // We were editing an existing object. Reset it.
      tableData.value.splice(idx, 1, latchedRecord.value);
    } else {
      // This was a new object, just remove it.
      tableData.value.splice(idx, 1);
    }
  }

  if (latchedPageSize.value) {
    pagination.value = {
      ...pagination.value,
      pageSize: latchedPageSize.value,
    };

    latchedPageSize.value = null;
  }

  latchedRecord.value = null;
  editingKey.value = null;
};

const saveEdit = async (
  record: Record<string, unknown>,
  replaceKey = record[props.rowKey]
) => {
  const idx = tableData.value.findIndex((r) => r[props.rowKey] === replaceKey);
  tableData.value.splice(idx, 1, record);

  // We do not reset pageSize to latchedPageSize here, since we want to keep the new item.
  latchedRecord.value = null;
  editingKey.value = null;
};

const deleteItem = (recordKey: unknown) => {
  const idx = tableData.value.findIndex((r) => r[props.rowKey] === recordKey);
  tableData.value.splice(idx, 1);
};

function columnsFromSchema(
  schemaProps: {
    [key: string]: OpenAPIV3.SchemaObject;
  },
  sortableKeys: string[] = []
): ColumnProps[] {
  const propertyNames = Object.keys(props.columnMapping);

  const cols: ColumnProps[] = propertyNames.flatMap((prop) => {
    const schemaData = schemaProps[prop];
    const sortTarget = props.columnMapping[prop].sort ?? prop;

    return {
      title:
        props.columnMapping[prop].title ||
        schemaData?.title ||
        "<no title set>",
      dataIndex: prop,
      key: prop,
      sorter: sortableKeys.findIndex((k) => k === sortTarget) >= 0,
      ...(props.columnMapping[prop] || {}),
      component: undefined,
    };
  });

  if ("row-buttons" in slots) {
    cols.push({
      title: "Actions",
      key: "row-buttons",
      align: "right",
      width: 110,
    });
  }

  return cols;
}
</script>

<style lang="sass" scoped>
@import '../../styles/_colors'

.data-table
  margin-top: 0.5em

:deep(.data-row) td
  background-color: #fafafa

:deep(.striped) td
  background-color: #f3f3f5

.data-table :deep(thead) th
  font-weight: bold
  background-color: #f0f0f0
  border: 1px solid #f0f0f0

:deep(.ant-table-cell-row-hover)
  background-color: #1890ff12 !important

.buttons-under
  // Relative because this would otherwise appear below the pagination, we want it on the same level.
  position: relative
  top: -56px
  width: 35%
</style>
