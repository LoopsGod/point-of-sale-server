import { Table } from "ant-design-vue";
import { ColumnProps } from "ant-design-vue/es/table";

export type { ColumnProps };

// Types cannot currently be exported from *.vue files, which is why we need
// a seperate typing file.

// Type of the Table.Column instance from Ant, which is a component that inherits from Vue
export type AntTableColumn = InstanceType<typeof Table.Column>;

/** Consumers of `DataTable` can use this to override Ant column properties. */
export type ColumnMappingKey = ColumnProps & {
  /** Component is the component that will render the property. E.g. `BooleanDisplay` */
  component?: unknown;
  /** Override the key that is used to pass data to component from record. E.g. record[columnDataKey]*/
  columnDataKey?: string;
  /** Sorting query parameter sent to the backend, will default to columnKey. */
  sort?: string;
};

export interface FilterDefinition {
  /** Title shown in the filter dropdown. */
  title: string;
  /** Name of the parameter that we will use to sort. E.g. `priceFilter`. */
  query: string;
  /** Filter component for the dropdown. */
  component: unknown;
}

export interface ColumnMapping {
  [columnKey: string]: ColumnMappingKey;
}

// Cast to unknown to make it assignable to things like numbers. In most cases
// we will use 'id' (number) as key.
export const NEW_ITEM_KEY = "$new-item" as unknown;

export interface DataTableContext<T = any> {
  isEditing: (record: T) => boolean;
  isLoading: (record: T) => boolean;
  startEdit: (record: T) => void;
  cancelEdit: () => void;
  saveEdit: (record: T, replaceKey?: unknown) => void;
  addItem: (item: T) => void;
  deleteItem: (recordKey: unknown) => void;
  setIsLoading: (record: T, flag: boolean) => void;
  data: Readonly<T[]>;
}

export interface DisplayProps {
  edit: boolean;
  loading: boolean;
}

export interface FilterProps {
  // Title of the parameter being filtered, E.g. "deposit"
  for: string;
  // Title of the parameter used for filtering, E.g. "depositFilter"
  title: string;
  // If readOnly is true, we display the button that shows the status of the filter. E.g. "Deposit is true"
  readOnly: boolean;

  modelValue: string;
}

export interface FilterEmits {
  (e: "update:modelValue", value: string): void;
}

export interface SorterProps {
  column?: ColumnProps;
  order?: "ascend" | "descend";
  field?: string;
  columnKey?: string;
}
