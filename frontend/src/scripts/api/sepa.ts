import { Api, ApiResponse } from "./api";
import { Account } from "../types";
import { SepaBatch } from "../admin_types";
import { jsonRequest } from "../util";

export class BatchResponse {
  batch: SepaBatch;
  accounts: BatchAccountOrder[];
}

export class EmailBatchResponse {
  mailCount: number;
}

export type SepaBatchOrder = {
  deleted: boolean;
  price: number;
  accountId: number;
  sepaBatchId: number;
};

export type BatchAccountOrder = {
  account: Account;
  order?: SepaBatchOrder;
};

export type BatchAccountOrdersResponse = {
  accounts: BatchAccountOrder[];
};

export interface ConfigurationResponse {
  configs: SepaConfig[];
}

export interface SepaConfigRequest {
  sepaName: string;
  sepaIban: string;
  sepaBic: string;
  sepaBatch: boolean;
  sepaCreditorId: string;
  sepaCurrency: string;
}
export interface SepaConfig extends SepaConfigRequest {
  id: number;
}

export interface DirectDebitConfig {
  creditorName: string | null;
  creditorIban: string | null;
  creditorBic: string | null;
  creditorIdentifier: string | null;
}

export interface CreateBatchResponse {
  batch_id: number;
}

export class SepaApi extends Api {
  async eligibleAccounts(): Promise<ApiResponse<BatchAccountOrdersResponse>> {
    return this.get<BatchAccountOrdersResponse>(`/api/admin/accounts/sepa/`);
  }

  async batch(batchId: number): Promise<ApiResponse<BatchResponse>> {
    return this.get<any>(`/api/admin/sepa_batches/${batchId}/`);
  }

  async createBatch(
    batch: SepaBatch
  ): Promise<ApiResponse<CreateBatchResponse>> {
    return this.post<CreateBatchResponse>(`/api/admin/sepa_batches/`, batch);
  }

  async emailBatch(batchId: number): Promise<ApiResponse<EmailBatchResponse>> {
    return this.post<EmailBatchResponse>(
      `/api/admin/sepa_batches/${batchId}/email/`,
      undefined
    );
  }

  async executeBatch(batchId: number): Promise<ApiResponse<null>> {
    return this.post<null>(
      `/api/admin/sepa_batches/${batchId}/execute/`,
      undefined
    );
  }

  async csvDownload(batchId: number): Promise<string> {
    return jsonRequest(
      "GET",
      `/api/admin/sepa_batches/directdebit/${batchId}.csv`,
      null,
      false
    );
  }

  async xmlDownload(batchId: number): Promise<string> {
    return jsonRequest(
      "GET",
      `/api/admin/sepa_batches/directdebit/${batchId}.xml`,
      null,
      false
    );
  }

  async deleteBatch(batchId: number): Promise<ApiResponse<void>> {
    return this.delete<void>(`/api/admin/sepa_batches/${batchId}/`, undefined);
  }

  async deleteOrder(
    batchId: number,
    accountId: number
  ): Promise<ApiResponse<void>> {
    return this.delete<void>(
      `/api/admin/sepa_batches/${batchId}/accounts/${accountId}/`,
      undefined
    );
  }

  async updateConfig(config: DirectDebitConfig) {
    return this.put<void>("/api/admin/direct_debit/config/", config);
  }

  async getConfig() {
    return this.get<DirectDebitConfig>("/api/admin/direct_debit/config/");
  }
}

export const sepaApi = new SepaApi();
