import { useQuery } from "@tanstack/vue-query";
import { Api, ApiResponse } from "./api";
import { PaginatedResponse } from "./pagination";

type RegisterCapbility =
  | "PAYMENT_CASH"
  | "PAYMENT_CARD"
  | "PAYMENT_OTHER"
  | "PAYMENT_ACCOUNT_AUTHED"
  | "PAYMENT_ACCOUNT_OTHER"
  | "PAYMENT_ACCOUNT_SELF"
  | "PAYMENT_UNDO";

export interface RegisterRequest {
  name: string;
  capabilities: string[];
  categoryIds: number[];
}

export interface Register {
  id: number;
  name: string;
  isDefault: boolean;
  capabilities: string[];
  categories: {
    id: number;
    name: string;
    color: string;
  }[];
}

interface RegisterResponse {
  register: Register;
}

interface RegisterInfoResponse {
  conflictingCapabilities: Record<RegisterCapbility, RegisterCapbility[]>;
}

export class RegisterApi extends Api {
  public async getAll(): Promise<ApiResponse<PaginatedResponse<Register>>> {
    return this.get<PaginatedResponse<Register>>("/api/admin/registers/");
  }

  public async getDefault(): Promise<ApiResponse<RegisterResponse>> {
    return this.get<RegisterResponse>("/api/admin/registers/default");
  }

  public async deleteRegister(registerId: number): Promise<ApiResponse<void>> {
    return this.delete<void>(`/api/admin/registers/${registerId}/`, null);
  }

  async setDefault(defaultRegister: number): Promise<ApiResponse<Register>> {
    return this.post<Register>(`/api/admin/registers/default`, {
      registerId: defaultRegister,
    });
  }

  async createRegister(register: RegisterRequest) {
    return this.post<RegisterResponse>("/api/admin/registers/", register);
  }

  async updateRegister(registerId: number, register: RegisterRequest) {
    return this.put<RegisterResponse>(
      `/api/admin/registers/${registerId}/`,
      register
    );
  }

  async getRegister(registerId: number) {
    return this.get<RegisterResponse>(`/api/admin/registers/${registerId}/`);
  }

  async info() {
    return this.get<RegisterInfoResponse>(`/api/admin/registers/info`);
  }
}

export const registerApi = new RegisterApi();

export const useDefaultRegisterQuery = () =>
  useQuery({
    queryKey: ["admin/registers/default"],
    queryFn: async () => {
      return (await registerApi.getDefault()).result.register;
    },
    staleTime: 60 * 1000,
  });
