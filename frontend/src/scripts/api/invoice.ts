import { Api, ApiResponse } from "./api";

export interface Invoice {
  id: number;
  invoiceId: string;
  vendor: string;
  invoiceFileId: string;
}

export interface ImportingInvoiceLineItem {
  item: string;
  quantity: number;
  totalPrice: number;
  multipack: number;
  productId: number | null;
  productPrice: number;
  matchWasGuessed: boolean;

  originalProductId: number | null;
  originalMultipack: number;
}

export interface ImportingInvoice {
  vendor: string;
  errors: string[];
  line_items: ImportingInvoiceLineItem[];
  invoice_id: string;
}

export class ProcessInvoiceResponse {
  invoice: ImportingInvoice;
}

export class InvoiceApi extends Api {
  public async processInvoice(
    data
  ): Promise<ApiResponse<ProcessInvoiceResponse>> {
    return this.post<any>("/api/admin/invoice/process", data);
  }

  public async updateClassification(data): Promise<ApiResponse<any>> {
    return this.post<void>("/api/admin/invoice/matcher/update", data);
  }

  public async updateQuantities(data): Promise<ApiResponse<any>> {
    return this.post<void>("/api/admin/invoice/quantity/add", data);
  }

  public async markImported(data): Promise<ApiResponse<any>> {
    return this.post<void>("/api/admin/invoice/mark_imported", data);
  }

  public async getInvoiceUrl(file_id): Promise<ApiResponse<any>> {
    return this.get<void>(`/api/admin/invoice/invoice_file/${file_id}`);
  }
}

export const invoiceApi = new InvoiceApi();
