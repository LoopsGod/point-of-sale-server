import { Api, ApiResponse } from "./api";
import { queryString } from "../util";

export type MolliePaymentStatus =
  | "paid"
  | "open"
  | "canceled"
  | "cancelled"
  | "failed"
  | "expired";

export interface IdealPayment {
  id: number;
  createdAt: string;
  updatedAt: string;
  deletedAt: string | null;
  status: MolliePaymentStatus;
  mollieId: string;
  transactionId: number;
  account: {
    id: number;
    name: string;
  };
}

export class IdealPaymentPage {
  items: IdealPayment[] = [];
  page: number;
  perPage: number;
  pageCount: number;
}

export class IdealDetails {
  idealPayment: IdealPayment;
  mollieData: unknown;
}

export interface MollieStatus {
  accountId: number;
  paymentStatus: MolliePaymentStatus;
}

export class IdealApi extends Api {
  public async payments(page: number): Promise<ApiResponse<IdealPaymentPage>> {
    const obj = {
      page: page,
    };
    return this.get<IdealPaymentPage>("/api/admin/ideal/" + queryString(obj));
  }

  public async webhook(mollieId: string): Promise<ApiResponse<void>> {
    const formData = new FormData();
    formData.append("id", mollieId);
    return this.post<void>("/api/mollie/webhook/", formData);
  }

  public async mollieStatus(mollieId: string) {
    return this.get<MollieStatus>(`/api/mollie/status/${mollieId}/`);
  }

  async details(paymentId: number): Promise<ApiResponse<IdealDetails>> {
    return this.post<IdealDetails>(
      `/api/admin/ideal/status/${paymentId}/`,
      undefined
    );
  }
}

export const idealApi = new IdealApi();
