import { Api, ApiResponse } from "./api";
import type { CategoryData, NewCategoryData } from "../admin_types";
import { PaginatedResponse } from "./pagination";

export class CategoryApi extends Api {
  public async categories({ page = 1, perPage = 15 } = {}): Promise<
    ApiResponse<PaginatedResponse<CategoryData>>
  > {
    return this.get<PaginatedResponse<CategoryData>>(
      `/api/admin/categories/?page=${page}&perPage=${perPage}`
    );
  }

  public async postCategory(category: NewCategoryData): Promise<CategoryData> {
    if ("id" in category) {
      category = { ...category }; // copy
      delete category["id"];
    }

    return Api.rawRequest("POST", `/api/admin/categories/`, category);
  }

  public async putCategory(category: CategoryData): Promise<CategoryData> {
    return Api.rawRequest(
      "PUT",
      `/api/admin/categories/${category.id}/`,
      category
    );
  }
}

export const categoryApi = new CategoryApi();
export default categoryApi;
