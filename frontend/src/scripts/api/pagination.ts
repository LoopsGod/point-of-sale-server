import type { OpenAPIV3 } from "openapi-types";
import { Ref, unref } from "vue";
import { Api, ApiResponse } from "./api";

/**
 * Based on `app.repository.util.pagination.PageSearchParameters`
 */
export interface SearchQuery {
  search?: string;
  page?: number;
  perPage?: number;
  sort?: string;
}

/**
 * Based on `app.repository.util.pagination.PaginatedResponse`
 */
export interface PaginatedResponse<T> {
  page: number;
  perPage: number;
  pageCount: number;
  items: T[];
}

export type NumberFilterString = string;

export function constructNumberFilter({
  eq,
  ge,
  le,
}: {
  eq?: number;
  ge?: number;
  le?: number;
}): NumberFilterString {
  const filter: string[] = [];

  if (eq !== undefined) {
    filter.push(`=${eq}`);
  }

  if (ge !== undefined) {
    filter.push(`=${ge}`);
  }

  if (le !== undefined) {
    filter.push(`=${le}`);
  }

  return filter.join(",");
}

/**
 * This class is used to communicate with an endpoint that returns a `PaginatedResponse[T]`
 * and takes `PageSearchParameters` as query.
 */
export default class PaginationApi extends Api {
  path: string;

  constructor(path: string) {
    super();

    this.path = path;
  }

  public async search<T = unknown>(
    pageSearchParameters: SearchQuery = {},
    extraQueryParameters: Record<string, string | Ref<string>> = {}
  ): Promise<ApiResponse<PaginatedResponse<T>>> {
    const stringQuery: Record<string, string> = {};

    for (const key of Object.keys(pageSearchParameters)) {
      // convert to string
      const value = "" + pageSearchParameters[key];

      if (value) {
        // Only add if the string is not empty.
        stringQuery[key] = value;
      }
    }

    for (const key of Object.keys(extraQueryParameters)) {
      const value = unref(extraQueryParameters[key]);
      if (value) {
        stringQuery[key] = value;
      }
    }

    const urlParams = new URLSearchParams(stringQuery);
    const params = urlParams.toString();
    const queryString = params ? "?" + params : "";

    return this.get<PaginatedResponse<T>>(`${this.path}${queryString}`);
  }

  public static async schema(): Promise<OpenAPIV3.Document> {
    return Api.rawRequest<OpenAPIV3.Document>("GET", "/api/openapi.json");
  }
}
