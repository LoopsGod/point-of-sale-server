import { Api, ApiResponse } from "./api";
import { DateRangeQueryParam, Transaction } from "../types";
import { queryString } from "../util";

export class DeleteTransactionResponse {
  transaction: Transaction;
}

export interface ProductCount {
  productName: string;
  count: number;
  totalPrice: number;
}

export interface ProductCountResponse {
  products: ProductCount[];
}

export class TransactionsApi extends Api {
  public async deleteTransaction(
    transactionId: number
  ): Promise<ApiResponse<DeleteTransactionResponse>> {
    return this.delete<DeleteTransactionResponse>(
      `/api/transactions/${transactionId}/`,
      null
    );
  }

  public async adminDeleteTransaction(
    transactionId: number
  ): Promise<ApiResponse<DeleteTransactionResponse>> {
    return this.delete<DeleteTransactionResponse>(
      `/api/admin/transactions/${transactionId}/`,
      null
    );
  }
  public async accountProductCount(
    query: DateRangeQueryParam
  ): Promise<ApiResponse<ProductCountResponse>> {
    return this.get<ProductCountResponse>(
      "/api/account/transactions/count" + queryString(query)
    );
  }

  public ledgerProductCount(
    ledgerId: number,
    query: DateRangeQueryParam
  ): Promise<ApiResponse<ProductCountResponse>> {
    return this.get<ProductCountResponse>(
      `/api/admin/ledgers/${ledgerId}/transactions/count/` + queryString(query)
    );
  }
}

export const transactionsApi = new TransactionsApi();
