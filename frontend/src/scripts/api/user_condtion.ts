import { Api, ApiResponse } from "./api";

export class UserConditionApi extends Api {
  public async updateAcceptedCondition(): Promise<ApiResponse<void>> {
    return this.post<void>("/api/user/condition/", null);
  }
}

export const userConditionApi = new UserConditionApi();
