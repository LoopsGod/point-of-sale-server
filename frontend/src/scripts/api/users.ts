import { Api, ApiResponse } from "./api";
import { PaginatedResponse } from "./pagination";

export interface UserDirectDebit {
  enabled: boolean;
  iban: string | null;
  bic: string | null;
  mandateId: string | null;
  signDate: string | null;
}

export interface UserResponse {
  id: number;
  username: string;
  name: string;
  longLogin: boolean;
  isAdmin: boolean;
  accountId: number | null;
  viaductUserId: number | null;
  register: {
    id: number;
    name: string;
    isDefault: boolean;
  } | null;
  directDebit: UserDirectDebit;
}

export interface UserRequest {
  username: string;
  name: string;
  longLogin: boolean;
  isAdmin: boolean;
  password: string | null;
  accountId: number | null;
  viaductUserId: number | null;
  registerId: number | null;
}

export class UsersApi extends Api {
  public async getUser(userId: number): Promise<ApiResponse<UserResponse>> {
    return this.get<UserResponse>(`/api/admin/users/${userId}/`);
  }

  public async users({
    page = 1,
    perPage = 15,
  }: {
    page: number;
    perPage: number;
  }): Promise<ApiResponse<PaginatedResponse<UserResponse>>> {
    return this.get<PaginatedResponse<UserResponse>>(
      `/api/admin/users/?page=${page}&perPage=${perPage}`
    );
  }

  createUser(user: UserRequest): Promise<ApiResponse<UserResponse>> {
    return this.post<UserResponse>("/api/admin/users/", user);
  }

  updateUser(
    userId: number,
    user: UserRequest
  ): Promise<ApiResponse<UserResponse>> {
    return this.put<UserResponse>(`/api/admin/users/${userId}/`, user);
  }

  updateUserDirectDebit(
    userId: number,
    user: UserDirectDebit
  ): Promise<ApiResponse<UserDirectDebit>> {
    return this.put<UserDirectDebit>(
      `/api/admin/users/${userId}/direct_debit/`,
      user
    );
  }

  deleteUser(userId: number): Promise<ApiResponse<UserResponse>> {
    return this.delete<UserResponse>(`/api/admin/users/${userId}/`, null);
  }
}

export const usersApi = new UsersApi();
