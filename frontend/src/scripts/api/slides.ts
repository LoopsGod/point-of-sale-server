import { Api, ApiResponse } from "./api";
import type { SlideBarProduct, SlideBarTrend } from "../types";

export interface SlideProduct {
  id: number;
  name: string;
  product: {
    id: number;
    name: string;
  };
  weight: number;
  order: number;
  show: boolean;
  trends: boolean;
  spacing: boolean;
}

export interface NewSlideProduct extends Omit<SlideProduct, "id" | "product"> {
  product: {
    id: null;
  };
}

export interface SlideBarProductsResponse {
  products: SlideBarProduct[];
}

export interface SlideBarBpmResponse {
  bpm: number;
}

export interface SlideBarActiveResponse {
  active: number;
}

export interface SlideBarTrendsResponse {
  trends: SlideBarTrend[];
}

export class SlidesApi extends Api {
  public async postSlideProduct(
    slideProduct: NewSlideProduct
  ): Promise<SlideProduct> {
    if ("id" in slideProduct) {
      // copy
      slideProduct = { ...slideProduct };
      delete slideProduct["id"];
    }

    return Api.rawRequest("POST", `/api/admin/slides/`, slideProduct);
  }

  public async putSlideProduct(
    slideProduct: SlideProduct
  ): Promise<SlideProduct> {
    return Api.rawRequest(
      "PUT",
      `/api/admin/slides/${slideProduct.id}/`,
      slideProduct
    );
  }

  public async deleteSlideProduct(slideProductId: number): Promise<void> {
    return Api.rawRequest("DELETE", `/api/admin/slides/${slideProductId}/`);
  }

  public async products(): Promise<ApiResponse<SlideBarProductsResponse>> {
    return this.get("/api/slides/products/");
  }

  public async bpm(): Promise<ApiResponse<SlideBarBpmResponse>> {
    return this.get("/api/slides/bpm/");
  }

  public async active(): Promise<ApiResponse<SlideBarActiveResponse>> {
    return this.get("/api/slides/active/");
  }

  public async trends(): Promise<ApiResponse<SlideBarTrendsResponse>> {
    return this.get("/api/slides/trends/");
  }
}

export const slidesApi = new SlidesApi();
