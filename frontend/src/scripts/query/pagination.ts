import { QueryClient, useQuery, UseQueryOptions } from "@tanstack/vue-query";
import { Api, objectToSearchParamString } from "../api/api";
import type { PaginationOptions } from "../api/api";
import { PaginatedResponse } from "../api/pagination";
import { computed, isRef, ref, Ref, watch, watchEffect } from "vue";
import { unref } from "vue";
import { MaybeRef } from "@vueuse/core";

const paginationQueryOptions = {
  // Keeps previous page's data in cache so the user can instantly navigate to a page
  // they have already seen.
  keepPreviousData: true,
  refetchOnWindowFocus: false,
  // When we get a failure it'll almost always be a programming error, not something
  // that a retry will fix.
  retry: false,
};

export interface UsePaginatedSearchQueryOptions<T> {
  onChangePage?: (page: number) => void;
  /**
   * Pass a query client to enable getting data from cache.
   */
  queryClient?: QueryClient; // Cannot use useQueryClient from outside a component
  /**
   * Use to indicate which query to use when taking data from the cache.
   */
  cacheQueryKey?: unknown[];
  enabled?: Ref<boolean>;
  additionalQueryOptions?: UseQueryOptions<PaginatedResponse<T>>;
}

function unrefRecord<T extends unknown>(
  record: Record<string, MaybeRef<T>> = {}
): Record<string, T> {
  const unreffed = {};
  for (const param of Object.keys(record) || []) {
    const ur = unref(record[param]);
    if (ur !== "") {
      unreffed[param] = ur;
    }
  }
  return unreffed;
}

function getFetchPaginatedResponseClosure<T>(
  endpoint: string,
  pagination: PaginationOptions,
  filters: Record<string, MaybeRef<string>> = {}
) {
  return ({ pageParam = pagination.page }) => {
    const unrefedFilters = {};
    for (const param of Object.keys(filters) || []) {
      const ur = unref(filters[param]);
      if (ur !== "") {
        unrefedFilters[param] = ur;
      }
    }

    const queryString = objectToSearchParamString({
      ...pagination,
      ...unrefedFilters,
      page: pageParam,
    });

    let url = endpoint;
    if (queryString) {
      url += "?" + queryString;
    }

    return Api.rawRequest<PaginatedResponse<T>>("GET", url);
  };
}

/**
 * Uses tanstack-query to fetch a paginated list of results based on a PaginatedSearch
 * endpoint. Filters are reactive.
 */
export function usePaginatedSearchQuery<T>(
  endpoint: string,
  pagination: PaginationOptions,
  filters: Record<string, MaybeRef<string>> = {},
  options: UsePaginatedSearchQueryOptions<T> = {}
) {
  if (!endpoint.endsWith("/")) {
    endpoint += "/";
  }
  const currentPage = ref(pagination.page);

  const fetchPaginatedResponse = getFetchPaginatedResponseClosure<T>(
    endpoint,
    pagination,
    filters
  );

  const queryKey: unknown[] = [
    "paginated-query",
    endpoint,
    { ...pagination, page: currentPage },
    filters,
  ];

  if (typeof options.enabled !== "undefined") {
    // initialData will run before the query is even enabled. We add enabled to the
    // query key so the query will rerun when enabled changes.
    queryKey.push(options.enabled);
  }

  const shouldTryToGetDataFromCache =
    options.queryClient && options.cacheQueryKey && "idFilter" in filters;

  const hook = useQuery<PaginatedResponse<T>>(
    queryKey,
    () => fetchPaginatedResponse({ pageParam: currentPage.value }),
    {
      ...options.additionalQueryOptions,
      ...paginationQueryOptions,
      enabled: options.enabled,
      staleTime: shouldTryToGetDataFromCache ? Infinity : undefined,
      initialData: () => {
        if (!shouldTryToGetDataFromCache) {
          return undefined;
        }

        const ids: number[] = JSON.parse(unref(filters["idFilter"]));
        if (!ids || !ids.length) {
          return undefined;
        }

        // We know these exists because of `shouldTryToGetDataFromCache`.
        const state = options.queryClient!.getQueryState(
          options.cacheQueryKey!
        );

        if (!state?.data) {
          // If there is no data in the cache, we must query.
          return undefined;
        }

        const data = state.data as PaginatedResponse<T & { id: number }>;
        const filteredItems = data.items.filter(
          (item) => ids.indexOf(item.id) !== -1
        );

        if (filteredItems.length !== ids.length) {
          // If not all ids are found in the cache, we must query.
          return undefined;
        }

        return data;
      },
    }
  );

  watch(
    Object.values(filters).filter((f) => isRef(f)),
    () => {
      // Filters changed, we want to go back to the first page.
      currentPage.value = 1;
    }
  );

  const changePage = (page?: number) => {
    if (typeof page === "number") {
      currentPage.value = page;
    } else {
      currentPage.value = currentPage.value + 1;
    }

    options.onChangePage?.(currentPage.value);
  };

  return {
    ...hook,
    currentPage,
    changePage,
    queryKey,
  };
}

/**
 * Adds an abstraction on top of usePaginatedSearchQuery to be able to navigate
 * to arbitrary pages. Data is a single page instead of all pages.
 */
export function usePaginatedSearchPagedQuery<T>(
  endpoint: string,
  paginationOptions: PaginationOptions,
  filters: Record<string, string | Ref<string>> = {},
  options: UsePaginatedSearchQueryOptions<T> = {}
) {
  const {
    data,
    error,
    changePage,
    currentPage,
    isLoading,
    status,
  } = usePaginatedSearchQuery<T>(endpoint, paginationOptions, filters, options);

  const perPage = ref(paginationOptions.perPage);
  const totalItems = computed(() => {
    if (!data.value && totalItems.value) {
      // There is no data since we are (probably) fetching, but if we had data
      // before, we want to keep displaying the pagination.
      return totalItems.value;
    } else if (!data.value) {
      // Never had data, first time fetching.
      return 1;
    }

    return data.value.perPage * data.value.pageCount;
  });

  const paginationResult = computed({
    get: () => ({
      current: currentPage.value,
      pageSize: perPage.value,
      total: totalItems.value,
    }),
    set(v) {
      // This allows modifying the pageSize
      perPage.value = v.pageSize;
    },
  });

  return {
    data,
    pagination: paginationResult,
    isLoading,
    changePage,
    error,
    status,
    currentPage,
  };
}
