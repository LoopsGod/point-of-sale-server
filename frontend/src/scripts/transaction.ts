import type { Order } from "./types";

export function transactionMethodName(method: string) {
  switch (method) {
    case "cash":
      return "Cash";
    case "card":
      return "PIN";
    case "account":
      return "Account";
    case "ideal":
      return "iDEAL";
    case "sepa":
      return "Direct Debit";
    case "other":
      return "Other";
    case "transfer":
      return "Transfer";
    default:
      return method;
  }
}

export function orderTypeName(orderType: Order["type"], price?: number) {
  switch (orderType) {
    case "ACCOUNT_TOP_UP_OR_ACCOUNT_PAYMENT":
      if (typeof price === "number" && price < 0) {
        return "Top-up";
      } else if (typeof price === "number" && price > 0) {
        return "Payment";
      }
      return "Top-up / Account Payment";
    case "DIRECT_PAYMENT":
      return "Payment";
    case "PRODUCT_PURCHASE":
      return "Purchase";
    case "INVALID":
      return "Invalid";
  }
}
