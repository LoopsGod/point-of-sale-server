import { defineStore } from "pinia";
import { ref } from "@vue/reactivity";
import { useAdminRequests } from "../util";
import { Account } from "../types";
import { unwrapPagination } from "../api/api";
import { accountsApi, LedgerAccountsResponse } from "../api/accounts";

export const useLedgerStore = defineStore("ledgerStore", () => {
  const ledgers = ref<LedgerAccountsResponse[]>([]);

  const { requestsActive: loading, requestWith } = useAdminRequests();
  const init = async () => {
    try {
      ledgers.value = await requestWith(
        unwrapPagination<LedgerAccountsResponse>(
          accountsApi.ledgers.bind(accountsApi)
        )
      );
    } catch (e) {
      console.error(e);
    }
  };
  init();
  return { ledgers, loading };
});
