import { defineStore } from "pinia";
import type {
  ExactCostCenter,
  ExactLedger,
  ExactVatCode,
  ExactDivision,
} from "../api/exact";
import { exactApi } from "../api/exact";

export interface ExactStore {
  ledgers: ExactLedger[];
  vatCodes: ExactVatCode[];
  costCenters: ExactCostCenter[];
  divisions: ExactDivision[];
  ledgersReady: boolean;
  vatCodesReady: boolean;
  costCentersReady: boolean;
  divisionsReady: boolean;
}

const collator = new Intl.Collator();

export const useExactStore = defineStore("exactStore", {
  state: (): ExactStore => {
    return {
      ledgers: [],
      vatCodes: [],
      costCenters: [],
      divisions: [],
      ledgersReady: false,
      vatCodesReady: false,
      costCentersReady: false,
      divisionsReady: false,
    };
  },
  actions: {
    loadAll() {
      return Promise.all([
        this.loadVatCodes(),
        this.loadCostCenters(),
        this.loadLedgers(),
        this.loadDivisions(),
      ]);
    },
    async loadLedgers() {
      if (this.ledgersReady) {
        return;
      }
      const ledgers = (await exactApi.getLedgers()).result.ledgers;
      this.ledgers = ledgers.sort((a, b) => collator.compare(a.code, b.code));
      this.ledgersReady = true;
    },
    async loadCostCenters() {
      if (this.costCentersReady) {
        return;
      }
      const costCenters = (await exactApi.getCostCenters()).result.costCenters;
      this.costCenters = costCenters.sort((a, b) =>
        collator.compare(a.code, b.code)
      );
      this.costCentersReady = true;
    },
    async loadVatCodes() {
      if (this.vatCodesReady) {
        return;
      }
      const vatCodes = (await exactApi.getVatCodes()).result.vatCodes;
      this.vatCodes = vatCodes.sort((a, b) =>
        collator.compare(a.description, b.description)
      );
      this.vatCodesReady = true;
    },
    async loadDivisions() {
      if (this.divisionsReady) {
        return;
      }
      const divisions = (await exactApi.getDivisions()).result.administrations;
      this.divisions = divisions.sort((a, b) =>
        collator.compare(a.name, b.name)
      );
      this.divisionsReady = true;
    },
  },
  getters: {
    isReady: (state) =>
      state.ledgersReady &&
      state.costCentersReady &&
      state.vatCodesReady &&
      state.divisionsReady,
  },
});
