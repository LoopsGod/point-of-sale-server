import { defineStore } from "pinia";
import { OpenAPIV3 } from "openapi-types";
import PaginationApi from "../api/pagination";
import { ref } from "@vue/reactivity";
import { useAdminRequests } from "../util";

export const useSchemaStore = defineStore("schemaStore", () => {
  const schema = ref<OpenAPIV3.Document | null>(null);
  const { requestsActive, requestWith } = useAdminRequests();
  const loadSchema = async () => {
    schema.value = await requestWith(PaginationApi.schema());
  };
  loadSchema();
  return { schema, requestsActive };
});
