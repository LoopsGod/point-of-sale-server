import { defineStore } from "pinia";
import { Category } from "../admin_types";
import { ref } from "@vue/reactivity";
import categoryApi from "../api/category";
import { useAdminRequests } from "../util";
import { unwrapPagination } from "../api/api";

export const useCategoryStore = defineStore("categoryStore", () => {
  const categories = ref<Category[]>([]);

  const { requestsActive: loading, requestWith } = useAdminRequests();
  const init = async () => {
    try {
      categories.value = await requestWith(
        unwrapPagination(categoryApi.categories.bind(categoryApi))
      );
    } catch (e) {
      console.error(e);
    }
  };
  init();
  return { categories, loading };
});
