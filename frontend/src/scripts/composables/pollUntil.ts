import type { Awaitable } from "@vueuse/core";
import { tryOnScopeDispose, useTimeoutFn, useTimeoutPoll } from "@vueuse/core";
import { ref } from "vue";

export function usePollUntil<T>(
  interval: number,
  fn: () => Awaitable<T>,
  predicate: (obj: T) => boolean
) {
  const { start } = useTimeoutFn(loop, interval);

  const isActive = ref(false);

  async function loop() {
    if (!isActive.value) {
      return;
    }

    const result = await fn();

    const gotExpectedValue = predicate(result);

    if (!gotExpectedValue) {
      // Did not get our final result, we will try again.
      start();
    }
  }

  function resume() {
    if (!isActive.value) {
      isActive.value = true;
      loop();
    }
  }

  function pause() {
    isActive.value = false;
  }

  resume();

  tryOnScopeDispose(pause);

  return {
    isActive,
    pause,
    resume,
  };
}
