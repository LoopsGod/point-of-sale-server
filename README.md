# Point of Sale
## Configuration

Docker is 'required' to run this project.

### Creating config & setting environment

Rename `backend/config.py.example` to `config.py` and rename `.env.example` to `.env`.

This project uses Viaduct's OAuth login system. You will need to login with a Viaduct account
to get admin capabilities. This account also needs to have two-factor authentication enabled.
Because of this it is not possible to create a local admin account. As a consequence of this
you will need a valid `VIADUCT_CLIENT_KEY` in your `config.py`. Ask any maintainer to give you this.

### Upgrade the database
Before the database can be used it needs to be updated to the lastest migration. To do this, run
the following command (in the root folder of the project, outside of docker):

```bash
docker compose exec backend alembic upgrade head
docker compose exec backend python -m app make-fixtures  # To create dummy data.
```

### Creating administrator account

First create an account using:
```bash
docker compose exec backend python -m app create-user --username admin --password admin --admin
```

You can now login using the account in the password tab. Using admin functionality requires
two-factor authentication, which is not available when using local accounts. You can manually
enable two-factor for a specific token (found in your database or browser local storage) using:

```bash
docker compose exec backend python -m app enable-tfa --username maico@example.com
```

#### Login with via
Alternatively, you use the via-login by setting both in `config.py`:
```python
VIADUCT_CLIENT_ID = 'pos-local'
VIADUCT_CLIENT_SECRET = '[VIADUCT_LOCAL_SECRET_KEY]'
```
After using your via-account with [two-factor enabled](https://svia.nl/users/self/tfa/), enable admin using:
```bash
docker compose exec backend python -m app make-admin --username maico@example.com
```

New accounts can be made from the admin interface.

## Running

### Config

Create `config.py` from `config.py.example` and create .env

### Development

To run the development version, install docker and docker compose and run:

```bash
docker compose up --build
```

### Production

To run the pos on a server, install docker and docker compose and run:

```bash
export COMPOSE_FILE=docker compose.yml:docker compose.prod.yml
docker compose up --build
```

## Tests

The tests require a clean database which can be rollback on a per test basis,
which needs to be created:
```bash
docker compose exec database psql -U pos -c "create database pos_test"
```

The backend contains tests, which can be run from pycharm or directly in
backend container:

```bash
docker compose exec backend pytest
```

See backend/tests/README.md for some background no building tests.

## Pre-commit

Before committing, install pre-commit by issuing the following in the root directory:

```bash
pre-commit install
```
