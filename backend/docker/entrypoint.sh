#!/bin/sh
if [ -z $DEBUG ]; then
    alembic upgrade head
    exec uvicorn app:fastapi_app  --workers 4 --host 0.0.0.0 --port 5000
else
    exec uvicorn app:fastapi_app  --reload --host 0.0.0.0 --port 5000
fi
