#!/usr/bin/env python
import os
import time
from urllib.parse import urljoin

import requests

host = os.environ.get("CI_ENVIRONMENT_URL")
if not host:
    host = "https://pos.svia.nl"

cookies = {"auth": os.environ.get("CI_ENVIRONMENT_COOKIE")}


print(f"Checking '{host}' health...")
for i in range(1, 5):
    try:
        response = requests.get(urljoin(host, "/api/health/"), cookies=cookies)
        print(response.status_code)
        if response.status_code == 200:
            print(response.json())
            if response.json().get("healthy", False):
                print("All systems healthy!")
                exit(0)
        else:
            print(f"/api/health/ returned {response.status_code}")
    except requests.exceptions.ConnectionError:
        print("ConnectionError.")
    else:
        print("No healthy system.")
    delay = i * 5
    print(f"Retry in {delay} seconds...")
    time.sleep(delay)

print("No healthy system, quiting as error.")
exit(1)
