import os

basedir = os.path.abspath(os.path.dirname(__file__))
LOG_FILE = os.path.join(basedir, "log/app_logger.log")
CSRF_ENABLED = True
SECRET_KEY = "sekrit"
DEBUG = False

SQLALCHEMY_DATABASE_URI = os.environ.get(
    "SQLALCHEMY_DATABASE_URI", "postgresql+psycopg2://pos:pos123@database/pos"
)
SQLALCHEMY_DATABASE_TEST_URI = ""
SQLALCHEMY_TRACK_MODIFICATIONS = False

BROKER_URL = os.environ.get("BROKER_URL", "redis://redis")


VIADUCT_CLIENT_ID = ""
VIADUCT_CLIENT_SECRET = ""

MOLLIE_API_KEY = "test_MOLLIE_API_KEY"

NGROK_HOST = ""

GSUITE_TREASURER_MAIL = "pos@svia.nl"
