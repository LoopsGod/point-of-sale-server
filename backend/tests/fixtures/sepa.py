from datetime import datetime, timezone

import pytest
from sqlalchemy import insert
from sqlalchemy.orm import Session

from app.models.setting import Setting
from app.models import SepaBatch


@pytest.fixture
def sepa_config(db_session: Session) -> None:
    db_session.execute(
        insert(Setting).values(
            [
                {
                    "key": "DIRECT_DEBIT_CREDITOR_NAME",
                    "value": "Vereniging Informatiewetenschappen Amsterdam",
                },
                {"key": "DIRECT_DEBIT_CREDITOR_IDENTIFIER", "value": "1234567890"},
                {"key": "DIRECT_DEBIT_CREDITOR_IBAN", "value": "NL20INGB0001234567"},
                {"key": "DIRECT_DEBIT_CREDITOR_BIC", "value": "INGBNL2A"},
            ]
        )
    )
    db_session.commit()


@pytest.fixture
def sepa_batch(db_session: Session) -> SepaBatch:
    b = SepaBatch(
        description="desc",
        execution_date=datetime.now(timezone.utc),
        success=False,
    )
    db_session.add(b)
    db_session.commit()
    return b
