from datetime import datetime

import bcrypt
import pytest
from sqlalchemy.orm import Session

from app.models import Account, User
from app.models.account import AccountRequest, AccountRequestStatus, AccountType
from app.models.checkout import CheckoutMethod
from app.service.setting_service import get_db_settings, save_db_settings


@pytest.fixture(scope="session")
def pin() -> dict[str, str]:
    """Single pin hash for the session to decrease testing time."""
    pin = "1234"
    return {
        "hash": bcrypt.hashpw(pin.encode("utf-8"), bcrypt.gensalt()).decode("utf-8"),
        "pin": pin,
    }


@pytest.fixture()
def account_ids_checkout_method(db_session: Session) -> dict[CheckoutMethod, int]:
    created_at = updated_at = datetime(year=2018, month=1, day=1)

    settings = get_db_settings(db_session)
    codes = {}

    for method in CheckoutMethod:
        if method == CheckoutMethod.ACCOUNT:
            continue

        acc = Account(
            created_at=created_at,
            updated_at=updated_at,
            name="income_ledger_" + method.value,
            balance=0,
            type=AccountType.INCOME_LEDGER.value,
            allow_negative_balance=True,
        )

        db_session.add(acc)
        db_session.flush()

        codes[method] = acc.id

        settings.set_setting_for_checkout_method(method, str(acc.id))
    save_db_settings(db_session, settings)

    db_session.commit()
    return codes


@pytest.fixture()
def ledger_accounts(db_session: Session) -> dict[str, Account]:
    created_at = updated_at = datetime(year=2018, month=1, day=1)
    room = Account(
        created_at=created_at,
        updated_at=updated_at,
        name="Room sale",
        balance=0,
        type=AccountType.PRODUCT_LEDGER.value,
    )
    bar = Account(
        created_at=created_at,
        updated_at=updated_at,
        name="Bar sale",
        balance=0,
        type=AccountType.PRODUCT_LEDGER.value,
    )
    db_session.add(room)
    db_session.add(bar)
    db_session.commit()
    return {"room": room, "bar": bar}


@pytest.fixture()
def event_account(db_session: Session) -> Account:
    created_at = updated_at = datetime(year=2018, month=1, day=1)

    event = Account(
        created_at=created_at,
        updated_at=updated_at,
        name="Event User",
        balance=0,
        type=AccountType.EVENT.value,
        allow_negative_balance=True,
    )

    db_session.add(event)
    db_session.commit()

    return event


@pytest.fixture()
def event_account_with_pin(db_session: Session, pin: dict[str, str]) -> Account:
    created_at = updated_at = datetime(year=2018, month=1, day=1)

    event = Account(
        created_at=created_at,
        updated_at=updated_at,
        name="Event User with PIN",
        balance=0,
        type=AccountType.EVENT.value,
        allow_negative_balance=False,
        pin=pin["hash"],
    )

    db_session.add(event)
    db_session.commit()

    return event


@pytest.fixture
def account_request(
    db_session: Session, users_without_account: dict[str, User], pin: dict[str, str]
) -> AccountRequest:
    a2 = AccountRequest()  # .create_with_user(users["random"], pin)
    a2.user = users_without_account["user2"]
    a2.pin = pin["hash"]
    a2.status = AccountRequestStatus.APPROVED.value

    a = AccountRequest()  # .create_with_user(users["random"], pin)
    a.user = users_without_account["user1"]
    a.pin = pin["hash"]
    a.status = AccountRequestStatus.PENDING.value

    db_session.add(a, a2)
    db_session.commit()
    return a


@pytest.fixture()
def normal_account(db_session: Session, pin: dict[str, str]) -> Account:
    created_at = updated_at = datetime(year=2018, month=1, day=1)
    normal = Account(
        created_at=created_at, updated_at=updated_at, name="Normal User", balance=0
    )
    normal.pin = pin["hash"]
    db_session.add(normal)
    db_session.commit()
    return normal


@pytest.fixture()
def normal_account2(db_session: Session, pin: dict[str, str]) -> Account:
    created_at = updated_at = datetime(year=2018, month=1, day=1)
    normal = Account(
        created_at=created_at, updated_at=updated_at, name="Normal Account 2", balance=0
    )
    normal.pin = pin["hash"]
    db_session.add(normal)
    db_session.commit()
    return normal


@pytest.fixture()
def sepa_account(
    db_session: Session,
    sepa_user: User,
    pin: dict[str, str],
) -> Account:
    created_at = updated_at = datetime(year=2018, month=1, day=1)

    sepa_account = Account(
        created_at=created_at,
        updated_at=updated_at,
        name="Sepa User",
        balance=0,
        allow_negative_balance=True,
    )
    sepa_account.pin = pin["hash"]

    db_session.add(sepa_account)
    db_session.flush()

    sepa_user.account_id = sepa_account.id
    db_session.add(sepa_user)
    db_session.commit()

    return sepa_account
