import pytest

from app.models import Category


@pytest.fixture()
def categories(db_session, products):
    category_room_cans = Category(
        name="Cans", position=1, color=2, products=[products["room_cola"]]
    )
    category_room_alcohol = Category(
        name="Alcohol",
        position=2,
        color=1,
        products=[products["room_beer"], products["room_cola"]],
    )
    category_bar = Category(
        name="Bar category", position=-2, color=3, products=[products["bar_beer"]]
    )

    db_session.add(category_room_cans)
    db_session.add(category_room_alcohol)
    db_session.add(category_bar)
    db_session.commit()
    return category_room_cans, category_room_alcohol, category_bar
