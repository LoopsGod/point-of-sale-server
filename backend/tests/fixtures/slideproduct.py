from datetime import datetime

import pytest
from sqlalchemy.orm import Session

from app.models import SlideProduct, Transaction, Product, Account, Order


@pytest.fixture()
def slide_products(
    db_session: Session, products: dict[str, Product]
) -> dict[str, SlideProduct]:
    beer = SlideProduct(
        product_id=products["bar_beer"].id,
        weight=1.0,
        name="Bier",
        order=1,
        show_slide=True,
        show_trends=True,
        add_spacing=False,
    )
    cola = SlideProduct(
        product_id=products["room_cola"].id,
        weight=1.0,
        name="Cola",
        order=2,
        show_slide=True,
        show_trends=True,
        add_spacing=False,
    )
    db_session.add(beer)
    db_session.add(cola)
    db_session.commit()
    return {"beer": beer, "cola": cola}


@pytest.fixture
def fixed_date_slide_product_order(
    db_session: Session,
    products: dict[str, Product],
    slide_products: dict[str, SlideProduct],
    sepa_account: Account,
) -> Transaction:
    fixed_date = datetime(year=2022, month=12, day=15, hour=20, minute=10)

    sepa_account.balance = -120
    transaction_buy = Transaction(
        created_at=fixed_date,
        updated_at=fixed_date,
        nonce="buy-nonce2",
        _type="account_purchase",
    )

    db_session.add_all([transaction_buy])
    db_session.flush()

    transaction_buy.orders = [
        Order(
            created_at=fixed_date,
            updated_at=fixed_date,
            product_id=slide_products["beer"].product_id,
            _method="account",
            account_id=products["bar_beer"].ledger_id,
            price=-120,
            _type="product_purchase",
        ),
        Order(
            created_at=fixed_date,
            updated_at=fixed_date,
            _method="account",
            account_id=sepa_account.id,
            price=120,
            _type="account_top_up_or_account_payment",
        ),
    ]
    db_session.commit()
    return transaction_buy
