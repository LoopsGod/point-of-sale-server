import pytest
from sqlalchemy.orm import Session

from app.models.account import AccountExactLedger, Account
from app.models.checkout import CheckoutMethod
from app.models.database import broker
from app.repository.settings import get_db_settings, save_db_settings
from app.domains.exact.api.exact_api import ExactRedisKeys


@pytest.fixture
def exact_client_settings(db_session):
    db_settings = get_db_settings(db_session)
    db_settings.exact_storage_base_url = "http://pos.svia.nl"
    db_settings.exact_storage_client_id = "client_id"
    db_settings.exact_storage_client_secret = "client_secret"
    save_db_settings(db_session, db_settings)


@pytest.fixture
def exact_token_settings(db_session):
    broker.setex(
        name=ExactRedisKeys.ACCESS_TOKEN,
        time=2010467808,
        value="access_token",
    )

    db_settings = get_db_settings(db_session)
    db_settings.exact_storage_refresh_token = "refresh_token"
    save_db_settings(db_session, db_settings)


@pytest.fixture
def exact_division_settings(db_session):
    db_settings = get_db_settings(db_session)
    db_settings.exact_storage_division = str(1337)
    save_db_settings(db_session, db_settings)


@pytest.fixture
def exact_product_ledgers(
    db_session: Session, ledger_accounts: dict[str, Account]
) -> dict[str, str]:
    codes = {
        "room": "44001",
        "bar": "44001",
    }

    db_session.add_all(
        [
            AccountExactLedger(
                account_id=ledger_accounts["room"].id,
                exact_ledger_code=codes["room"],
                exact_cost_center_code="1210",
                exact_vat_code="5",
            ),
            AccountExactLedger(
                account_id=ledger_accounts["bar"].id,
                exact_ledger_code=codes["bar"],
                exact_cost_center_code="1211",
                exact_vat_code="4",
            ),
        ]
    )

    db_session.commit()
    return codes


@pytest.fixture
def exact_income_ledgers(
    db_session: Session,
    account_ids_checkout_method: dict[CheckoutMethod, int],
) -> dict[CheckoutMethod, str]:
    methods = list(CheckoutMethod)
    methods.remove(CheckoutMethod.ACCOUNT)

    codes: dict[CheckoutMethod, str] = {}

    for idx, e in enumerate(methods):
        codes[e] = f"1400{idx}"

    db_session.add_all(
        [
            AccountExactLedger(
                account_id=account_ids_checkout_method[e],
                exact_ledger_code=codes[e],
                associated_checkout_method=e.value,
            )
            for e in methods
        ]
    )
    db_session.commit()

    return codes


@pytest.fixture
def exact_journal_settings(db_session):
    db_settings = get_db_settings(db_session)
    db_settings.exact_journal_pos_code = str(24)
    db_settings.exact_journal_pos_description = "Point of Sale"
    save_db_settings(db_session, db_settings)


@pytest.fixture
def exact_finished_settings(
    db_session,
    exact_client_settings,
    exact_token_settings,
    exact_division_settings,
    exact_journal_settings,
    exact_income_ledgers,
    exact_product_ledgers,
):
    db_settings = get_db_settings(db_session)
    db_settings.exact_setup_finished = True
    save_db_settings(db_session, db_settings)
