from datetime import date

import pytest
from sqlalchemy.orm import Session

from app.models import Transaction, Order, Account, Product
from app.service import transaction_service


@pytest.fixture
def transaction_account_order(
    db_session: Session, products: dict[str, Product], sepa_account: Account
) -> Transaction:
    assert sepa_account.balance == 0
    sepa_account.balance = -120
    transaction_buy = Transaction(
        created_at=date(2020, 9, 1),
        updated_at=date(2020, 9, 1),
        nonce="buy-nonce",
        _type="account_purchase",
    )

    db_session.add_all([transaction_buy])
    db_session.flush()

    transaction_buy.orders = [
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            product_id=products["bar_beer"].id,
            _method="account",
            account_id=products["bar_beer"].ledger_id,
            price=-120,
            _type="product_purchase",
        ),
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            _method="account",
            account_id=sepa_account.id,
            price=120,
            _type="account_top_up_or_account_payment",
        ),
    ]

    # Validate the order type with transaction type
    assert (
        transaction_service.determine_transaction_type(transaction_buy)
        == transaction_buy.type
    )
    db_session.commit()
    return transaction_buy
