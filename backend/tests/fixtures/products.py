import pytest
from sqlalchemy.orm import Session

from app.models import Product, Account


@pytest.fixture()
def products(
    db_session: Session, ledger_accounts: dict[str, Account]
) -> dict[str, Product]:
    room, bar = ledger_accounts["room"], ledger_accounts["bar"]
    cola = Product(name="Cola", price=50, ledger_id=room.id, deposit=False)
    bottled_beer = Product(
        name="Bottled Beer",
        price=70,
        ledger_id=room.id,
        deposit=False,
    )
    beer = Product(
        name="Tap beer",
        price=120,
        ledger_id=bar.id,
        deposit=True,
        quantity=-10,
    )
    db_session.add(cola)
    db_session.add(bottled_beer)
    db_session.add(beer)
    db_session.commit()
    return {"room_cola": cola, "room_beer": bottled_beer, "bar_beer": beer}
