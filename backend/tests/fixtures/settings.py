import pytest
from sqlalchemy.orm import Session

from app.models.setting import CheckoutReason
from app.repository.settings import get_db_settings, save_db_settings


@pytest.fixture
def card_reader_secret(db_session: Session) -> str:
    db_settings = get_db_settings(db_session)
    db_settings.checkout_reader_secret = "$abcde"
    save_db_settings(db_session, db_settings)
    return db_settings.checkout_reader_secret


@pytest.fixture
def notification_settings(db_session: Session) -> None:
    db_settings = get_db_settings(db_session)
    db_settings.notification_sepa_destination = "sepa@example.com"
    save_db_settings(db_session, db_settings)


@pytest.fixture
def checkout_reasons_settings(db_session: Session) -> None:
    db_settings = get_db_settings(db_session)
    relatie = CheckoutReason(name="Relatiegeschenken", pin=None)
    bla = CheckoutReason(name="Bla", pin="1234")
    db_settings.checkout_reasons = [relatie, bla]
    save_db_settings(db_session, db_settings)
