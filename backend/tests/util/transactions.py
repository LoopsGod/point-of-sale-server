from datetime import date
from random import random

from sqlalchemy.orm import Session

from app.dependencies import UserRequestContext
from app.models import Transaction, Order, Account, Product
from app.models.checkout import CheckoutMethod
from app.service import transaction_service, order_service, checkout_service


def _create_top_up(
    db_session: Session,
    account: Account,
    amount: int = 1000,
    method: CheckoutMethod = CheckoutMethod.CASH,
    reason: str | None = None,
) -> Transaction:
    account.balance += amount
    transaction_top_up = Transaction(
        created_at=date(2020, 9, 1),
        updated_at=date(2020, 9, 1),
        reason=reason,
        nonce=f"nonce-top-up-{account.id}-{random()}",
        _type="top_up",
    )

    db_session.add_all([account, transaction_top_up])
    db_session.flush()

    transaction_top_up.orders = [
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            _method="account",
            account_id=account.id,
            price=-amount,
            _type="account_top_up_or_account_payment",
        ),
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            _method=method.value,
            account_id=checkout_service.get_account_for_checkout_method(
                db_session, method
            ),
            price=amount,
            _type="direct_payment",
        ),
    ]

    assert (
        transaction_service.determine_transaction_type(transaction_top_up)
        == transaction_top_up.type
    )

    db_session.commit()
    return transaction_top_up


def _create_account_purchase(
    db_session: Session, account: Account, products: list[Product]
) -> Transaction:

    total_price = 0
    for p in products:
        total_price += p.price

    account.balance -= total_price
    transaction_buy = Transaction(
        created_at=date(2020, 9, 1),
        updated_at=date(2020, 9, 1),
        nonce=f"nonce-account-purchase-{account.id}-{random()}",
        _type="account_purchase",
    )

    db_session.add_all([account, transaction_buy])
    db_session.flush()

    transaction_buy.orders = [
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            _method="account",
            account_id=account.id,
            price=total_price,
            _type="account_top_up_or_account_payment",
        ),
    ]

    for p in products:
        transaction_buy.orders.append(
            Order(
                created_at=date(2020, 9, 1),
                updated_at=date(2020, 9, 1),
                product_id=p.id,
                _method="account",
                account_id=p.ledger_id,
                price=-p.price,
                _type="product_purchase",
            )
        )

    # Validate the order type with transaction type
    assert (
        transaction_service.determine_transaction_type(transaction_buy)
        == transaction_buy.type
    )
    db_session.commit()
    return transaction_buy


def _create_direct_purchase(
    db_session: Session,
    products: list[Product],
    method: CheckoutMethod = CheckoutMethod.CARD,
    reason: str = "",
) -> Transaction:

    total_price = 0
    for p in products:
        total_price += p.price

    transaction_buy = Transaction(
        created_at=date(2020, 9, 1),
        updated_at=date(2020, 9, 1),
        reason=reason,
        nonce=f"nonce-direct-purchase-{method.value.lower()}-{random()}",
        _type="direct_purchase",
    )

    db_session.add_all([transaction_buy])
    db_session.flush()

    transaction_buy.orders = [
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            _method=method.value.lower(),
            account_id=checkout_service.get_account_for_checkout_method(
                db_session, method
            ),
            price=total_price,
            _type="direct_payment",
        ),
    ]

    for p in products:
        transaction_buy.orders.append(
            Order(
                created_at=date(2020, 9, 1),
                updated_at=date(2020, 9, 1),
                product_id=p.id,
                _method="account",
                account_id=p.ledger_id,
                price=-p.price,
                _type="product_purchase",
            )
        )

    # Validate the order type with transaction type
    assert (
        transaction_service.determine_transaction_type(transaction_buy)
        == transaction_buy.type
    )
    db_session.commit()
    return transaction_buy


def _delete_transaction(db_session: Session, transaction: Transaction) -> Transaction:
    context = UserRequestContext(db_session, None)  # type: ignore[arg-type]
    transaction.deleted = True
    order_service.delete_orders(context, transaction.orders, do_commit=False)
    db_session.add(transaction)
    db_session.commit()
    return transaction


def _create_account_purchase_with_top_up(
    db_session: Session,
    account: Account,
    products: list[Product],
    amount: int,
) -> Transaction:
    product_price = sum(p.price for p in products)
    assert product_price <= amount, "top up not more than products prices"

    topup_amount = amount - product_price
    account.balance += topup_amount

    transaction = Transaction(
        created_at=date(2020, 9, 1),
        updated_at=date(2020, 9, 1),
        reason=None,
        nonce=f"nonce-purchase-with-top-up-cash-{random()}",
        _type="top_up_and_purchase",
    )

    db_session.add_all([transaction])
    db_session.flush()

    transaction.orders = [
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            _method="account",
            account_id=account.id,
            price=-topup_amount,
            _type="account_top_up_or_account_payment",
        ),
        Order(
            created_at=date(2020, 9, 1),
            updated_at=date(2020, 9, 1),
            _method="cash",
            account_id=checkout_service.get_account_for_checkout_method(
                db_session, CheckoutMethod.CASH
            ),
            price=amount,
            _type="direct_payment",
        ),
    ]

    for product in products:
        transaction.orders.append(
            Order(
                created_at=date(2020, 9, 1),
                updated_at=date(2020, 9, 1),
                product_id=product.id,
                _method="cash",
                account_id=product.ledger_id,
                price=-product.price,
                _type="product_purchase",
            ),
        )

    # Validate the order type with transaction type
    assert (
        transaction_service.determine_transaction_type(transaction) == transaction.type
    )

    db_session.commit()
    return transaction
