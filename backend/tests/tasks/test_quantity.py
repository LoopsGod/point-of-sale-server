from unittest.mock import Mock, patch
from json import dumps

from app.repository.settings import get_db_settings, save_db_settings
from app.service import mail_service
from app.tasks.quantity import email_product_quantity


def test_quantity(
    db_session,
    bar_client,
    products,
    normal_account,
    sepa_account,
    event_account,
):
    r1 = bar_client.post(
        "/api/checkout/account/",
        json={
            "nonce": "123",
            "account_id": sepa_account.id,
            "products": [{"id": products["bar_beer"].id, "amount": 1}],
        },
    )
    assert r1.status_code == 200, r1.json()

    db_settings = get_db_settings(db_session)
    db_settings.summary_included_categories = '["1"]'
    save_db_settings(db_session, db_settings)

    with patch.object(mail_service, "send_email", Mock()) as mocked_method:
        email_product_quantity()
        # Should not email anything, because category was not included
        mocked_method.assert_not_called()

    category_ids = [c.id for c in products["bar_beer"].categories]

    db_settings.summary_included_categories = dumps(category_ids)
    save_db_settings(db_session, db_settings)

    with patch.object(mail_service, "send_email", Mock()) as mocked_method2:
        email_product_quantity()
        mocked_method2.assert_called_once()
        args, _ = mocked_method2.call_args
        assert products["bar_beer"].name in args[0].html
