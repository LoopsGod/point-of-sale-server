import pytest

from app.tasks.account import (
    check_balance_integrity,
    check_allow_negative_balance_integrity,
)
from app.views.exceptions import ApplicationException


def test_balance_check(
    db_session,
    bar_client,
    products,
    sepa_account,
    event_account,
):
    r1 = bar_client.post(
        "/api/checkout/account/",
        json={
            "nonce": "123",
            "account_id": sepa_account.id,
            "products": [{"id": products["bar_beer"].id, "amount": 1}],
        },
    )
    assert r1.status_code == 200, r1.json()

    event_account.balance = -1
    db_session.commit()

    with pytest.raises(ApplicationException) as e:
        check_balance_integrity()
    assert "Total balances at" in e.value.error, "Total balance should not be 0"


def test_allow_negative_check(
    db_session,
    normal_account,
):
    normal_account.allow_negative_balance = True
    db_session.commit()

    with pytest.raises(ApplicationException) as e:
        check_allow_negative_balance_integrity()
    assert "allow_negative_balance" in e.value.error
