import base64
import email.parser
import json
import re
from datetime import datetime, timezone, timedelta, date
from email import policy
from email.message import EmailMessage
from unittest.mock import patch

import freezegun
import pytest
from googleapiclient.discovery import build
from googleapiclient.http import HttpMockSequence
from sqlalchemy.orm import Session
from starlette.testclient import TestClient

from app.models import SepaBatch, Account, User
from app.service import mail_service
from app.tasks.sepa import (
    send_sepa_notification,
    check_sepa_batch_created_current_month,
    check_sepa_batch_not_executed,
)


@pytest.fixture
def pending_sepa_batch(
    sepa_account: Account,
    db_session: Session,
    admin_client: TestClient,
    users: dict[str, User],
) -> SepaBatch:
    sepa_account.balance = -100
    db_session.add(sepa_account)
    db_session.commit()

    rv = admin_client.post(
        "/api/admin/sepa_batches/",
        json={
            "accounts": [sepa_account.id],
            "executionDate": (
                datetime.now(timezone.utc) + timedelta(days=-1)
            ).isoformat(),
            "description": "Fixture",
        },
    )
    assert rv.status_code == 201, rv.json
    batch = db_session.get(SepaBatch, rv.json()["batch_id"])
    assert batch is not None
    return batch


def test_send_sepa_mail(
    pending_sepa_batch: SepaBatch, db_session: Session, sepa_account: Account
) -> None:
    assert sepa_account.user

    http = HttpMockSequence([({"status": 200}, "{}")])  # Empty Google API response
    with patch.object(
        mail_service,
        "build_gmail_service",
        lambda: build("gmail", "v1", http=http),
    ):
        # Do not use .delay() to call task in same process.
        send_sepa_notification(pending_sepa_batch.id)

    db_session.refresh(pending_sepa_batch)
    assert pending_sepa_batch.mail_date is not None
    assert pending_sepa_batch.mail_date - datetime.now() < timedelta(minutes=1)

    # A bold effort to validate email contents =D
    req_body = json.loads(http.request_sequence[0][2])
    email_content = email.message_from_bytes(
        base64.urlsafe_b64decode(req_body["raw"]), policy=policy.strict
    )
    assert isinstance(email_content, EmailMessage)
    assert email_content["to"] == sepa_account.user.username
    assert email_content["from"] == "Point of Sale <pos@svia.nl>"
    assert email_content["subject"].startswith("VIA direct debit")
    body = email_content.get_body()
    assert body is not None and isinstance(body, EmailMessage)
    assert (
        "we will automatically deduce your bank account by €1.00 for"
        in body.get_content()
    )


@pytest.mark.usefixtures("notification_settings")
@freezegun.freeze_time(date(year=2022, month=9, day=20))
def test_check_sepa_batch_created_current_month(
    normal_account: Account, db_session: Session
) -> None:
    normal_account.balance = -10000  # 100 EUR
    db_session.commit()

    http = HttpMockSequence([({"status": 200}, "{}")])  # Empty Google API response
    with patch.object(
        mail_service,
        "build_gmail_service",
        lambda: build("gmail", "v1", http=http),
    ):
        # Do not use .delay() to call task in same process.
        check_sepa_batch_created_current_month()

    req_body = json.loads(http.request_sequence[0][2])
    email_content = email.message_from_bytes(
        base64.urlsafe_b64decode(req_body["raw"]), policy=policy.strict
    )
    assert isinstance(email_content, EmailMessage)
    assert email_content["to"] == "sepa@example.com"
    assert email_content["from"] == "Point of Sale <pos@svia.nl>"
    assert re.match(r"Sepa batch '\w+' should be created.", email_content["subject"])
    body = email_content.get_body()
    assert body is not None and isinstance(body, EmailMessage)
    assert re.search(
        r"This is a reminder to create the sepa batch for month: \w+.",
        body.get_content(),
    )


@pytest.mark.usefixtures("notification_settings")
def test_check_sepa_batch_not_executed(
    pending_sepa_batch: SepaBatch, db_session: Session
) -> None:
    http = HttpMockSequence([({"status": 200}, "{}")])  # Empty Google API response
    with patch.object(
        mail_service,
        "build_gmail_service",
        lambda: build("gmail", "v1", http=http),
    ):
        # Do not use .delay() to call task in same process.
        check_sepa_batch_not_executed()

    req_body = json.loads(http.request_sequence[0][2])
    email_content = email.message_from_bytes(
        base64.urlsafe_b64decode(req_body["raw"]), policy=policy.strict
    )
    assert isinstance(email_content, EmailMessage)
    assert email_content["to"] == "sepa@example.com"
    assert email_content["from"] == "Point of Sale <pos@svia.nl>"
    assert email_content["subject"] == "Sepa batch Fixture is due for execution"
    body = email_content.get_body()
    assert body is not None and isinstance(body, EmailMessage)
    assert (
        f"https://pos.svia.nl/admin/sepa_batches/{pending_sepa_batch.id}"
        in body.get_content()
    )


def test_send_sepa_notification(
    pending_sepa_batch: SepaBatch, db_session: Session
) -> None:
    http = HttpMockSequence([({"status": 200}, "{}")])  # Empty Google API response
    with patch.object(
        mail_service,
        "build_gmail_service",
        lambda: build("gmail", "v1", http=http),
    ):
        # Do not use .delay() to call task in same process.
        send_sepa_notification(pending_sepa_batch.id)

    req_body = json.loads(http.request_sequence[0][2])
    email_content = email.message_from_bytes(
        base64.urlsafe_b64decode(req_body["raw"]), policy=policy.strict
    )
    assert isinstance(email_content, EmailMessage)
    assert email_content["to"] == "user3@gmail.com"
    assert email_content["from"] == "Point of Sale <pos@svia.nl>"
    assert re.match(r"VIA direct debit \w+ \d+", email_content["subject"])
    body = email_content.get_body()
    assert body is not None and isinstance(body, EmailMessage)
    assert (
        "we will automatically deduce your bank account by €1.00" in body.get_content()
    )
