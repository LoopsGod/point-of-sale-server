from datetime import datetime
from urllib.parse import urlencode

import pytest
from sqlalchemy.orm import Session
from starlette.testclient import TestClient

from app.models.account import Account
from app.models.product import Product
from tests.util.transactions import (
    _create_top_up,
    _create_account_purchase,
    _create_direct_purchase,
    _delete_transaction,
)


@pytest.mark.usefixtures("products")
def test_pagination_basic(admin_client):
    r = admin_client.get("/api/admin/products/")
    res = r.json()
    assert r.status_code == 200, res
    assert res["page"] == 1
    assert res["perPage"] == 15
    assert res["pageCount"] == 1
    assert len(res["items"]) == 3


@pytest.mark.usefixtures("products")
def test_pagination_multi_page(admin_client):
    r = admin_client.get("/api/admin/products/?perPage=1")
    res = r.json()
    assert r.status_code == 200, res
    assert res["page"] == 1
    assert res["perPage"] == 1
    assert res["pageCount"] == 3
    assert len(res["items"]) == 1


@pytest.mark.usefixtures("products")
def test_pagination_second_page(admin_client):
    r = admin_client.get("/api/admin/products/?perPage=2&page=2")
    res = r.json()
    assert r.status_code == 200, res
    assert res["page"] == 2
    assert res["perPage"] == 2
    assert res["pageCount"] == 2
    assert len(res["items"]) == 1


@pytest.mark.usefixtures("products")
def test_pagination_search(admin_client):
    r = admin_client.get("/api/admin/products/?search=cola")
    res = r.json()
    assert r.status_code == 200, res
    assert res["page"] == 1
    assert res["pageCount"] == 1
    assert len(res["items"]) == 1
    assert res["items"][0]["name"] == "Cola"


@pytest.mark.usefixtures("products")
def test_pagination_sort(admin_client):
    r = admin_client.get("/api/admin/products/?search=beer&sort=name")
    res = r.json()
    assert r.status_code == 200, res
    assert res["page"] == 1
    assert res["pageCount"] == 1
    assert len(res["items"]) == 2
    assert res["items"][0]["name"] == "Bottled Beer"
    assert res["items"][1]["name"] == "Tap beer"


@pytest.mark.usefixtures("products")
def test_pagination_sort_descending(admin_client):
    r = admin_client.get("/api/admin/products/?search=beer&sort=-name")
    res = r.json()
    assert r.status_code == 200, res
    assert res["page"] == 1
    assert res["pageCount"] == 1
    assert len(res["items"]) == 2
    assert res["items"][0]["name"] == "Tap beer"
    assert res["items"][1]["name"] == "Bottled Beer"


@pytest.mark.usefixtures("products")
def test_pagination_sort_multiple(admin_client):
    r = admin_client.get("/api/admin/products/?sort=quantity, -price, name")
    res = r.json()
    assert r.status_code == 200, res
    assert res["page"] == 1
    assert res["pageCount"] == 1
    assert len(res["items"]) == 3
    assert res["items"][0]["name"] == "Tap beer"
    assert res["items"][1]["name"] == "Bottled Beer"
    assert res["items"][2]["name"] == "Cola"


@pytest.mark.usefixtures("products")
@pytest.mark.parametrize(
    "input", [">=asd", "", ",,,", 123, None, "=>123", "==123", "=123,=--456"]
)
def test_pagination_invalid_number_filter(admin_client, input):
    r = admin_client.get("/api/admin/products/?" + urlencode({"priceFilter": input}))

    assert r.status_code == 422, input


@pytest.mark.usefixtures("products")
def test_pagination_number_filter(admin_client):
    r = admin_client.get(
        "/api/admin/products/?" + urlencode({"priceFilter": ">=60, <=100"})
    )

    res = r.json()
    assert r.status_code == 200, res
    assert res["page"] == 1
    assert res["pageCount"] == 1
    assert len(res["items"]) == 1
    assert res["items"][0]["name"] == "Bottled Beer"

    r = admin_client.get(
        "/api/admin/products/?" + urlencode({"priceFilter": ">=70, <=70"})
    )

    res = r.json()
    assert r.status_code == 200, res
    assert res["page"] == 1
    assert res["pageCount"] == 1
    assert len(res["items"]) == 1
    assert res["items"][0]["name"] == "Bottled Beer"

    r = admin_client.get("/api/admin/products/?" + urlencode({"priceFilter": "=70"}))

    res = r.json()
    assert r.status_code == 200, res
    assert res["page"] == 1
    assert res["pageCount"] == 1
    assert len(res["items"]) == 1
    assert res["items"][0]["name"] == "Bottled Beer"

    r = admin_client.get("/api/admin/products/?" + urlencode({"priceFilter": "<=-70"}))

    res = r.json()
    assert r.status_code == 200, res
    assert res["page"] == 1
    assert res["pageCount"] == 0
    assert len(res["items"]) == 0

    r = admin_client.get(
        "/api/admin/products/?" + urlencode({"quantityFilter": "<=-5"})
    )

    res = r.json()
    assert r.status_code == 200, res
    assert res["page"] == 1
    assert res["pageCount"] == 1
    assert len(res["items"]) == 1
    assert res["items"][0]["name"] == "Tap beer"


@pytest.mark.usefixtures("products")
def test_pagination_boolean_filter(admin_client):
    r = admin_client.get("/api/admin/products/?" + urlencode({"depositFilter": "true"}))

    res = r.json()
    assert r.status_code == 200, res
    assert res["page"] == 1
    assert res["pageCount"] == 1
    assert len(res["items"]) == 1
    #   Tap beer is the only product with deposit=True :^)
    assert res["items"][0]["name"] == "Tap beer"

    r = admin_client.get("/api/admin/products/?" + urlencode({"depositFilter": "no"}))

    res = r.json()
    assert r.status_code == 200, res
    assert res["page"] == 1
    assert res["pageCount"] == 1
    assert len(res["items"]) == 2

    r = admin_client.get(
        "/api/admin/products/?" + urlencode({"depositFilter": "invalid"})
    )

    res = r.json()
    assert r.status_code == 422, res


@pytest.mark.usefixtures("products")
def test_pagination_foreign_key_filter(admin_client, categories):
    cat_ids = [str(c.id) for c in categories]

    r = admin_client.get(
        "/api/admin/products/?" + urlencode({"categoryFilter": ",".join(cat_ids)})
    )
    res = r.json()

    assert r.status_code == 200, res
    assert res["page"] == 1
    assert res["pageCount"] == 1
    assert len(res["items"]) == 3

    r = admin_client.get(
        "/api/admin/products/?" + urlencode({"categoryFilter": cat_ids[0]})
    )
    res = r.json()

    assert r.status_code == 200, res
    assert res["page"] == 1
    assert res["pageCount"] == 1
    assert len(res["items"]) == 1
    assert res["items"][0]["name"] == "Cola"

    r = admin_client.get(
        "/api/admin/products/?"
        + urlencode({"categoryFilter": f"{cat_ids[0]},{cat_ids[2]}"})
    )
    res = r.json()

    assert r.status_code == 200, res
    assert res["page"] == 1
    assert res["pageCount"] == 1
    assert len(res["items"]) == 2
    assert res["items"][0]["name"] == "Cola"
    assert res["items"][1]["name"] == "Tap beer"


@pytest.mark.usefixtures("products")
def test_pagination_foreign_key_filter_empty(admin_client):
    r = admin_client.get("/api/admin/products/?" + urlencode({"categoryFilter": ""}))
    assert r.status_code == 422, r.json()


@pytest.mark.usefixtures("products")
def test_pagination_foreign_key_filter_none(
    admin_client: TestClient, db_session: Session, ledger_accounts: dict[str, Account]
) -> None:

    product_without_category = Product(
        name="Deprecated product",
        price=150,
        ledger_id=ledger_accounts["bar"].id,
        deposit=False,
    )
    db_session.add(product_without_category)
    db_session.commit()

    r = admin_client.get(
        "/api/admin/products/?" + urlencode({"categoryFilter": "none"})
    )

    res = r.json()
    assert r.status_code == 200, res
    assert res["page"] == 1
    assert len(res["items"]) > 0

    for i in res["items"]:
        assert len(i["categoryIds"]) == 0


@pytest.mark.usefixtures("products")
def test_pagination_ledger_filter(admin_client, ledger_accounts):
    room = ledger_accounts["room"]
    bar = ledger_accounts["bar"]

    r = admin_client.get(
        "/api/admin/products/?" + urlencode({"ledgerFilter": f"{room.id}"})
    )
    res = r.json()
    assert r.status_code == 200, res
    assert len(res["items"]) > 0
    for i in res["items"]:
        assert i["ledgerId"] == room.id

    r = admin_client.get(
        "/api/admin/products/?" + urlencode({"ledgerFilter": f"{room.id},{bar.id}"})
    )
    res = r.json()
    assert r.status_code == 200, res
    assert len(res["items"]) > 0
    for i in res["items"]:
        assert i["ledgerId"] == room.id or i["ledgerId"] == bar.id

    r = admin_client.get("/api/admin/products/?" + urlencode({"ledgerFilter": "none"}))
    res = r.json()
    assert r.status_code == 200, res
    #  Products are required to have a ledger
    assert len(res["items"]) == 0


@pytest.mark.usefixtures("products")
def test_pagination_ledger_and_category_filter(
    admin_client, ledger_accounts, categories
):
    room = ledger_accounts["room"]
    cat_ids = [str(c.id) for c in categories]

    r = admin_client.get(
        "/api/admin/products/?"
        + urlencode({"ledgerFilter": f"{room.id}", "categoryFilter": f"{cat_ids[0]}"})
    )

    res = r.json()
    assert r.status_code == 200, res


@pytest.mark.usefixtures("products")
def test_pagination_ledger_filter_and_sort(admin_client, ledger_accounts):
    room = ledger_accounts["room"]

    r = admin_client.get(
        "/api/admin/products/?"
        + urlencode({"ledgerFilter": f"{room.id}", "sort": "ledger"})
    )

    res = r.json()
    assert r.status_code == 200, res
    assert len(res["items"]) == 2


@pytest.mark.usefixtures("products")
def test_pagination_sort_on_foreign_key(admin_client: TestClient) -> None:
    r = admin_client.get("/api/admin/products/?" + urlencode({"sort": "ledger"}))

    res = r.json()
    assert r.status_code == 200, res
    assert len(res["items"]) == 3


@pytest.mark.usefixtures("products")
def test_pagination_invalid_params(admin_client):
    r = admin_client.get("/api/admin/products/?perPage=1337&page=-5")
    assert r.status_code == 422, r.json()

    r = admin_client.get("/api/admin/products/?page=0")
    assert r.status_code == 422, r.json()

    r = admin_client.get("/api/admin/products/?page=asd")
    assert r.status_code == 422, r.json()


@pytest.mark.usefixtures("products")
def test_pagination_search_no_result(admin_client):
    r = admin_client.get("/api/admin/products/?search=wilco")
    res = r.json()
    assert r.status_code == 200, res
    assert res["page"] == 1
    assert res["pageCount"] == 0
    assert len(res["items"]) == 0


@pytest.mark.usefixtures("products")
def test_pagination_search_non_existing_page(admin_client):
    r = admin_client.get("/api/admin/products/?page=10")
    res = r.json()
    assert r.status_code == 200, res
    assert res["page"] == 10
    assert res["pageCount"] == 1
    assert len(res["items"]) == 0


@pytest.mark.usefixtures("account_request")
def test_pagination_default_sort_if_empty_string(admin_client):
    r1 = admin_client.get("/api/admin/account_requests/")
    res1 = r1.json()

    r2 = admin_client.get("/api/admin/account_requests/?sort=")
    res2 = r2.json()

    assert res1["items"] == res2["items"]


def test_null_filter(
    db_session: Session,
    normal_account: Account,
    products: dict[str, Product],
    admin_client: TestClient,
) -> None:
    top_up = _create_top_up(db_session, normal_account)
    _create_account_purchase(db_session, normal_account, [products["bar_beer"]])
    _create_direct_purchase(db_session, [products["room_beer"], products["room_beer"]])

    r = admin_client.get("/api/admin/v2/transactions/").json()
    #  Check initial transaction creation
    assert len(r["items"]) == 3

    _delete_transaction(db_session, top_up)

    r = admin_client.get("/api/admin/v2/transactions/").json()
    #  No filter, still 3
    assert len(r["items"]) == 3

    r = admin_client.get(
        "/api/admin/v2/transactions/?" + urlencode({"deletedFilter": "true"})
    ).json()
    assert len(r["items"]) == 1

    r = admin_client.get(
        "/api/admin/v2/transactions/?" + urlencode({"deletedFilter": "false"})
    ).json()
    assert len(r["items"]) == 2


def test_literal_equals_filter(
    db_session: Session,
    normal_account: Account,
    products: dict[str, Product],
    admin_client: TestClient,
) -> None:
    _create_top_up(db_session, normal_account)
    _create_account_purchase(db_session, normal_account, [products["bar_beer"]])
    _create_direct_purchase(db_session, [products["room_beer"], products["room_beer"]])

    r = admin_client.get("/api/admin/v2/transactions/").json()
    assert len(r["items"]) == 3

    r = admin_client.get(
        "/api/admin/v2/transactions/?"
        + urlencode({"transactionTypeFilter": '"account_purchase"'})
    ).json()
    assert len(r["items"]) == 1

    r = admin_client.get(
        "/api/admin/v2/transactions/?"
        + urlencode({"transactionTypeFilter": '"direct_purchase"'})
    ).json()
    assert len(r["items"]) == 1

    r = admin_client.get(
        "/api/admin/v2/transactions/?"
        + urlencode(
            {"transactionTypeFilter": '["direct_purchase", "account_purchase"]'}
        )
    ).json()
    assert len(r["items"]) == 2

    r = admin_client.get(
        "/api/admin/v2/transactions/?"
        + urlencode({"productFilter": products["bar_beer"].id})
    ).json()
    assert len(r["items"]) == 1

    r = admin_client.get(
        "/api/admin/v2/transactions/?"
        + urlencode({"productFilter": products["room_beer"].id})
    ).json()
    assert len(r["items"]) == 1

    r = admin_client.get(
        "/api/admin/v2/transactions/?"
        + urlencode(
            {"productFilter": [products["bar_beer"].id, products["room_beer"].id]}
        )
    ).json()
    assert len(r["items"]) == 2


def test_datetime_filter(
    db_session: Session,
    normal_account: Account,
    event_account: Account,
    products: dict[str, Product],
    admin_client: TestClient,
) -> None:
    _create_account_purchase(db_session, normal_account, [products["bar_beer"]])
    _create_account_purchase(
        db_session, normal_account, [products["room_beer"], products["bar_beer"]]
    )
    t3 = _create_account_purchase(db_session, event_account, [products["room_beer"]])
    t3.created_at = datetime(2022, 9, 30, 13, 37, 28, 101000)

    t4 = _create_direct_purchase(
        db_session, [products["room_beer"], products["room_beer"]]
    )
    t4.created_at = datetime(2022, 10, 1, 14, 37, 28, 101000)

    db_session.add_all([t3, t4])
    db_session.commit()

    r = admin_client.get(
        "/api/admin/v2/transactions/?"
        + urlencode(
            {
                "createdAtFilter": "1990-10-01T13:37:28.101Z",
            }
        )
    ).json()

    assert len(r["items"]) == 4

    r = admin_client.get(
        "/api/admin/v2/transactions/?"
        + urlencode(
            {
                "createdAtFilter": "2022-09-01T01:00:28.101Z",
            }
        )
    ).json()

    assert len(r["items"]) == 2

    r = admin_client.get(
        "/api/admin/v2/transactions/?"
        + urlencode(
            {
                "createdAtFilter": "2022-09-01T13:00:28.101Z~2022-09-30T23:00:28.101Z",
            }
        )
    ).json()

    assert len(r["items"]) == 1

    r = admin_client.get(
        "/api/admin/v2/transactions/?"
        + urlencode(
            {
                "createdAtFilter": "13:00:28.101Z",
            }
        )
    ).json()

    assert len(r["items"]) == 2

    r = admin_client.get(
        "/api/admin/v2/transactions/?"
        + urlencode(
            {
                "createdAtFilter": "13:00:28.101Z~14:00:28.101Z",
            }
        )
    ).json()

    assert len(r["items"]) == 1

    r = admin_client.get(
        "/api/admin/v2/transactions/?"
        + urlencode(
            {
                "createdAtFilter": "13:00:28.101Z~2099-01-01T14:00:28.101Z",
            }
        )
    ).json()

    assert len(r["items"]) == 4


def test_datetime_filter_bad_date(
    admin_client: TestClient,
) -> None:
    res = admin_client.get(
        "/api/admin/v2/transactions/?"
        + urlencode(
            {
                "createdAtFilter": "asdasd",
            }
        )
    )

    assert res.status_code == 422


def test_paginated_search_transactions(
    db_session: Session,
    normal_account: Account,
    event_account: Account,
    products: dict[str, Product],
    admin_client: TestClient,
) -> None:
    _create_account_purchase(db_session, normal_account, [products["bar_beer"]])
    _create_account_purchase(
        db_session, normal_account, [products["room_beer"], products["bar_beer"]]
    )
    _create_account_purchase(db_session, event_account, [products["room_beer"]])
    _create_direct_purchase(db_session, [products["room_beer"], products["room_beer"]])

    r = admin_client.get("/api/admin/v2/transactions/").json()
    assert len(r["items"]) == 4

    r = admin_client.get(
        "/api/admin/v2/transactions/?"
        + urlencode(
            {
                "accountFilter": normal_account.id,
                "productFilter": products["bar_beer"].id,
            }
        )
    ).json()

    assert len(r["items"]) == 2


def test_paginated_search_transactions_ordering(
    db_session: Session,
    normal_account: Account,
    event_account: Account,
    products: dict[str, Product],
    admin_client: TestClient,
) -> None:
    # Tests that newest transaction is always shown first
    _create_account_purchase(db_session, normal_account, [products["bar_beer"]])
    _create_account_purchase(
        db_session, normal_account, [products["room_beer"], products["bar_beer"]]
    )
    _create_account_purchase(db_session, event_account, [products["room_beer"]])
    last_transaction = _create_direct_purchase(
        db_session, [products["room_beer"], products["room_beer"]]
    )
    last_transaction.created_at = last_transaction.updated_at = datetime.now()
    db_session.add(last_transaction)
    db_session.commit()

    r = admin_client.get(
        "/api/admin/v2/transactions/?"
        + urlencode(
            {
                "sort": "-created_at",
            }
        )
    ).json()

    assert len(r["items"]) == 4
    assert r["items"][0]["id"] == last_transaction.id


def test_paginated_search_transactions_page_count(
    db_session: Session,
    normal_account: Account,
    event_account: Account,
    products: dict[str, Product],
    admin_client: TestClient,
) -> None:
    _create_account_purchase(db_session, normal_account, [products["bar_beer"]])
    _create_account_purchase(
        db_session, normal_account, [products["room_beer"], products["bar_beer"]]
    )
    _create_account_purchase(db_session, event_account, [products["room_beer"]])
    _create_direct_purchase(db_session, [products["room_beer"], products["room_beer"]])

    r = admin_client.get("/api/admin/v2/transactions/?perPage=2").json()
    assert len(r["items"]) == 2
    assert r["pageCount"] == 2


def test_schema(admin_client):
    r = admin_client.get("/api/openapi.json")
    res = r.json()
    assert r.status_code == 200, res

    admin_products_schema = res["paths"]["/api/admin/products/"]

    expected_sort_param = {
        "required": False,
        "schema": {
            "title": "Sort",
            "type": "string",
            "default": "-deleted_at,name",
            "allowed_columns": [
                "name",
                "price",
                "quantity",
                "ledger",
                "deposit",
                "deleted_at",
            ],
        },
        "name": "sort",
        "in": "query",
    }

    assert expected_sort_param in admin_products_schema["get"]["parameters"]

    expected_filter_param = {
        "required": False,
        "schema": {
            "title": "NumberFilter",
            "type": "string",
            "examples": ["=102.3", "=100", ">=100", "<=100", ">=50, <=100"],
        },
        "name": "priceFilter",
        "in": "query",
    }

    #  Want to be sure that schema.title includes the name of the used filter.
    assert expected_filter_param in admin_products_schema["get"]["parameters"]
