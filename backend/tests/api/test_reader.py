import pytest
from sqlalchemy import select, insert
from sqlalchemy.orm import Session
from starlette.testclient import TestClient

from app.models import User, Account
from app.models.checkout_card import CheckoutCard


@pytest.mark.usefixtures("card_reader_secret")
def test_reader_register_card(
    db_session: Session,
    room_client: TestClient,
    sepa_account: Account,
    pin: dict[str, str],
) -> None:
    rv = room_client.post(
        "/api/reader/register/",
        json={
            "cardSecret": "$abcde1",
            "accountId": sepa_account.id,
            "pin": pin["pin"],
        },
    )
    assert rv.status_code == 204, rv.json()
    c: CheckoutCard = db_session.execute(select(CheckoutCard)).scalar_one()
    assert c is not None
    assert sepa_account.user
    assert c.user_id == sepa_account.user.id


def test_reader_register_card_secret_setting_missing(
    db_session: Session,
    room_client: TestClient,
    sepa_account: Account,
    pin: dict[str, str],
) -> None:
    rv = room_client.post(
        "/api/reader/register/",
        json={
            "cardSecret": "$abcde1",
            "accountId": sepa_account.id,
            "pin": pin["pin"],
        },
    )
    assert rv.status_code == 409, rv.json()
    assert rv.json()["error"] == "reader_disabled"
    assert not db_session.scalar(select(CheckoutCard))


@pytest.mark.usefixtures("card_reader_secret")
@pytest.mark.parametrize(
    "card_secret", ["$1234567890", "$abcde"], ids=["wrong prefix", "equal to prefix"]
)
def test_reader_register_card_wrong_secret(
    db_session: Session,
    room_client: TestClient,
    sepa_account: Account,
    pin: dict[str, str],
    card_secret: str,
) -> None:
    rv = room_client.post(
        "/api/reader/register/",
        json={
            "cardSecret": card_secret,
            "accountId": sepa_account.id,
            "pin": pin["pin"],
        },
    )
    assert rv.status_code == 400, rv.json()
    assert rv.json()["error"] == "invalid_card_secret"
    assert not db_session.scalar(select(CheckoutCard))


def test_reader_register_card_duplicate(
    db_session: Session,
    room_client: TestClient,
    sepa_account: Account,
    pin: dict[str, str],
    card_reader_secret: str,
) -> None:
    assert sepa_account.user
    db_session.execute(
        insert(CheckoutCard).values(
            {"card_secret": "67890", "user_id": sepa_account.user.id}
        )
    )
    db_session.commit()

    rv = room_client.post(
        "/api/reader/register/",
        json={
            "cardSecret": f"{card_reader_secret}67890",
            "accountId": sepa_account.id,
            "pin": pin["pin"],
        },
    )
    assert rv.status_code == 409, rv.json()
    assert rv.json()["error"] == "card_secret_exists"
    assert len(db_session.scalars(select(CheckoutCard)).all()) == 1


@pytest.mark.usefixtures("card_reader_secret")
def test_reader_register_card_unknown_account(
    db_session: Session,
    room_client: TestClient,
    pin: dict[str, str],
) -> None:
    account_id = 1e10
    assert db_session.get(Account, account_id) is None
    rv = room_client.post(
        "/api/reader/register/",
        json={
            "cardSecret": "$1234567890",
            "accountId": account_id,
            "pin": pin["pin"],
        },
    )
    assert rv.status_code == 404, rv.json()
    assert rv.json()["error"] == "not_found"
    assert not db_session.scalar(select(CheckoutCard))


@pytest.mark.usefixtures("card_reader_secret")
def test_reader_register_card_no_account_user(
    db_session: Session,
    room_client: TestClient,
    event_account: Account,
    pin: dict[str, str],
) -> None:
    event_account.pin = pin["hash"]
    db_session.commit()
    assert (
        db_session.scalar(select(User).filter(User.account_id == event_account.id))
        is None
    )
    rv = room_client.post(
        "/api/reader/register/",
        json={
            "cardSecret": "$abcde1",
            "accountId": event_account.id,
            "pin": pin["pin"],
        },
    )
    assert rv.status_code == 404, rv.json()
    assert rv.json()["error"] == "no_user_for_account"
    assert not db_session.scalar(select(CheckoutCard))


@pytest.mark.usefixtures("card_reader_secret")
def test_reader_register_card_invalid_pin(
    db_session: Session,
    room_client: TestClient,
    sepa_account: Account,
    pin: dict[str, str],
) -> None:
    rv = room_client.post(
        "/api/reader/register/",
        json={
            "cardSecret": "$abcde1",
            "accountId": sepa_account.id,
            "pin": "4321",
        },
    )
    assert rv.status_code == 422, rv.json()
    assert rv.json()["error"] == "invalid_account_pin"
    assert not db_session.scalar(select(CheckoutCard))
