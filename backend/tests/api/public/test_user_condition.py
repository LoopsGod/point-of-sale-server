from sqlalchemy import delete
from unittest import mock

from app.models import UserCondition


def test_checkout_without_terms(db_session, sepa_client, products):
    # This test assumes no user conditions accepted."""
    db_session.execute(delete(UserCondition))
    db_session.commit()

    r1 = sepa_client.post(
        "/api/checkout/account/self/",
        json={
            "nonce": "123",
            "account_id": None,
            "products": [{"id": products["room_beer"].id, "amount": 1}],
        },
    )

    assert r1.status_code == 400, r1.json()
    assert r1.json()["status"] == "err", r1.json()
    assert r1.json()["error"] == "not_accepted_terms", r1.json()


def test_user_accept_condition(db_session, sepa_client):
    # This test assumes no user conditions accepted."""
    db_session.execute(delete(UserCondition))
    db_session.commit()

    r1 = sepa_client.get("/api/config/")
    assert r1.status_code == 200, r1.json()
    assert not r1.json()["acceptedCurrentCondition"], r1.json()

    r2 = sepa_client.post("/api/user/condition/")
    assert r2.status_code == 200, r2.json()

    r3 = sepa_client.get("/api/config/")
    assert r3.status_code == 200, r3.json()
    assert r3.json()["acceptedCurrentCondition"], r3.json()

    uc: UserCondition = db_session.query(UserCondition).one()
    assert uc.accepted_cond_version == 1


@mock.patch("app.service.user_condition_service.CURRENT_CONDITION_VERSION", 2)
def test_user_need_update_condition(db_session, sepa_client):
    uc: UserCondition = db_session.query(UserCondition).one()
    assert uc.accepted_cond_version == 1

    r1 = sepa_client.get("/api/config/")
    assert r1.status_code == 200, r1.json()
    assert not r1.json()["acceptedCurrentCondition"], r1.json()

    r2 = sepa_client.post("/api/user/condition/")
    assert r2.status_code == 200, r2.json()

    r3 = sepa_client.get("/api/config/")
    assert r3.status_code == 200, r3.json()
    assert r3.json()["acceptedCurrentCondition"], r3.json()
