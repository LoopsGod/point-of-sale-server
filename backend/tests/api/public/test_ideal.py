from mollie.api.objects.payment import Payment

from app.models import IdealPayment

MOLLIE_PAYMENT_PAID = Payment(
    {
        "resource": "payment",
        "id": "tr_HQ7KU5bBd7",
        "mode": "test",
        "createdAt": "2018-10-04T12:05:57+00:00",
        "amount": {"value": "10.00", "currency": "EUR"},
        "description": "Upgrade via account",
        "method": "ideal",
        "metadata": None,
        "status": "paid",
        "paidAt": "2018-10-04T12:06:15+00:00",
        "amountRefunded": {"value": "0.00", "currency": "EUR"},
        "amountRemaining": {"value": "35.00", "currency": "EUR"},
        "locale": "nl_NL",
        "countryCode": "NL",
        "profileId": "pfl_Qq54yD43me",
        "settlementId": "stl_yyrkGAxNS2",
        "sequenceType": "oneoff",
        "redirectUrl": "https://pos.svia.nl/accounts/428/saldo/?cb=28",
        "webhookUrl": "https://pos.svia.nl/api/mollie/webhook/",
        "settlementAmount": {"value": "10.00", "currency": "EUR"},
        "details": {
            "consumerName": "J DOE",
            "consumerAccount": "NL02ABNA0123456789",
            "consumerBic": "ABNANL2A",
        },
        "_links": {
            "self": {
                "href": "https://api.mollie.com/v2/payments/tr_HQ7KU5bBd7",
                "type": "application/hal+json",
            },
            "settlement": {
                "href": "https://api.mollie.com/v2/settlements/stl_yyrkGAxNS2",
                "type": "application/hal+json",
            },
            "documentation": {
                "href": "https://docs.mollie.com/reference/v2/payments-api/get-payment",
                "type": "text/html",
            },
        },
    }
)

MOLLIE_PAYMENT_CANCELLED = Payment(
    {
        "resource": "payment",
        "id": "tr_thRS56Cw3G",
        "mode": "test",
        "createdAt": "2018-09-25T13:38:21+00:00",
        "amount": {"value": "20.00", "currency": "EUR"},
        "description": "Upgrade via account",
        "method": "ideal",
        "metadata": None,
        "status": "canceled",
        "canceledAt": "2018-09-25T13:38:32+00:00",
        "countryCode": "NL",
        "profileId": "pfl_Qq54yD43me",
        "sequenceType": "oneoff",
        "redirectUrl": "https://pos.svia.nl/accounts/531/saldo/?cb=5",
        "webhookUrl": "https://pos.svia.nl/api/mollie/webhook/",
        "_links": {
            "self": {
                "href": "https://api.mollie.com/v2/payments/tr_thRS56Cw3G",
                "type": "application/hal+json",
            },
            "documentation": {
                "href": "https://docs.mollie.com/reference/v2/payments-api/get-payment",
                "type": "text/html",
            },
        },
    }
)


def test_upgrade_paid(
    db_session,
    normal_client,
    normal_account,
    anonymous_client,
    requests_mocker,
):
    requests_mocker.get(
        "https://api.mollie.com/v2/payments/tr_123456789", json=MOLLIE_PAYMENT_PAID
    )
    mollie_id = "tr_123456789"
    payment = IdealPayment(account_id=normal_account.id, mollie_id=mollie_id)
    db_session.add(payment)
    db_session.commit()
    assert normal_account.balance == 0

    rv = anonymous_client.post("/api/mollie/webhook/", data={"id": mollie_id})
    assert rv.status_code == 204, rv.json()

    # Upgraded 10 Euro
    db_session.refresh(normal_account)
    db_session.refresh(payment)
    assert normal_account.balance == 1000
    assert payment.status == "paid"

    rv = normal_client.get(f"/api/mollie/status/{payment.id}/")
    assert rv.status_code == 200, rv.json()
    assert rv.json()["paymentStatus"] == "paid", rv.json()


def test_upgrade_canceled(
    db_session, normal_client, normal_account, anonymous_client, requests_mocker
):
    requests_mocker.get(
        "https://api.mollie.com/v2/payments/tr_123456789", json=MOLLIE_PAYMENT_CANCELLED
    )

    payment = IdealPayment(account_id=normal_account.id, mollie_id="tr_123456789")
    db_session.add(payment)
    db_session.commit()

    rv = anonymous_client.post("/api/mollie/webhook/", data={"id": "tr_123456789"})
    assert rv.status_code == 204, rv.json()

    assert normal_account.balance == 0
    assert payment.status == "canceled"

    rv = normal_client.get(f"/api/mollie/status/{payment.id}/")
    assert rv.status_code == 200, rv.json()
    assert rv.json()["paymentStatus"] == "canceled", rv.json()
