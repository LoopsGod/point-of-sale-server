from datetime import datetime

import pytest
from sqlalchemy import select
from sqlalchemy.orm import Session
from starlette.testclient import TestClient

from app.models import User, LoginToken


@pytest.fixture(params=[(), ("admin",)])
def user(db_session, request):
    user = User(username="kelder")
    user.set_password("kelder")
    user.is_admin = "admin" in request.param
    db_session.add(user)
    db_session.commit()
    return user


def test_login_password(anonymous_client, user):
    rv = anonymous_client.post(
        "/api/login/", json={"username": "kelder", "password": "kelder"}
    )
    assert rv.status_code == 200, rv.json()
    assert rv.json()["username"] == user.username, rv.json()
    assert rv.json()["user_id"] == user.id, rv.json()
    assert "access_token" in rv.json()
    assert rv.json()["access_token"], rv.json()


def test_login_wrong_password(anonymous_client, user):
    rv = anonymous_client.post(
        "/api/login/", json={"username": "kelder", "password": "notkelder"}
    )
    assert rv.status_code == 401, rv.json()
    assert rv.json()["status"] == "err", rv.json()
    assert rv.json()["error"] == "invalid_username_password", rv.json()


def test_login_deleted_user(
    db_session: Session, anonymous_client: TestClient, user: User
) -> None:
    user.deleted = True
    db_session.commit()

    rv = anonymous_client.post(
        "/api/login/", json={"username": "kelder", "password": "kelder"}
    )
    assert rv.status_code == 401, rv.json()
    assert rv.json()["status"] == "err", rv.json()
    assert rv.json()["error"] == "invalid_username_password", rv.json()


@pytest.mark.parametrize(
    "payload", [{}, {"username": "kelder"}, {"password": "kelder"}]
)
def test_login_missing_fields(anonymous_client, payload):
    rv = anonymous_client.post("/api/login/", json=payload)
    assert rv.status_code == 422, rv.json()


def test_login_viaduct(anonymous_client, viaduct_user, requests_mocker):
    requests_mocker.post(
        "https://svia.nl/oauth/token",
        json={
            "access_token": "some_access_token",
            "refresh_token": "some_refresh_token",
            "expires_in": 864000,
            "token_type": "Bearer",
        },
    )

    requests_mocker.post(
        "https://svia.nl/oauth/introspect",
        json={
            "active": True,
            "aud": "public",
            "client_id": "public",
            "exp": 1536852643,
            "iat": 1535988643,
            "iss": "https://svia.nl/",
            "scope": "pimpy",
            "sub": 69,  # Maico
            "token_type": "Bearer",
            "username": "john.doe@mail.com",
            "full_name": "John Doe",
        },
    )

    requests_mocker.get(
        "https://svia.nl/api/users/self/",
        json={
            "id": 69,
            "email": "john.doe@mail.com",
            "disabled": "False",
            "first_name": "John",
            "last_name": "Doe",
            "student_id": "123456789",
            "birth_date": "1990-01-01",
            "phone_nr": "0624705415",
            "locale": "en",
            "study_start": "1899-12-31",
            "educations": [
                {
                    "id": 11,
                    "nl_name": "Artificial Intelligence",
                    "en_name": "Artificial Intelligence",
                }
            ],
            "tfa_enabled": True,
            "address": "Some street",
            "zip": "1000aa",
            "city": "Amsterdam",
            "country": "Nederland",
            "member": True,
            "paid_date": "2019-10-03",
            "honorary_member": False,
            "favourer": False,
            "iban": "",
        },
    )
    rv = anonymous_client.post(
        "/api/login/oauth/viaduct/",
        json={
            "code": "some_authorization_code",
        },
    )

    assert rv.status_code == 200, rv.json()
    assert rv.json()["username"] == "john.doe@mail.com"
    assert rv.json()["user_id"] == viaduct_user.id
    assert "access_token" in rv.json()


def test_login_viaduct_wrong_code(anonymous_client, requests_mocker):
    requests_mocker.post(
        "https://svia.nl/oauth/token",
        json={
            "error": "invalid_request",
            "error_description": 'Invalid "code" in request.',
        },
        status_code=400,
    )

    rv = anonymous_client.post(
        "/api/login/oauth/viaduct/",
        json={
            "code": "some_wrong_authorization_code",
        },
    )

    assert rv.status_code == 400, rv.json()
    assert rv.json()["status"] == "err", rv.json()
    assert rv.json()["error"] == "oauth_code_invalid", rv.json()


def test_login_viaduct_missing_code(anonymous_client):
    rv = anonymous_client.post("/api/login/oauth/viaduct/", json={})
    assert rv.status_code == 422, rv.json()


@pytest.mark.parametrize(
    "route,expected_status_code",
    [
        ("/api/config/", 200),
        ("/api/products/", 401),
    ],
)
def test_expired_login_token(
    anonymous_client: TestClient,
    db_session: Session,
    users: dict[str, User],
    route: str,
    expected_status_code: int,
) -> None:
    user = users["random"]
    token_str = "TOKEN"
    token = LoginToken(
        user_id=user.id, token=token_str, tfa_enabled=False, created_at=datetime.min
    )
    db_session.add(token)
    db_session.commit()

    rv = anonymous_client.get(route, headers={"Authorization": f"Bearer {token_str}"})
    assert rv.status_code == expected_status_code

    assert not db_session.scalar(
        select(LoginToken).filter(LoginToken.token == token_str)
    )
