from starlette.testclient import TestClient


def test_admin_client_config(admin_client: TestClient) -> None:
    rv = admin_client.get("/api/config/")
    assert rv.status_code == 200, rv.json()


def test_room_client_config(room_client: TestClient) -> None:
    rv = room_client.get("/api/config/")
    assert rv.status_code == 200, rv.json()
    assert rv.status_code == 200


def test_bar_client_config(bar_client: TestClient) -> None:
    rv = bar_client.get("/api/config/")
    assert rv.status_code == 200, rv.json()


def test_normal_client_config(normal_client: TestClient) -> None:
    rv = normal_client.get("/api/config/")
    assert rv.status_code == 200, rv.json()


def test_sepa_client_config(sepa_client: TestClient) -> None:
    rv = sepa_client.get("/api/config/")
    assert rv.status_code == 200, rv.json()
