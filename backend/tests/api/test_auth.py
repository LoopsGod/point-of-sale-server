from datetime import datetime

from sqlalchemy.orm import Session
from starlette.testclient import TestClient

from app.models import LoginToken


def test_token_no_auth(anonymous_client):
    rv = anonymous_client.get("/api/products/")
    assert rv.status_code == 403, rv.json()


def test_token_not_found(anonymous_client, db_session):
    assert not db_session.query(LoginToken).all()
    rv = anonymous_client.get(
        "/api/products/", headers={"Authorization": "Bearer NOTFOUNDTOKEN"}
    )
    assert rv.status_code == 401, rv.json()


def test_token_expired(anonymous_client, db_session, users):
    user = users["random"]
    lt = LoginToken(
        user_id=user.id, token="SOMETOKEN", tfa_enabled=False, created_at=datetime.min
    )
    db_session.add(lt)
    db_session.commit()
    assert db_session.query(LoginToken).one()

    rv = anonymous_client.get(
        "/api/products/", headers={"Authorization": f"Bearer {lt.token}"}
    )
    assert rv.status_code == 401, rv.json()

    assert not db_session.query(LoginToken).all()


def test_not_admin(bar_client):
    rv = bar_client.get("/api/admin/products/")
    assert rv.status_code == 401, rv.json()
    assert rv.json()["error"] == "insufficient_permissions", rv.json()


def test_missing_tfa_user(
    admin_client: TestClient, admin_token: LoginToken, db_session: Session
) -> None:
    admin_token.tfa_enabled = False
    db_session.commit()
    rv = admin_client.get("/api/admin/products/")
    assert rv.status_code == 401, rv.json()
    assert rv.json()["error"] == "insufficient_permissions"


def test_no_account(room_client):
    rv = room_client.get("/api/account/")
    assert rv.status_code == 404, rv.json()
    assert rv.json()["error"] == "no_linked_account"
