import pytest
from sqlalchemy.orm import Session
from starlette.testclient import TestClient

from app.models import Product, Order, Transaction, User, Account
from app.models.checkout import CheckoutMethod
from app.models.checkout_card import CheckoutCard
from app.models.order import OrderType
from app.service.checkout_service import get_account_for_checkout_method
from app.views.api.checkout import MAX_CHECKOUT_AMOUNT


def test_get_account_for_checkout_method(
    db_session: Session, account_ids_checkout_method: dict[CheckoutMethod, int]
) -> None:
    for method, account_id in account_ids_checkout_method.items():
        account_id_for_checkout_method = get_account_for_checkout_method(
            db_session, method
        )
        assert account_id_for_checkout_method == account_id

    with pytest.raises(Exception):
        get_account_for_checkout_method(db_session, CheckoutMethod.ACCOUNT)


@pytest.mark.parametrize("method", ["cash", "card"])
@pytest.mark.parametrize("product", ["room_cola", "room_beer"])
def test_checkout_room_purchase_direct_single_product(
    db_session, room_client, products, method, product
):
    product: Product = products[product]

    r1 = room_client.post(
        f"/api/checkout/{method}/",
        json={"nonce": "123", "products": [{"id": product.id, "amount": 1}]},
    )

    assert r1.status_code == 200, r1.json()

    order = (
        db_session.query(Order)
        .filter(Order.account_id == product.ledger_id)
        .one_or_none()
    )

    assert order.product_id == product.id
    assert order.price == -product.price


def test_checkout_other(db_session, room_client, products):
    product: Product = products["room_cola"]

    r1 = room_client.post(
        "/api/checkout/other/",
        json={
            "nonce": "123",
            "products": [{"id": product.id, "amount": 1}],
            "reason": "Some reason",
        },
    )

    assert r1.status_code == 200, r1.json()

    order = (
        db_session.query(Order)
        .filter(Order.account_id == product.ledger_id)
        .one_or_none()
    )

    assert order.product_id == product.id
    assert order.price == -product.price

    transaction = db_session.query(Transaction).one()
    assert transaction.reason == "Some reason"


@pytest.mark.parametrize("method", ["cash", "card"])
def test_checkout_room_purchase_direct_multiple_products(
    db_session, room_client, products, method
):
    r1 = room_client.post(
        f"/api/checkout/{method}/",
        json={
            "nonce": "123",
            "products": [
                {"id": products["room_cola"].id, "amount": 1},
                {"id": products["room_beer"].id, "amount": 2},
            ],
        },
    )

    assert r1.status_code == 200, r1.json()

    order = db_session.query(Order).filter(Order.product_id.isnot(None)).all()

    assert any(o.product_id == products["room_cola"].id for o in order)
    assert any(o.product_id == products["room_beer"].id for o in order)
    assert any(o.price == -products["room_cola"].price for o in order)
    assert any(o.price == -products["room_beer"].price for o in order)

    assert len(order) == 3


@pytest.mark.parametrize("product", ["room_cola", "room_beer"])
def test_checkout_room_purchase_account_single_product(
    db_session, room_client, products, normal_account, product
):
    normal_account.balance = 1000
    db_session.add(normal_account)
    db_session.commit()
    product: Product = products[product]

    r1 = room_client.post(
        "/api/checkout/account/",
        json={
            "nonce": "123",
            "account_id": normal_account.id,
            "pin": "1234",
            "products": [{"id": product.id, "amount": 1}],
        },
    )

    assert r1.status_code == 200, r1.json()

    order = (
        db_session.query(Order)
        .filter(Order.account_id == product.ledger_id)
        .one_or_none()
    )

    assert order.product_id == product.id
    assert order.price == -product.price

    order = (
        db_session.query(Order)
        .filter(Order.account_id == normal_account.id)
        .one_or_none()
    )

    assert order.price == product.price


def test_checkout_bar_purchase_account_single_product(
    db_session,
    bar_client,
    products,
    normal_account,
):
    normal_account.balance = 1000
    db_session.add(normal_account)
    db_session.commit()
    product = products["bar_beer"]

    r1 = bar_client.post(
        "/api/checkout/account/",
        json={
            "nonce": "123",
            "account_id": normal_account.id,
            "products": [{"id": product.id, "amount": 1}],
        },
    )

    assert r1.status_code == 200, r1.json()

    order = (
        db_session.query(Order)
        .filter(Order.account_id == product.ledger_id)
        .one_or_none()
    )

    assert order.product_id == product.id
    assert order.price == -product.price

    order = (
        db_session.query(Order)
        .filter(Order.account_id == normal_account.id)
        .one_or_none()
    )

    assert order.price == product.price
    assert order.type == OrderType.ACCOUNT_TOP_UP_OR_ACCOUNT_PAYMENT


@pytest.mark.parametrize("acc_idx", [0, 1])
def test_checkout_bar_direct_debit_and_event(
    db_session, bar_client, products, sepa_account, event_account, acc_idx
):
    product = products["bar_beer"]

    acc = [sepa_account, event_account][acc_idx]

    r1 = bar_client.post(
        "/api/checkout/account/",
        json={
            "nonce": "123",
            "account_id": acc.id,
            "products": [{"id": product.id, "amount": 1}],
        },
    )

    assert r1.status_code == 200, r1.json()

    order = (
        db_session.query(Order)
        .filter(Order.account_id == product.ledger_id)
        .one_or_none()
    )

    assert order.product_id == product.id
    assert order.price == -product.price
    assert order.type == OrderType.PRODUCT_PURCHASE

    order = db_session.query(Order).filter(Order.account_id == acc.id).one_or_none()

    assert order.price == product.price
    assert order.type == OrderType.ACCOUNT_TOP_UP_OR_ACCOUNT_PAYMENT
    db_session.refresh(acc)
    assert acc.balance == -product.price


@pytest.mark.parametrize("method", [CheckoutMethod.CASH, CheckoutMethod.CARD])
def test_checkout_bar_purchase_direct_single_product(
    db_session,
    bar_client,
    products,
    method,
    account_ids_checkout_method,
):
    product = products["bar_beer"]

    r1 = bar_client.post(
        f"/api/checkout/{method.value}/",
        json={"nonce": "123", "products": [{"id": product.id, "amount": 1}]},
    )

    assert r1.status_code == 200, r1.json()

    order = (
        db_session.query(Order)
        .filter(Order.account_id == product.ledger_id)
        .one_or_none()
    )

    assert order.product_id == product.id
    assert order.price == -product.price
    assert order.type == OrderType.PRODUCT_PURCHASE

    order = (
        db_session.query(Order)
        .filter(Order.account_id == account_ids_checkout_method[method])
        .one_or_none()
    )

    assert order.product_id is None
    assert order.price == product.price
    assert order.type == OrderType.DIRECT_PAYMENT


@pytest.mark.parametrize("method", ["cash", "card"])
def test_checkout_bar_account_upgrade(db_session, bar_client, normal_account, method):
    upgrade_amount = 500

    r1 = bar_client.post(
        f"/api/checkout/{method}/",
        json={
            "nonce": "123",
            "upgrade_account_id": normal_account.id,
            "upgrade_amount": upgrade_amount,
            "products": [],
        },
    )

    assert r1.status_code == 200, r1.json()

    order = (
        db_session.query(Order)
        .filter(Order.account_id == normal_account.id)
        .one_or_none()
    )

    assert order.price == -upgrade_amount
    assert order.type == OrderType.ACCOUNT_TOP_UP_OR_ACCOUNT_PAYMENT
    db_session.refresh(normal_account)
    assert normal_account.balance == upgrade_amount


@pytest.mark.parametrize("method", ["cash", "card"])
def test_checkout_bar_account_upgrade_and_purchase(
    db_session, bar_client, normal_account, products, method
):
    upgrade_amount = 500

    product = products["bar_beer"]

    r1 = bar_client.post(
        f"/api/checkout/{method}/",
        json={
            "nonce": "123",
            "upgrade_account_id": normal_account.id,
            "upgrade_amount": upgrade_amount,
            "products": [{"id": product.id, "amount": 1}],
        },
    )

    assert r1.status_code == 200, r1.json()

    upgrade_order = (
        db_session.query(Order)
        .filter(Order.account_id == normal_account.id)
        .one_or_none()
    )

    assert upgrade_order.price == -upgrade_amount
    assert upgrade_order.type == OrderType.ACCOUNT_TOP_UP_OR_ACCOUNT_PAYMENT

    db_session.refresh(normal_account)
    assert normal_account.balance == upgrade_amount

    product_order = (
        db_session.query(Order)
        .filter(Order.account_id == product.ledger_id)
        .one_or_none()
    )

    assert product_order.product_id == product.id
    assert product_order.price == -product.price
    assert product_order.type == OrderType.PRODUCT_PURCHASE


def test_checkout_self_single_product(
    db_session, normal_client, normal_account, products
):
    acc_id = normal_account.id
    normal_account.balance = 1000
    db_session.add(normal_account)
    db_session.commit()
    product = products["room_beer"]

    r1 = normal_client.post(
        "/api/checkout/account/self/",
        json={
            "nonce": "123",
            "account_id": None,
            "products": [{"id": product.id, "amount": 1}],
        },
    )

    assert r1.status_code == 200, r1.json()

    product_order = (
        db_session.query(Order)
        .filter(Order.account_id == product.ledger_id)
        .one_or_none()
    )

    assert product_order.product_id == product.id
    assert product_order.price == -product.price

    account_order = (
        db_session.query(Order).filter(Order.account_id == acc_id).one_or_none()
    )

    assert account_order.price == product.price


def test_checkout_admin_other_account(
    db_session, admin_client, admin_user, registers, sepa_account, products
):
    if admin_user.register_id == registers["admin_self"].id:
        pytest.skip("Admin self does not checkout on other accounts")

    product = products["room_beer"]
    assert sepa_account.balance == 0
    assert product.price != 0

    r1 = admin_client.post(
        "/api/checkout/account/",
        json={
            "nonce": "123",
            "account_id": sepa_account.id,
            "products": [{"id": product.id, "amount": 1}],
        },
    )
    assert r1.status_code == 200, r1.json()
    db_session.refresh(sepa_account)
    assert sepa_account.balance != 0


@pytest.mark.usefixtures("card_reader_secret")
def test_checkout_card_reader(
    db_session: Session,
    room_client: TestClient,
    sepa_account: Account,
    products: dict[str, Product],
) -> None:
    assert sepa_account.user

    db_session.add(CheckoutCard(user_id=sepa_account.user.id, card_secret="1234567890"))
    db_session.commit()

    r1 = room_client.post(
        "/api/checkout/account/reader/",
        json={
            "nonce": "123",
            "cardSecret": "$abcde1234567890",
            "products": [{"id": products["room_cola"].id, "amount": 1}],
        },
    )
    assert r1.status_code == 200, r1.json()
    db_session.refresh(sepa_account)
    assert sepa_account.balance != 0


@pytest.mark.usefixtures("card_reader_secret")
def test_checkout_card_reader_no_account(
    db_session: Session,
    room_client: TestClient,
    users: dict[str, User],
    products: dict[str, Product],
) -> None:
    db_session.add(CheckoutCard(user_id=users["random2"].id, card_secret="1234567890"))
    db_session.commit()

    assert users["random2"].account_id is None

    r1 = room_client.post(
        "/api/checkout/account/reader/",
        json={
            "nonce": "123",
            "cardSecret": "$abcde1234567890",
            "products": [{"id": products["room_cola"].id, "amount": 1}],
        },
    )
    assert r1.status_code == 404, r1.json()


@pytest.mark.usefixtures("card_reader_secret")
def test_checkout_card_reader_unknown_secret(
    db_session: Session,
    room_client: TestClient,
    sepa_account: Account,
    products: dict[str, Product],
) -> None:
    r1 = room_client.post(
        "/api/checkout/account/reader/",
        json={
            "nonce": "123",
            "cardSecret": "$abcde1234567890",
            "products": [{"id": products["room_cola"].id, "amount": 1}],
        },
    )
    assert r1.status_code == 404, r1.json()
    assert r1.json()["error"] == "unknown_card_secret"
    db_session.refresh(sepa_account)
    assert sepa_account.balance == 0


def test_error_checkout_self_deleted_account(
    db_session, normal_account, normal_client, products
):
    normal_account.balance = 1000
    normal_account.deleted = True
    db_session.add(normal_account)
    db_session.commit()

    r1 = normal_client.post(
        "/api/checkout/account/self/",
        json={
            "nonce": "123",
            "account_id": None,
            "products": [{"id": products["room_beer"].id, "amount": 1}],
        },
    )
    assert r1.status_code == 404, r1.json()
    assert r1.json()["error"] == "invalid_account"


@pytest.mark.parametrize("amount", [-1, MAX_CHECKOUT_AMOUNT + 10])
@pytest.mark.parametrize("method", ["account", "cash"])
def test_error_invalid_amount(room_client, amount, method, normal_account):
    r1 = room_client.post(
        f"/api/checkout/{method}/",
        json={
            "nonce": "123",
            "pin": "1234",
            "account_id": normal_account.id,
            "products": [{"id": 1, "amount": amount}],
        },
    )

    if amount == -1:
        assert r1.status_code == 422, r1.json()
        assert r1.json()["detail"] == [
            {
                "ctx": {"limit_value": 0},
                "loc": ["body", "products", 0, "amount"],
                "msg": "ensure this value is greater than 0",
                "type": "value_error.number.not_gt",
            }
        ]
    else:
        assert r1.status_code == 400, r1.json()
        assert r1.json()["error"] == "invalid_product_in_cart"


def test_error_cant_access_register(normal_client, products):
    bar_beer = products["bar_beer"]
    r1 = normal_client.post(
        "/api/checkout/account/",
        json={
            "nonce": "123",
            "account_id": None,
            "products": [{"id": bar_beer.id, "amount": 1}],
        },
    )

    assert r1.status_code == 401, r1.json()


def test_error_nonce(db_session, normal_client, normal_account, products):
    normal_account.balance = 10000
    db_session.add(normal_account)
    db_session.commit()

    bottle_beer = products["room_beer"]
    r1 = normal_client.post(
        "/api/checkout/account/self/",
        json={
            "account_id": None,
            "products": [{"id": bottle_beer.id, "amount": 1}],
            "nonce": "123",
        },
    )
    assert r1.status_code == 200, r1.json()

    r2 = normal_client.post(
        "/api/checkout/account/self/",
        json={
            "account_id": None,
            "products": [{"id": bottle_beer.id, "amount": 1}],
            "nonce": "123",
        },
    )

    assert r2.status_code == 409, r2.json()


def test_error_invalid_pin_code(db_session, room_client, normal_account, products):
    normal_account.balance = 1000
    db_session.add(normal_account)
    db_session.commit()
    bottle_beer = products["room_beer"]

    r1 = room_client.post(
        "/api/checkout/account/",
        json={
            "nonce": "123",
            "account_id": normal_account.id,
            "pin": "6969",
            "products": [{"id": bottle_beer.id, "amount": 1}],
        },
    )

    assert r1.status_code == 422, r1.json()


def test_error_no_balance(room_client, normal_account, products):
    bottle_beer = products["room_beer"]

    r1 = room_client.post(
        "/api/checkout/account/",
        json={
            "nonce": "123",
            "account_id": normal_account.id,
            "pin": "1234",
            "products": [{"id": bottle_beer.id, "amount": 1}],
        },
    )

    assert r1.status_code == 406, r1.json()


def test_error_no_nonce_account(room_client, normal_account, products):
    bottle_beer = products["room_beer"]

    rv = room_client.post(
        "/api/checkout/account/",
        json={
            "account_id": normal_account.id,
            "pin": "1234",
            "products": [{"id": bottle_beer.id, "amount": 1}],
        },
    )

    assert rv.status_code == 422, rv.json()
    assert rv.json()["detail"] == [
        {
            "loc": ["body", "nonce"],
            "msg": "field required",
            "type": "value_error.missing",
        }
    ], rv.json()


@pytest.mark.parametrize("method", ["cash", "card", "other"])
def test_error_no_nonce_method(admin_client, products, method):
    bottle_beer = products["room_beer"]
    rv = admin_client.post(
        f"/api/checkout/{method}/",
        json={
            "products": [{"id": bottle_beer.id, "amount": 1}],
        },
    )
    assert rv.status_code == 422, rv.json()
    assert rv.json()["detail"] == [
        {
            "loc": ["body", "nonce"],
            "msg": "field required",
            "type": "value_error.missing",
        }
    ]


@pytest.mark.parametrize(
    "method",
    # cash, card, other are support methods, account has seperate route.
    {m.value for m in CheckoutMethod}.difference({"cash", "card", "other", "account"}),
)
def test_error_invalid_checkout_method(room_client, products, method):
    bottle_beer = products["room_beer"]
    rv = room_client.post(
        f"/api/checkout/{method}/",
        json={
            "nonce": "1234",
            "products": [{"id": bottle_beer.id, "amount": 1}],
        },
    )

    assert rv.status_code == 404, rv.json()


def test_error_invalid_account_upgrade(bar_client, normal_account):
    rv = bar_client.post(
        "/api/checkout/cash/",
        json={
            "nonce": "123",
            "upgrade_account_id": normal_account.id,
            "products": [],
        },
    )

    assert rv.status_code == 400, rv.json()
    assert rv.json()["error"] == "no_upgrade_amount_give"

    rv = bar_client.post(
        "/api/checkout/cash/",
        json={
            "nonce": "123",
            "products": [],
            "upgrade_account_id": normal_account.id,
            "upgrade_amount": "NaN",
        },
    )

    assert rv.status_code == 422, rv.json()
    assert rv.json()["detail"] == [
        {
            "loc": ["body", "upgrade_amount"],
            "msg": "value is not a valid integer",
            "type": "type_error.integer",
        }
    ]

    rv = bar_client.post(
        "/api/checkout/cash/",
        json={
            "nonce": "123",
            "products": [],
            "upgrade_account_id": normal_account.id,
            "upgrade_amount": -100,
        },
    )

    assert rv.status_code == 400, rv.json()
    assert rv.json()["error"] == "reason_expected"


def test_error_products_not_list(room_client):
    rv = room_client.post(
        "/api/checkout/cash/",
        json={"nonce": "123", "products": "asdf"},
    )
    assert rv.status_code == 422, rv.json()


def test_error_products_product_deleted(room_client, products, db_session):
    products["room_cola"].deleted = True
    db_session.commit()

    rv = room_client.post(
        "/api/checkout/cash/",
        json={
            "nonce": "123",
            "products": [{"id": products["room_cola"].id, "amount": 1}],
        },
    )
    assert rv.status_code == 404, rv.json()


def test_error_products_product_without_id(room_client):
    rv = room_client.post(
        "/api/checkout/cash/",
        json={
            "nonce": "123",
            "products": [{"id": None, "amount": 1}],
        },
    )
    assert rv.status_code == 422, rv.json()
    assert rv.json()["detail"] == [
        {
            "loc": ["body", "products", 0, "id"],
            "msg": "none is not an allowed value",
            "type": "type_error.none.not_allowed",
        }
    ], rv.json()


def test_error_products_product_without_amount(room_client, products):
    rv = room_client.post(
        "/api/checkout/cash/",
        json={
            "nonce": "123",
            "products": [{"id": products["room_cola"].id, "amount": None}],
        },
    )
    assert rv.status_code == 422, rv.json()
    assert rv.json()["detail"] == [
        {
            "loc": ["body", "products", 0, "amount"],
            "msg": "none is not an allowed value",
            "type": "type_error.none.not_allowed",
        }
    ]


def test_error_account_need_account_id(room_client, products):
    rv = room_client.post(
        "/api/checkout/account/",
        json={
            "nonce": "123",
            "products": [{"id": products["room_cola"].id, "amount": 1}],
        },
    )
    assert rv.status_code == 400, rv.json()
    assert rv.json()["error"] == "account_id_missing", rv.json()


def test_error_account_account_id_not_int(room_client, products):
    rv = room_client.post(
        "/api/checkout/account/",
        json={
            "nonce": "123",
            "pin": "1231",
            "account_id": "NaN",
            "products": [{"id": products["room_cola"].id, "amount": 1}],
        },
    )
    assert rv.status_code == 422, rv.json()
    assert rv.json()["detail"] == [
        {
            "loc": ["body", "account_id"],
            "msg": "value is not a valid integer",
            "type": "type_error.integer",
        }
    ], rv.json()


def test_error_account_need_pin(room_client, normal_account, products):
    rv = room_client.post(
        "/api/checkout/account/",
        json={
            "nonce": "123",
            "account_id": normal_account.id,
            "products": [{"id": products["room_cola"].id, "amount": 1}],
        },
    )
    assert rv.status_code == 400, rv.json()
    assert rv.json()["error"] == "pin_missing", rv.json()


def test_error_account_no_products(room_client, normal_account):
    rv = room_client.post(
        "/api/checkout/account/",
        json={
            "nonce": "123",
            "account_id": normal_account.id,
            "pin": "1234",
            "products": [],
        },
    )
    assert rv.status_code == 422, rv.json()
    assert rv.json()["detail"] == [
        {
            "ctx": {"limit_value": 1},
            "loc": ["body", "products"],
            "msg": "ensure this value has at least 1 items",
            "type": "value_error.list.min_items",
        }
    ], rv.json()
