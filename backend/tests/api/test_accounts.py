from starlette.testclient import TestClient

from app.models import Account


def test_events_present_in_bar(
    bar_client: TestClient,
    event_account: Account,
    sepa_account: Account,
    normal_account: Account,
) -> None:
    rv = bar_client.get("/api/accounts/")
    assert rv.status_code == 200

    assert len(rv.json()["accounts"]) == 3


def test_event_without_pin_not_present_in_room(
    room_client: TestClient,
    event_account: Account,
    sepa_account: Account,
    normal_account: Account,
) -> None:
    rv = room_client.get("/api/accounts/")
    assert rv.status_code == 200

    assert len(rv.json()["accounts"]) == 2


def test_event_with_pin_not_present_in_room(
    room_client: TestClient,
    event_account_with_pin: Account,
    sepa_account: Account,
    normal_account: Account,
) -> None:
    rv = room_client.get("/api/accounts/")
    assert rv.status_code == 200

    assert len(rv.json()["accounts"]) == 3
