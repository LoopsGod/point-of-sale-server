from datetime import datetime, timezone, timedelta
from xml.etree import ElementTree

import pytest
from sqlalchemy import select
from sqlalchemy.orm import Session
from starlette.testclient import TestClient

from app.models import SepaBatch, Transaction, SepaBatchOrder, Account, Setting, User
from app.util.util import format_iban


@pytest.mark.usefixtures("sepa_config")
def test_execute_sepa_batch(
    db_session: Session,
    admin_client: TestClient,
    sepa_account: Account,
) -> None:
    sepa_account.balance = -1000
    db_session.commit()

    rv = admin_client.post(
        "/api/admin/sepa_batches/",
        json={
            "accounts": [sepa_account.id],
            "executionDate": (
                datetime.now(timezone.utc) + timedelta(days=-1)
            ).isoformat(),
            "description": "asdf",
        },
    )
    assert rv.status_code == 201, rv.json()
    batch_id = rv.json()["batch_id"]

    rv = admin_client.get(f"/api/admin/sepa_batches/{batch_id}/")
    assert rv.status_code == 200, rv.json()
    assert rv.json()["batch"]["success"] is False, rv.json()

    sepa_batch_order = db_session.query(SepaBatchOrder).one()
    assert sepa_batch_order.price == 1000
    assert sepa_batch_order.sepa_batch_id == batch_id
    assert sepa_batch_order.account_id == sepa_account.id

    rv = admin_client.post(f"/api/admin/sepa_batches/{batch_id}/execute/")
    assert rv.status_code == 204, rv.json()

    batch: SepaBatch | None = db_session.get(SepaBatch, batch_id)
    assert batch is not None

    transaction: Transaction | None = db_session.scalar(
        select(Transaction).filter(
            Transaction.nonce == f"sepa-batch-{batch_id}-account-{sepa_account.id}"
        )
    )
    assert transaction is not None
    assert batch.success is True
    assert len(transaction.orders) == 2
    assert all(o.sepa_batch_id == batch_id for o in transaction.orders)

    db_session.refresh(sepa_account)
    assert sepa_account.balance == 0

    rv = admin_client.get("/api/admin/transactions/", params={"sepaBatchId": batch.id})
    assert rv.status_code == 200, rv.json()
    assert len(rv.json()["items"]) == 1, rv.json()


@pytest.mark.usefixtures("sepa_config")
def test_pending_sepa_account(
    db_session: Session,
    sepa_client: TestClient,
    admin_client: TestClient,
    sepa_user: User,
) -> None:
    sepa_account = db_session.get(Account, sepa_user.account_id)
    assert sepa_account is not None
    sepa_account.balance = -1000
    db_session.commit()

    rv = admin_client.post(
        "/api/admin/sepa_batches/",
        json={
            "accounts": [sepa_account.id],
            "executionDate": (
                datetime.now(timezone.utc) + timedelta(days=-1)
            ).isoformat(),
            "description": "asdf",
        },
    )
    assert rv.status_code == 201, rv.json()

    rv = sepa_client.get("/api/account/")
    assert rv.status_code == 200, rv.json()
    assert rv.json()["sepa"]["balance"] == 1000


@pytest.mark.usefixtures("sepa_config")
def test_sepa_csv_lines(db_session, admin_client, sepa_account):
    sepa_account.balance = -100
    db_session.add(sepa_account)
    db_session.commit()

    rv = admin_client.post(
        "/api/admin/sepa_batches/",
        json={
            "accounts": [sepa_account.id],
            "executionDate": (
                datetime.now(timezone.utc) + timedelta(days=-1)
            ).isoformat(),
            "description": "asdf",
        },
    )
    assert rv.status_code == 201, rv.json()

    batch = db_session.query(SepaBatch).one_or_none()

    expected_iban = sepa_account.user.direct_debit.iban

    rv = admin_client.get(f"/api/admin/sepa_batches/directdebit/{batch.id}.csv")
    assert rv.status_code == 200, rv.json()
    data = rv.content.decode()
    assert len(data.splitlines()) == 2
    assert format_iban(expected_iban) in data
    assert "1.00" in data

    rv = admin_client.get(f"/api/admin/sepa_batches/directdebit/{batch.id}.xml")
    assert rv.status_code == 200, rv.json()
    root = ElementTree.fromstring(rv.content)
    ns = "{urn:iso:std:iso:20022:tech:xsd:pain.008.001.02}"
    transactions = root.findall(f".//{ns}DrctDbtTxInf")
    assert len(transactions) == 1
    assert transactions[0].find(f"./{ns}InstdAmt").text == "1.00"
    assert transactions[0].find(f".//{ns}IBAN").text == expected_iban


@pytest.mark.usefixtures("sepa_config")
def test_delete_sepa_unexecuted(db_session, admin_client, sepa_account):
    sepa_account.balance = -100
    db_session.add(sepa_account)
    db_session.commit()

    rv = admin_client.post(
        "/api/admin/sepa_batches/",
        json={
            "accounts": [sepa_account.id],
            "executionDate": (
                datetime.now(timezone.utc) + timedelta(days=-1)
            ).isoformat(),
            "description": "asdf",
        },
    )
    assert rv.status_code == 201, rv.json()

    batch = db_session.query(SepaBatch).one()

    rv = admin_client.delete(f"/api/admin/sepa_batches/{batch.id}/")
    assert rv.status_code == 204, rv.json()

    db_session.refresh(batch)
    db_session.refresh(sepa_account)
    assert batch.deleted
    assert not db_session.query(Transaction).all()
    assert sepa_account.balance == -100

    # Duplicate delete
    rv = admin_client.delete(f"/api/admin/sepa_batches/{batch.id}/")
    assert rv.status_code == 400, rv.json()


@pytest.mark.usefixtures("sepa_config")
def test_delete_sepa_executed(db_session, admin_client, sepa_account):
    sepa_account.balance = -100
    db_session.add(sepa_account)
    db_session.commit()

    rv = admin_client.post(
        "/api/admin/sepa_batches/",
        json={
            "accounts": [sepa_account.id],
            "executionDate": (
                datetime.now(timezone.utc) + timedelta(days=-1)
            ).isoformat(),
            "description": "asdf",
        },
    )
    assert rv.status_code == 201, rv.json()

    batch = db_session.query(SepaBatch).one_or_none()
    rv = admin_client.post(f"/api/admin/sepa_batches/{batch.id}/execute/")
    assert rv.status_code == 204, rv.json()

    db_session.refresh(sepa_account)
    assert sepa_account.balance == 0

    rv = admin_client.delete(f"/api/admin/sepa_batches/{batch.id}/")
    assert rv.status_code == 400, rv.json()


@pytest.mark.usefixtures("sepa_config")
def test_execute_sepa_batch_with_deleted_pending_order(
    db_session, admin_client, sepa_account
):
    sepa_account.balance = -100
    db_session.add(sepa_account)
    db_session.commit()

    rv = admin_client.post(
        "/api/admin/sepa_batches/",
        json={
            "accounts": [sepa_account.id],
            "executionDate": (
                datetime.now(timezone.utc) + timedelta(days=-1)
            ).isoformat(),
            "description": "asdf",
        },
    )
    assert rv.status_code == 201, rv.json()
    batch_id = rv.json()["batch_id"]

    rv = admin_client.delete(
        f"/api/admin/sepa_batches/{batch_id}/accounts/{sepa_account.id}/"
    )
    assert rv.status_code == 204, rv.json()

    rv = admin_client.post(f"/api/admin/sepa_batches/{batch_id}/execute/")
    assert rv.status_code == 204, rv.json()

    db_session.refresh(sepa_account)
    assert sepa_account.balance == -100

    rv = admin_client.delete(
        f"/api/admin/sepa_batches/{batch_id}/accounts/{sepa_account.id}/"
    )
    assert rv.status_code == 400, rv.json()


@pytest.mark.usefixtures("sepa_config")
def test_email_sepa_batch_with_deleted_pending_order(
    db_session, admin_client, sepa_account, users
):
    sepa_account.balance = -100
    db_session.add(sepa_account)
    db_session.commit()

    rv = admin_client.post(
        "/api/admin/sepa_batches/",
        json={
            "accounts": [sepa_account.id],
            "executionDate": (
                datetime.now(timezone.utc) + timedelta(days=-1)
            ).isoformat(),
            "description": "asdf",
        },
    )
    assert rv.status_code == 201, rv.json()
    batch_id = rv.json()["batch_id"]

    rv = admin_client.post(f"/api/admin/sepa_batches/{batch_id}/email/")
    assert rv.status_code == 202, rv.json()
    assert rv.json()["mailCount"] == 1

    rv = admin_client.delete(
        f"/api/admin/sepa_batches/{batch_id}/accounts/{sepa_account.id}/"
    )
    assert rv.status_code == 204, rv.json()

    rv = admin_client.post(f"/api/admin/sepa_batches/{batch_id}/email/")
    assert rv.status_code == 202, rv.json()
    assert rv.json()["mailCount"] == 0, "Deleted orders should not be mailed"


@pytest.mark.usefixtures("sepa_config")
def test_send_sepa_email_twice(db_session, admin_client, sepa_account, users):
    sepa_account.balance = -100
    db_session.add(sepa_account)
    db_session.commit()

    rv = admin_client.post(
        "/api/admin/sepa_batches/",
        json={
            "accounts": [sepa_account.id],
            "executionDate": (
                datetime.now(timezone.utc) + timedelta(days=-1)
            ).isoformat(),
            "description": "asdf",
        },
    )
    assert rv.status_code == 201, rv.json()
    batch_id = rv.json()["batch_id"]
    batch = db_session.get(SepaBatch, batch_id)
    # The mail date is set in the worker, which we can not test currently.
    batch.mail_date = datetime.now(timezone.utc)
    db_session.commit()

    rv = admin_client.post(f"/api/admin/sepa_batches/{batch_id}/email/")
    assert rv.status_code == 400, rv.json()


@pytest.mark.usefixtures("sepa_config")
def test_get_sepa_configs(admin_client):
    rv = admin_client.get("/api/admin/direct_debit/config/")
    assert rv.status_code == 200, rv.json()


@pytest.mark.usefixtures("sepa_batch")
def test_api_admin_sepa_batches(admin_client):
    rv = admin_client.get("/api/admin/sepa_batches/")
    assert rv.status_code == 200, rv.json()


@pytest.mark.usefixtures("sepa_config")
def test_api_admin_sepa_batch_put(sepa_batch, admin_client):
    rv = admin_client.get(f"/api/admin/sepa_batches/{sepa_batch.id}/")
    assert rv.status_code == 200, rv.json()

    rv = admin_client.put(
        f"/api/admin/sepa_batches/{sepa_batch.id}/",
        json={
            "executionDate": (
                datetime.now(timezone.utc) + timedelta(days=-1)
            ).isoformat(),
            "description": "asdf",
        },
    )
    assert rv.status_code == 200, rv.json()


def test_api_admin_sepa_config_put(admin_client, db_session):
    rv = admin_client.put(
        "/api/admin/direct_debit/config/",
        json={
            "creditorBic": "INGBNL2A",
            "creditorIdentifier": "NL00ZZZ000000000000",
            "creditorIban": "NL00INGB0000000000",
            "creditorName": "Vereniging Informatiewetenschappen Amsterdam",
        },
    )
    assert rv.status_code == 204

    creditor_setting = (
        db_session.query(Setting)
        .filter(Setting.key == "DIRECT_DEBIT_CREDITOR_IDENTIFIER")
        .one()
    )

    assert creditor_setting.value == "NL00ZZZ000000000000"
