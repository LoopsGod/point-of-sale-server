import pytest
from sqlalchemy.orm import Session
from starlette.testclient import TestClient

from app.models import Order, Product
from app.models.account import Account
from app.models.checkout import CheckoutMethod
from tests.util.transactions import _create_account_purchase


@pytest.mark.usefixtures("event_account", "normal_account")
def test_api_admin_sepa_accounts(
    admin_client: TestClient, sepa_account: Account, db_session: Session
) -> None:
    sepa_account.balance -= 100
    db_session.commit()
    rv = admin_client.get("/api/admin/accounts/sepa/")
    assert rv.status_code == 200, rv.json()
    assert len(rv.json()["accounts"]) == 1, rv.json()


def test_api_admin_accounts_last_used(
    admin_client: TestClient,
    normal_account: Account,
    db_session: Session,
    products: dict[str, Product],
) -> None:
    normal_account.balance = 10000
    db_session.add(normal_account)
    db_session.flush()

    rv = admin_client.get("/api/admin/accounts/")
    assert rv.status_code == 200, rv.json()

    # Normal, Bar and Room
    # Also every CheckoutMethod, minus Account
    assert len(rv.json()["items"]) == 3 + 6, rv.json()

    rv = admin_client.get("/api/admin/accounts/", params={"lastUsageFilter": "<=10"})
    assert rv.status_code == 200, rv.json()
    assert len(rv.json()["items"]) == 0

    _create_account_purchase(db_session, normal_account, list(products.values()))

    rv = admin_client.get("/api/admin/accounts/", params={"lastUsageFilter": "<=10"})
    assert rv.status_code == 200, rv.json()
    # TODO Should the ledger account be updated when being used?
    assert len(rv.json()["items"]) == 1


@pytest.mark.usefixtures("normal_account", "sepa_account", "normal_account")
@pytest.mark.parametrize(
    "query_string,check_field,check_value",
    [
        [{"typeFilter": '"user"'}, "type", "user"],
        [{"typeFilter": '"income_ledger"'}, "type", "income_ledger"],
        [{"typeFilter": '"product_ledger"'}, "type", "product_ledger"],
        [{"typeFilter": '"event"'}, "type", "event"],
        [
            {"allowNegativeFilter": True, "sort": "-allowNegative"},
            "allowNegativeBalance",
            None,
        ],
        [{"deletedFilter": True, "sort": "deleted"}, "deleted", None],
        [{"sort": "name,balance"}, "id", None],
        [{"sort": "name,type"}, "id", None],
    ],
)
def test_api_list_accounts(
    admin_client: TestClient,
    query_string: str,
    check_field: str,
    check_value: str | None,
) -> None:
    rv = admin_client.get("/api/admin/accounts/", params=query_string)
    assert rv.status_code == 200, rv.json()
    assert all(
        [
            (a[check_field] == check_value)
            if check_value is not None
            else a[check_field]
            for a in rv.json()["items"]
        ]
    ), rv.json()


@pytest.mark.usefixtures("exact_income_ledgers")
def test_api_admin_account_income_ledgers(admin_client: TestClient) -> None:
    rv = admin_client.get('/api/admin/accounts/ledgers/?typeFilter="income_ledger"')
    assert rv.status_code == 200, rv.json()
    # All checkout methods, minus account
    assert len(rv.json()["items"]) == 6, rv.json()
    assert all([a["incomeLedger"] for a in rv.json()["items"]]), rv.json()

    for method in CheckoutMethod:
        if method == CheckoutMethod.ACCOUNT:
            continue
        assert any(
            [method.name in item["name"].upper() for item in rv.json()["items"]]
        ), rv.json()

    cash_ledger = next(
        item for item in rv.json()["items"] if CheckoutMethod.CASH.value in item["name"]
    )

    rv = admin_client.put(
        f"/api/admin/accounts/ledgers/{cash_ledger['id']}/",
        json={
            "ledgerCode": "1",
            "costCenterCode": "2",
            "vatCode": "3",
        },
    )

    # Request should fail, since it's an income ledger
    assert rv.status_code == 400, rv.json()

    rv = admin_client.put(
        f"/api/admin/accounts/ledgers/{cash_ledger['id']}/",
        json={
            "ledgerCode": "1",
        },
    )

    assert rv.status_code == 200, rv.json()

    rv = admin_client.get("/api/admin/accounts/ledgers/?incomeLedgerFilter=true")
    assert rv.status_code == 200, rv.json()
    assert {
        "id": cash_ledger["id"],
        "name": cash_ledger["name"],
        "incomeLedger": True,
        "ledgerCode": "1",
        "costCenterCode": None,
        "vatCode": None,
        "associatedCheckoutMethod": "cash",
    } in rv.json()["items"]

    assert all([item["incomeLedger"] for item in rv.json()["items"]])


def test_api_admin_account_ledgers(
    admin_client: TestClient, ledger_accounts: dict[str, Account]
) -> None:
    rv = admin_client.get("/api/admin/accounts/ledgers/")

    assert rv.status_code == 200, rv.json()
    assert {
        "id": ledger_accounts["room"].id,
        "name": "Room sale",
        "incomeLedger": False,
        "ledgerCode": None,
        "costCenterCode": None,
        "vatCode": None,
        "associatedCheckoutMethod": None,
    } in rv.json()["items"]

    rv = admin_client.put(
        f"/api/admin/accounts/ledgers/{ledger_accounts['room'].id}/",
        json={
            "ledgerCode": "1",
        },
    )
    assert rv.status_code == 400, rv.json()

    rv = admin_client.put(
        f"/api/admin/accounts/ledgers/{ledger_accounts['room'].id}/",
        json={
            "ledgerCode": "1",
            "costCenterCode": "2",
            "vatCode": "3",
        },
    )
    assert rv.status_code == 200, rv.json()

    rv = admin_client.get("/api/admin/accounts/ledgers/")
    assert rv.status_code == 200, rv.json()
    assert {
        "id": ledger_accounts["room"].id,
        "name": "Room sale",
        "incomeLedger": False,
        "ledgerCode": "1",
        "costCenterCode": "2",
        "vatCode": "3",
        "associatedCheckoutMethod": None,
    } in rv.json()["items"]


@pytest.mark.usefixtures("transaction_account_order")
def test_api_account_balances_now(
    admin_client: TestClient,
) -> None:
    rv = admin_client.get("/api/admin/accounts/balance/")
    assert rv.status_code == 200, rv.json()
    assert rv.json()["balanceAt"]["totalBalance"] == -120


@pytest.mark.usefixtures("transaction_account_order")
def test_api_account_balances_long_ago(
    admin_client: TestClient,
) -> None:
    rv = admin_client.get("/api/admin/accounts/balance/?date=2014-04-24")
    assert rv.status_code == 200, rv.json()
    assert rv.json()["balanceAt"]["totalBalance"] == 0


@pytest.mark.parametrize(
    "request_json",
    [
        {"type": "event", "pin": "1234"},
        {"type": "product_ledger"},
        {
            "allowNegativeBalance": True,
            "pin": "1234",
            "type": "event",
        },
        {"pin": "1234"},
    ],
    ids=["Event account", "Ledger account", "Credit account", "Regular account"],
)
def test_api_admin_accounts_post(admin_client, request_json):
    rv = admin_client.post(
        "/api/admin/accounts/",
        json=dict(
            name="Maico Timmerman", description="Maico's account", **request_json
        ),
    )
    assert rv.status_code == 201, rv.json()


def test_api_admin_accounts_event_without_pin_then_update(admin_client):
    rv = admin_client.post(
        "/api/admin/accounts/",
        json=dict(
            name="Maico Timmerman", description="Maico's account", eventAccount=True
        ),
    )
    resp = rv.json()
    assert rv.status_code == 201, resp

    rv = admin_client.patch(
        f"/api/admin/accounts/{resp['id']}",
        json=dict(pin="1234"),
    )
    assert rv.status_code == 200, rv.json()


@pytest.mark.parametrize(
    "request_json",
    [
        {"eventAccount": True, "pin": "1234"},
        {"ledgerAccount": True},
        {
            "allowNegativeBalance": True,
            "pin": "1234",
            "eventAccount": True,
        },
        {"pin": "1234"},
        {},
    ],
    ids=[
        "Event-Account",
        "Ledger-Account",
        "Credit-event-Account",
        "RegularAccount-with-pin",
        "RegularAccount-without-pin",
    ],
)
def test_api_admin_accounts_put(admin_client, normal_account, request_json):
    rv = admin_client.put(
        f"/api/admin/accounts/{normal_account.id}/",
        json=dict(
            name=normal_account.name, description="Maico's account", **request_json
        ),
    )
    assert rv.status_code == 200, rv.json()


def test_set_allow_negative_balance_to_true_when_direct_debit_user(
    admin_client, sepa_account
):
    rv = admin_client.patch(
        f"/api/admin/accounts/{sepa_account.id}/", json={"allowNegativeBalance": True}
    )

    assert rv.status_code == 200, rv.json()


def test_dont_allow_negative_for_normal_account(admin_client, normal_account):
    rv = admin_client.patch(
        f"/api/admin/accounts/{normal_account.id}/", json={"allowNegativeBalance": True}
    )

    assert rv.status_code == 400, rv.json()


def test_allow_event_account_negative_balance(
    admin_client: TestClient, event_account_with_pin: Account
) -> None:
    rv = admin_client.patch(
        f"/api/admin/accounts/{event_account_with_pin.id}/",
        json={"allowNegativeBalance": True},
    )

    assert rv.status_code == 200, rv.json()


@pytest.mark.parametrize("method", ["cash", "card", "other"])
@pytest.mark.parametrize("upgrade_amount", [1000, -1000])
def test_checkout_admin_upgrade(
    db_session: Session,
    admin_client: TestClient,
    normal_account: Account,
    method: str,
    upgrade_amount: int,
) -> None:
    acc = db_session.get(Account, normal_account.id)
    assert acc is not None and acc.balance == 0

    r1 = admin_client.post(
        f"/api/admin/accounts/{normal_account.id}/upgrade/",
        json={
            "method": method,
            "nonce": "123",
            "amount": upgrade_amount,
            "reason": "Some reason",
        },
    )

    assert r1.status_code == 200, r1.json()

    rv2 = admin_client.get(f"/api/admin/accounts/{normal_account.id}/")
    assert rv2.status_code == 200, rv2.json()
    assert rv2.json()["balance"] == upgrade_amount

    order = db_session.query(Order).filter(Order.account_id == normal_account.id).one()
    assert order.price == -upgrade_amount

    db_session.refresh(normal_account)
    assert acc.balance == upgrade_amount


def test_error_checkout_admin_upgrade_other_no_reason(admin_client, normal_account):
    r1 = admin_client.post(
        f"/api/admin/accounts/{normal_account.id}/upgrade/",
        json={
            "method": "other",
            "nonce": "123",
            "amount": 1000,
        },
    )
    assert r1.status_code == 400, r1.json()
    assert r1.json()["error"] == "reason_expected"


def test_api_admin_account_delete_undelete(
    admin_client: TestClient, normal_account: Account, db_session: Session
) -> None:
    assert normal_account.balance == 0

    rv = admin_client.delete(f"/api/admin/accounts/{normal_account.id}/")
    assert rv.status_code == 204, rv.json()

    db_session.refresh(normal_account)
    assert normal_account.deleted is True

    rv = admin_client.delete(f"/api/admin/accounts/{normal_account.id}/")
    assert rv.status_code == 204, rv.json()

    db_session.refresh(normal_account)
    assert normal_account.deleted is False


def test_api_admin_account_delete_error_ledger(admin_client, ledger_accounts):
    rv = admin_client.delete(f"/api/admin/accounts/{ledger_accounts['bar'].id}/")
    assert rv.status_code == 409, rv.json()
    assert rv.json()["error"] == "account_is_ledger", rv.json()


def test_api_admin_account_delete_error_nonzero_balance(
    admin_client, normal_account, db_session
):
    normal_account.balance = 1000
    db_session.commit()

    rv = admin_client.delete(f"/api/admin/accounts/{normal_account.id}/")
    assert rv.status_code == 409, rv.json()
    assert rv.json()["error"] == "account_has_balance", rv.json()
