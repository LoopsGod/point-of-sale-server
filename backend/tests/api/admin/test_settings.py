import json
import pytest
from app.models.setting import CheckoutReason, Setting
from app.service import setting_service
from sqlalchemy.orm import Session


def test_api_admin_settings(admin_client):
    rv = admin_client.get("/api/admin/settings/notification/")
    assert rv.status_code == 200, rv.json()

    rv = admin_client.get("/api/admin/settings/notification/sender_options/")
    assert rv.status_code == 200, rv.json()

    rv = admin_client.post(
        "/api/admin/settings/notification/",
        json={
            "notificationSepaDestination": "penningmeester@svia.nl",
            "notificationAccountRequestDestination": "penningmeester@svia.nl",
            "notificationProductQuantityDestination": "penningmeester@svia.nl",
            "notificationSender": "pos@svia.nl",
        },
    )
    assert rv.status_code == 204, rv.json()


def test_api_admin_exact_get_old_division(admin_client):
    rv = admin_client.get("/api/admin/settings/exact/old_division/")
    assert rv.status_code == 200, rv.json()


def test_api_admin_checkout_reasons(admin_client):
    rv = admin_client.get("/api/checkout/reasons/")
    assert rv.status_code == 200, rv.json()

    rv = admin_client.post("/api/admin/checkout/reasons/", json=rv.json())
    assert rv.status_code == 204, rv.json()


@pytest.mark.usefixtures(
    "exact_client_settings",
    "exact_token_settings",
    "exact_division_settings",
    "exact_journal_settings",
    "card_reader_secret",
    "notification_settings",
    "checkout_reasons_settings",
)
def test_database_settings(db_session: Session) -> None:
    settings = setting_service.get_db_settings(db_session)

    assert isinstance(settings.april_fools_enabled, bool)
    assert isinstance(settings.checkout_reasons, list)
    assert isinstance(settings.checkout_reader_secret, str)


@pytest.mark.usefixtures(
    "notification_settings", "card_reader_secret", "exact_journal_settings"
)
def test_insert_database_settings(db_session: Session) -> None:
    settings = setting_service.get_db_settings(db_session)

    settings.notification_sepa_destination = "wilco@kruijer.nl"
    settings.checkout_reader_secret = "prosperity"
    settings.exact_journal_pos_code = None
    settings.checkout_reasons = [CheckoutReason(name="a", pin=None)]

    setting_service.save_db_settings(db_session, settings)

    notification_setting = (
        db_session.query(Setting)
        .filter(Setting.key == "NOTIFICATION_SEPA_DESTINATION")
        .one()
    )
    reader_setting = (
        db_session.query(Setting).filter(Setting.key == "CHECKOUT_READER_SECRET").one()
    )
    exact_journal_setting = (
        db_session.query(Setting)
        .filter(Setting.key == "EXACT_JOURNAL_POS_CODE")
        .one_or_none()
    )

    checkout_reasons_setting = (
        db_session.query(Setting).filter(Setting.key == "CHECKOUT_REASONS").one()
    )

    assert notification_setting.value == "wilco@kruijer.nl"
    assert reader_setting.value == "prosperity"
    assert exact_journal_setting is None
    assert checkout_reasons_setting.value == json.dumps(
        [CheckoutReason(name="a", pin=None).dict()]
    )
