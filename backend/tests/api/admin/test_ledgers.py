def test_api_admin_ledgers_transactions_count(admin_client, ledger_accounts):
    for ledger in ledger_accounts.values():
        rv = admin_client.get(f"/api/admin/ledgers/{ledger.id}/transactions/count/")
        assert rv.status_code == 200, rv.json()
