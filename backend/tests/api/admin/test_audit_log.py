def test_api_admin_audit_log(admin_client, admin_user):
    rv = admin_client.get("/api/admin/audit_log/")
    assert rv.status_code == 200, rv.json()

    rv = admin_client.get(
        "/api/admin/audit_log/",
        params={
            "page": 2,
            "userFilter": admin_user.id,
            "search": "someevent",
        },
    )
    assert rv.status_code == 200, rv.json()
