def test_api_admin_transactions(admin_client, room_client, products):
    rv = room_client.post(
        "/api/checkout/cash/",
        json={
            "nonce": "123",
            "products": [{"id": products["room_cola"].id, "amount": 1}],
        },
    )
    assert rv.status_code == 200, rv.json()

    rv = admin_client.get("/api/admin/transactions/")
    assert rv.status_code == 200, rv.json()

    rv = admin_client.get(
        "/api/admin/transactions/", params={"accountId": 1, "sepaBatchId": 1}
    )
    assert rv.status_code == 200, rv.json()

    rv = admin_client.get(
        "/api/admin/transactions/",
        params={"accountId": "Maico", "sepaBatchId": "Maico"},
    )
    assert rv.status_code == 422, rv.json()
