import urllib.parse
from datetime import datetime, date, timedelta, timezone
from unittest import mock
from urllib.parse import urlencode

import pytest
from requests_mock import Mocker
from sqlalchemy import update
from sqlalchemy.orm import Session
from starlette.testclient import TestClient

from app.models import Transaction, Order
from app.domains.exact.export.exact_export import ExactExport
from app.repository import accounts as accounts_repo
from tests.util.transactions import _create_direct_purchase


@pytest.fixture
def account_with_balance_and_orders(
    db_session,
    sepa_account,
    room_client,
    bar_client,
    products,
):
    db_session.refresh(sepa_account)
    assert sepa_account.balance == 0

    # There are seperate test-cases to validate the upgrade responses
    for method in ["cash", "card"]:
        r = bar_client.post(
            f"/api/checkout/{method}/",
            json={
                "nonce": f"123-{method}",
                "upgrade_amount": 1000,
                "upgrade_account_id": sepa_account.id,
                "reason": f"Initial {method} upgrade",
                "products": [],
            },
        )
        assert r.status_code == 200, r.json()

    db_session.refresh(sepa_account)
    assert sepa_account.balance == 2000

    # There are seperate test-cases to validate the account checkout response
    product = products["room_cola"]
    r = room_client.post(
        "/api/checkout/account/",
        json={
            "nonce": f"123-{product.id}",
            "account_id": sepa_account.id,
            "pin": "1234",
            "products": [{"id": product.id, "amount": 1}],
        },
    )
    assert r.status_code == 200, r.json()

    product = products["bar_beer"]
    r = bar_client.post(
        "/api/checkout/account/",
        json={
            "nonce": f"123-{product.id}",
            "account_id": sepa_account.id,
            "pin": "1234",
            "products": [{"id": product.id, "amount": 1}],
        },
    )
    assert r.status_code == 200, r.json()

    # The products are cola (50ct), beer tap (120ct)
    db_session.refresh(sepa_account)
    assert sepa_account.balance == (2000 - 50 - 120)

    # Make sure all transactions and orders are in the past
    yesterday = datetime.now() - timedelta(days=1)
    db_session.execute(update(Transaction).values(created_at=yesterday))
    db_session.execute(update(Order).values(created_at=yesterday))

    return sepa_account


@pytest.fixture
def exact_export_day(
    db_session, account_with_balance_and_orders, exact_finished_settings
):
    export = ExactExport(
        date=date.today() - timedelta(days=1),
        division=str(1337),
        status="pending",
    )

    db_session.add(export)
    db_session.commit()

    return export


@pytest.mark.usefixtures(
    "exact_client_settings",
    "exact_token_settings",
    "exact_division_settings",
    "exact_journal_settings",
)
def test_exact_account_ledgers_not_ready(admin_client: TestClient) -> None:
    r = admin_client.get("/api/admin/exact/export/ready/")
    assert r.status_code == 200, r.json()
    assert r.json()["allLedgersSet"] is False, r.json()


def _mock_accounts_cross_reference(
    requests_mocker: Mocker, db_session: Session
) -> None:
    ledgers = accounts_repo.get_all_distinct_exact_codes(db_session)
    requests_mocker.get(
        ("https://start.exactonline.nl/api/v1/1337/financial/GLAccounts"),
        json={
            "d": {
                "results": [
                    {
                        "ID": "d646794e-5641-45bc-a63a-aa16665b71fc",
                        "Code": ledger_code,
                        "Description": "Account ledger in exact",
                    }
                    for ledger_code in ledgers
                ]
            }
        },
    )


@pytest.mark.usefixtures("exact_finished_settings")
def test_exact_setup_correct(
    admin_client: TestClient, requests_mocker: Mocker, db_session: Session
) -> None:
    _mock_accounts_cross_reference(requests_mocker, db_session)

    r = admin_client.get("/api/admin/exact/export/ready/")
    assert r.status_code == 200, r.json()
    assert r.json()["allLedgersSet"] is True, r.json()
    assert r.json()["setupFinished"] is True, r.json()


@pytest.mark.usefixtures("exact_finished_settings", "account_with_balance_and_orders")
def test_exact_export_missing_date(
    admin_client: TestClient, requests_mocker: Mocker, db_session: Session
) -> None:
    _mock_accounts_cross_reference(requests_mocker, db_session)

    r = admin_client.post("/api/admin/exact/export/")
    assert r.status_code == 422, r.json()
    assert r.json() == {
        "detail": [
            {
                "loc": ["query", "year"],
                "msg": "field required",
                "type": "value_error.missing",
            },
            {
                "loc": ["query", "month"],
                "msg": "field required",
                "type": "value_error.missing",
            },
            {
                "loc": ["query", "day"],
                "msg": "field required",
                "type": "value_error.missing",
            },
        ]
    }, r.json()


@pytest.mark.usefixtures("account_with_balance_and_orders")
def test_exact_export_missing_setup(admin_client):
    today = datetime.today()
    r = admin_client.post(
        "/api/admin/exact/export/",
        params={"year": today.year, "month": today.month, "day": today.day},
    )
    assert r.status_code == 406, r.json()
    assert r.json()["status"] == "err", r.json()
    assert r.json()["error"] == "setup_not_finished", r.json()


@pytest.mark.usefixtures("account_with_balance_and_orders", "exact_finished_settings")
@mock.patch("app.domains.exact.exact_tasks.export_transactions.delay")
def test_exact_export_normal_transactions(
    delay_mock: mock.Mock,
    admin_client: TestClient,
    requests_mocker: Mocker,
    db_session: Session,
) -> None:
    _mock_accounts_cross_reference(requests_mocker, db_session)

    yesterday = datetime.today() - timedelta(days=1)

    r = admin_client.post(
        "/api/admin/exact/export/",
        params={
            "year": yesterday.year,
            "month": yesterday.month,
            "day": yesterday.day,
        },
    )
    assert r.status_code == 202, r.json()
    assert delay_mock.call_count == 1
    assert "exact_export_id" in delay_mock.call_args.kwargs


def test_api_admin_exact_client(admin_client):
    rv = admin_client.post(
        "/api/admin/exact/setup/client/",
        json={
            "baseUrl": "https://pos.svia.nl",
            "clientId": "clientId",
            "clientSecret": "clientSecret",
        },
    )
    assert rv.status_code == 200, rv.json()
    assert rv.json()["url"] == (
        "https://start.exactonline.nl/api/oauth2/auth"
        "?client_id=clientId"
        "&redirect_uri=https%3A%2F%2Fpos.svia.nl%2Fadmin%2Fexact%2Foauth%2Fsuccess"
        "&response_type=code"
    )


@pytest.mark.usefixtures("exact_finished_settings")
def test_api_admin_exact_setup_unlink(admin_client):
    rv = admin_client.post("/api/admin/exact/setup/unlink/")
    assert rv.status_code == 204, rv.json()


@pytest.mark.usefixtures("exact_client_settings")
def test_api_admin_exact_token(admin_client, requests_mocker):
    requests_mocker.post(
        "https://start.exactonline.nl/api/oauth2/token/",
        json={
            "expires_in": 1000,
            "access_token": "access_token",
            "refresh_token": "refresh_token",
        },
    )

    requests_mocker.get(
        "https://start.exactonline.nl/api/v1/current/Me?%24select=CurrentDivision",
        json={"d": {"results": [{"CurrentDivision": 1337}]}},
    )

    requests_mocker.get(
        "https://start.exactonline.nl/api/v1/1337/hrm/Divisions"
        "?%24select=Code%2CDescription",
        json={
            "d": {
                "results": [
                    {"Code": 2, "Description": "Testomgeving"},
                    {"Code": 1, "Description": "Studievereniging VIA"},
                ]
            }
        },
    )

    rv = admin_client.post("/api/admin/exact/setup/token/", json={"code": "somecode"})
    assert rv.status_code == 200, rv.json()


@pytest.mark.usefixtures("exact_client_settings", "exact_token_settings")
def test_api_admin_exact_division(admin_client, requests_mocker):
    requests_mocker.get(
        (
            "https://start.exactonline.nl/api/v1/1337/crm/Accounts?"
            "%24filter=Name+eq+%27NON_EXISTENT%27&%24select=ID"
        ),
        json={"d": {"results": []}},
    )

    rv = admin_client.post("/api/admin/exact/setup/division/", json={"code": 1337})
    assert rv.status_code == 204, rv.json()


@pytest.mark.usefixtures(
    "exact_client_settings", "exact_token_settings", "exact_division_settings"
)
def test_api_admin_exact_journals(admin_client, requests_mocker):
    requests_mocker.get(
        (
            "https://start.exactonline.nl/api/v1/1337/financial/Journals"
            "?%24select=Code%2C+Description%2C+GLAccountCode"
            "%2C+GLAccountDescription%2C+GLAccountType"
        ),
        json={
            "d": {
                "results": [
                    {
                        "Code": "24",
                        "Description": "Point of Sale",
                        "GLAccountCode": "1104",
                        "GLAccountDescription": "Point of Sale",
                        "GLAccountType": 12,
                    }
                ]
            }
        },
    )
    rv = admin_client.get("/api/admin/exact/journals/")
    assert rv.status_code == 200, rv.json()

    rv = admin_client.post(
        "/api/admin/exact/setup/journals/",
        json={
            "code": "1104",
            "description": "Point of Sale",
        },
    )
    assert rv.status_code == 204, rv.json()


@pytest.mark.usefixtures("exact_finished_settings")
def test_api_admin_exact_ledgers(admin_client, requests_mocker):
    requests_mocker.get(
        (
            "https://start.exactonline.nl/api/v1/1337/financial/GLAccounts"
            "?%24filter=IsBlocked+eq+false&%24select=ID%2CCode%2CDescription"
        ),
        json={
            "d": {
                "results": [
                    {
                        "ID": "d646794e-5641-45bc-a63a-aa16665b71fc",
                        "Code": "44200",
                        "Description": "Account ledger in exact",
                    }
                ]
            }
        },
    )
    rv = admin_client.get("/api/admin/exact/ledgers/")
    assert rv.status_code == 200, rv.json()


@pytest.mark.usefixtures("exact_finished_settings")
def test_api_admin_exact_costcenters(
    admin_client: TestClient, requests_mocker: Mocker
) -> None:
    requests_mocker.get(
        "https://start.exactonline.nl/api/v1/1337/hrm/Costcenters"
        "?%24filter=Active+eq+true&%24select=Code%2CDescription",
        json={
            "d": {
                "results": [
                    {
                        "__metadata": {
                            "uri": "https://start.exactonline.nl/api/v1/1337"
                            "/hrm/Costcenters(guid'redactedguid')",
                            "type": "Exact.Web.Api.Models.Costcenter",
                        },
                        "Code": "2212    ",
                        "Description": "Kookcommissie - ALV's",
                    },
                ]
            }
        },
    )

    rv = admin_client.get("/api/admin/exact/cost-centers/")
    assert rv.status_code == 200, rv.json()
    assert rv.json() == {
        "costCenters": [{"code": "2212", "description": "Kookcommissie - ALV's"}]
    }


@pytest.mark.usefixtures("exact_finished_settings")
def test_api_admin_exact_vatcodes(
    admin_client: TestClient, requests_mocker: Mocker
) -> None:
    requests_mocker.get(
        "https://start.exactonline.nl/api/v1/1337/vat/VATCodes?"
        "%24filter=IsBlocked+eq+false&%24select=Code%2CDescription%2CPercentage%2CType",
        json={
            "d": {
                "results": [
                    {
                        "__metadata": {
                            "uri": "https://start.exactonline.nl/api/v1/1337"
                            "/vat/VATCodes(guid'redactedguid')",
                            "type": "Exact.Web.Api.Models.VATCode",
                        },
                        "Code": "4  ",
                        "Description": "BTW hoog tarief, inclusief",
                        "Percentage": 0.21,
                        "Type": "I",
                    },
                ]
            }
        },
    )

    rv = admin_client.get("/api/admin/exact/vat-codes/")
    assert rv.status_code == 200, rv.json()
    assert rv.json() == {
        "vatCodes": [
            {
                "code": "4",
                "description": "BTW hoog tarief, inclusief",
                "percentage": 0.21,
                "type": "I",
            }
        ]
    }


@pytest.mark.usefixtures("account_with_balance_and_orders", "exact_finished_settings")
def test_api_admin_exact_divisions(
    admin_client: TestClient, requests_mocker: Mocker
) -> None:
    requests_mocker.get(
        "https://start.exactonline.nl/api/v1/current/Me?%24select=CurrentDivision",
        json={"d": {"results": [{"CurrentDivision": 1337}]}},
    )
    requests_mocker.get(
        "https://start.exactonline.nl/api/v1/1337/hrm/Divisions"
        "?%24select=Code%2CDescription",
        json={
            "d": {
                "results": [
                    {"Code": 2, "Description": "Testomgeving"},
                    {"Code": 1, "Description": "Studievereniging VIA"},
                ]
            }
        },
    )
    rv = admin_client.get("/api/admin/exact/divisions/")
    assert rv.status_code == 200, rv.json()
    assert rv.json()["administrations"] == [
        {"code": "2", "name": "Testomgeving"},
        {"code": "1", "name": "Studievereniging VIA"},
    ]


@pytest.mark.usefixtures(
    "exact_client_settings",
    "exact_token_settings",
    "exact_division_settings",
    "exact_journal_settings",
    "exact_income_ledgers",
    "exact_product_ledgers",
)
def test_api_admin_exact_setup_finish(admin_client):
    rv = admin_client.put("/api/admin/exact/setup/finish/", json={"finished": True})
    assert rv.status_code == 204, rv.json()


def test_api_admin_exact_gltransactions_status(admin_client, exact_export_day):
    rv = admin_client.get(f"/api/admin/exact/export/{exact_export_day.id}/")
    assert rv.status_code == 200, rv.json()


def test_api_admin_exact_token_request(
    admin_client: TestClient, requests_mocker: Mocker
) -> None:
    requests_mocker.post(
        "https://start.exactonline.nl/api/oauth2/token/",
        json={
            "expires_in": 600,
            "access_token": "access_token",
            "refresh_token": "refresh_token",
        },
    )

    rv = admin_client.post(
        "/api/admin/exact/setup/client/",
        json={
            "baseUrl": "http://pos.svia.nl",
            "clientId": "client_id",
            "clientSecret": "client_secret",
        },
    )
    assert rv.status_code == 200, rv.json()
    assert (
        rv.json()["url"] == "https://start.exactonline.nl/api/oauth2/auth"
        "?client_id=client_id"
        "&redirect_uri=http%3A%2F%2Fpos.svia.nl%2Fadmin%2Fexact%2Foauth%2Fsuccess"
        "&response_type=code"
    )

    rv = admin_client.post(
        "/api/admin/exact/setup/token/",
        json={
            "code": "some_code",
        },
    )
    assert rv.status_code == 200, rv.json()
    assert rv.json().get("finished") is False, rv.json()


@pytest.mark.usefixtures("exact_finished_settings")
def test_api_admin_exact_token_reauth(
    admin_client: TestClient, requests_mocker: Mocker
) -> None:
    requests_mocker.post(
        "https://start.exactonline.nl/api/oauth2/token/",
        json={
            "expires_in": 600,
            "access_token": "access_token",
            "refresh_token": "refresh_token",
        },
    )
    rv = admin_client.get("/api/admin/exact/setup/reauth/")
    assert rv.status_code == 200, rv.json()
    url = urllib.parse.unquote(rv.json()["url"])
    assert (
        url == "https://start.exactonline.nl/api/oauth2/auth"
        "?client_id=client_id"
        "&redirect_uri=http://pos.svia.nl/admin/exact/oauth/success"
        "&response_type=code"
    )


TRANSACTION_LINES_RESPONSE = {
    "d": [
        {
            "__metadata": {
                "uri": "https://start.exactonline.nl/api/v1/3119907/"
                "financialtransaction/TransactionLines(guid'abc')",
                "type": "Exact.Web.Api.Models.Financial.TransactionLine",
            },
            "Date": "/Date(1641600000000)/",
            "Description": "BOUGHT - Bier",
            "EntryNumber": 22240001,
        }
    ]
}


@pytest.mark.usefixtures("exact_finished_settings")
@pytest.mark.parametrize(
    "pre_export_status,post_export_status,mock_response,status_code,expected_error",
    [
        ("exported", "deleted", {"d": []}, 200, None),
        (
            "exported",
            "exported",
            TRANSACTION_LINES_RESPONSE,
            409,
            "exact_booking_exists",
        ),
        ("pending", "pending", {"d": []}, 409, "batch_not_exported"),
    ],
)
def test_api_admin_delete_exact_booking_success(
    admin_client: TestClient,
    requests_mocker: Mocker,
    exact_export_day: ExactExport,
    pre_export_status: str,
    post_export_status: str,
    db_session: Session,
    mock_response: object,
    status_code: int,
    expected_error: str | None,
) -> None:
    exact_export_day.status = pre_export_status
    exact_export_day.data = "123456789"
    db_session.commit()

    requests_mocker.get(
        "https://start.exactonline.nl/api/v1/1337/"
        "financialtransaction/TransactionLines",
        json=mock_response,  # Nothing exists on date.
    )

    rv = admin_client.delete(f"/api/admin/exact/export/{exact_export_day.id}/")
    assert rv.status_code == status_code
    if rv.status_code != 200:
        assert rv.json().get("error") == expected_error

    db_session.refresh(exact_export_day)
    assert exact_export_day.status == post_export_status


@pytest.mark.usefixtures("account_with_balance_and_orders", "exact_export_day")
def test_api_admin_get_exact_export_generates_unexported_exact_exports(
    db_session, admin_client, products
):
    # This creates a purchase in 2020
    _create_direct_purchase(db_session, [products["room_beer"], products["room_beer"]])

    r = admin_client.get("/api/admin/exact/export/")
    assert r.status_code == 200, r.json()
    exports = r.json()["items"]

    # Since we are more than 15 days away from 2020, this page will be completely
    # filled. Note that if you time travel back to before the 15th of September 2020,
    # this test will fail.
    assert len(exports) == 15

    yesterday = date.today() - timedelta(days=1)
    day_before = date.today() - timedelta(days=2)

    assert exports[0]["status"] == "pending"
    assert exports[1]["status"] == "unexported"

    assert exports[0]["date"] == yesterday.isoformat()
    assert exports[1]["date"] == day_before.isoformat()


@pytest.mark.usefixtures("account_with_balance_and_orders", "exact_finished_settings")
def test_api_admin_get_exact_exports_generates_from_first_day_with_transaction(
    admin_client: TestClient,
) -> None:
    # `account_with_balance_and_orders` creates a purchase yesterday,
    # it is the only transaction

    rv = admin_client.get("/api/admin/exact/export/")
    assert rv.status_code == 200, rv.json()
    exports = rv.json()["items"]

    # Since yesterday is the first day with transactions, we only have one
    # exportable day.
    assert len(exports) == 1

    yesterday = date.today() - timedelta(days=1)
    assert exports[0]["status"] == "unexported"
    assert exports[0]["date"] == yesterday.isoformat()


@pytest.mark.usefixtures("account_with_balance_and_orders", "exact_export_day")
def test_api_admin_get_exact_export_filters(admin_client: TestClient) -> None:
    # Test unexported as literal string
    r = admin_client.get(
        "/api/admin/exact/export?" + urlencode({"statusFilter": '"unexported"'})
    )

    assert r.status_code == 200, r.json()
    exports = r.json()["items"]

    for e in exports:
        assert e["status"] == "unexported"

    # Test unexported as literal item in list
    r = admin_client.get(
        "/api/admin/exact/export?"
        + urlencode({"statusFilter": '["unexported", "exported"]'})
    )

    assert r.status_code == 200, r.json()
    exports = r.json()["items"]

    for e in exports:
        assert e["status"] == "unexported"

    # Test date filter
    yesterday = date.today() - timedelta(days=1)
    r = admin_client.get(
        "/api/admin/exact/export?" + urlencode({"dateFilter": yesterday.isoformat()})
    )

    assert r.status_code == 200, r.json()
    exports = r.json()["items"]

    assert len(exports) == 1
    assert exports[0]["status"] == "pending"

    # Test count filter
    r = admin_client.get("/api/admin/exact/export?" + urlencode({"countFilter": ">=1"}))

    assert r.status_code == 200, r.json()
    exports = r.json()["items"]
    assert len(exports) == 1


@pytest.mark.usefixtures("account_with_balance_and_orders", "exact_export_day")
def test_api_admin_get_exact_export_filters_when_unexported(
    admin_client: TestClient, db_session: Session
) -> None:
    # When something goes wrong in the export process, but we haven't exported
    # yet, an ExactExport item can be generated with status 'unexported'. This
    # is different than the virtual unexported 'exports' we create for every day.

    exact_export = ExactExport(
        created_at=date(2020, 9, 2),
        updated_at=date(2020, 9, 2),
        date=date(2020, 9, 1),
        division="1337",
        status="unexported",
    )
    db_session.add(exact_export)
    db_session.commit()

    # Test unexported as literal string
    r = admin_client.get(
        "/api/admin/exact/export?" + urlencode({"statusFilter": '"unexported"'})
    )

    assert r.status_code == 200, r.json()
    exports = r.json()["items"]

    for e in exports:
        assert e["status"] == "unexported"

    # Test unexported as literal item in list
    r = admin_client.get(
        "/api/admin/exact/export?" + urlencode({"statusFilter": '["unexported"]'})
    )

    assert r.status_code == 200, r.json()
    exports = r.json()["items"]

    for e in exports:
        assert e["status"] == "unexported"


@pytest.mark.usefixtures("exact_finished_settings")
def test_exact_bank_transactions(
    admin_client: TestClient, requests_mocker: Mocker
) -> None:
    requests_mocker.get(
        (
            "https://start.exactonline.nl/api/v1/1337/financial/GLAccounts"
            "?%24filter=Code+eq+%2714001%27&%24select=ID%2CCode%2CDescription"
        ),
        json={
            "d": {
                "results": [
                    {
                        "ID": "d646794e-5641-45bc-a63a-aa16665b71fc",
                        "Code": "44200",
                        "Description": "Account ledger in exact",
                    }
                ]
            }
        },
    )

    sept5 = int(datetime(2020, 9, 5, tzinfo=timezone.utc).timestamp()) * 1000
    sept6 = int(datetime(2020, 9, 6, tzinfo=timezone.utc).timestamp()) * 1000

    requests_mocker.get(
        "https://start.exactonline.nl/api/v1/1337/bulk/Financial/TransactionLines",
        json={
            "d": {
                "results": [
                    {
                        "AmountDC": 10,
                        "Date": f"/Date({sept5})/",
                        "EntryNumber": 24900001,
                        "FinancialPeriod": 9,
                        "FinancialYear": 2020,
                        "JournalCode": "90",
                        "Status": 20,
                        "Type": 90,
                    },
                    {
                        "AmountDC": 20,
                        "Date": f"/Date({sept6})/",
                        "EntryNumber": 24900002,
                        "FinancialPeriod": 6,
                        "FinancialYear": 2020,
                        "JournalCode": "90",
                        "Status": 20,
                        "Type": 90,
                    },
                ]
            }
        },
    )

    rv = admin_client.post("/api/admin/bank_transactions/exact/?year=2020")
    assert rv.status_code == 200, rv.json()

    rv = admin_client.get(
        "/api/admin/bank_transactions/?start=2020-09-01&end=2020-09-10"
    )
    assert rv.status_code == 200, rv.json()

    bank_transactions = rv.json()["bankTransactions"]

    #  Note that these are shifted one day compared to the exact data
    assert {"amount": -1000, "count": 1, "date": "2020-09-04"} in bank_transactions
    assert {"amount": -2000, "count": 1, "date": "2020-09-05"} in bank_transactions
