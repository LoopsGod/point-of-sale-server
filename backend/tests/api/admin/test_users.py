from datetime import datetime, timedelta, timezone
from starlette.testclient import TestClient

from app.models import User, UserDirectDebit
from app.models.account import Account

from sqlalchemy.orm import Session


def test_get_api_admin_user(admin_client: TestClient, admin_user: User) -> None:
    rv = admin_client.get(f"/api/admin/users/{admin_user.id}/")
    assert rv.status_code == 200, rv.json()


def test_get_api_admin_user_with_viaduct_filter(
    admin_client: TestClient, admin_user: User
) -> None:
    rv = admin_client.get("/api/admin/users/", params={"viaductFilter": False})
    assert rv.status_code == 200, rv.json()
    assert len(rv.json()["items"]) == 1, rv.json()

    rv = admin_client.get("/api/admin/users/", params={"viaductFilter": True})
    assert rv.status_code == 200, rv.json()
    assert len(rv.json()["items"]) == 0, rv.json()


def test_post_api_admin_user(admin_client):
    rv = admin_client.post(
        "/api/admin/users/",
        json={
            "accountId": None,
            "isAdmin": False,
            "longLogin": True,
            "name": "Maico Timmerman",
            "registerId": None,
            "username": "someuser",
            "viaductUserId": None,
            "password": "somepassword",
        },
    )
    assert rv.status_code == 201, rv.json()


def test_post_api_admin_user_nullable(admin_client):
    rv = admin_client.post(
        "/api/admin/users/",
        json={
            "accountId": None,
            "isAdmin": None,
            "longLogin": None,
            "name": "Maico Timmerman",
            "registerId": None,
            "username": "someuser",
            "viaductUserId": None,
            "password": "somepassword",
        },
    )

    assert rv.status_code == 422, rv.json()


def test_post_api_admin_user_empty_admin_long(admin_client):
    rv = admin_client.post(
        "/api/admin/users/",
        json={
            "accountId": None,
            "name": "Maico Timmerman",
            "registerId": None,
            "username": "someuser",
            "viaductUserId": None,
            "password": "somepassword",
        },
    )

    assert rv.status_code == 201, rv.json()


def test_put_api_admin_user_password_short(
    admin_client: TestClient, admin_user: User
) -> None:
    rv = admin_client.put(
        f"/api/admin/users/{admin_user.id}/",
        json={
            "accountId": None,
            "isAdmin": False,
            "longLogin": True,
            "name": "Maico Timmerman",
            "registerId": None,
            "username": "admin",
            "viaductUserId": None,
            "password": "short",
        },
    )
    assert rv.status_code == 422, rv.json()


def test_put_api_admin_user(admin_client: TestClient, admin_user: User) -> None:
    rv = admin_client.put(
        f"/api/admin/users/{admin_user.id}/",
        json={
            "accountId": None,
            "isAdmin": False,
            "longLogin": True,
            "name": "Maico Timmerman",
            "registerId": None,
            "username": "admiN ",
            "viaductUserId": None,
            "password": "somepassword",
        },
    )
    assert rv.status_code == 200, rv.json()

    rv = admin_client.post(
        "/api/login/", json={"username": "admin", "password": "somepassword"}
    )
    assert rv.status_code == 200, rv.json()


def test_put_api_admin_user_account_already_coupled(
    admin_client: TestClient, users: dict[str, User]
) -> None:
    assert users["random"].account_id
    assert users["random"].account_id != users["random2"].account_id

    rv = admin_client.put(
        f"/api/admin/users/{users['random2'].id}/",
        json={
            "accountId": users["random"].account_id,
            "isAdmin": False,
            "longLogin": True,
            "name": "Maico Timmerman",
            "registerId": None,
            "username": users["random2"].username,
            "viaductUserId": None,
            "password": None,
        },
    )
    assert rv.status_code == 409, rv.json()
    assert rv.json()["error"] == "account_taken_by_user"


def test_disable_direct_debit_should_affect_allow_negative_balance(
    db_session: Session,
    admin_client: TestClient,
    sepa_user: User,
    sepa_account: Account,
) -> None:
    assert sepa_user.account is not None
    assert sepa_user.account.allow_negative_balance is True

    # Disable DD but don't clear all data
    rv = admin_client.put(
        f"/api/admin/users/{sepa_user.id}/direct_debit/",
        json={
            "enabled": False,
            "iban": "NL91ABNA0417164300",
            "bic": "ABNANL2A",
            "mandateId": 123,
        },
    )
    assert rv.status_code == 200, rv.json()
    assert rv.json()["enabled"] is False
    assert rv.json()["iban"] == "NL91ABNA0417164300"

    db_session.refresh(sepa_account)
    assert sepa_account.allow_negative_balance is False

    user_dd: UserDirectDebit = (
        db_session.query(UserDirectDebit)
        .filter(UserDirectDebit.user_id == sepa_user.id)
        .one()
    )
    assert user_dd.enabled is False
    assert user_dd.iban == "NL91ABNA0417164300"


def test_clear_direct_debit_should_fully_delete_all_data(
    db_session: Session,
    admin_client: TestClient,
    sepa_user: User,
    sepa_account: Account,
) -> None:
    assert sepa_user.account is not None
    assert sepa_user.account.allow_negative_balance is True

    rv = admin_client.put(
        f"/api/admin/users/{sepa_user.id}/direct_debit/",
        json={
            "enabled": False,
        },
    )
    assert rv.status_code == 200, rv.json()
    assert rv.json()["enabled"] is False
    assert rv.json()["iban"] is None

    db_session.refresh(sepa_account)
    assert sepa_account.allow_negative_balance is False

    user_dd = (
        db_session.query(UserDirectDebit)
        .filter(UserDirectDebit.user_id == sepa_user.id)
        .one_or_none()
    )
    assert user_dd is None


def test_cannot_disable_direct_debit_when_batch_is_pending(
    db_session: Session,
    admin_client: TestClient,
    sepa_user: User,
    sepa_account: Account,
) -> None:
    assert sepa_user.account is not None
    assert sepa_user.account.allow_negative_balance is True

    #  Create pending batch
    sepa_account.balance = -1000
    db_session.commit()

    rv = admin_client.post(
        "/api/admin/sepa_batches/",
        json={
            "accounts": [sepa_account.id],
            "executionDate": (
                datetime.now(timezone.utc) + timedelta(days=1)
            ).isoformat(),
            "description": "asdf",
        },
    )
    assert rv.status_code == 201, rv.json()

    #  Try to disable users dd
    rv = admin_client.put(
        f"/api/admin/users/{sepa_user.id}/direct_debit/",
        json={
            "enabled": False,
        },
    )

    assert rv.status_code == 400, rv.json()
    assert (
        rv.json()["error"]
        == "cannot_disable_direct_debit_when_pending_sepa_batch_exists"
    )


def test_switch_user_account_should_affect_allow_negative_balance(
    db_session: Session,
    admin_client: TestClient,
    sepa_user: User,
    users: dict[str, User],
    sepa_account: Account,
    normal_account2: Account,
) -> None:
    # user 'random' has 'normal_account' linked
    assert users["random"].account is not None
    assert sepa_user.account is not None
    assert sepa_user.account.allow_negative_balance is True

    rv = admin_client.put(
        f"/api/admin/users/{sepa_user.id}/",
        json={
            "accountId": normal_account2.id,
            "isAdmin": False,
            "longLogin": True,
            "name": sepa_user.name,
            "registerId": None,
            "username": sepa_user.username,
            "viaductUserId": None,
            "password": None,
        },
    )
    assert rv.status_code == 200, rv.json()
    assert normal_account2.id == rv.json()["accountId"]

    db_session.expire_all()
    assert sepa_account.allow_negative_balance is False
    assert normal_account2.allow_negative_balance is True


def test_remove_account_should_affect_allow_negative_balance(
    db_session: Session,
    admin_client: TestClient,
    sepa_user: User,
    sepa_account: Account,
) -> None:
    assert sepa_user.account is not None
    assert sepa_user.account.allow_negative_balance is True

    rv = admin_client.put(
        f"/api/admin/users/{sepa_user.id}/",
        json={
            "accountId": None,
            "isAdmin": False,
            "longLogin": True,
            "name": sepa_user.name,
            "registerId": None,
            "username": sepa_user.username,
            "viaductUserId": None,
            "password": None,
        },
    )
    assert rv.status_code == 200, rv.json()
    assert rv.json()["accountId"] is None

    db_session.refresh(sepa_account)
    assert sepa_account.allow_negative_balance is False


def test_put_api_admin_user_with_account_unchanged(
    admin_client: TestClient, users: dict[str, User]
) -> None:
    assert users["random"].account_id
    rv = admin_client.put(
        f"/api/admin/users/{users['random'].id}/",
        json={
            "accountId": users["random"].account_id,
            "isAdmin": users["random"].is_admin,
            "longLogin": users["random"].long_login,
            "name": users["random"].name,
            "registerId": users["random"].register_id,
            "username": users["random"].username,
            "viaductUserId": users["random"].viaduct_user_id,
            "password": None,
        },
    )
    assert rv.status_code == 200, rv.json()


def test_delete_api_admin_user(
    db_session: Session,
    admin_client: TestClient,
    sepa_user: User,
    sepa_account: Account,
) -> None:
    assert sepa_user.account_id == sepa_account.id
    rv = admin_client.delete(f"/api/admin/users/{sepa_user.id}/")
    assert rv.status_code == 200, rv.json()
    db_session.refresh(sepa_user)
    db_session.refresh(sepa_account)

    #  Deleting a user should unlink account and disable direct debit
    assert sepa_user.account_id is None
    assert sepa_user.direct_debit is None
    #  Check data fully deleted
    user_dd = (
        db_session.query(UserDirectDebit)
        .filter(UserDirectDebit.user_id == sepa_user.id)
        .one_or_none()
    )
    assert user_dd is None
    assert sepa_account.allow_negative_balance is False


def test_cannot_delete_user_when_batch_is_pending(
    db_session: Session,
    admin_client: TestClient,
    sepa_user: User,
    sepa_account: Account,
) -> None:
    #  Create pending batch
    sepa_account.balance = -1000
    db_session.commit()

    rv = admin_client.post(
        "/api/admin/sepa_batches/",
        json={
            "accounts": [sepa_account.id],
            "executionDate": (
                datetime.now(timezone.utc) + timedelta(days=1)
            ).isoformat(),
            "description": "asdf",
        },
    )
    assert rv.status_code == 201, rv.json()

    rv = admin_client.delete(f"/api/admin/users/{sepa_user.id}/")
    assert rv.status_code == 400, rv.json()
    assert (
        rv.json()["error"]
        == "cannot_disable_direct_debit_when_pending_sepa_batch_exists"
    )


def test_delete_api_admin_user_self(admin_client: TestClient, admin_user: User) -> None:
    rv = admin_client.delete(f"/api/admin/users/{admin_user.id}/")
    assert rv.status_code == 400, rv.json()
    assert rv.json()["error"] == "cant_remove_self", rv.json()


def test_delete_api_admin_user_viaduct_user(
    admin_client: TestClient, viaduct_user: User
) -> None:
    rv = admin_client.delete(f"/api/admin/users/{viaduct_user.id}/")
    assert rv.status_code == 409, rv.json()
    assert rv.json()["error"] == "cant_remove_viaduct_user", rv.json()
