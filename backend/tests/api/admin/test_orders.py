import pytest


def test_api_admin_orders_card(admin_client):
    rv = admin_client.get("/api/admin/orders/card/")
    assert rv.status_code == 200, rv.json()


@pytest.mark.parametrize(
    "date,timezone,status_code",
    [
        ("2020-01-01", "NaN", 422),
        ("202120-01-01", "1", 422),
        ("2021-01-01", "1", 200),
    ],
)
def test_api_admin_orders(admin_client, date, timezone, status_code):
    rv = admin_client.get(
        "/api/admin/orders/card/",
        params={
            "start": date,
            "timezone": timezone,
        },
    )

    assert rv.status_code == status_code, rv.json()
