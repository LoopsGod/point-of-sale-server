import pytest

from app.models import Register


@pytest.mark.usefixtures("registers")
@pytest.mark.parametrize(
    "url",
    [
        "/api/admin/registers/",
        "/api/admin/registers/info",
        "/api/admin/registers/default",
    ],
)
def test_get_functions(admin_client, url):
    rv = admin_client.get(url)
    assert rv.status_code == 200, rv.json()


def test_get_register(admin_client, registers):
    rv = admin_client.get(f"/api/admin/registers/{registers['room'].id}/")
    assert rv.status_code == 200, rv.json()


def test_api_admin_register_create_deleted(admin_client, categories):
    rv = admin_client.post(
        "/api/admin/registers/",
        json={
            "capabilities": [
                "PAYMENT_CASH",
                "PAYMENT_CARD",
                "PAYMENT_OTHER",
                "PAYMENT_ACCOUNT_OTHER",
                "PAYMENT_UNDO",
            ],
            "categoryIds": [categories[0].id],
            "name": "Some register",
        },
    )
    assert rv.status_code == 201, rv.json()

    register_id = rv.json()["register"]["id"]

    rv = admin_client.delete(f"/api/admin/registers/{register_id}/")
    assert rv.status_code == 204, rv.json()

    rv = admin_client.get(f"/api/admin/registers/{register_id}/")
    assert rv.status_code == 404, rv.json()


def test_api_admin_register_create_invalid_cap(admin_client, categories):
    rv = admin_client.post(
        "/api/admin/registers/",
        json={
            "capabilities": [
                "PAYMENT_UNSUPPORTED",
            ],
            "categoryIds": [categories[0].id],
            "name": "Some register",
        },
    )
    assert rv.status_code == 422, rv.json()


def test_api_admin_register_update(admin_client, registers, categories):
    rv = admin_client.put(
        f"/api/admin/registers/{registers['room'].id}/",
        json={
            "capabilities": [
                "PAYMENT_CASH",
                "PAYMENT_CARD",
                "PAYMENT_OTHER",
                "PAYMENT_ACCOUNT_OTHER",
                "PAYMENT_UNDO",
            ],
            "categoryIds": [categories[0].id],
            "name": "Some register",
        },
    )
    assert rv.status_code == 200, rv.json()


def test_api_admin_register_default_post(admin_client, registers):
    rv = admin_client.post(
        "/api/admin/registers/default",
        json={
            "registerId": registers["room"].id,
        },
    )
    assert rv.status_code == 200, rv.json()


def test_api_admin_register_delete_default(admin_client, registers):
    register: Register = registers["self"]
    assert register.default is True

    rv = admin_client.delete(f"/api/admin/registers/{register.id}/")
    assert rv.status_code == 409, rv.json()
    assert rv.json()["error"] == "register_is_default"


def test_api_admin_register_delete_used(admin_client, registers, sale_points):
    room, bar_user = sale_points
    register = registers["bar"]
    assert bar_user.register_id == register.id
    assert register.default is False

    rv = admin_client.delete(f"/api/admin/registers/{register.id}/")
    assert rv.status_code == 409, rv.json()
    assert rv.json()["error"] == "register_enabled_for_users"
