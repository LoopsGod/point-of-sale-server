import json
import os.path

import pytest

from app.models import ProductClassification
from app.service.invoice.interpreter import (
    interpret_analysis,
    Invoice,
)
from app.service.invoice.s3 import upload_and_analyse_with_textract

invoices = [
    "ah_1",
    "ah_2",
    "brouwerijtij",
    "dorstlust",
    "dorstlust2",
    "goedkoop",
    "klinkers",
    "compliment",
]


def convert_test_invoices_to_textract_data():
    """
    Calls Textract for the invoices in invoices/ and saves the responses
    as JSON files. These can then be used in the tests below. This has to be
    done only once or when the invoice file is changed.
    """
    for filename in invoices:
        print(f"Creating data for {filename}")

        with open(
            os.path.join(os.path.dirname(__file__), "invoices", filename + ".pdf"), "rb"
        ) as invoice_pdf:
            document_analysis_result, _ = upload_and_analyse_with_textract(
                invoice_pdf, filename + ".pdf"
            )

        with open(
            os.path.join(
                os.path.dirname(__file__), "invoices", filename + ".textract.json"
            ),
            "w",
        ) as textract_json:
            json.dump(document_analysis_result, textract_json)

        with open(
            os.path.join(
                os.path.dirname(__file__), "invoices", filename + ".invoice.json"
            ),
            "w",
        ) as invoice_json:
            invoice = interpret_analysis(document_analysis_result["ExpenseDocuments"])

            json.dump(invoice.dict(), invoice_json)


# if __name__ == "__main__":
#     convert_test_invoices_to_textract_data()


@pytest.mark.parametrize(
    "filename",
    invoices,
)
def test_invoices(admin_client, filename):
    with open(
        os.path.join(
            os.path.dirname(__file__), "invoices", filename + ".textract.json"
        ),
        "rb",
    ) as textract_file:
        document_analysis_result = json.load(textract_file)

    with open(
        os.path.join(os.path.dirname(__file__), "invoices", filename + ".invoice.json"),
        "rb",
    ) as invoice_file:
        invoice_test = json.load(invoice_file)

    invoice = interpret_analysis(document_analysis_result["ExpenseDocuments"])

    assert invoice.dict() == Invoice(**invoice_test).dict()


def test_match_classify_update_products(db_session, admin_client, products):
    """
    Full example of a walkthrough of the invoices uploader
    """
    cola = products["room_cola"]
    coca_cola_invoice_line = "Coca Cola Troep"
    existing_cola_match = ProductClassification(
        description=coca_cola_invoice_line, multiplier=2, product_id=cola.id
    )
    db_session.add(existing_cola_match)
    db_session.commit()

    rv = admin_client.post(
        "/api/admin/invoice/matcher/update",
        json={
            "updates": [
                {"item": coca_cola_invoice_line, "multipack": 1, "productId": cola.id}
            ]
        },
    )
    assert rv.status_code == 204, rv.json()

    update_body = {"updates": [{"productId": cola.id, "diff": 5}]}

    rv = admin_client.post("/api/admin/invoice/quantity/add", json=update_body)

    assert rv.status_code == 204, rv.json()

    db_session.refresh(products["room_cola"])

    assert products["room_cola"].quantity == 5
