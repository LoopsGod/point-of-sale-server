import pytest

from app.models import Product, Category, Account, User
from app.models.checkout import CheckoutMethod
from starlette.testclient import TestClient


def test_api_admin_products_count(
    admin_client: TestClient, products: dict[str, Product]
) -> None:
    for product in products.values():
        r = admin_client.get(f"/api/admin/products/{product.id}/count/")
        assert r.status_code == 200, r.json()


def test_api_admin_products_count_per_day(
    admin_client: TestClient, products: dict[str, Product]
) -> None:
    from datetime import date

    current_year = date.today().year
    for product in products.values():
        r = admin_client.get(
            f"/api/admin/products/{product.id}/count_per_day/?year={current_year}"
        )
        assert r.status_code == 200, r.json()


@pytest.mark.usefixtures("products")
def test_get_api_admin_products(admin_client: TestClient) -> None:
    r = admin_client.get("/api/admin/products/all/")
    assert r.status_code == 200, r.json()
    res = r.json()
    assert len(res["products"])
    assert len(res["categories"])


@pytest.mark.usefixtures("products")
def test_get_api_admin_products_search(admin_client: TestClient) -> None:
    r = admin_client.get("/api/admin/products/")
    assert r.status_code == 200, r.json()
    assert len(r.json()["items"]) > 0
    assert r.json()["items"][0]
    assert r.json()["perPage"] > 0
    assert r.json()["page"] == 1
    assert r.json()["pageCount"] >= 1


def test_api_admin_product_create(
    admin_client: TestClient,
    categories: tuple[Category, Category, Category],
    ledger_accounts: dict[str, Account],
) -> None:
    rv = admin_client.post(
        "/api/admin/products/",
        json={
            "categoryIds": [c.id for c in categories],
            "deposit": False,
            "ledgerId": ledger_accounts["room"].id,
            "name": "New Product",
            "price": 100,
        },
    )

    assert rv.status_code == 201, rv.json()
    p = rv.json()

    assert p["name"] == "New Product"
    assert p["quantity"] == 0


def test_api_admin_product_create_float_price(
    admin_client: TestClient,
    categories: tuple[Category, Category, Category],
    ledger_accounts: dict[str, Account],
) -> None:
    rv = admin_client.post(
        "/api/admin/products/",
        json={
            "categoryIds": [c.id for c in categories],
            "deposit": False,
            "ledgerId": ledger_accounts["room"].id,
            "name": "New Product",
            "price": 10.1,
        },
    )

    assert rv.status_code == 422, rv.json()
    assert rv.json()["detail"][0]["type"] == "type_error.integer"


def test_api_admin_product_update(
    admin_client: TestClient,
    products: dict[str, Product],
    categories: tuple[Category, Category, Category],
    ledger_accounts: dict[str, Account],
) -> None:
    p = products["room_cola"]

    rv = admin_client.put(
        f"/api/admin/products/{p.id}/",
        json={
            "id": p.id,
            "categoryIds": [c.id for c in categories],
            "deposit": True,
            "ledgerId": ledger_accounts["bar"].id,
            "name": "No Longer Cola",
            "price": 1337,
            "quantity": 25,
        },
    )

    assert rv.status_code == 200, rv.json()
    result = rv.json()

    assert result["id"] == products["room_cola"].id
    assert result["deposit"]
    assert result["ledgerId"] == ledger_accounts["bar"].id
    assert result["name"] == "No Longer Cola"
    assert result["quantity"] == 25
    assert result["price"] == 1337


def test_api_admin_product_delete(
    admin_client: TestClient, products: dict[str, Product]
) -> None:
    p = products["room_cola"]

    rv = admin_client.delete(f"/api/admin/products/{p.id}/")

    assert rv.status_code == 204
    #  Must have no length
    assert len(rv.text) == 0
    #  Must not have content length
    assert "content-length" not in rv.headers


def test_api_admin_product_no_modify_after_delete(
    admin_client: TestClient,
    products: dict[str, Product],
    categories: tuple[Category, Category, Category],
    ledger_accounts: dict[str, Account],
) -> None:
    p = products["room_cola"]

    rv = admin_client.delete(f"/api/admin/products/{p.id}/")

    assert rv.status_code == 204

    rv = admin_client.put(
        f"/api/admin/products/{p.id}/",
        json={
            "id": p.id,
            "categoryIds": [c.id for c in categories],
            "deposit": False,
            "ledgerId": ledger_accounts["room"].id,
            "name": "Changed name",
            "price": 500,
            "quantity": 5,
        },
    )

    assert rv.status_code == 400, rv.json()


@pytest.mark.parametrize(
    "ledger,expected_result",
    [
        ("notfound", "ledger_not_found"),
        ("income", "ledger_not_product_ledger"),
        ("event", "ledger_not_product_ledger"),
        ("user", "ledger_not_product_ledger"),
    ],
)
def test_api_admin_product_update_invalid_ledger(
    admin_client: TestClient,
    products: dict[str, Product],
    account_ids_checkout_method: dict[CheckoutMethod, Account],
    event_account: Account,
    users: dict[str, User],
    ledger: str,
    expected_result: str,
) -> None:
    ledger_map = {
        "notfound": -1,
        "income": account_ids_checkout_method[CheckoutMethod.CARD],
        "event": event_account.id,
        "user": users["random"].account_id,
    }

    p = products["room_cola"]

    rv = admin_client.put(
        f"/api/admin/products/{p.id}/",
        json={
            "id": p.id,
            "categoryIds": [],
            "deposit": True,
            "ledgerId": ledger_map[ledger],
            "name": "No Longer Cola",
            "price": 1337,
            "quantity": 25,
        },
    )

    assert rv.status_code == 400, rv.json()
    result = rv.json()
    assert result["error"] == expected_result
