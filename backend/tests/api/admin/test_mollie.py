import pytest
from mollie.api.objects.payment import Payment
from requests_mock import Mocker
from sqlalchemy.orm import Session
from starlette.testclient import TestClient

from app.models import IdealPayment, Account

MOLLIE_PAYMENT_CANCELLED = Payment(
    {
        "resource": "payment",
        "id": "tr_nonexistent",
        "mode": "test",
        "createdAt": "2018-09-25T13:38:21+00:00",
        "amount": {"value": "20.00", "currency": "EUR"},
        "description": "Upgrade via account",
        "method": "ideal",
        "metadata": None,
        "status": "canceled",
        "canceledAt": "2018-09-25T13:38:32+00:00",
        "countryCode": "NL",
        "profileId": "pfl_Qq54yD43me",
        "sequenceType": "oneoff",
        "redirectUrl": "https://pos.svia.nl/accounts/1/saldo/?cb=5",
        "webhookUrl": "https://pos.svia.nl/api/mollie/webhook/",
        "_links": {
            "self": {
                "href": "https://api.mollie.com/v2/payments/tr_nonexistent",
                "type": "application/hal+json",
            },
            "documentation": {
                "href": "https://docs.mollie.com/reference/v2/payments-api/get-payment",
                "type": "text/html",
            },
        },
    }
)


@pytest.fixture
def ideal_payment(db_session: Session, normal_account: Account) -> IdealPayment:
    i = IdealPayment()
    i.account_id = normal_account.id
    i.status = "expired"
    i.mollie_id = "tr_nonexistent"
    i.transaction_id = None
    db_session.add(i)
    db_session.commit()
    return i


@pytest.mark.usefixtures("ideal_payment")
def test_api_admin_ideal_payments(admin_client: TestClient) -> None:
    rv = admin_client.get("/api/admin/ideal/")
    assert rv.status_code == 200


def test_api_admin_mollie_status(
    admin_client: TestClient,
    ideal_payment: IdealPayment,
    requests_mocker: Mocker,
) -> None:
    assert ideal_payment.status == "expired"
    requests_mocker.get(
        "https://api.mollie.com/v2/payments/tr_nonexistent",
        json=MOLLIE_PAYMENT_CANCELLED,
    )
    rv = admin_client.post(f"/api/admin/ideal/status/{ideal_payment.id}/")
    assert rv.status_code == 200
    assert rv.json()["idealPayment"]["status"] == "canceled"
    assert rv.json()["mollieData"]
