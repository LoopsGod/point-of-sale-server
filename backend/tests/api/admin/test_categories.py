import pytest

from app.repository import categories as category_repo


@pytest.mark.usefixtures("categories")
def test_api_admin_categories(admin_client):
    r = admin_client.get("/api/admin/categories/")
    assert r.status_code == 200, r.json()
    assert len(r.json()["items"]) > 0
    assert r.json()["items"][0]
    assert r.json()["perPage"] > 0
    assert r.json()["page"] == 1
    assert r.json()["pageCount"] >= 1


def test_api_admin_category_create(admin_client, db_session):
    rv = admin_client.post(
        "/api/admin/categories/",
        json={"name": "New Catta G", "color": 1, "position": 1, "deleted": False},
    )

    assert rv.status_code == 201, rv.json()
    c = rv.json()

    all_categories = category_repo.get_all_with_ids(db_session, [c["id"]])
    assert len(all_categories) == 1
    assert all_categories[0].name == "New Catta G"
    assert all_categories[0].color == 1
    assert all_categories[0].position == 1
    assert all_categories[0].deleted is False


def test_api_admin_category_update(admin_client, db_session, categories):
    category_room_cans, _, __ = categories

    rv = admin_client.put(
        f"/api/admin/categories/{category_room_cans.id}/",
        json={
            "id": category_room_cans.id,
            "name": "Cans (edited)",
            "color": 1,
            "position": 2,
            "deleted": False,
        },
    )

    assert rv.status_code == 200, rv.json()
    assert category_room_cans.id == rv.json()["id"]

    db_session.refresh(category_room_cans)
    assert category_room_cans.name == "Cans (edited)"
    assert category_room_cans.color == 1
    assert category_room_cans.position == 2
    assert category_room_cans.deleted is False
