from datetime import datetime

import freezegun
from starlette.testclient import TestClient

from app.models import SlideProduct, Transaction


def test_slides_products(
    normal_client: TestClient, slide_products: dict[str, SlideProduct]
) -> None:
    rv = normal_client.get("/api/slides/products/")

    assert rv.status_code == 200

    products = rv.json()["products"]
    assert len(products) == 2


@freezegun.freeze_time(datetime(year=2022, month=12, day=15, hour=20, minute=12))
def test_slides_bpm(
    normal_client: TestClient, fixed_date_slide_product_order: Transaction
) -> None:
    rv = normal_client.get("/api/slides/bpm/")
    assert rv.status_code == 200

    bpm = rv.json()["bpm"]

    assert bpm > 0.0


@freezegun.freeze_time(datetime(year=2022, month=12, day=15, hour=20, minute=12))
def test_slides_active(
    normal_client: TestClient, fixed_date_slide_product_order: Transaction
) -> None:
    rv = normal_client.get("/api/slides/active/")
    assert rv.status_code == 200

    active = rv.json()["active"]

    assert active > 0


@freezegun.freeze_time(datetime(year=2022, month=12, day=15, hour=20, minute=12))
def test_slides_trends(
    normal_client: TestClient, fixed_date_slide_product_order: Transaction
) -> None:
    rv = normal_client.get("/api/slides/trends/")
    assert rv.status_code == 200

    trends = rv.json()["trends"]

    assert len(trends) > 0
