from datetime import datetime, timezone, timedelta

import pytest

from app.models import Order, SepaBatch, Transaction


def test_checkout_bar_direct_debit_and_event(
    db_session, bar_client, products, sepa_account
):
    product = products["bar_beer"]

    assert sepa_account.balance == 0

    r1 = bar_client.post(
        "/api/checkout/account/",
        json={
            "nonce": "123",
            "account_id": sepa_account.id,
            "products": [{"id": product.id, "amount": 1}],
        },
    )

    assert r1.status_code == 200, r1.json()

    db_session.refresh(sepa_account)
    assert sepa_account.balance != 0

    r2 = bar_client.delete(f"/api/transactions/{r1.json()['transaction_id']}/")

    assert r2.status_code == 200, r2.json()

    orders1 = (
        db_session.query(Order).filter(Order.account_id == product.ledger_id).all()
    )

    # Both product orders should cancel out
    assert sum(o.price for o in orders1) == 0

    orders2 = db_session.query(Order).filter(Order.account_id == sepa_account.id).all()

    # Both account orders should cancel out
    assert sum(o.price for o in orders2) == 0

    orders3 = db_session.query(Order).all()

    assert len(orders3) == 4

    db_session.refresh(sepa_account)

    assert sepa_account.balance == 0


@pytest.mark.usefixtures("sepa_config")
def test_delete_bounced_sepa_batch(db_session, admin_client, sepa_account):
    sepa_account.balance = -100
    db_session.add(sepa_account)
    db_session.commit()

    rv = admin_client.post(
        "/api/admin/sepa_batches/",
        json={
            "accounts": [sepa_account.id],
            "executionDate": (
                datetime.now(timezone.utc) + timedelta(days=-1)
            ).isoformat(),
            "description": "asdf",
        },
    )
    assert rv.status_code == 201, rv.json()

    batch = db_session.query(SepaBatch).one()

    rv = admin_client.post(f"/api/admin/sepa_batches/{batch.id}/execute/")
    assert rv.status_code == 204, rv.json()
    db_session.refresh(sepa_account)
    assert sepa_account.balance == 0

    t = db_session.query(Transaction).one()

    r2 = admin_client.delete(f"/api/admin/transactions/{t.id}/")
    assert r2.status_code == 200, r2.json()
    db_session.refresh(sepa_account)
    assert sepa_account.balance == -100
