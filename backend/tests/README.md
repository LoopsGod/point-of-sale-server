# Testing
## Database connection

In de backend we use SQLAlchemy to connect to the database. In a test, requests
are made using test_clients. A new separate database
session is made for in test usage, which will prevent assertions on uncommitted data.
Add the `db_session` as parameter to your test function to have access to a session

The `db_session` is unaware of any changes made during in a concurrent
transaction. This means models queried prior to a request are not automatically
updated with the new data. Use `db_session.refresh()`, `db_session.expire()`, or
`db_session.expire_all()` to signal the session that data needs to be queried
again. See [SQLAlchemy documentation](https://docs.sqlalchemy.org/en/13/orm/session_state_management.html#when-to-expire-or-refresh) for more on this behaviour.

Example:

```python
from app.models.account import Account

def test_something(db_session, normal_client):
    account = db_session.query(User).first()
    account.balance = 1000
    db_session.commit()
    rv = normal_client.get("/api/checkout/account/", json={...})
    assert rv.status_code == 200

    db_session.refresh(account)
    assert account.balance != 1000
```
