import math
from cmath import isclose
from datetime import date, timedelta
from decimal import Decimal
from pathlib import Path

import freezegun
import pytest
from lxml.etree import (
    fromstring,
    XMLSyntaxError,
    XMLParser,
    XML,
    XMLSchema,
    _Element,
    tostring,
)
from sqlalchemy.orm import Session

from app.dependencies import RequestContext
from app.models import Account, Product
from app.domains.exact.export import exact_transaction_generator_service
from app.domains.exact.export.exact_export import ExactExport
from app.models.checkout import CheckoutMethod
from tests.util.transactions import (
    _create_top_up,
    _create_account_purchase,
    _delete_transaction,
    _create_direct_purchase,
    _create_account_purchase_with_top_up,
)


@pytest.fixture
def export(
    db_session: Session,
    normal_account: Account,
    exact_finished_settings: None,
) -> ExactExport:
    # Same date as used in the util functions.
    export_date = date(2020, 9, 1)

    exact_export = ExactExport(
        date=export_date,
        division="storage_division",
        status="not_exported",
    )
    db_session.add(exact_export)
    db_session.commit()
    return exact_export


@pytest.fixture(scope="module")
def parser() -> XMLParser:
    repo_root = Path(__file__).resolve().parents[2]
    xsd_file = repo_root / "app" / "domains" / "exact" / "templates" / "eExact-XML.xsd"
    schema_root = XML(xsd_file.read_bytes())
    schema = XMLSchema(schema_root)
    return XMLParser(schema=schema)


def assert_balance(
    xml_tree: _Element,
    opening_balance: float,
    closing_balance: float,
) -> None:
    node = Decimal(xml_tree.findtext(".//OpeningBalance"))
    if node is None or not isclose(node, opening_balance):
        raise AssertionError(
            f"<OpeningBalance>{node}</OpeningBalance> != {opening_balance=}\n"
            f"{tostring(xml_tree).decode()}"
        )

    node = Decimal(xml_tree.findtext(".//ClosingBalance"))
    if node is None or not isclose(node, closing_balance):
        raise AssertionError(
            f"<ClosingBalance>{node}</ClosingBalance != {closing_balance=}\n"
            f"{tostring(xml_tree).decode()}"
        )

    diff = closing_balance - opening_balance

    nodes = xml_tree.findall(".//Value")
    if not math.isclose(diff, sum(Decimal(node.text) for node in nodes)):
        raise AssertionError(
            f"{closing_balance=} - {opening_balance=} should match transactions {diff=}"
            f"\n{tostring(xml_tree).decode()}"
        )


def assert_transaction_count(tree: _Element, num_transaction_line: int) -> None:
    nodes = tree.xpath("/eExact/GLTransactions/GLTransaction/GLTransactionLine")
    if len(nodes) != num_transaction_line:
        raise AssertionError(
            f"Expected {num_transaction_line} transaction lines present in XML:\n"
            f"{tostring(tree).decode()}"
        )


def assert_transaction_present(
    tree: _Element,
    ledger_account_code: str,
    description: str,
    expected_amount: float,
) -> None:
    xpath = "/eExact/GLTransactions/GLTransaction/GLTransactionLine"
    # Make it so text includes the description.
    description_node = tree.xpath(
        f'{xpath}/Description[contains(text(), "{description}")]'
    )

    if len(description_node) != 1:
        raise AssertionError(
            f"Expected single line with {description=} in XML:\n"
            f"{tostring(tree).decode()}"
        )
    gltransaction = description_node[0].getparent()
    amount = float(gltransaction.findtext("./Amount/Value"))
    if not math.isclose(amount, expected_amount):
        raise AssertionError(
            f"Expected {expected_amount=} for {description=} in XML:\n"
            f"{tostring(tree).decode()}"
        )

    glaccount = gltransaction.find("./GLAccount")
    if glaccount.attrib.get("code") != ledger_account_code:
        raise AssertionError(
            f"Expected {ledger_account_code=} on {description=} in XML:\n"
            f"{tostring(tree).decode()}"
        )


@pytest.mark.usefixtures("exact_finished_settings")
def test_exact_generated_xml_no_transactions(
    db_session: Session,
    normal_account: Account,
    export: ExactExport,
    parser: XMLParser,
) -> None:
    # No transactions happened yet.
    xml = exact_transaction_generator_service.generate_transaction_xml(
        RequestContext(db_session),
        export.date - timedelta(days=10),
    )
    # No GLTransactionLine in the XML is invalid.
    with pytest.raises(XMLSyntaxError):
        fromstring(xml, parser)


@pytest.mark.usefixtures("exact_finished_settings")
def test_exact_generated_xml_top_up_transactions_with_delete(
    db_session: Session,
    normal_account: Account,
    products: dict[str, Product],
    ledger_accounts: dict[str, Account],
    exact_income_ledgers: dict[CheckoutMethod, str],
    exact_product_ledgers: dict[str, str],
    export: ExactExport,
    parser: XMLParser,
) -> None:
    _create_top_up(db_session, normal_account)
    _create_account_purchase(
        db_session,
        normal_account,
        [
            products["room_cola"],
            products["room_cola"],
        ],
    )
    _create_account_purchase(
        db_session, normal_account, [products["room_cola"], products["room_cola"]]
    )
    t = _create_account_purchase(db_session, normal_account, [products["room_cola"]])
    with freezegun.freeze_time(t.created_at):
        _delete_transaction(db_session, t)

    xml = exact_transaction_generator_service.generate_transaction_xml(
        RequestContext(db_session),
        export.date,
    )
    tree = fromstring(xml, parser)
    assert_balance(tree, opening_balance=0, closing_balance=-8.0)
    assert_transaction_count(tree, num_transaction_line=3)
    assert_transaction_present(
        tree,
        ledger_account_code=exact_income_ledgers[CheckoutMethod.CASH],
        description="1x revenue - cash",
        expected_amount=-10,
    )
    assert_transaction_present(
        tree,
        ledger_account_code=exact_product_ledgers["room"],
        description="5x sold - Cola",
        expected_amount=2.5,
    )
    assert_transaction_present(
        tree,
        ledger_account_code=exact_product_ledgers["room"],
        description="1x refund - Cola",
        expected_amount=-0.5,
    )


@pytest.mark.usefixtures("exact_finished_settings")
def test_exact_generated_xml_top_up_transactions_with_delete_day_after(
    db_session: Session,
    normal_account: Account,
    products: dict[str, Product],
    ledger_accounts: dict[str, Account],
    exact_product_ledgers: dict[str, str],
    exact_income_ledgers: dict[CheckoutMethod, str],
    export: ExactExport,
    parser: XMLParser,
) -> None:
    _create_top_up(db_session, normal_account)
    t = _create_account_purchase(db_session, normal_account, [products["room_cola"]])
    with freezegun.freeze_time(t.created_at + timedelta(days=2)):
        _delete_transaction(db_session, t)

    xml = exact_transaction_generator_service.generate_transaction_xml(
        RequestContext(db_session),
        export.date,
    )
    tree = fromstring(xml, parser)
    assert_balance(tree, opening_balance=0, closing_balance=-9.5)
    assert_transaction_count(tree, num_transaction_line=2)
    assert_transaction_present(
        tree,
        ledger_account_code=exact_income_ledgers[CheckoutMethod.CASH],
        description="1x revenue - cash",
        expected_amount=-10,
    )
    assert_transaction_present(
        tree,
        ledger_account_code=exact_product_ledgers["room"],
        description="1x sold - Cola",
        expected_amount=0.5,
    )

    # we need to reload, as otherwise the transaction.order eager load
    # will have the order from 2020-09-01.`
    db_session.expire(t)

    # Check day after for the deletion order
    xml = exact_transaction_generator_service.generate_transaction_xml(
        RequestContext(db_session),
        export.date + timedelta(days=2),
    )
    tree = fromstring(xml, parser)
    assert_balance(tree, opening_balance=-9.5, closing_balance=-10)
    assert_transaction_count(tree, num_transaction_line=1)
    assert_transaction_present(
        tree,
        ledger_account_code=exact_product_ledgers["room"],
        description="1x refund - Cola",
        expected_amount=-0.5,
    )


@pytest.mark.usefixtures("exact_finished_settings")
def test_exact_generated_xml_top_up_transactions(
    db_session: Session,
    normal_account: Account,
    ledger_accounts: dict[str, Account],
    exact_product_ledgers: dict[str, str],
    exact_income_ledgers: dict[CheckoutMethod, str],
    products: dict[str, Product],
    export: ExactExport,
    parser: XMLParser,
) -> None:
    _create_top_up(db_session, normal_account, method=CheckoutMethod.IDEAL)
    _create_account_purchase(db_session, normal_account, [products["room_cola"]])
    _create_account_purchase(db_session, normal_account, [products["room_cola"]])

    xml = exact_transaction_generator_service.generate_transaction_xml(
        RequestContext(db_session),
        export.date,
    )
    tree = fromstring(xml, parser)
    assert_balance(tree, opening_balance=0, closing_balance=-9)
    assert_transaction_count(tree, num_transaction_line=2)
    assert_transaction_present(
        tree,
        ledger_account_code=exact_product_ledgers["room"],
        description="2x sold - Cola",
        expected_amount=1,
    )
    assert_transaction_present(
        tree,
        ledger_account_code=exact_income_ledgers[CheckoutMethod.IDEAL],
        description="1x revenue - ideal",
        expected_amount=-10,
    )


@pytest.mark.usefixtures("exact_finished_settings")
def test_exact_generated_xml_top_up_method_grouping(
    db_session: Session,
    normal_account: Account,
    ledger_accounts: dict[str, Account],
    exact_product_ledgers: dict[str, str],
    exact_income_ledgers: dict[CheckoutMethod, str],
    products: dict[str, Product],
    export: ExactExport,
    parser: XMLParser,
) -> None:
    _create_top_up(db_session, normal_account, amount=1000, method=CheckoutMethod.CASH)
    _create_top_up(db_session, normal_account, amount=2200, method=CheckoutMethod.CASH)
    _create_top_up(db_session, normal_account, amount=1730, method=CheckoutMethod.CARD)

    xml = exact_transaction_generator_service.generate_transaction_xml(
        RequestContext(db_session),
        export.date,
    )
    tree = fromstring(xml, parser)
    assert_balance(tree, opening_balance=0, closing_balance=-49.30)
    assert_transaction_count(tree, num_transaction_line=2)
    assert_transaction_present(
        tree,
        ledger_account_code=exact_income_ledgers[CheckoutMethod.CASH],
        description="2x revenue - cash",
        expected_amount=-32,
    )
    assert_transaction_present(
        tree,
        ledger_account_code=exact_income_ledgers[CheckoutMethod.CARD],
        description="1x revenue - card",
        expected_amount=-17.30,
    )


@pytest.mark.usefixtures("exact_finished_settings")
def test_exact_generated_xml_direct_product_purchases(
    db_session: Session,
    normal_account: Account,
    ledger_accounts: dict[str, Account],
    exact_product_ledgers: dict[str, str],
    exact_income_ledgers: dict[CheckoutMethod, str],
    products: dict[str, Product],
    export: ExactExport,
    parser: XMLParser,
) -> None:
    products["room_beer"].name = products["bar_beer"].name = "Beer"
    db_session.commit()

    _create_direct_purchase(
        db_session,
        products=[products["room_beer"], products["room_beer"]],
        method=CheckoutMethod.CARD,
    )
    _create_direct_purchase(
        db_session, products=[products["room_beer"]], method=CheckoutMethod.CARD
    )
    _create_direct_purchase(
        db_session, products=[products["room_beer"]], method=CheckoutMethod.CASH
    )
    _create_direct_purchase(
        db_session, products=[products["bar_beer"]], method=CheckoutMethod.CASH
    )

    xml = exact_transaction_generator_service.generate_transaction_xml(
        RequestContext(db_session),
        export.date,
    )
    tree = fromstring(xml, parser)
    assert_balance(tree, opening_balance=0, closing_balance=0)
    assert_transaction_count(tree, num_transaction_line=4)
    assert_transaction_present(
        tree,
        ledger_account_code=exact_income_ledgers[CheckoutMethod.CASH],
        description="2x revenue - cash",
        expected_amount=-1.90,
    )
    assert_transaction_present(
        tree,
        ledger_account_code=exact_income_ledgers[CheckoutMethod.CARD],
        description="2x revenue - card",
        expected_amount=-2.10,
    )
    assert_transaction_present(
        tree,
        ledger_account_code=exact_product_ledgers["room"],
        description="4x sold - Beer",
        expected_amount=2.80,
    )
    assert_transaction_present(
        tree,
        ledger_account_code=exact_product_ledgers["bar"],
        description="1x sold - Beer",
        expected_amount=1.20,
    )


@pytest.mark.usefixtures("exact_finished_settings")
def test_exact_generated_xml_top_up_delete(
    db_session: Session,
    normal_account: Account,
    products: dict[str, Product],
    ledger_accounts: dict[str, Account],
    exact_product_ledgers: dict[str, str],
    exact_income_ledgers: dict[CheckoutMethod, str],
    export: ExactExport,
    parser: XMLParser,
) -> None:
    t = _create_top_up(db_session, normal_account)
    with freezegun.freeze_time(t.created_at):
        _delete_transaction(db_session, t)

    xml = exact_transaction_generator_service.generate_transaction_xml(
        RequestContext(db_session),
        export.date,
    )
    tree = fromstring(xml, parser)
    assert_balance(tree, opening_balance=0, closing_balance=0)
    assert_transaction_count(tree, num_transaction_line=2)
    assert_transaction_present(
        tree,
        ledger_account_code=exact_income_ledgers[CheckoutMethod.CASH],
        description="1x revenue - cash",
        expected_amount=-10,
    )
    assert_transaction_present(
        tree,
        ledger_account_code=exact_income_ledgers[CheckoutMethod.CASH],
        description="1x revenue (deleted) - cash",
        expected_amount=10,
    )


@pytest.mark.usefixtures("exact_finished_settings")
def test_exact_generated_xml_single_top_up(
    db_session: Session,
    normal_account: Account,
    products: dict[str, Product],
    ledger_accounts: dict[str, Account],
    exact_product_ledgers: dict[str, str],
    exact_income_ledgers: dict[CheckoutMethod, str],
    export: ExactExport,
    parser: XMLParser,
) -> None:
    _create_top_up(db_session, normal_account)

    xml = exact_transaction_generator_service.generate_transaction_xml(
        RequestContext(db_session),
        export.date,
    )
    tree = fromstring(xml, parser)
    assert_balance(tree, opening_balance=0, closing_balance=-10)
    assert_transaction_count(tree, num_transaction_line=1)
    assert_transaction_present(
        tree,
        ledger_account_code=exact_income_ledgers[CheckoutMethod.CASH],
        description="1x revenue - cash",
        expected_amount=-10,
    )


@pytest.mark.usefixtures("exact_finished_settings")
def test_exact_generated_xml_direct_product_purchases_with_reason(
    db_session: Session,
    normal_account: Account,
    ledger_accounts: dict[str, Account],
    exact_product_ledgers: dict[str, str],
    exact_income_ledgers: dict[CheckoutMethod, str],
    products: dict[str, Product],
    export: ExactExport,
    parser: XMLParser,
) -> None:
    _create_direct_purchase(
        db_session,
        products=[products["room_beer"]],
        method=CheckoutMethod.OTHER,
        reason="reason1",
    )
    _create_direct_purchase(
        db_session,
        products=[products["room_beer"]],
        method=CheckoutMethod.OTHER,
        reason="reason1",
    )
    _create_direct_purchase(
        db_session,
        products=[products["bar_beer"]],
        method=CheckoutMethod.OTHER,
        reason="reason1",
    )
    _create_direct_purchase(
        db_session,
        products=[products["bar_beer"]],
        method=CheckoutMethod.OTHER,
        reason="reason2",
    )

    xml = exact_transaction_generator_service.generate_transaction_xml(
        RequestContext(db_session),
        export.date,
    )
    tree = fromstring(xml, parser)
    assert_balance(tree, opening_balance=0, closing_balance=0)
    assert_transaction_count(tree, num_transaction_line=4)
    assert_transaction_present(
        tree,
        ledger_account_code=exact_income_ledgers[CheckoutMethod.OTHER],
        description="3x revenue - other - reason1",
        expected_amount=-2.6,
    )
    assert_transaction_present(
        tree,
        ledger_account_code=exact_income_ledgers[CheckoutMethod.OTHER],
        description="1x revenue - other - reason2",
        expected_amount=-1.20,
    )
    assert_transaction_present(
        tree,
        ledger_account_code=exact_product_ledgers["room"],
        description="2x sold - Bottled Beer",
        expected_amount=1.40,
    )
    assert_transaction_present(
        tree,
        ledger_account_code=exact_product_ledgers["bar"],
        description="2x sold - Tap beer",
        expected_amount=2.40,
    )


@pytest.mark.usefixtures("exact_finished_settings")
def test_exact_generated_xml_single_negative_top_up(
    db_session: Session,
    normal_account: Account,
    products: dict[str, Product],
    ledger_accounts: dict[str, Account],
    exact_product_ledgers: dict[str, str],
    exact_income_ledgers: dict[CheckoutMethod, str],
    export: ExactExport,
    parser: XMLParser,
) -> None:
    _create_top_up(
        db_session,
        normal_account,
        amount=-1000,
        method=CheckoutMethod.OTHER,
        reason="unpaid dinner",
    )

    xml = exact_transaction_generator_service.generate_transaction_xml(
        RequestContext(db_session),
        export.date,
    )
    tree = fromstring(xml, parser)
    assert_balance(tree, opening_balance=0, closing_balance=10)
    assert_transaction_count(tree, num_transaction_line=1)
    assert_transaction_present(
        tree,
        ledger_account_code=exact_income_ledgers[CheckoutMethod.OTHER],
        description="1x revenue - other - unpaid dinner",
        expected_amount=10,
    )


@pytest.mark.usefixtures("exact_finished_settings")
def test_exact_generated_xml_debit_enabled_account(
    db_session: Session,
    sepa_account: Account,
    products: dict[str, Product],
    ledger_accounts: dict[str, Account],
    exact_product_ledgers: dict[str, str],
    exact_income_ledgers: dict[CheckoutMethod, str],
    export: ExactExport,
    parser: XMLParser,
) -> None:
    _create_account_purchase(db_session, sepa_account, [products["room_cola"]])

    xml = exact_transaction_generator_service.generate_transaction_xml(
        RequestContext(db_session),
        export.date,
    )
    tree = fromstring(xml, parser)
    assert_balance(tree, opening_balance=0, closing_balance=0.5)
    assert_transaction_count(tree, num_transaction_line=1)
    assert_transaction_present(
        tree,
        ledger_account_code=exact_product_ledgers["room"],
        description="1x sold - Cola",
        expected_amount=0.5,
    )


@pytest.mark.usefixtures("exact_finished_settings")
def test_exact_generated_xml_top_up_purchase_single_transaction(
    db_session: Session,
    sepa_account: Account,
    products: dict[str, Product],
    ledger_accounts: dict[str, Account],
    exact_product_ledgers: dict[str, str],
    exact_income_ledgers: dict[CheckoutMethod, str],
    export: ExactExport,
    parser: XMLParser,
) -> None:
    _create_account_purchase_with_top_up(
        db_session,
        sepa_account,
        [products["room_cola"]],
        500,
    )

    xml = exact_transaction_generator_service.generate_transaction_xml(
        RequestContext(db_session),
        export.date,
    )
    tree = fromstring(xml, parser)
    assert_balance(tree, opening_balance=0, closing_balance=-4.5)
    assert_transaction_count(tree, num_transaction_line=2)
    assert_transaction_present(
        tree,
        ledger_account_code=exact_product_ledgers["room"],
        description="1x sold - Cola",
        expected_amount=0.5,
    )

    assert_transaction_present(
        tree,
        ledger_account_code=exact_income_ledgers[CheckoutMethod.CASH],
        description="1x revenue - cash",
        expected_amount=-5,
    )
