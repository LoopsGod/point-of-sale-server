import pytest
from click.testing import CliRunner
from sqlalchemy import select
from sqlalchemy.orm import Session

from app.__main__ import create_user, make_admin, enable_tfa, make_fixtures
from app.models import User, LoginToken


@pytest.mark.parametrize("admin", [["--admin"], ["--no-admin"]])
def test_create_user(admin: list[str], db_session: Session) -> None:
    result = CliRunner().invoke(
        create_user,
        args=["--username", "kelder", "--password", "kelder", *admin],
        catch_exceptions=False,
    )
    assert result.exit_code == 0, result.output

    user = db_session.scalar(select(User))
    assert user.username == "kelder"
    if admin == ["--admin"]:
        assert user.is_admin
        assert user.long_login
    elif admin == ["--no-admin"]:
        assert not user.is_admin
        assert not user.long_login
    else:
        pytest.fail("Unknown option")


def test_make_admin_non_existant() -> None:
    result = CliRunner().invoke(
        make_admin, args=["--username", "maico@example.com"], catch_exceptions=False
    )
    assert result.exit_code == 0, result.output
    assert "Did not find user 'maico@example.com'." in result.output, result.output


def test_make_admin(admin_user: User, db_session: Session) -> None:
    admin_user.is_admin = False
    db_session.commit()
    result = CliRunner().invoke(
        make_admin, args=["--username", admin_user.username], catch_exceptions=False
    )
    assert result.exit_code == 0, result.output
    db_session.refresh(admin_user)
    assert admin_user.is_admin


def test_enable_tfa(
    admin_token: LoginToken, admin_user: User, db_session: Session
) -> None:
    admin_token.tfa_enabled = False
    db_session.commit()
    result = CliRunner().invoke(
        enable_tfa, args=["--username", admin_user.username], catch_exceptions=False
    )
    assert result.exit_code == 0, result.output
    db_session.refresh(admin_token)
    assert admin_token.tfa_enabled


def test_make_fixtures() -> None:
    result = CliRunner().invoke(make_fixtures, catch_exceptions=False)
    assert result.exit_code == 0, result.output
    assert "Created database entries" in result.output, result.output
