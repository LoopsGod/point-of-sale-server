import logging
import os
from collections.abc import Iterator
from datetime import datetime

import pytest
import requests_mock
from _pytest.fixtures import SubRequest
from celery.app.base import Celery
from requests_mock.mocker import Mocker
from sqlalchemy.orm.session import Session

from config import SQLALCHEMY_DATABASE_TEST_URI

os.environ["SQLALCHEMY_DATABASE_URI"] = os.environ.get(
    "SQLALCHEMY_DATABASE_TEST_URI", SQLALCHEMY_DATABASE_TEST_URI
)
os.environ["BROKER_URL"] = os.environ.get("BROKER_TEST_URL", "redis://redis")

from app import cache  # noqa: E402
from app.models.database import (  # noqa: E402
    engine,
    mapper_registry,
    SessionLocal,
)
from app.models import User, LoginToken  # noqa: E402
from app.models.register import Register  # noqa: E402

from sqlalchemy import inspect  # noqa: E402
from sqlalchemy.orm import close_all_sessions  # noqa: E402

collect_ignore = ["node_modules"]
pytest_plugins = [
    "tests.fixtures.clients",
    "tests.fixtures.account",
    "tests.fixtures.products",
    "tests.fixtures.categories",
    "tests.fixtures.registers",
    "tests.fixtures.users",
    "tests.fixtures.sepa",
    "tests.fixtures.exact",
    "tests.fixtures.orders",
    "tests.fixtures.slideproduct",
    "tests.fixtures.settings",
]

logging.getLogger("app").propagate = True


@pytest.fixture(scope="session")
def db_tables() -> Iterator[None]:
    mapper_registry.metadata.create_all(engine)

    yield

    mapper_registry.metadata.drop_all(engine)


@pytest.fixture(autouse=True, scope="function")
def db_empty_tables(db_tables: None):
    """Session-wide test database."""

    inspector = inspect(engine)
    close_all_sessions()
    table_names = set(inspector.get_table_names())
    with SessionLocal.begin() as db_session:
        for table in reversed(mapper_registry.metadata.sorted_tables):
            if table.name in table_names:
                db_session.execute(table.delete())


@pytest.fixture(scope="function")
def db_session() -> Iterator[Session]:
    """
    Creates a new database connection for tests.

    This connection is completely separate from the connection
    our API uses to communicate with the database.
    """
    session = SessionLocal()

    yield session

    session.close()


@pytest.fixture(autouse=True)
def worker() -> Celery:
    from app import worker as _worker

    assert cache.clear()
    _worker.config_from_object(
        {
            "result_backend": "rpc://",
            "task_serializer": "pickle",
            "accept_content": ["pickle", "json"],
            "result_serializer": "pickle",
            "broker_url": os.environ["BROKER_URL"],
            "broker_transport_options": {"max_retries": 3},
            "task_routes": {"*": "celery_test_queue"},
        }
    )
    _worker.control.purge()
    return _worker


@pytest.fixture(autouse=True, scope="function")
def requests_mocker() -> Iterator[Mocker]:
    with requests_mock.Mocker() as m:
        yield m


@pytest.fixture(params=["admin", "admin_self"])
def admin_user(
    request: SubRequest,
    db_session: Session,
    registers: dict[str, Register],
    password: dict[str, str],
) -> User:
    user = User(username="admin")
    user.password = password["hash"]
    user.is_admin = True
    user.long_login = True
    user.register_id = registers[request.param].id
    db_session.add(user)
    db_session.flush()
    return user


@pytest.fixture
def admin_token(db_session: Session, admin_user: User) -> LoginToken:
    token = LoginToken(user=admin_user, token="SOMEACCESSTOKEN", tfa_enabled=True)
    token.created_at = datetime.now()
    db_session.add(token)
    db_session.commit()
    return token
