from sqlalchemy import Column
from app.models.account_type import AccountType
from app.repository.util.filters import BooleanFilter, FilterGeneric
from sqlalchemy.sql.elements import ColumnElement


class IncomeLedgerFilter(BooleanFilter):
    def filter(
        self, query: FilterGeneric, column: Column | ColumnElement
    ) -> FilterGeneric:
        if self.as_bool:
            return query.filter(column == AccountType.INCOME_LEDGER.value)

        return query.filter(column != AccountType.INCOME_LEDGER.value)
