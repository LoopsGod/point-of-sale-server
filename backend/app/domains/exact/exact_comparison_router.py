from datetime import date, datetime
from fastapi import APIRouter, Depends, Query
from pydantic import BaseModel

from app.dependencies import UserRequestContext, admin_required
from app.domains.exact.api.exact_api_session import ExactContext, exact_api
from app.domains.exact.comparison import exact_comparison_service
from app.domains.exact import exact_tasks as exact_tasks
from app.models.account import Account, AccountData
from app.models.checkout import CheckoutMethod
from app.repository import orders as orders_repo
from app.repository.util.filters import DateTimeFilter, LiteralEqualsFilter
from app.service import checkout_service
from app.views.exceptions import (
    ResourceConflictException,
    UnprocessableEntityException,
    ValidationException,
)

router = APIRouter()


class ExactComparisonResponse(BaseModel):
    account: AccountData
    startDate: date
    endDate: date
    dailyOrders: list[orders_repo.DailyOrder]
    dailyTransactions: list[exact_comparison_service.DailyTransaction]


@router.get("/", response_model=ExactComparisonResponse)
def exact_comparison_get(
    context: UserRequestContext = Depends(admin_required),
    exact_context: ExactContext = Depends(exact_api),
    date_filter: DateTimeFilter = Query(alias="dateFilter"),
    account_filter: LiteralEqualsFilter | None = Query(None, alias="accountFilter"),
) -> ExactComparisonResponse:
    if date_filter.end is None:
        date_filter.end = datetime.combine(
            date(date_filter.start.year, 12, 31), datetime.max.time()
        )

    card_account_id = checkout_service.get_account_for_checkout_method(
        context.db_session, CheckoutMethod.CARD
    )
    if account_filter is None:
        account_filter = LiteralEqualsFilter(card_account_id)

    account_id = account_filter.value
    if type(account_id) is not int:
        raise ValidationException("accountfilter_must_be_integer")

    account = context.db_session.get(Account, account_id)
    if account is None:
        raise UnprocessableEntityException("account_not_found")

    if account.exact_ledger is None:
        raise ResourceConflictException("account_ledger_code_not_set")

    ledger_code = account.exact_ledger.exact_ledger_code

    #  We shift the lines by 1 day because card transactions get
    #  transfered one day later
    shift_lines = account_id == card_account_id

    daily_transactions = exact_comparison_service.compare_orders_to_exact(
        context,
        exact_context.api,
        date_filter.start.date(),
        date_filter.end.date(),
        ledger_code,
        shift_lines,
    )

    daily_orders = orders_repo.get_daily_orders_for_account(
        context.db_session, date_filter, account_filter
    )
    return ExactComparisonResponse(
        account=account.to_pydantic(),
        startDate=date_filter.start.astimezone(),
        endDate=date_filter.end.astimezone(),
        dailyOrders=daily_orders,
        dailyTransactions=daily_transactions,
    )
