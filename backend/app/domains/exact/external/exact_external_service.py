from datetime import date
from typing import Literal, TypedDict

from sqlalchemy.orm import Session

from app.service import setting_service
from app.domains.exact.api.exact_api import ExactApi


class ExactDivision(TypedDict):
    code: str
    name: str


class ExactJournal(TypedDict):
    code: str
    description: str
    account_code: str | None
    account_description: str | None
    account_type: int | None


class ExactLedger(TypedDict):
    id: str
    code: str
    description: str


class ExactCostCenter(TypedDict):
    code: str
    description: str


class ExactVat(TypedDict):
    code: str
    description: str
    percentage: float
    type: Literal["E", "I", "N", "B"]


def set_token(api: ExactApi, code: str) -> None:
    """Update the access- and refresh token based on authentication code."""
    api.request_token(code)


def get_divisions(api: ExactApi) -> tuple[list[ExactDivision], str]:
    """
    Returns a tuple with the current selected "division" (administration)
    and a key/value dict where the key is the 'code' and value the name of
    the administration
    """
    division_choices, current_division = api.get_divisions()

    divisions = [
        ExactDivision(code=key, name=division_choices[key]) for key in division_choices
    ]

    return divisions, current_division


def check_division_exists(api: ExactApi, division: str) -> None:
    api.check_division_exists(division)


def get_ledgers(api: ExactApi, filter: str = "IsBlocked eq false") -> list[ExactLedger]:
    """Gets all ledgers in the current administration"""
    ledgers = api.json_request(
        "GET",
        "financial/GLAccounts",
        filter=filter,
        select="ID,Code,Description",
    )

    return [
        ExactLedger(
            id=ledger["ID"], code=ledger["Code"], description=ledger["Description"]
        )
        for ledger in ledgers
    ]


def get_cost_centers(api: ExactApi) -> list[ExactCostCenter]:
    """Gets all cost centers in the current administration"""
    cost_centers = api.json_request(
        "GET",
        "hrm/Costcenters",
        filter="Active eq true",
        select="Code,Description",
    )
    return [
        ExactCostCenter(code=c["Code"].strip(), description=c["Description"])
        for c in cost_centers
    ]


def get_vat_codes(api: ExactApi) -> list[ExactVat]:
    """Gets all vat codes in the current administration"""
    vat_codes = api.json_request(
        "GET",
        "vat/VATCodes",
        filter="IsBlocked eq false",
        select="Code,Description,Percentage,Type",
    )
    return [
        ExactVat(
            code=v["Code"].strip(),
            description=v["Description"],
            percentage=v["Percentage"],
            type=v["Type"],
        )
        for v in vat_codes
    ]


def get_journals(api: ExactApi) -> list[ExactJournal]:
    journals = api.json_request(
        "GET",
        "financial/Journals",
        select="Code, Description, GLAccountCode, "
        "GLAccountDescription, GLAccountType",
    )

    return [
        ExactJournal(
            code=j["Code"],
            description=j["Description"],
            account_code=j["GLAccountCode"],
            account_description=j["GLAccountDescription"],
            account_type=j["GLAccountType"],
        )
        for j in journals
    ]


def get_booking_id_for_date(
    db_session: Session, api: ExactApi, date: date
) -> int | None:
    journal_code = setting_service.get_db_settings(db_session).exact_journal_pos_code
    bookings = api.json_request(
        "GET",
        "financialtransaction/TransactionLines",
        select="EntryNumber,Date,Description",
        filter=f"JournalCode eq '{journal_code}' and"
        f" Date eq datetime'{date.isoformat()}'",
        top="1",
    )
    if bookings:
        return bookings[0]["EntryNumber"]
    return None


def booking_id_exists(api: ExactApi, division: str, booking_id: int) -> bool:
    """Gets a booking. I.e. '19200010', returns a list with 0 or 1 item."""
    # Overwrite the division with the specific division.
    api.division = division
    bookings = api.json_request(
        "GET",
        "financialtransaction/TransactionLines",
        filter=f"EntryNumber eq {booking_id}",
        top="1",
    )

    return bool(bookings)
