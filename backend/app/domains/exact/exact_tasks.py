import logging

from requests import HTTPError

from app import worker
from app.dependencies import RequestContext
from app.domains.exact.api.exact_api_session import get_exact_api_for_session
from app.models.database import SessionLocal
from app.domains.exact.external import (
    exact_external_service,
    exact_external_transactions_service,
)
from app.domains.exact.export import (
    exact_export_db_repo,
    exact_transaction_generator_service,
)
from app.domains.exact.api.exact_api import ExactApiException
from app.views.exceptions import (
    ValidationException,
    ApplicationException,
    ServiceUnavailableException,
)

_logger = logging.getLogger(__name__)


@worker.task
def export_transactions(exact_export_id: int) -> None:
    with SessionLocal() as db_session:
        api = get_exact_api_for_session(db_session)
        export = exact_export_db_repo.get_export_by_id(db_session, exact_export_id)

        context = RequestContext(db_session)

        try:
            _logger.info("Generating transactions xml.")
            xml = exact_transaction_generator_service.generate_transaction_xml(
                context, export.date
            )
        except (TypeError, ValidationException, ApplicationException) as e:
            _logger.warning(
                f"Could not generate transaction XML. "
                f"Export_id={export.id}, "
                f"Error:\n{str(e)}"
            )
            export.status = "unexported"
            exact_export_db_repo.save_exact_export(db_session, export)
            return

        try:
            if exact_external_service.get_booking_id_for_date(
                db_session, api, export.date
            ):
                _logger.error(
                    "Could not export %s (export_id=%d), booking exists.",
                    export.date.isoformat(),
                    export.id,
                )
                export.status = "unexported"
                exact_export_db_repo.save_exact_export(db_session, export)
                return
            #  This request may take ~30 seconds
            _logger.info("Uploading transactions xml.")
            response_xml = exact_external_transactions_service.upload_gl_transactions(
                db_session, api, xml
            )
        except (
            TypeError,
            HTTPError,
            ServiceUnavailableException,
        ) as e:
            #  All these errors occur before the upload is done.
            #  Nothing is actually uploaded so we aren't in an invalid state.
            _logger.warning(
                f"Could not export to Exact."
                f"Export_id={export.id}, "
                f"Error:\n{str(e)}"
            )
            export.status = "unexported"
            exact_export_db_repo.save_exact_export(db_session, export)
            return
        except ExactApiException as e:
            _logger.warning(
                f"Exact API returned error. "
                f"Export_id={export.id}, "
                f"Error:\n{repr(e)}"
            )
            export.status = "error"
            exact_export_db_repo.save_exact_export(db_session, export)
            return

        try:
            _logger.info("Parsing Exact response xml.")
            messages = exact_transaction_generator_service.parse_upload_status_xml(
                response_xml
            )
            _logger.debug("response XML: %s", str(response_xml))
        except NameError as e:
            #  Unexpected response from the Exact API.
            #  We don't know what happened.
            _logger.exception(
                f"Could not parse Exact xml api response.\n"
                f"You should manually check Exact to see "
                f"if the data was exported. Delete the booking"
                f"if it is.\n"
                f"Export_id={export.id}, "
                f"Error:\n{str(e)}\n"
                f"Response XML:\n{str(response_xml)}"
            )
            export.status = "error"
            exact_export_db_repo.save_exact_export(db_session, export)
            return

        if isinstance(export.messages, list):
            #  This ExactExport already has old messages.
            export.messages.extend(messages)
        else:
            export.messages = messages

        msg = exact_transaction_generator_service.export_messages_get_node(
            "GLTransaction", messages
        )

        if msg is not None and msg.data:
            export.status = "exported"
            export.data = msg.data

            _logger.info(f"Export done. Booking: {msg.data}")
        else:
            _logger.error(
                "Export failed."
                "No data in Exact xml response. "
                f"Export_id={export.id}, "
                f"Response XML:\n{str(response_xml)}"
            )
            export.status = "error"

        exact_export_db_repo.save_exact_export(db_session, export)
