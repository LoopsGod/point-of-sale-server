from datetime import timedelta, date
from typing import Sequence, TypedDict
from pydantic import BaseModel
from sqlalchemy.orm import Session
from app.models import Account
from app.models.checkout import CheckoutMethod

from app.domains.exact.external import (
    exact_external_service,
    exact_external_transactions_service,
)
from app.domains.exact.external.exact_external_transactions_service import (
    TransactionLine,
)
from app.domains.exact.api.exact_api import ExactApi
from app.domains.exact.comparison.bank_transaction import BankTransaction
from app.dependencies import UserRequestContext
from app.repository import bank_transaction_repo
from app.service import setting_service
from app.views.exceptions import ResourceConflictException
from app.service.checkout_service import get_account_for_checkout_method


class DateShiftedTransactionLine(TypedDict):
    #  Does not have financial period since it does not make sense when a line has been
    #  shifted to a different month.
    financial_year: int
    amount_default_currency: float
    date: date


class DateGroupedTransactionLine(DateShiftedTransactionLine):
    financial_period: int


def compare_bank_transactions_to_exact(
    context: UserRequestContext, api: ExactApi, year: int
) -> tuple[date, date] | tuple[None, None]:
    lines = _get_card_transactions(context.db_session, api, year)

    if not len(lines):
        return None, None

    multiplied_lines = _multiply_transaction_lines_by_factor(lines, -1)
    grouped_lines = _group_transaction_lines_by_date(multiplied_lines)
    #  We shift the lines by 1 day because card transactions get
    #  transfered one day later
    shifted_lines = _shift_grouped_transaction_lines_by_one_day(grouped_lines)
    bank_transactions = (
        _grouped_transaction_lines_to_continious_list_of_bank_transactions(
            shifted_lines
        )
    )

    first_date = min(bank_transactions, key=lambda t: t.date).date
    last_date = max(bank_transactions, key=lambda t: t.date).date

    bank_transaction_repo.replace_bank_transactions(
        context, first_date, last_date, bank_transactions
    )

    return first_date, last_date


class DailyTransaction(BaseModel):
    date: date
    amount: int


def compare_orders_to_exact(
    context: UserRequestContext,
    api: ExactApi,
    start_date: date,
    end_date: date,
    ledger_code: str,
    shift_lines: bool = False,
) -> Sequence[DailyTransaction]:
    lines = _get_transaction_lines_for_ledger_code(
        context.db_session, api, start_date, end_date, ledger_code
    )

    if not len(lines):
        return []

    multiplied_lines = _multiply_transaction_lines_by_factor(lines, -1)
    grouped_lines = _group_transaction_lines_by_date(multiplied_lines)

    if shift_lines:
        return _grouped_transaction_lines_to_continious_list_of_daily_transactions(
            _shift_grouped_transaction_lines_by_one_day(
                grouped_lines,
            ),
            start_date,
            end_date,
        )
    else:
        return _grouped_transaction_lines_to_continious_list_of_daily_transactions(
            grouped_lines, start_date, end_date
        )


def _grouped_transaction_lines_to_continious_list_of_daily_transactions(
    lines: Sequence[DateShiftedTransactionLine],
    first_date: date | None = None,
    last_date: date | None = None,
) -> Sequence[DailyTransaction]:
    last_date = last_date or max(lines, key=lambda line: line["date"])["date"]

    # If no date is given, always start on the first day of the year
    first_date = first_date or last_date.replace(day=1, month=1)

    lines_by_date = {line["date"]: line for line in lines}

    output = []
    for i in range((last_date - first_date).days + 1):
        # Loop over all days to check if we are missing any dates
        # A missing date means there were no transactions
        d = first_date + timedelta(days=i)

        amount = (
            lines_by_date[d]["amount_default_currency"] if d in lines_by_date else 0
        )

        cents = int(amount * 100)
        output.append(DailyTransaction(date=d, amount=cents))

    return output


def _grouped_transaction_lines_to_continious_list_of_bank_transactions(
    lines: Sequence[DateGroupedTransactionLine] | Sequence[DateShiftedTransactionLine],
) -> Sequence[BankTransaction]:
    daily = _grouped_transaction_lines_to_continious_list_of_daily_transactions(lines)

    #  We don't know the 'count' of transactions that make up the amount
    return [BankTransaction(date=d.date, amount=d.amount, count=1) for d in daily]


def _get_transaction_lines_for_ledger_code(
    db_session: Session,
    api: ExactApi,
    start_date: date,
    end_date: date,
    ledger_code: str,
) -> list[TransactionLine]:
    """Gets transactions transfered on a given account in a given year.

    This function first uses the Exact api to get the GUID for this account.
    This is neccesary because the bulk transaction lines api cannot filter on code,
    only on GUID (eventhough the documentation says it can)."""

    db_settings = setting_service.get_db_settings(db_session)
    pos_journal_code = db_settings.exact_journal_pos_code

    card_ledger_info = exact_external_service.get_ledgers(
        api, f"Code eq '{ledger_code}'"
    )

    if len(card_ledger_info) != 1:
        raise ResourceConflictException("not_precisely_one_card_ledger")

    card_ledger_guid = card_ledger_info[0]["id"]

    #  It filters `JournalCode != pos_journal_code` because that would retrieve
    #  thousands of lines. That data is already contained in this system anyhow.
    card_transactions = (
        exact_external_transactions_service.get_bulk_financial_transactions_lines(
            api,
            filter=f"GLAccount eq guid'{card_ledger_guid}'"
            + f" and JournalCode ne '{pos_journal_code}'"
            + f" and Date gt datetime'{start_date.isoformat()}'"
            + f" and Date lt datetime'{end_date.isoformat()}'",
        )
    )

    return card_transactions


def _get_card_transactions(
    db_session: Session, api: ExactApi, year: int
) -> list[TransactionLine]:
    """Gets all card transactions transfered in a given year."""

    card_ledger_account_id = get_account_for_checkout_method(
        db_session, CheckoutMethod.CARD
    )
    card_ledger_account = db_session.get(Account, card_ledger_account_id)

    if card_ledger_account is None or card_ledger_account.exact_ledger is None:
        raise ResourceConflictException("card_ledger_account_not_set")

    card_ledger_code = card_ledger_account.exact_ledger.exact_ledger_code

    return _get_transaction_lines_for_ledger_code(
        db_session, api, date(year, 1, 1), date(year, 12, 31), card_ledger_code
    )


def _group_transaction_lines_by_date(
    lines: Sequence[TransactionLine] | Sequence[DateGroupedTransactionLine],
) -> Sequence[DateGroupedTransactionLine]:
    grouped_lines: dict[date, DateGroupedTransactionLine] = {}

    for line in lines:
        curr_date = line["date"]
        amount = line["amount_default_currency"]

        if curr_date not in grouped_lines:
            grouped_lines[curr_date] = DateGroupedTransactionLine(
                financial_period=line["financial_period"],
                financial_year=line["financial_year"],
                date=line["date"],
                amount_default_currency=0,
            )

        grouped_lines[curr_date]["amount_default_currency"] += amount

    return sorted(grouped_lines.values(), key=lambda line: line["date"])


def _shift_grouped_transaction_lines_by_one_day(
    lines: Sequence[DateGroupedTransactionLine],
) -> Sequence[DateShiftedTransactionLine]:
    shifted_lines: list[DateGroupedTransactionLine] = []

    for line in lines:
        line_copy = line.copy()

        if not (line_copy["date"].day == 1 and line_copy["date"].month == 1):
            #  Only shift if we do not change year by doing this
            line_copy["date"] = line_copy["date"] - timedelta(days=1)

        shifted_lines.append(line_copy)

    #  Group the lines again because we can have multiple 01/01 after shifting
    return [
        DateShiftedTransactionLine(
            financial_year=line["financial_year"],
            amount_default_currency=line["amount_default_currency"],
            date=line["date"],
        )
        for line in _group_transaction_lines_by_date(shifted_lines)
    ]


def _multiply_transaction_lines_by_factor(
    lines: list[TransactionLine], factor: int | float
) -> list[TransactionLine]:
    multiplied_lines = []

    for line in lines:
        line_copy = line.copy()
        line_copy["amount_default_currency"] = (
            line_copy["amount_default_currency"] * factor
        )
        multiplied_lines.append(line_copy)

    return multiplied_lines
