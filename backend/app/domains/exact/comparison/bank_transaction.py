from datetime import date, datetime
from typing import Any

import pydantic
from sqlalchemy import Column, DateTime, Integer
from sqlalchemy.orm import Mapped

from app.models._base import Base
from app.models.database import Model

# This file has a legacy name, as we originally used to compare transactions in PoS to
# a transaction csv exported from our bank account. We now use the Exact API instead.


class BankTransactionData(pydantic.BaseModel):
    date: date
    count: int
    amount: int


class BankTransaction(Model, Base):
    __tablename__ = "bank_transaction"

    date: Mapped[datetime] = Column(DateTime, unique=True, nullable=False)  # type: ignore[assignment]  # noqa: E501
    count: Mapped[int] = Column(Integer, nullable=False)  # type: ignore[assignment]  # noqa: E501
    amount: Mapped[int] = Column(Integer, nullable=False)  # type: ignore[assignment]  # noqa: E501

    def serialize(self) -> dict[str, Any]:
        return {"date": self.date, "count": self.count, "amount": self.amount}

    def to_pydantic(self) -> BankTransactionData:
        return BankTransactionData(date=self.date, count=self.count, amount=self.amount)
