from datetime import date
from http import HTTPStatus

import pydantic
from fastapi import APIRouter, Depends, Query, Response
from pydantic import BaseModel

from app.dependencies import UserRequestContext, admin_required
from app.domains.exact.api.exact_api_session import (
    ExactContext,
    exact_api,
    exact_api_export_ready,
    get_exact_api_for_session,
)
from app.domains.exact.export.exact_export import (
    ExactExportData,
    ExactExportStatusFilter,
)
from app.domains.exact.export import (
    exact_export_db_repo,
    exact_transaction_generator_service,
)
from app.repository.util.filters import (
    DateTimeFilter,
    LiteralEqualsFilter,
    NumberFilter,
)
from app.repository.util.pagination import (
    PageParameters,
    PaginatedResponse,
    SearchParameters,
    SortParameters,
    page_sort_parameters,
)
from app.domains.exact.setup import exact_setup_service
from app.domains.exact.export import exact_export_service
from app.domains.exact import exact_tasks as exact_tasks
from app.views.exceptions import ValidationException


router = APIRouter()


class YearMonthDay(pydantic.BaseModel):
    year: int
    month: int
    day: int

    def to_date(self) -> date:
        return date(year=self.year, month=self.month, day=self.day)


class ExportIdResponse(BaseModel):
    exportId: int


def get_export_date(params: YearMonthDay = Depends()) -> date:
    return params.to_date()


@router.post(path="/", response_model=ExportIdResponse)
def api_admin_exact_export_gltransactions(
    response: Response,
    export_date: date = Depends(get_export_date),
    context: UserRequestContext = Depends(admin_required),
    _exact_context: ExactContext = Depends(exact_api_export_ready),
) -> ExportIdResponse:
    export = exact_export_db_repo.get_export_by_date(context.db_session, export_date)
    if export is not None:
        if export.status == "exported" or export.status == "pending":
            #  Don't reexport and don't export while waiting.
            return ExportIdResponse(exportId=export.id)

        export.status = "pending"
        exact_export_db_repo.save_exact_export(context.db_session, export)
    else:
        export = exact_export_db_repo.create_exact_export(
            context.db_session, export_date
        )

    # Generate the xml sync, this will raise errors if there are unset accounts
    # or settings missing.
    exact_transaction_generator_service.generate_transaction_xml(context, export.date)

    exact_tasks.export_transactions.delay(exact_export_id=export.id)
    response.status_code = HTTPStatus.ACCEPTED
    return ExportIdResponse(exportId=export.id)


@router.delete("/{export_id}/", response_model=ExactExportData)
def api_admin_exact_gltransactions_delete(
    export_id: int,
    context: UserRequestContext = Depends(admin_required),
    exact_context: ExactContext = Depends(exact_api),
) -> ExactExportData:
    export = exact_export_service.delete_export_if_booking_does_not_exist(
        context, exact_context.api, export_id
    )
    return export.to_pydantic()


class ExactReadyResponse(BaseModel):
    setupFinished: bool
    allLedgersSet: bool


@router.get("/ready/", response_model=ExactReadyResponse)
def api_admin_exact_export_ready(
    context: UserRequestContext = Depends(admin_required),
) -> ExactReadyResponse:
    try:
        exact_api = get_exact_api_for_session(context.db_session)
    except ValidationException:
        return ExactReadyResponse(setupFinished=False, allLedgersSet=False)

    ready = exact_setup_service.can_export(context.db_session, exact_api)
    return ExactReadyResponse(
        setupFinished=ready.setup_finished
        and ready.journal_code_set
        and ready.journal_description_set,
        allLedgersSet=ready.all_ledgers_set and ready.all_ledgers_valid,
    )


@router.get("/", response_model=PaginatedResponse[ExactExportData])
def api_admin_exact_export(
    context: UserRequestContext = Depends(admin_required),
    page_params: PageParameters = Depends(),
    search_params: SearchParameters = Depends(),
    sort_params: SortParameters = Depends(
        page_sort_parameters(
            "date",
            "status",
            "data",
            "orderCount",
            default_sort="-date",
        )
    ),
    id_filter: LiteralEqualsFilter | None = Query(None, alias="idFilter"),
    date_filter: DateTimeFilter | None = Query(None, alias="dateFilter"),
    division_filter: LiteralEqualsFilter | None = Query(None, alias="divisionFilter"),
    status_filter: ExactExportStatusFilter | None = Query(None, alias="statusFilter"),
    count_filter: NumberFilter | None = Query(None, alias="countFilter"),
) -> PaginatedResponse[ExactExportData]:
    return exact_export_db_repo.list_exact_exports_paginated(
        context.db_session,
        page_params,
        search_params,
        sort_params,
        id_filter,
        date_filter,
        division_filter,
        status_filter,
        count_filter,
    )


@router.get("/{export_id}/", response_model=ExactExportData)
def api_admin_exact_gltransactions_status(
    export_id: int,
    context: UserRequestContext = Depends(admin_required),
) -> ExactExportData:
    export = exact_export_db_repo.get_export_by_id(context.db_session, export_id)
    return export.to_pydantic()
