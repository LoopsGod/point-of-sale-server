from datetime import date, datetime, timedelta

from sqlalchemy import Date, and_, func, select
from sqlalchemy.orm import Session, contains_eager

from app.models import Order
from app.service import setting_service
from app.domains.exact.export.exact_export import (
    ExactExport,
    ExactExportStatusFilter,
    ExactExportStatusMessage,
)
from app.views.exceptions import NotFoundException
from app.repository.util.filters import (
    DateTimeFilter,
    LiteralEqualsFilter,
    NumberFilter,
)
from app.domains.exact.export.exact_export import ExactExportData
from app.repository.util.pagination import (
    PageParameters,
    PaginatedResponse,
    PaginatedSearch,
    SearchParameters,
    SortParameters,
)


def save_exact_export(db_session: Session, export: ExactExport) -> None:
    db_session.add(export)
    db_session.commit()


def get_export_by_id(db_session: Session, export_id: int) -> ExactExport:
    export = (
        db_session.query(ExactExport)
        .filter_by(id=export_id)
        # Only join messages that do not indicate success.
        .join(
            ExactExportStatusMessage,
            and_(
                ExactExportStatusMessage.export_id == ExactExport.id,
                ExactExportStatusMessage.status != 2,
            ),
            isouter=True,
        )
        .options(contains_eager(ExactExport.messages))
        .one_or_none()
    )
    if not export:
        raise NotFoundException("export_not_found")

    return export


def get_export_by_date(db_session: Session, export_date: date) -> ExactExport | None:
    division = setting_service.get_db_settings(db_session).exact_storage_division

    export = (
        db_session.query(ExactExport)
        .filter(and_(ExactExport.division == division, ExactExport.date == export_date))
        .one_or_none()
    )

    return export


def create_exact_export(db_session: Session, export_date: date) -> ExactExport:
    division = setting_service.get_db_settings(db_session).exact_storage_division

    export = ExactExport(
        date=export_date,
        division=division,
        status="pending",
    )

    db_session.add(export)
    db_session.commit()

    return export


def list_exact_exports_paginated(
    db_session: Session,
    page_params: PageParameters,
    search_params: SearchParameters,
    sort_params: SortParameters,
    id_filter: LiteralEqualsFilter | None = None,
    date_filter: DateTimeFilter | None = None,
    division_filter: LiteralEqualsFilter | None = None,
    status_filter: ExactExportStatusFilter | None = None,
    count_filter: NumberFilter | None = None,
) -> PaginatedResponse[ExactExportData]:
    yesterday = datetime.now() - timedelta(days=1)

    first_order = select(
        func.cast(func.min(Order.created_at), Date).label("date")
    ).cte()

    # Generates a list of dates in Postgres from the first order date to yesterday
    # (since we cannot export today).
    date_list = select(
        func.cast(
            func.generate_series(first_order.c.date, yesterday, "1 day"),
            Date,
        ).label("date")
    ).cte()

    orders_per_day = (
        select(date_list.c.date, func.count(Order.id).label("count"))
        .join(Order, func.cast(Order.created_at, Date) == date_list.c.date)
        .group_by(date_list.c.date)
        .cte()
    )

    query = (
        db_session.query(ExactExport, date_list, orders_per_day.c.count)
        .join(ExactExport, date_list.c.date == ExactExport.date, isouter=True)
        .join(orders_per_day, orders_per_day.c.date == date_list.c.date, isouter=True)
        # Only join messages that do not indicate success.
        .join(
            ExactExportStatusMessage,
            and_(
                ExactExportStatusMessage.export_id == ExactExport.id,
                ExactExportStatusMessage.status != 2,
            ),
            isouter=True,
        )
        .options(contains_eager(ExactExport.messages))
    )

    def _mapper(row):
        export, date, order_count = row

        return ExactExportData(
            id=export.id if export else None,
            createdAt=export.created_at.astimezone() if export else None,
            date=date,
            division=export.division if export else None,
            status=(export.status if export else "unexported"),
            data=export.data if export else None,
            orderCount=order_count or 0,
            messages=[message.to_pydantic() for message in export.messages]
            if export
            else [],
        )

    return (
        PaginatedSearch[ExactExport, ExactExportData](query, page_params)
        .set_result_mapper(_mapper)
        .enable_searching(search_params, [ExactExport.data])
        .enable_sorting(
            sort_params,
            sorting_column_definition={
                "date": date_list.c.date,
                "status": ExactExport.status,
                "data": ExactExport.data,
                "orderCount": orders_per_day.c.count,
            },
        )
        .enable_filtering(
            {
                ExactExport.id: id_filter,
                date_list.c.date: date_filter,
                ExactExport.division: division_filter,
                ExactExport.status: status_filter,
                orders_per_day.c.count: count_filter,
            }
        )
        .execute()
    )
