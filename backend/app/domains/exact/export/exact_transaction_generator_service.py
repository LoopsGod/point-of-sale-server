from datetime import datetime, timedelta, date
from xml.etree import ElementTree

from dateutil.parser import parse
from jinja2 import Environment, FileSystemLoader, StrictUndefined

from app.dependencies import RequestContext
from app.repository import transactions, accounts as account_repo
from app.service import account_service, setting_service
from app.domains.exact.export.exact_export import ExactExportStatusMessage
from app.domains.exact.export.exact_transaction_generator import (
    ExactTransactionGenerator,
    GLTransactionLine,
    GLTransaction,
)
from app.util.util import get_last_moment_of_day
from app.views.exceptions import ValidationException

jinja_environment = Environment(
    loader=FileSystemLoader("app/domains/exact/templates"),
    cache_size=0,  # Easier for debugging
    undefined=StrictUndefined,
)


def generate_transaction_xml(context: RequestContext, export_date: date) -> bytes:
    """Generates an xml for use in Exact Online"""

    start_date = datetime(export_date.year, export_date.month, export_date.day)
    end_date = get_last_moment_of_day(start_date)

    start_balances = account_service.get_total_account_balance_at_date(
        context, start_date - timedelta(days=1)
    )
    end_balances = account_service.get_total_account_balance_at_date(context, end_date)

    if datetime.now() <= end_date:
        raise ValidationException("end_date_in_future")

    gl_transaction_lines = _generate_gl_transaction_lines(context, start_date, end_date)

    # Convert to euro and swap the sign, the sign is swapped because
    # the PoS journal in Exact should be positive (credit) when we owe
    # people money.
    opening_balance = start_balances.totalBalance / -100
    closing_balance = end_balances.totalBalance / -100

    db_settings = setting_service.get_db_settings(context.db_session)
    if db_settings.exact_journal_pos_code is None:
        raise ValidationException("exact_journal_pos_code_not_set")
    if db_settings.exact_journal_pos_description is None:
        raise ValidationException("exact_journal_pos_description_not_set")

    gl_transaction = GLTransaction(
        journal_code=db_settings.exact_journal_pos_code,
        journal_description=db_settings.exact_journal_pos_description,
        start_date=start_date.strftime("%Y-%m-%d"),
        opening_balance=opening_balance,
        closing_balance=closing_balance,
        transaction_lines=gl_transaction_lines,
    )

    template = jinja_environment.get_template("GLTransactions.xml")

    return template.render(t=gl_transaction).encode("ascii", "ignore")


def _generate_gl_transaction_lines(
    context: RequestContext, start_date: datetime, end_date: datetime
) -> list[GLTransactionLine]:

    setting_service.get_db_settings(context.db_session)
    if len(account_repo.find_unset_exact_codes(context.db_session)) != 0:
        raise ValidationException("exact_income_ledgers_not_set")

    all_transactions = transactions.get_all_between_datetime_joinedload(
        context.db_session, start_date, end_date
    )

    generator = ExactTransactionGenerator(
        all_transactions,
    )

    return generator.generate()


def parse_upload_status_xml(body: str) -> list[ExactExportStatusMessage]:
    messages = []

    root = ElementTree.fromstring(body)
    for message_tag in root.findall("Messages/Message"):
        topic_tag = message_tag.find("Topic")
        message_type = message_tag.get("type")

        if topic_tag is None:
            raise NameError("No topic tag in Exact message.")

        if message_type is None:
            raise NameError("No message type in Exact message.")

        key = None
        if int(message_type) == 2:
            data_tag = topic_tag.find("Data")

            if data_tag is None:
                raise NameError("No data tag in Exact message.")

            key = data_tag.get("keyAlt")

            if key is None:
                # Some results use key instead of keyAlt
                key = data_tag.get("key")

            if key is None:
                raise NameError("No key or keyAlt in Exact message.")

        date_tag = message_tag.find("Date")
        description_tag = message_tag.find("Description")
        node = topic_tag.get("node")

        if date_tag is None or date_tag.text is None:
            raise NameError("No date tag in Exact message.")

        if node is None:
            raise NameError("No node in Exact message.")

        status_message = ExactExportStatusMessage(
            status=int(message_type),
            import_done=parse(date_tag.text),
            node=node,
            data=key,
        )

        if description_tag is None or description_tag.text is None:
            status_message.description = ""
        else:
            status_message.description = description_tag.text

        messages.append(status_message)

    if not len(messages):
        raise NameError("Malformed Exact messages.")

    return messages


def export_messages_get_node(
    node: str, messages: list[ExactExportStatusMessage]
) -> ExactExportStatusMessage | None:
    """Gets the message with a specified type from the list.

    I.e. node='GLTransaction' would return the booking number message."""
    for msg in messages:
        if msg.node == node:
            return msg

    return None
