from datetime import datetime, date
from typing import Any, Literal

from pydantic import BaseModel
from sqlalchemy import (
    UniqueConstraint,
    Date,
    String,
    Column,
    ForeignKey,
    Integer,
    DateTime,
    or_,
)
from sqlalchemy.orm import relationship, Mapped
from sqlalchemy.sql.elements import ColumnElement

from app.models._base import Base
from app.models.database import Model
from app.repository.util.filters import FilterGeneric, LiteralEqualsFilter


class ExactExportStatusMessageData(BaseModel):
    status: Literal["error", "warn", "success", "fatal"]
    description: str | None
    node: str
    data: str | None


class ExactExportData(BaseModel):
    id: int | None
    createdAt: datetime | None
    date: date
    division: str | None
    #  Anything besides the literals is an error
    status: str | Literal["exported", "pending", "deleted", "unexported", "error"]
    data: str | None
    orderCount: int
    messages: list[ExactExportStatusMessageData]


class ExactExport(Model, Base):
    __tablename__ = "exact_export"
    date: Mapped[date] = Column(Date, nullable=False)  # type: ignore[assignment]  # noqa: E501
    division: Mapped[str] = Column(String(128), nullable=False)  # type: ignore[assignment]  # noqa: E501
    status: Mapped[str] = Column(String(128), nullable=False)  # type: ignore[assignment]  # noqa: E501
    data: Mapped[str | None] = Column(String(128), nullable=True)  # type: ignore[assignment]  # noqa: E501

    messages: Mapped[list["ExactExportStatusMessage"]] = relationship("ExactExportStatusMessage", uselist=True)  # type: ignore[assignment]  # noqa: E501

    __table_args__ = (UniqueConstraint("date", "division", name="_date_division_uc"),)

    def to_pydantic(self) -> ExactExportData:
        return ExactExportData(
            id=self.id,
            createdAt=self.created_at.astimezone(),
            date=self.date.isoformat(),
            division=self.division,
            status=self.status,
            data=self.data,
            orderCount=0,
            messages=[message.to_pydantic() for message in self.messages],
        )

    def serialize(self) -> dict[str, Any]:
        return {
            "id": self.id,
            "createdAt": self.created_at.astimezone().isoformat(),
            "date": self.date.isoformat(),
            "division": self.division,
            "status": self.status,
            "data": self.data,
        }


class ExactExportStatusMessage(Model, Base):
    __tablename__ = "exact_export_status_message"
    export_id: Mapped[int] = Column(Integer, ForeignKey("exact_export.id"), nullable=False)  # type: ignore[assignment]  # noqa: E501

    #  0 = error, 1 = warn, 2 = success, 3 = fatal
    status: Mapped[int] = Column(Integer, nullable=False)  # type: ignore[assignment]  # noqa: E501
    import_done: Mapped[datetime] = Column(DateTime, nullable=False)  # type: ignore[assignment]  # noqa: E501
    description: Mapped[str] = Column(String(512))  # type: ignore[assignment]  # noqa: E501
    node: Mapped[str] = Column(String(128), nullable=False)  # type: ignore[assignment]  # noqa: E501
    data: Mapped[str] = Column(String(128))  # type: ignore[assignment]

    def to_pydantic(self) -> ExactExportStatusMessageData:
        return ExactExportStatusMessageData(
            status=["error", "warn", "success", "fatal"][self.status],
            description=self.description,
            node=self.node,
            data=self.data,
        )


class ExactExportStatusFilter(LiteralEqualsFilter):
    def filter(
        self, query: FilterGeneric, column: Column | ColumnElement
    ) -> FilterGeneric:
        if self.value == "unexported":
            return query.filter(or_(column.is_(None), column == "unexported"))

        if type(self.value) is list and "unexported" in self.value:
            return query.filter(or_(column.is_(None), column.in_(self.value)))

        return super().filter(query, column)
