import logging
import sys
from collections import defaultdict
from dataclasses import dataclass
from html import escape
from typing import NamedTuple, Callable

from app.models import Transaction, Order, Account
from app.models.account import AccountExactLedger
from app.models.checkout import CheckoutMethod
from app.models.order import OrderType
from app.models.transaction import TransactionType
from app.views.exceptions import ValidationException, ApplicationException

logger = logging.getLogger(__name__)


@dataclass
class TransactionBundleValue:
    count: int = 0
    amount: int = 0

    def increase(self, amount: int) -> None:
        self.count += 1
        self.amount += amount


class TransactionBundleKey(NamedTuple):
    date: str
    account_code: str
    description: str
    cost_center_code: str | None
    vat_code: str | None


class GLTransactionLine(NamedTuple):
    date: str
    account_code: str
    description: str
    amount: float
    note: str | None
    cost_center_code: str | None
    vat_code: str | None


class GLTransaction(NamedTuple):
    journal_code: str
    journal_description: str
    start_date: str

    opening_balance: float
    closing_balance: float

    transaction_lines: list[GLTransactionLine]


class ExactTransactionGenerator:
    def __init__(self, transactions: list[Transaction]) -> None:
        self.bundle: dict[TransactionBundleKey, TransactionBundleValue] = defaultdict(
            lambda: TransactionBundleValue()
        )
        self.min_transaction_id = sys.maxsize
        self.max_transaction_id = 0

        self._process(transactions)

    def _process(self, transactions: list[Transaction]) -> None:
        for transaction in transactions:
            # Keep track of the transaction range
            self.min_transaction_id = min(self.min_transaction_id, transaction.id)
            self.max_transaction_id = max(self.max_transaction_id, transaction.id)

            self.handle_transaction(transaction)

    def generate(self) -> list[GLTransactionLine]:
        gl_transactions: list[GLTransactionLine] = []

        for key, value in self.bundle.items():
            note = (
                f"Transaction {self.min_transaction_id} until {self.max_transaction_id}"
            )
            description = f"{value.count}x {key.description}"

            gl_transactions.append(
                self._post_handle(
                    GLTransactionLine(
                        date=key.date,
                        account_code=key.account_code,
                        description=f"{description} ({note})",
                        # Convert to euro and swap the sign, the sign is swapped because
                        # the PoS journal in Exact should be positive (credit) when we
                        # owe people money, i.e. they put money into their own account.
                        amount=-value.amount / 100,
                        note=f"{description} ({note})",
                        cost_center_code=key.cost_center_code,
                        vat_code=key.vat_code,
                    )
                )
            )

        return gl_transactions

    def _order_direct_payment(self, t: Transaction, o: Order) -> None:
        """Helper function for order with type DIRECT_PAYMENT"""
        # TODO hybrid_property typing is a b*tch
        method: CheckoutMethod = o.method  # type: ignore[assignment]

        # TODO Should we also change description based on debit/credit?
        if o.deleted:
            desc = f"revenue (deleted) - {method.value.lower()}"
        else:
            desc = f"revenue - {method.value.lower()}"

        if t.reason:
            desc += f" - {t.reason}"

        exact_ledger = self._get_account_ledger(o.account, True)

        key = TransactionBundleKey(
            date=o.created_at.strftime("%Y-%m-%d"),
            account_code=exact_ledger.exact_ledger_code,
            description=desc,
            # Vat en costcenter are on the product orders.
            cost_center_code=None,
            vat_code=None,
        )
        self.bundle[key].increase(amount=o.price)

    def _order_product_purchase(self, t: Transaction, o: Order) -> None:
        """Helper function for order with types PRODUCT_PURCHASE"""
        if o.account is None:
            raise ValidationException("PRODUCT_PURCHASE order has no account")
        exact_ledger = self._get_account_ledger(o.account)
        if o.product is None:
            raise ValidationException("PRODUCT_PURCHASE order has no product")

        if o.deleted:
            desc = f"refund - {o.product.name}"
        else:
            desc = f"sold - {o.product.name}"

        key = TransactionBundleKey(
            date=o.created_at.strftime("%Y-%m-%d"),
            account_code=exact_ledger.exact_ledger_code,
            description=desc,
            cost_center_code=exact_ledger.exact_cost_center_code,
            vat_code=exact_ledger.exact_vat_code,
        )
        self.bundle[key].increase(amount=o.price)

    def _top_up(self, t: Transaction, o: Order) -> None:
        if o.type == OrderType.DIRECT_PAYMENT:
            self._order_direct_payment(t, o)
            return

        elif o.type == OrderType.ACCOUNT_TOP_UP_OR_ACCOUNT_PAYMENT:
            # This case increases the account's balance. Individual balances aren't
            # tracked. No transaction line means the DIRECT_PAYMENT side of this
            # transaction increases the POS Journal's balance
            return
        raise ValidationException("invalid_order_type")

    def _top_up_and_purchase(self, t: Transaction, o: Order) -> None:
        if o.type == OrderType.DIRECT_PAYMENT:
            self._order_direct_payment(t, o)
        elif o.type == OrderType.ACCOUNT_TOP_UP_OR_ACCOUNT_PAYMENT:
            pass
        elif o.type == OrderType.PRODUCT_PURCHASE:
            self._order_product_purchase(t, o)
        else:
            raise ValidationException("invalid_order_type")

    def _direct_purchase(self, t: Transaction, o: Order) -> None:
        if o.type == OrderType.DIRECT_PAYMENT:
            self._order_direct_payment(t, o)
        elif o.type == OrderType.PRODUCT_PURCHASE:
            self._order_product_purchase(t, o)
        else:
            raise ValidationException("invalid_order_type")

    def _account_purchase(self, t: Transaction, o: Order) -> None:
        if o.type == OrderType.ACCOUNT_TOP_UP_OR_ACCOUNT_PAYMENT:
            # This is the account that bought the product
            pass
        elif o.type == OrderType.PRODUCT_PURCHASE:
            self._order_product_purchase(t, o)
        else:
            raise ValidationException("invalid_order_type")

    def handle_transaction(self, transaction: Transaction) -> None:
        """Switch on transaction type and call the corresponding handler function"""
        # TODO I think the type problem here is from sh*tty typed hybrid_property
        handler: Callable[[Transaction, Order], None] = {  # type: ignore[call-overload]
            TransactionType.TOP_UP: self._top_up,
            TransactionType.TOP_UP_AND_PURCHASE: self._top_up_and_purchase,
            TransactionType.DIRECT_PURCHASE: self._direct_purchase,
            TransactionType.ACCOUNT_PURCHASE: self._account_purchase,
        }.get(transaction.type, None)

        if handler:
            for order in transaction.orders:
                handler(transaction, order)
        else:
            raise ValidationException(
                f"{transaction.id=} has invalid {transaction.type=}"
            )

    def _post_handle(self, line: GLTransactionLine) -> GLTransactionLine:
        description = line.description
        note = line.note

        line = line._replace(description=escape(description)[:60])
        if note:
            line = line._replace(note=escape(note))
        return line

    def _get_account_ledger(
        self, account: Account, income: bool = False
    ) -> AccountExactLedger:
        if not account.exact_ledger or not account.exact_ledger.exact_ledger_code:
            raise ApplicationException("account_exact_ledger_not_existing")

        # Fail if it's not an income account and the cost center or vat code is missing
        if not income and (
            not account.exact_ledger.exact_cost_center_code
            or not account.exact_ledger.exact_vat_code
        ):
            raise ApplicationException("account_exact_ledger_not_complete")

        if income and (
            account.exact_ledger.exact_cost_center_code
            or account.exact_ledger.exact_vat_code
        ):
            raise ApplicationException("account_income_exact_ledger_not_complete")

        return account.exact_ledger
