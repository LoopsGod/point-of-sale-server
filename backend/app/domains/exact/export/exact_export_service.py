from app.dependencies import UserRequestContext
from app.domains.exact.external import exact_external_service
from app.domains.exact.export import exact_export_db_repo
from app.domains.exact.export.exact_export import ExactExport
from app.domains.exact.api.exact_api import ExactApi
from app.views.exceptions import ResourceConflictException


def delete_export_if_booking_does_not_exist(
    context: UserRequestContext, api: ExactApi, export_id: int
) -> ExactExport:
    export = exact_export_db_repo.get_export_by_id(context.db_session, export_id)

    if export.status != "exported":
        raise ResourceConflictException("batch_not_exported")

    if export.data is None:
        raise ResourceConflictException("unknown_booking_id")

    if exact_external_service.booking_id_exists(api, export.division, int(export.data)):
        raise ResourceConflictException("exact_booking_exists")

    #  Soft delete to preserve logs
    export.status = "deleted"
    export.data = None

    exact_export_db_repo.save_exact_export(context.db_session, export)
    return export
