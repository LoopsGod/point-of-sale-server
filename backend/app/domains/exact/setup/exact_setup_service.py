from pydantic import BaseModel
from app.dependencies import UserRequestContext
from app.domains.exact.external import exact_external_service
from app.repository import accounts as account_repo
from app.service import setting_service
from app.domains.exact.api.exact_api import ExactApi
from app.views.exceptions import ValidationException
from sqlalchemy.orm.session import Session


class ExactExportReadyStatus(BaseModel):
    setup_finished: bool

    journal_code_set: bool
    journal_description_set: bool

    all_ledgers_set: bool
    all_ledgers_valid: bool

    def is_ready(self) -> bool:
        return (
            self.setup_finished
            and self.journal_code_set
            and self.journal_description_set
            and self.all_ledgers_set
            and self.all_ledgers_valid
        )


def can_export(db_session: Session, api: ExactApi) -> ExactExportReadyStatus:
    db_settings = setting_service.get_db_settings(db_session)

    setup_finished = db_settings.exact_setup_finished

    journal_code_set = db_settings.exact_journal_pos_code is not None
    journal_description_set = db_settings.exact_journal_pos_description is not None

    all_ledgers_set = len(account_repo.find_unset_exact_codes(db_session)) == 0

    # This can only be checked when the setup is finished.
    all_ledgers_valid = (
        _all_used_exact_ledger_codes_exist(db_session, api) if setup_finished else False
    )

    return ExactExportReadyStatus(
        setup_finished=setup_finished,
        journal_code_set=journal_code_set,
        journal_description_set=journal_description_set,
        all_ledgers_set=all_ledgers_set,
        all_ledgers_valid=all_ledgers_valid,
    )


def _all_used_exact_ledger_codes_exist(db_session: Session, api: ExactApi) -> bool:
    """
    Cross references all Exact codes in our db with the Exact API.
    """
    exact_codes_in_use = account_repo.get_all_distinct_exact_codes(db_session)

    if not len(exact_codes_in_use):
        return True

    filter_parts = [f"Code eq '{code}'" for code in exact_codes_in_use]

    # Exact's OData version does not support 'in' operator
    ledger_filter = f"IsBlocked eq false and ({' or '.join(filter_parts)})"

    ledgers = exact_external_service.get_ledgers(api, ledger_filter)

    return len(exact_codes_in_use) == len(ledgers)


def set_client(
    context: UserRequestContext, base_url: str, client_id: str, client_secret: str
) -> str:
    db_settings = setting_service.get_db_settings(context.db_session)

    db_settings.exact_storage_base_url = base_url
    db_settings.exact_storage_client_id = client_id
    db_settings.exact_storage_client_secret = client_secret
    setting_service.save_db_settings(context.db_session, db_settings)

    return ExactApi.create_auth_request_url(base_url, client_id)


def get_oauth_url(context: UserRequestContext) -> str:
    db_settings = setting_service.get_db_settings(context.db_session)
    if (
        db_settings.exact_storage_base_url is None
        or db_settings.exact_storage_client_id is None
    ):
        raise ValidationException("exact_incomplete_setup")
    return ExactApi.create_auth_request_url(
        db_settings.exact_storage_base_url, db_settings.exact_storage_client_id
    )


def unlink(context: UserRequestContext) -> None:
    db_settings = setting_service.get_db_settings(context.db_session)

    db_settings.exact_setup_finished = False
    db_settings.exact_old_division = db_settings.exact_storage_division
    db_settings.exact_storage_base_url = "https://pos.svia.nl"
    db_settings.exact_storage_client_id = None
    db_settings.exact_storage_client_secret = None
    db_settings.exact_storage_division = None
    db_settings.exact_storage_refresh_token = None

    setting_service.save_db_settings(context.db_session, db_settings)


def set_token(context: UserRequestContext, api: ExactApi, code: str) -> bool:
    exact_external_service.set_token(api, code)
    return can_export(context.db_session, api).is_ready()


def set_division(context: UserRequestContext, api: ExactApi, division: str) -> None:
    """Selects the Exact administration. Deletes data from old linked division
    if a different division is currently being linked."""
    exact_external_service.check_division_exists(api, division)

    db_settings = setting_service.get_db_settings(context.db_session)
    db_settings.exact_storage_division = division

    if db_settings.exact_old_division and db_settings.exact_old_division != division:
        db_settings.exact_journal_pos_code = None
        db_settings.exact_journal_pos_description = None

    setting_service.save_db_settings(context.db_session, db_settings)
