import logging
from datetime import datetime, timedelta

from celery.task import periodic_task
from celery.schedules import crontab

import config
from app.models.database import SessionLocal
from app.repository.transactions import get_transaction_product_count
from app.service import mail_service, setting_service

_logger = logging.getLogger(__name__)

# Every sunday at 12:00 (Midday).
INTERVAL = crontab(day_of_week=6, hour=12, minute=0)

if getattr(config, "DEBUG", False):
    INTERVAL = timedelta(minutes=2)


@periodic_task(run_every=INTERVAL)
def email_product_quantity() -> None:
    with SessionLocal.begin() as db_session:
        included_categories = setting_service.get_db_settings(
            db_session
        ).summary_included_categories
        destination_email = setting_service.get_db_settings(
            db_session
        ).notification_product_quantity_destination

        if not destination_email:
            return _logger.info(
                "Not running summary email because there is no notification email set."
            )

        _logger.info(
            "Running product quantity query with categories: %s ", included_categories
        )

        result = get_transaction_product_count(db_session, included_categories)

        if len(result) == 0:
            _logger.info(
                "Did not send summary email because there were no products sold"
            )
            return

        command = mail_service.MailCommand(
            db_session=db_session,
            to=destination_email,
        )

        command.with_template(
            "email/product_purchase_summary.html",
            week=datetime.now().strftime("%V"),
            products=result,
        )

        mail_service.send_email(command)
