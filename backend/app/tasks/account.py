import logging
from datetime import datetime, timedelta

from celery.task import periodic_task

import config
from app.dependencies import RequestContext
from app.models.account_type import AccountType
from app.models.database import SessionLocal
from app.service import account_service
from app.views.exceptions import ApplicationException

_logger = logging.getLogger(__name__)
BALANCE_DATE = datetime.fromtimestamp(0)
INTERVAL = timedelta(minutes=30)

if getattr(config, "DEBUG", False):
    _logger.info("Setting interval for periodic tasks to 10 seconds.")
    INTERVAL = timedelta(seconds=10)


@periodic_task(run_every=INTERVAL)
def check_balance_integrity() -> None:
    with SessionLocal.begin() as db_session:
        context = RequestContext(db_session)
        balances = account_service.get_total_account_balance_at_date(
            context, BALANCE_DATE
        )
        for account_balance in balances.accountList:
            if account_balance.oldBalance != 0:
                _logger.error(
                    "Account %s balance at %s is not 0: %d",
                    account_balance.accountName,
                    str(BALANCE_DATE),
                    account_balance.oldBalance,
                )

        if balances.totalBalance != 0:
            raise ApplicationException(
                "Total balances at %s: %d" % (str(BALANCE_DATE), balances.totalBalance)
            )


@periodic_task(run_every=INTERVAL)
def check_allow_negative_balance_integrity() -> None:
    with SessionLocal.begin() as db_session:
        for acc, dd in account_service.get_all_with_joined_user_direct_debit(
            db_session
        ):
            direct_debit_enabled = dd and dd.enabled

            if direct_debit_enabled:
                continue

            if acc.type == AccountType.EVENT:
                continue

            if acc.allow_negative_balance:
                raise ApplicationException(
                    f"Account '{acc.name}' (id={acc.id}) has allow_negative_balance "
                    "set to true but is neither an event account nor has direct "
                    "debit details on record."
                )
