import logging
import time
from datetime import timedelta, datetime

from celery.task import periodic_task

import config
from app import cache, worker
from app.dependencies import RequestContext
from app.models import SepaBatch
from app.models.database import SessionLocal
from app.service import sepa_service, mail_service, setting_service
from app.views.exceptions import NotFoundException

_logger = logging.getLogger(__name__)

INTERVAL = timedelta(minutes=30)

if getattr(config, "DEBUG", False):
    _logger.info("Setting interval for periodic tasks to 10 seconds.")
    INTERVAL = timedelta(seconds=10)


@periodic_task(run_every=INTERVAL)
def check_sepa_batch_created_current_month():
    with SessionLocal.begin() as db_session:
        try:
            notification_email = setting_service.get_db_settings(
                db_session
            ).notification_sepa_destination

            if not notification_email:
                return _logger.info("Notification disabled")

            sepa_batch = sepa_service.get_of_current_month(db_session)
            cache_key = "reminder_sepa_creation"
            today = datetime.today()
            sepa_amount = sepa_service.get_total_batch_amount(db_session)
            total_minimal_batch_debt = -5000

            if (
                not sepa_batch
                and today.day >= 14
                and sepa_amount < total_minimal_batch_debt
                and cache.get(cache_key) is None
            ):
                command = mail_service.MailCommand(
                    db_session=db_session, to=notification_email
                )
                command.with_template(
                    template_name="email/reminder_sepa_creation.html",
                    month=today.strftime("%B"),
                )
                mail_service.send_email(command)
                cache.set(
                    cache_key, time.time(), int(timedelta(days=1).total_seconds())
                )

        except NotFoundException:
            _logger.warning("Notifications not running, missing settings.")


@periodic_task(run_every=INTERVAL)
def check_sepa_batch_not_executed() -> None:
    with SessionLocal.begin() as db_session:
        try:
            notification_email = setting_service.get_db_settings(
                db_session
            ).notification_sepa_destination

            if not notification_email:
                return _logger.info("Notification disabled")

            sepa_batches = sepa_service.get_unexecuted_before_today(db_session)
            cache_key = "reminder_sepa_execution"
            if sepa_batches and cache.get(cache_key) is None:
                command = mail_service.MailCommand(
                    db_session=db_session, to=notification_email
                )
                command.with_template(
                    template_name="email/reminder_sepa_execution.html",
                    sepa_batches=sepa_batches,
                )
                mail_service.send_email(command)
                cache.set(
                    cache_key, time.time(), int(timedelta(days=1).total_seconds())
                )
        except NotFoundException:
            _logger.warning("Notifications not running, missing settings.")


@worker.task
def send_sepa_notification(sepa_batch_id: int) -> None:
    with SessionLocal.begin() as db_session:
        context = RequestContext(db_session)
        sepa_batch = db_session.get(SepaBatch, sepa_batch_id)
        if sepa_batch is None:
            raise NotFoundException("sepa_batch_not_found")
        pending_orders = sepa_service.get_pending_orders_with_user_accounts(
            context, sepa_batch
        )

        for order, _, user, _ in pending_orders:
            if order.deleted:
                continue

            command = mail_service.MailCommand(db_session=db_session, to=user.username)
            command.with_template(
                "email/sepa_batch_amount_notification.html",
                batch=sepa_batch,
                user=user,
                amount=order.price / 100,
            )  # To EUR

            mail_service.send_email(command)

        # Mark batch as all mails send.
        sepa_batch.mail_date = datetime.now()
