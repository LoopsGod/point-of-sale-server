import logging

from app.domains.exact import exact_tasks
from app.tasks import sepa, account, quantity

_logger = logging.getLogger(__name__)
_logger.info("Registered tasks in %s", sepa)
_logger.info("Registered tasks in %s", exact_tasks)
_logger.info("Registered tasks in %s", account)
_logger.info("Registered tasks in %s", quantity)
