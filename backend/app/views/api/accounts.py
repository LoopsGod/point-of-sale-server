from datetime import date, datetime
from http import HTTPStatus
from typing import Any

import pydantic
from fastapi import Depends, Response
from pydantic import BaseModel
from sqlalchemy import and_, func, text
from starlette.requests import Request

from app import dependencies
from app.dependencies import (
    login_required,
    UserRequestContext,
    get_date_range,
    RegisterCapabilities,
    AccountRequestContext,
)
from app.models import Transaction, Order, Product
from app.models.account import AccountData, SerializedAccountRequest
from app.models.account_type import AccountType
from app.models.register import RegisterCap
from app.service import (
    account_service,
    mollie_service,
    sepa_service,
    user_service,
)
from app.util.util import DateRangeParameters
from app.util.util import format_iban, PinStr
from app.views.api import fapi_auth, fapi
from app.views.exceptions import NotFoundException, ValidationException


@fapi.get("/api/account/transactions/count")
def api_account_transactions_count(
    context: AccountRequestContext = Depends(dependencies.account_required),
    date_range: DateRangeParameters = Depends(get_date_range),
) -> dict[str, Any]:
    account = context.account

    q_inner = (
        context.db_session.query(Transaction.id)
        .select_from(Order)
        .join(Transaction)
        .filter(
            and_(
                Order.account_id == account.id,
                Order.deleted_at.is_(None),
                Transaction.deleted_at.is_(None),
                Transaction.created_at.between(date_range.start, date_range.end),
            )
        )
        .group_by(Transaction.id)
    )

    q = (
        context.db_session.query(
            Product.name,
            func.count(Order.product_id).label("c"),
            func.abs(func.sum(Order.price)),
        )
        .select_from(Product)
        .join(Order)
        .filter(Order.transaction_id.in_(q_inner))
        .group_by(Product.id)
        .order_by(text("c DESC"))
    )

    return {
        "products": [
            {"productName": e[0], "count": e[1], "totalPrice": e[2]} for e in q.all()
        ],
    }


class ApiAccountDetailsResponse(pydantic.BaseModel):
    class Sepa(pydantic.BaseModel):
        iban: str
        bic: str
        mandateId: str
        signDate: date
        balance: int | None
        execDate: date | None

    account: AccountData
    balance: int
    creditAccount: bool
    sepa: Sepa | None


@fapi.get("/api/account/", response_model=ApiAccountDetailsResponse)
def get_account_details(
    context: AccountRequestContext = Depends(dependencies.account_required),
) -> ApiAccountDetailsResponse:
    account = context.account
    batch, order = sepa_service.get_pending_batch_for_account(context, account.id)
    direct_debit = user_service.get_user_direct_debit(context, context.user.id)

    sepa = None
    if direct_debit and direct_debit.enabled:
        if (
            not direct_debit.iban
            or not direct_debit.bic
            or not direct_debit.mandate_id
            or not direct_debit.sign_date
        ):
            #  This cannot happen as we have a check constraint on the database,
            # but we need to make types happy.
            raise ValidationException("direct_debit_incomplete")

        sepa = ApiAccountDetailsResponse.Sepa(
            iban=format_iban(direct_debit.iban),
            bic=direct_debit.bic,
            mandateId=direct_debit.mandate_id,
            signDate=direct_debit.sign_date,
            balance=None if order is None else order.price,
            execDate=None if batch is None else batch.execution_date,
        )

    return ApiAccountDetailsResponse(
        account=account.to_pydantic(),
        balance=account.balance,
        creditAccount=account.allow_negative_balance,
        sepa=sepa,
    )


class AccountUpgradeAmount(pydantic.ConstrainedFloat):
    """Amount in cents, between 20 and 100 euro"""

    ge = 2000
    lt = 10000


class AccountUpgradeJson(pydantic.BaseModel):
    amount: AccountUpgradeAmount


class AccountUpgradeResponse(pydantic.BaseModel):
    paymentUrl: str
    paymentId: str
    accountId: int


@fapi.post(
    "/api/account/upgrade",
    response_model=AccountUpgradeResponse,
    status_code=HTTPStatus.CREATED,
)
def mollie_upgrade(
    req_json: AccountUpgradeJson,
    request: Request,
    context: AccountRequestContext = Depends(dependencies.account_required),
) -> AccountUpgradeResponse:
    account = context.user.account
    if not account or account.deleted:
        raise NotFoundException("no_linked_account")

    payment = mollie_service.create_ideal_payment(
        context, request, account.id, req_json.amount / 100
    )
    mollie_data = mollie_service.get_mollie_data(payment)
    return AccountUpgradeResponse(
        paymentUrl=mollie_data.checkout_url,
        paymentId=mollie_data["id"],
        accountId=account.id,
    )


class AccountSetPinJson(pydantic.BaseModel):
    pin: PinStr


@fapi.post(
    "/api/account/pin", status_code=HTTPStatus.NO_CONTENT, response_class=Response
)
def api_account_pin_set(
    request_json: AccountSetPinJson,
    context: AccountRequestContext = Depends(dependencies.account_required),
) -> None:
    account_service.set_account_pin(context.account, request_json.pin)
    context.db_session.commit()


class ApiAccountRequestResponse(pydantic.BaseModel):
    account_request: SerializedAccountRequest | None


@fapi_auth.get("/api/account_request/pending", response_model=ApiAccountRequestResponse)
def api_account_request_pending(
    user: UserRequestContext = Depends(login_required),
) -> ApiAccountRequestResponse:
    account_request = account_service.find_latest_account_request(user)
    return ApiAccountRequestResponse(
        account_request=account_request.to_pydantic() if account_request else None
    )


class AccountRequestRequest(BaseModel):
    pin: PinStr


@fapi_auth.post(
    "/api/account_request/request", response_model=ApiAccountRequestResponse
)
def api_account_request(
    request_json: AccountRequestRequest,
    context: UserRequestContext = Depends(login_required),
) -> ApiAccountRequestResponse:
    account_request = account_service.request_account(context, request_json.pin)

    return ApiAccountRequestResponse(
        account_request=account_request.to_pydantic() if account_request else None
    )


class ApiAccountResponse(pydantic.BaseModel):
    class Account(pydantic.BaseModel):
        id: int
        name: str
        balance: int | None = None
        directDebit: bool | None = None
        type: AccountType
        lastUsed: datetime

    accounts: list[Account]


@fapi.get("/api/accounts/")
def api_accounts(
    context: UserRequestContext = Depends(login_required),
    caps: list[RegisterCap] = Depends(
        RegisterCapabilities(
            [RegisterCap.PAYMENT_ACCOUNT_AUTHED, RegisterCap.PAYMENT_ACCOUNT_OTHER]
        )
    ),
) -> ApiAccountResponse:
    """
    Controller for /api/accounts/, which returns json containing a list of
    non-ledger accounts, each containing their id and their name, and containing a
    balance field if in bar mode.
    """

    if RegisterCap.PAYMENT_ACCOUNT_OTHER in caps:
        accounts_with_debit_details = (
            account_service.get_all_with_joined_user_direct_debit(context.db_session)
        )
        acc_list = [
            ApiAccountResponse.Account(
                id=account.id,
                name=account.name,
                balance=account.balance,
                directDebit=direct_debit is not None,
                type=account.type,
                lastUsed=account.updated_at,
            )
            for account, direct_debit in accounts_with_debit_details
        ]
    else:
        accounts = account_service.get_all(context, with_pin=True)
        acc_list = [
            ApiAccountResponse.Account(
                id=account.id,
                name=account.name,
                type=account.type,
                lastUsed=account.updated_at,
            )
            for account in accounts
        ]

    return ApiAccountResponse(accounts=acc_list)
