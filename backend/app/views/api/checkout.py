from datetime import datetime, timedelta
from http import HTTPStatus

import pydantic
from fastapi import Depends
from sqlalchemy.orm import Session
from starlette.responses import Response

from app import dependencies
from app.dependencies import UserRequestContext, RegisterCapabilities, get_db
from app.models.checkout import CheckoutMethod
from app.models.register import RegisterCap
from app.models.transaction import TransactionData
from app.service import (
    register_service,
    checkout_service,
    transaction_service,
    user_condition_service,
    setting_service,
)
from app.service.checkout_service import (
    CheckoutDirectRequest,
    CheckoutAccountRequest,
    MAX_CHECKOUT_AMOUNT_ADMIN,
    MAX_CHECKOUT_AMOUNT,
    CartItem,
)
from app.util.util import PinStr
from app.views.api import fapi, fapi_auth
from app.views.api_admin.settings import AdminCheckoutReason
from app.views.exceptions import (
    ValidationException,
    AuthorizationException,
)

MAX_UNDO_TIMEOUT = timedelta(seconds=10)


class ReasonStr(pydantic.ConstrainedStr):
    strip_whitespace = True
    max_length = 60


class CardSecretStr(pydantic.ConstrainedStr):
    strip_whitespace = True
    max_length = 64


class CheckoutDirectJson(pydantic.BaseModel):
    nonce: str
    products: list[CartItem]  # Can be empty, with cash/card upgrade.
    upgrade_account_id: int | None
    upgrade_amount: int | None
    reason: str | None


class CheckoutAccountJson(pydantic.BaseModel):
    nonce: str
    products: list[CartItem] = pydantic.Field(..., min_items=1)
    account_id: int | None
    pin: PinStr | None


class CheckoutAccountReaderJson(pydantic.BaseModel):
    nonce: str
    products: list[CartItem] = pydantic.Field(..., min_items=1)
    cardSecret: CardSecretStr


class CheckoutResponse(pydantic.BaseModel):
    transaction_id: int


def api_checkout(
    request_json: CheckoutDirectJson,
    context: UserRequestContext,
    method: CheckoutMethod,
) -> CheckoutResponse:
    """
    Controller for /api/checkout/(cash|card|other)/, which tries to perform
    a checkout for <method>, registering the checkout on success.

    Expects a JSON object with a products field, which is a list of CartEntry
    objects. A CartEntry object contains an id field (the product ID) and an
    amount field (how many times the product is ordered).
    """
    register = register_service.get_register_for_user(context.db_session, context.user)
    caps = register_service.get_capabilities_for_register(register)
    max_amount = (
        MAX_CHECKOUT_AMOUNT_ADMIN if context.user.is_admin else MAX_CHECKOUT_AMOUNT
    )

    if (
        request_json.upgrade_account_id is not None
        and request_json.upgrade_amount is None
    ):
        raise ValidationException("no_upgrade_amount_give")

    req = CheckoutDirectRequest(
        method=method,
        nonce=request_json.nonce,
        cart_items=request_json.products,
        upgrade_account_id=request_json.upgrade_account_id,
        upgrade_amount=request_json.upgrade_amount,
        reason=request_json.reason,
    )

    if req.upgrade_account_id is not None and req.upgrade_amount is not None:
        # Also upgrading in this transaction
        if RegisterCap.PAYMENT_ACCOUNT_OTHER not in caps:
            raise AuthorizationException("insufficient_permissions")

    transaction_id = checkout_service.checkout_other(context, req, register, max_amount)

    return CheckoutResponse(transaction_id=transaction_id)


@fapi_auth.post(
    "/api/checkout/cash/",
    response_model=CheckoutResponse,
    dependencies=[
        Depends(RegisterCapabilities(require_any_of=[RegisterCap.PAYMENT_CASH]))
    ],
)
def api_checkout_cash(
    request_json: CheckoutDirectJson,
    context: UserRequestContext = Depends(dependencies.login_required),
) -> CheckoutResponse:
    return api_checkout(request_json, context, method=CheckoutMethod.CASH)


@fapi_auth.post(
    "/api/checkout/card/",
    response_model=CheckoutResponse,
    dependencies=[
        Depends(RegisterCapabilities(require_any_of=[RegisterCap.PAYMENT_CARD]))
    ],
)
def api_checkout_card(
    request_json: CheckoutDirectJson,
    context: UserRequestContext = Depends(dependencies.login_required),
) -> CheckoutResponse:
    return api_checkout(request_json, context, method=CheckoutMethod.CARD)


@fapi_auth.post(
    "/api/checkout/other/",
    response_model=CheckoutResponse,
    dependencies=[
        Depends(RegisterCapabilities(require_any_of=[RegisterCap.PAYMENT_OTHER]))
    ],
)
def api_checkout_other(
    request_json: CheckoutDirectJson,
    context: UserRequestContext = Depends(dependencies.login_required),
) -> CheckoutResponse:
    return api_checkout(request_json, context, method=CheckoutMethod.OTHER)


class CheckoutAccountResponse(CheckoutResponse):
    balance_before: int
    balance_after: int


@fapi_auth.post(
    "/api/checkout/account/",
    dependencies=[
        Depends(
            RegisterCapabilities(
                require_any_of=[
                    RegisterCap.PAYMENT_ACCOUNT_AUTHED,
                    RegisterCap.PAYMENT_ACCOUNT_OTHER,
                ]
            )
        )
    ],
)
def api_checkout_account(
    request_json: CheckoutAccountJson,
    context: UserRequestContext = Depends(dependencies.login_required),
) -> CheckoutAccountResponse:
    """
    Controller for /api/checkout/account/, which tries to perform a checkout
    for an account, checking its PIN and balance, registering the checkout
    on success.

    Expects a JSON object with a products field, which is a list of CartEntry
    objects, an account_id field (the ID of the credit account) and a pin field
    (string with the account's PIN code)A CartEntry object contains an id field
    (the product ID) and an amount field (how many times the product is
    ordered).
    """
    register = register_service.get_register_for_user(context.db_session, context.user)
    caps = register_service.get_capabilities_for_register(register)
    max_amount = (
        MAX_CHECKOUT_AMOUNT_ADMIN if context.user.is_admin else MAX_CHECKOUT_AMOUNT
    )
    check_pin = RegisterCap.PAYMENT_ACCOUNT_OTHER not in caps

    if (account_id := request_json.account_id) is None:
        raise ValidationException("account_id_missing")

    if check_pin and request_json.pin is None:
        raise ValidationException("pin_missing")

    req = CheckoutAccountRequest(
        account_id=account_id,
        nonce=request_json.nonce,
        cart_items=request_json.products,
        pin_code=request_json.pin,
    )
    resp = checkout_service.checkout_account(
        context, req, register, check_pin, max_amount
    )

    return CheckoutAccountResponse(
        balance_before=resp.balance_before,
        balance_after=resp.balance_after,
        transaction_id=resp.transaction_id,
    )


@fapi_auth.post(
    "/api/checkout/account/self/",
    response_model=CheckoutAccountResponse,
    dependencies=[
        Depends(
            RegisterCapabilities(
                require_any_of=[
                    RegisterCap.PAYMENT_ACCOUNT_SELF,
                ]
            )
        )
    ],
)
def api_checkout_account_self(
    request_json: CheckoutAccountJson,
    context: UserRequestContext = Depends(dependencies.login_required),
) -> CheckoutAccountResponse:
    """
    Controller for /api/checkout/account/self/, which tries to perform a checkout
    for a users own account.
    """
    user = context.user
    register = register_service.get_register_for_user(context.db_session, user)
    max_amount = MAX_CHECKOUT_AMOUNT_ADMIN if user.is_admin else MAX_CHECKOUT_AMOUNT

    if not user.account:
        raise ValidationException("no_linked_account")
    account_id = user.account.id
    if not user_condition_service.verify_accepted_condition(context):
        raise ValidationException("not_accepted_terms")

    req = CheckoutAccountRequest(
        account_id=account_id,
        nonce=request_json.nonce,
        cart_items=request_json.products,
        pin_code=request_json.pin,
    )
    resp = checkout_service.checkout_account(
        context, req, register, check_pin=False, max_checkout_amount=max_amount
    )

    return CheckoutAccountResponse(
        balance_before=resp.balance_before,
        balance_after=resp.balance_after,
        transaction_id=resp.transaction_id,
    )


class CheckoutReaderResponse(CheckoutAccountResponse):
    accountName: str


@fapi.post(
    "/api/checkout/account/reader/",
    response_model=CheckoutReaderResponse,
    dependencies=[
        Depends(
            RegisterCapabilities(
                require_any_of=[
                    RegisterCap.PAYMENT_ACCOUNT_AUTHED,
                ]
            )
        )
    ],
)
def api_checkout_account_reader(
    request_json: CheckoutAccountReaderJson,
    context: UserRequestContext = Depends(dependencies.login_required),
) -> CheckoutReaderResponse:
    """
    Controller for /api/checkout/account/reader/, which tries to perform a checkout
    for a users own account using a pre-registered card.
    """
    register = register_service.get_register_for_user(context.db_session, context.user)
    max_amount = (
        MAX_CHECKOUT_AMOUNT_ADMIN if context.user.is_admin else MAX_CHECKOUT_AMOUNT
    )

    account = checkout_service.get_account_for_card_secret(
        context.db_session, request_json.cardSecret
    )

    req = CheckoutAccountRequest(
        account_id=account.id,
        nonce=request_json.nonce,
        cart_items=request_json.products,
        pin_code=None,
    )

    resp = checkout_service.checkout_account(
        context, req, register, check_pin=False, max_checkout_amount=max_amount
    )

    return CheckoutReaderResponse(
        balance_before=resp.balance_before,
        balance_after=resp.balance_after,
        transaction_id=resp.transaction_id,
        accountName=account.name,
    )


class RegisterCheckoutCardRequest(pydantic.BaseModel):
    cardSecret: CardSecretStr
    accountId: int
    pin: PinStr


@fapi_auth.post(
    "/api/reader/register/",
    status_code=HTTPStatus.NO_CONTENT,
    response_class=Response,
    dependencies=[
        Depends(
            RegisterCapabilities(
                require_any_of=[
                    RegisterCap.PAYMENT_ACCOUNT_AUTHED,
                    # TODO Should this still require pin, when payments normally dont?
                    RegisterCap.PAYMENT_ACCOUNT_OTHER,
                ]
            )
        )
    ],
)
def register_checkout_card(
    request_json: RegisterCheckoutCardRequest,
    context: UserRequestContext = Depends(dependencies.login_required),
) -> None:
    checkout_service.register_card_for_account(
        context, request_json.accountId, request_json.cardSecret, request_json.pin
    )


class DeleteTransactionResponse(pydantic.BaseModel):
    transaction: TransactionData


@fapi_auth.delete(
    "/api/transactions/{transaction_id}/",
    dependencies=[
        Depends(RegisterCapabilities(require_any_of=[RegisterCap.PAYMENT_UNDO]))
    ],
)
def api_checkout_undo(
    transaction_id: int,
    context: UserRequestContext = Depends(dependencies.login_required),
) -> DeleteTransactionResponse:
    transaction = transaction_service.get_by_id(context.db_session, transaction_id)

    if datetime.now() - transaction.created_at > MAX_UNDO_TIMEOUT:
        raise AuthorizationException("delete_timeout_expired")

    transaction_service.delete_by_id(context, transaction_id)

    return DeleteTransactionResponse(transaction=transaction.to_pydantic())


class ApiCheckoutReasonsResponse(pydantic.BaseModel):
    reasons: list[AdminCheckoutReason]


@fapi.get(
    "/api/checkout/reasons/",
    dependencies=[
        Depends(RegisterCapabilities(require_any_of=[RegisterCap.PAYMENT_OTHER]))
    ],
)
def api_checkout_other_reasons(
    db_session: Session = Depends(get_db),
) -> ApiCheckoutReasonsResponse:

    reasons = setting_service.get_db_settings(db_session).checkout_reasons

    return ApiCheckoutReasonsResponse(
        reasons=[AdminCheckoutReason(name=r.name, pin=PinStr(r.pin)) for r in reasons]
    )
