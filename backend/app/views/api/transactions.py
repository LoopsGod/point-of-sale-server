from fastapi import Depends, Query

from app import dependencies
from app.dependencies import (
    AccountRequestContext,
)
from app.models.transaction import TransactionData
from app.repository.util.pagination import (
    PageParameters,
    PaginatedResponse,
    SearchParameters,
    SortParameters,
    page_sort_parameters,
)
from app.service import (
    transaction_service,
)
from app.views.api import fapi
from app.repository.util.filters import DateTimeFilter, LiteralEqualsFilter, NullFilter


@fapi.get("/api/transactions/", response_model=PaginatedResponse[TransactionData])
def api_transactions(
    page_params: PageParameters = Depends(),
    search_params: SearchParameters = Depends(),
    sort_params: SortParameters = Depends(
        page_sort_parameters(
            "created_at",
            default_sort="-created_at",
        )
    ),
    deleted_filter: NullFilter | None = Query(None, alias="deletedFilter"),
    transaction_type_filter: LiteralEqualsFilter
    | None = Query(None, alias="transactionTypeFilter"),
    product_filter: LiteralEqualsFilter | None = Query(None, alias="productFilter"),
    created_at_filter: DateTimeFilter | None = Query(None, alias="createdAtFilter"),
    method_filter: LiteralEqualsFilter | None = Query(None, alias="methodFilter"),
    context: AccountRequestContext = Depends(dependencies.account_required),
) -> PaginatedResponse[TransactionData]:
    account_filter = LiteralEqualsFilter(context.account.id)

    return transaction_service.paginated_search_all_v2(
        context.db_session,
        page_params,
        search_params,
        sort_params,
        None,  # id filter
        deleted_filter,
        transaction_type_filter,
        product_filter,
        account_filter,
        created_at_filter,
        method_filter,
        None,  # sepa filter
    )
