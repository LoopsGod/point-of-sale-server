import pydantic
from fastapi import Depends
from sqlalchemy.orm import Session

from app.dependencies import get_db
from app.service.slide_service import (
    get_bpm,
    get_active_accounts,
    get_slide_products,
    get_trends,
)
from app.views.api import fapi


class ApiSlideProductsResponse(pydantic.BaseModel):
    class Product(pydantic.BaseModel):
        name: str
        price: int
        add_spacing: bool

    products: list[Product]


class ApiSlideBpmResponse(pydantic.BaseModel):
    bpm: float


class ApiSlideActiveResponse(pydantic.BaseModel):
    active: int


class ApiSlideTrendsResponse(pydantic.BaseModel):
    class Trend(pydantic.BaseModel):
        name: str
        sales: int
        trend: float | None

    trends: list[Trend]


@fapi.get("/api/slides/products/", response_model=ApiSlideProductsResponse)
def api_slide_products(session: Session = Depends(get_db)) -> ApiSlideProductsResponse:
    products = get_slide_products(session)

    response = []
    for product in products:
        response.append(
            ApiSlideProductsResponse.Product(
                name=product.name,
                price=product.product.price,
                add_spacing=product.add_spacing,
            )
        )

    return ApiSlideProductsResponse(products=response)


@fapi.get("/api/slides/bpm/", response_model=ApiSlideBpmResponse)
def api_slide_bpm(session: Session = Depends(get_db)) -> ApiSlideBpmResponse:
    return ApiSlideBpmResponse(bpm=get_bpm(session))


@fapi.get("/api/slides/active/", response_model=ApiSlideActiveResponse)
def api_slide_active(session: Session = Depends(get_db)) -> ApiSlideActiveResponse:
    return ApiSlideActiveResponse(active=get_active_accounts(session))


@fapi.get("/api/slides/trends/", response_model=ApiSlideTrendsResponse)
def api_slide_trends(session: Session = Depends(get_db)) -> ApiSlideTrendsResponse:
    return ApiSlideTrendsResponse(trends=get_trends(session))
