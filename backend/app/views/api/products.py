import pydantic
from fastapi import Depends

from app.dependencies import UserRequestContext, login_required
from app.service import register_service
from app.views.api import fapi


class ApiProductsResponse(pydantic.BaseModel):
    class Category(pydantic.BaseModel):
        class Product(pydantic.BaseModel):
            id: int
            name: str
            price: int
            deposit: bool

        id: int
        name: str
        color: str
        products: list[Product]

    categories: list[Category]


@fapi.get("/api/products/", response_model=ApiProductsResponse)
def api_products(
    context: UserRequestContext = Depends(login_required),
) -> ApiProductsResponse:
    """
    Controller for /api/products/, which returns json containing a list of
    categories, each containing a list of products.
    """

    register = register_service.get_register_for_user(context.db_session, context.user)
    categories = []
    for category in register.categories:
        if category.deleted:
            continue

        products = []
        for product in category.products:
            if product.deleted:
                continue

            products.append(
                ApiProductsResponse.Category.Product(
                    id=product.id,
                    name=product.name,
                    price=product.price,
                    deposit=product.deposit,
                )
            )

        categories.append(
            ApiProductsResponse.Category(
                id=category.id,
                name=category.name,
                color=category.color,
                products=products,
            )
        )

    return ApiProductsResponse(categories=categories)
