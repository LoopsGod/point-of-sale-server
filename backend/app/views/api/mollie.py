from http import HTTPStatus

import pydantic
from fastapi import Form, Depends, Response
from sqlalchemy.orm import Session

from app.dependencies import RequestContext, UserRequestContext, get_db, login_required
from app.repository import mollie_repo
from app.service import mollie_service
from app.views.api import fapi
from app.views.exceptions import NotFoundException, AuthorizationException


class MollieCallbackForm(pydantic.BaseModel):
    id: str


# public usage for mollie
@fapi.post(
    "/api/mollie/webhook/",
    response_class=Response,
    status_code=HTTPStatus.NO_CONTENT,
)
def mollie_callback(
    mollie_id: str = Form(..., alias="id"),
    db_session: Session = Depends(get_db),
) -> None:
    payment = mollie_repo.get_by_mollie_id(db_session, mollie_id)

    if not payment:
        raise NotFoundException("payment_unknown")

    context = RequestContext(db_session)
    mollie_service.update_ideal_payment(context, payment)


class MollieStatusResponse(pydantic.BaseModel):
    accountId: int
    paymentStatus: str


@fapi.get("/api/mollie/status/{payment_id}/", response_model=MollieStatusResponse)
def mollie_status(
    payment_id: int,
    context: UserRequestContext = Depends(login_required),
) -> MollieStatusResponse:
    payment = mollie_repo.get_by_id(context.db_session, payment_id)

    if payment.account_id != context.user.account_id:
        raise AuthorizationException("forbidden")

    mollie_service.update_ideal_payment(context, payment)

    return MollieStatusResponse(
        accountId=payment.account_id,
        paymentStatus=payment.status,
    )
