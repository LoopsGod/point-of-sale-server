from typing import Literal

import pydantic
from fastapi import HTTPException
from starlette.requests import Request
from starlette.responses import JSONResponse

from app import fastapi_app
from app.views.exceptions import ApplicationException


# TODO #198 Make the error field more detailed
class ErrorResponse(pydantic.BaseModel):
    status: Literal["err"]
    error: str


@fastapi_app.exception_handler(ApplicationException)
def exception_handler(
    request: Request,
    e: ApplicationException,
) -> JSONResponse:
    return JSONResponse({"status": "err", "error": e.error}, status_code=e.code)


@fastapi_app.exception_handler(HTTPException)
def http_exception_handler(request: Request, e: HTTPException) -> JSONResponse:
    return JSONResponse({"status": "err", "error": e.detail}, status_code=e.status_code)
