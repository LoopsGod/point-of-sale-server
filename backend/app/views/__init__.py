"""
All the controllers for the webapp
"""

from app.views import error_handling  # noqa: F401

# Reexport for the fastapi in app/__init__.py
from app.views.api import (
    fapi as fapi,
    fapi_auth as fapi_auth,
)
from app.views.api_admin import fapi_admin as fapi_admin
