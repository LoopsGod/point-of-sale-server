from http import HTTPStatus
from typing import Any

import pydantic
from fastapi import Depends, Query, Response
from pydantic import BaseModel, Field

from app.dependencies import admin_required, UserRequestContext
from app.models import CONFLICTING_CAPABILITIES, RegisterCap
from app.models.register import RegisterData, RegisterCapStr
from app.service import register_service
from app.views.api_admin import fapi_admin
from app.repository import registers
from app.repository.util.filters import (
    BooleanFilter,
    ForeignKeyFilter,
    LiteralEqualsFilter,
    NullFilter,
)
from app.repository.util.pagination import (
    PageParameters,
    PaginatedResponse,
    SearchParameters,
    SortParameters,
    page_sort_parameters,
)


class RegistersResponse(BaseModel):
    registers: list[RegisterData]


@fapi_admin.get("/registers/", response_model=PaginatedResponse[RegisterData])
def api_admin_registers(
    page_params: PageParameters = Depends(),
    search_params: SearchParameters = Depends(),
    sort_params: SortParameters = Depends(
        page_sort_parameters(
            "name",
            "default",
            default_sort="-default,name",
        )
    ),
    id_filter: LiteralEqualsFilter | None = Query(None, alias="idFilter"),
    category_filter: ForeignKeyFilter | None = Query(None, alias="categoryFilter"),
    default_filter: BooleanFilter | None = Query(None, alias="defaultFilter"),
    deleted_filter: NullFilter | None = Query(None, alias="deletedFilter"),
    context: UserRequestContext = Depends(admin_required),
) -> PaginatedResponse[RegisterData]:
    return registers.paginated_search(
        context.db_session,
        page_params,
        search_params,
        sort_params,
        id_filter,
        category_filter,
        default_filter,
        deleted_filter,
    )


class ConflictingCapsResponse(BaseModel):
    # TODO This generates schema with empty object with additionalProperties
    #  I think that could be improved in pydantic with literal dict keys.
    conflictingCapabilities: dict[RegisterCapStr, list[RegisterCapStr]]


@fapi_admin.get("/registers/info", response_model=ConflictingCapsResponse)
def api_admin_registers_info() -> dict[str, Any]:
    conflicting_caps = {}
    for c in CONFLICTING_CAPABILITIES:
        conflicting_caps[c.name] = [i.name for i in CONFLICTING_CAPABILITIES[c]]

    return {"conflictingCapabilities": conflicting_caps}


class RegisterResponse(BaseModel):
    register_: RegisterData = Field(alias="register")


@fapi_admin.get("/registers/default", response_model=RegisterResponse)
def api_admin_register_default_get(
    context: UserRequestContext = Depends(admin_required),
) -> RegisterResponse:
    register = register_service.get_default_register(context.db_session)
    return RegisterResponse(register=register.to_pydantic())


class DefaultRegisterJson(pydantic.BaseModel):
    registerId: int


@fapi_admin.post("/registers/default", response_model=RegisterData)
def api_admin_register_default_post(
    request_json: DefaultRegisterJson,
    context: UserRequestContext = Depends(admin_required),
) -> RegisterData:
    register = register_service.set_default_register(context, request_json.registerId)
    return register.to_pydantic()


class RegisterCreateUpdateBody(pydantic.BaseModel):
    name: str
    categoryIds: list[int]
    capabilities: list[RegisterCap]


@fapi_admin.post(
    "/registers/", response_model=RegisterResponse, status_code=HTTPStatus.CREATED
)
def api_admin_register_create(
    request_json: RegisterCreateUpdateBody,
    context: UserRequestContext = Depends(admin_required),
) -> RegisterResponse:
    register = register_service.create(context, request_json)

    return RegisterResponse(register=register.to_pydantic())


@fapi_admin.get("/registers/{register_id}/")
def api_admin_register_get(
    register_id: int,
    context: UserRequestContext = Depends(admin_required),
) -> RegisterResponse:
    return RegisterResponse(
        register=register_service.get_by_id(context, register_id).to_pydantic()
    )


@fapi_admin.put("/registers/{register_id}/")
def api_admin_register_update(
    request_json: RegisterCreateUpdateBody,
    register_id: int,
    context: UserRequestContext = Depends(admin_required),
) -> RegisterResponse:
    register = register_service.update(context, register_id, request_json)

    return RegisterResponse(register=register.to_pydantic())


@fapi_admin.delete(
    "/registers/{register_id}/",
    response_class=Response,
    status_code=HTTPStatus.NO_CONTENT,
)
def api_admin_register_delete(
    register_id: int,
    context: UserRequestContext = Depends(admin_required),
) -> None:
    register_service.delete(context, register_id)
