from datetime import datetime, date
from typing import TypedDict

import pydantic
from fastapi import Depends
from sqlalchemy import select
from sqlalchemy.orm import Session

from app.dependencies import UserRequestContext
from app.dependencies import get_date_range, get_db, admin_required
from app.domains.exact.api.exact_api_session import ExactContext, exact_api
from app.domains.exact.comparison.bank_transaction import (
    BankTransaction,
    BankTransactionData,
)
from app.domains.exact.comparison import exact_comparison_service
from app.util.util import DateRangeParameters
from app.views.api_admin import fapi_admin


class ApiBankTransactionUploadResponse(pydantic.BaseModel):
    first_date: date | None
    last_date: date | None


class DayDict(TypedDict):
    count: int
    amount: int
    date: datetime


@fapi_admin.post(
    "/bank_transactions/exact/", response_model=ApiBankTransactionUploadResponse
)
def api_admin_bank_transactions_exact(
    year: int | None,
    context: UserRequestContext = Depends(admin_required),
    exact_context: ExactContext = Depends(exact_api),
) -> ApiBankTransactionUploadResponse:
    if year is None:
        year = date.today().year

    first_date, last_date = exact_comparison_service.compare_bank_transactions_to_exact(
        context, exact_context.api, year
    )

    return ApiBankTransactionUploadResponse(first_date=first_date, last_date=last_date)


class ApiBankTransactionResponse(pydantic.BaseModel):
    bankTransactions: list[BankTransactionData]


@fapi_admin.get("/bank_transactions/", response_model=ApiBankTransactionResponse)
def api_admin_bank_transactions(
    date_range: DateRangeParameters = Depends(get_date_range),
    session: Session = Depends(get_db),
) -> ApiBankTransactionResponse:
    transactions = session.scalars(
        select(BankTransaction)
        .filter(BankTransaction.date.between(date_range.start, date_range.end))
        .order_by(BankTransaction.date.asc())
        .filter_by(deleted_at=None)
    )
    return ApiBankTransactionResponse(
        bankTransactions=[t.to_pydantic() for t in transactions]
    )
