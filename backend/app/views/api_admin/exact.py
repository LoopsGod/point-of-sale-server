from app.domains.exact import exact_tasks as exact_tasks
from app.views.api_admin import fapi_admin
from app.domains.exact.exact_export_router import router as export_router
from app.domains.exact.exact_setup_router import router as setup_router
from app.domains.exact.exact_router import router
from app.domains.exact.exact_comparison_router import router as comparison_router


fapi_admin.include_router(
    export_router, prefix="/exact/export", tags=["Exact", "Export"]
)

fapi_admin.include_router(setup_router, prefix="/exact/setup", tags=["Exact", "Setup"])


fapi_admin.include_router(router, prefix="/exact", tags=["Exact"])

fapi_admin.include_router(
    comparison_router, prefix="/exact/comparison", tags=["Exact", "Comparison"]
)
