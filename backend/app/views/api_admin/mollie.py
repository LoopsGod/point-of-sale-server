from typing import Any

from fastapi import Depends, Query
from pydantic import BaseModel

from app.dependencies import UserRequestContext, admin_required
from app.models.ideal_payment import IdealPaymentData
from app.repository import mollie_repo
from app.repository.util.pagination import (
    PageParameters,
    PaginatedResponse,
    SearchParameters,
    SortParameters,
    page_sort_parameters,
)
from app.repository.util.filters import DateTimeFilter, LiteralEqualsFilter
from app.service import mollie_service
from app.views.api_admin import fapi_admin


@fapi_admin.get("/ideal/", response_model=PaginatedResponse[IdealPaymentData])
def api_admin_ideal_payments(
    page_params: PageParameters = Depends(),
    search_params: SearchParameters = Depends(),
    sort_params: SortParameters = Depends(
        page_sort_parameters(
            "createdAt",
            "updatedAt",
            "status",
            default_sort="-createdAt",
        )
    ),
    id_filter: LiteralEqualsFilter | None = Query(None, alias="idFilter"),
    created_at_filter: DateTimeFilter | None = Query(None, alias="createdAtFilter"),
    updated_at_filter: DateTimeFilter | None = Query(None, alias="updatedAtFilter"),
    account_filter: LiteralEqualsFilter | None = Query(None, alias="accountFilter"),
    context: UserRequestContext = Depends(admin_required),
) -> PaginatedResponse[IdealPaymentData]:
    return mollie_repo.paginated_search_all(
        context.db_session,
        page_params,
        search_params,
        sort_params,
        id_filter,
        created_at_filter,
        updated_at_filter,
        account_filter,
    )


class MollieAdminStatusResponse(BaseModel):
    idealPayment: IdealPaymentData
    mollieData: dict[str, Any]


@fapi_admin.post("/ideal/status/{payment_id}/")
def api_admin_mollie_status(
    payment_id: int,
    context: UserRequestContext = Depends(admin_required),
) -> MollieAdminStatusResponse:
    payment = mollie_repo.get_by_id(context.db_session, payment_id)

    ideal_payment, mollie_data = mollie_service.update_ideal_payment(context, payment)

    return MollieAdminStatusResponse(
        mollieData=mollie_data, idealPayment=ideal_payment.to_pydantic()
    )
