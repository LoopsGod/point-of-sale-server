import csv
import io
from datetime import datetime
from http import HTTPStatus

import pydantic
from fastapi import Depends, Response, Query
from pydantic import BaseModel, Field
from sepaxml import SepaDD

from app.dependencies import UserRequestContext, admin_required
from app.models import SepaBatchOrder
from app.models.account import DirectDebitAccountData
from app.models.sepa_batch import SepaBatchData, SepaBatchOrderData
from app.repository import sepa_repo
from app.repository.util.filters import BooleanFilter, LiteralEqualsFilter
from app.repository.util.pagination import (
    PaginatedResponse,
    PageParameters,
    SearchParameters,
    SortParameters,
    page_sort_parameters,
)
from app.service import audit_service, sepa_service, account_service
from app.tasks import sepa
from app.util.util import format_iban
from app.views.api_admin import fapi_admin
from app.views.exceptions import (
    ValidationException,
    NotFoundException,
)
from app.service import setting_service


class ApiSepaBatchesResponse(BaseModel):
    batches: list[SepaBatchData]


@fapi_admin.get("/sepa_batches/", response_model=PaginatedResponse[SepaBatchData])
def api_admin_sepa_batches(
    page_params: PageParameters = Depends(),
    search_params: SearchParameters = Depends(),
    sort_params: SortParameters = Depends(
        page_sort_parameters(
            "execution_date",
            "description",
            "deleted",
            default_sort="-execution_date,-deleted,description",
        )
    ),
    id_filter: LiteralEqualsFilter | None = Query(None, alias="idFilter"),
    success_filter: BooleanFilter | None = Query(None, alias="successFilter"),
    context: UserRequestContext = Depends(admin_required),
) -> PaginatedResponse[SepaBatchData]:
    return sepa_repo.paginated_search(
        context.db_session,
        page_params,
        search_params,
        sort_params,
        id_filter,
        success_filter,
    )


class SepaBatchUpdateJson(pydantic.BaseModel):
    description: str
    executionDate: datetime


class SepaBatchCreateJson(SepaBatchUpdateJson):
    accounts: list[int]


class ApiSepaBatchCreateResponse(BaseModel):
    batch_id: int


@fapi_admin.post(
    "/sepa_batches/",
    response_model=ApiSepaBatchCreateResponse,
    status_code=HTTPStatus.CREATED,
)
def api_admin_sepa_batches_post(
    request_json: SepaBatchCreateJson,
    context: UserRequestContext = Depends(admin_required),
) -> ApiSepaBatchCreateResponse:
    batch = sepa_service.create_sepa_batch(
        context=context,
        description=request_json.description,
        execution_date=request_json.executionDate,
        accounts=request_json.accounts,
    )
    return ApiSepaBatchCreateResponse(batch_id=batch.id)


class SepaBatchResponse(BaseModel):
    class AccountOrder(BaseModel):
        order: SepaBatchOrderData
        account: DirectDebitAccountData

    batch: SepaBatchData
    accounts: list[AccountOrder]


@fapi_admin.get("/sepa_batches/{batch_id}/")
def api_admin_sepa_batch_get(
    batch_id: int,
    context: UserRequestContext = Depends(admin_required),
) -> SepaBatchResponse:
    batch = sepa_service.get_batch_by_id(context, batch_id)
    accounts = sepa_service.get_pending_orders_with_user_accounts(context, batch)

    # This response is polymorphic with /api/admin/accounts/sepa/
    return SepaBatchResponse(
        batch=batch.to_pydantic(),
        accounts=[
            SepaBatchResponse.AccountOrder(
                account=account_service.account_and_user_direct_debit_to_direct_debit_account_data(
                    account, direct_debit
                ),
                order=order.to_pydantic(),
            )
            for order, account, _, direct_debit in accounts
        ],
    )


@fapi_admin.put("/sepa_batches/{batch_id}/", response_model=SepaBatchData)
def api_admin_sepa_batch_put(
    request_json: SepaBatchUpdateJson,
    batch_id: int,
    context: UserRequestContext = Depends(admin_required),
) -> SepaBatchData:
    batch = sepa_service.get_batch_by_id(context, batch_id)
    if batch.deleted:
        raise ValidationException("batch_deleted")

    if batch.success:
        raise ValidationException("batch_already_executed")

    batch.description = request_json.description
    batch.execution_date = request_json.executionDate

    audit_service.audit_create(context, "sepa.batch.update", object_id=batch_id)

    context.db_session.add(batch)
    context.db_session.commit()

    return batch.to_pydantic()


@fapi_admin.delete(
    "/sepa_batches/{batch_id}/",
    response_class=Response,
    status_code=HTTPStatus.NO_CONTENT,
)
def api_admin_sepa_batch_delete(
    batch_id: int,
    context: UserRequestContext = Depends(admin_required),
) -> None:
    batch = sepa_service.get_batch_by_id(context, batch_id)
    if batch.deleted:
        raise ValidationException("batch_deleted")

    if batch.success:
        raise ValidationException("batch_already_executed")

    sepa_service.delete_batch(context, batch)


@fapi_admin.post(
    "/sepa_batches/{batch_id}/execute/",
    response_class=Response,
    status_code=HTTPStatus.NO_CONTENT,
)
def api_admin_direct_debit_execute(
    batch_id: int, context: UserRequestContext = Depends(admin_required)
) -> None:
    batch = sepa_service.get_batch_by_id(context, batch_id)
    if batch.deleted:
        raise ValidationException("batch_deleted")

    if batch.success:
        raise ValidationException("batch_already_executed")
    sepa_service.execute_sepa_batch(context, batch)


class SepaMailResponse(BaseModel):
    mailCount: int


@fapi_admin.post(
    "/sepa_batches/{batch_id}/email/",
    response_model=SepaMailResponse,
    status_code=HTTPStatus.ACCEPTED,
)
def api_admin_direct_debit_email(
    batch_id: int,
    context: UserRequestContext = Depends(admin_required),
) -> SepaMailResponse:
    batch = sepa_service.get_batch_by_id(context, batch_id)
    if batch.deleted:
        raise ValidationException("batch_deleted")

    if batch.mail_date:
        raise ValidationException("batch_email_already_sent")

    audit_service.audit_create(context, "sepa.batch.email", object_id=batch_id)

    sepa.send_sepa_notification.delay(batch.id)

    number_recipients = batch.pending_orders.filter(
        SepaBatchOrder.deleted_at.is_(None)
    ).count()

    context.db_session.commit()

    return SepaMailResponse(mailCount=number_recipients)


class XMLResponse(Response):
    media_type = "text/xml"


@fapi_admin.get(
    "/sepa_batches/directdebit/{batch_id}.xml",
    response_class=Response,
    responses={
        "200": {"content": {"text/xml": {}}},
    },
)
def api_admin_direct_debit_xml(
    batch_id: int,
    context: UserRequestContext = Depends(admin_required),
) -> XMLResponse:
    batch = sepa_service.get_batch_by_id(context, batch_id)
    if batch.deleted:
        raise NotFoundException()

    sepa_config = sepa_service.get_direct_debit_config_from_settings(context.db_session)
    sepa = SepaDD(sepa_config, schema="pain.008.001.02")

    transactions = sepa_service.get_pending_orders_with_user_accounts(context, batch)

    for pending_order, account, _, direct_debit in transactions:
        if pending_order.deleted:
            continue

        if not direct_debit or not direct_debit.enabled:
            raise ValidationException("incomplete_account_data")

        if not direct_debit.iban or not direct_debit.bic:
            #  This cannot happen as we have a check constraint on the database,
            # but we need to make types happy.
            raise ValidationException("direct_debit_incomplete")

        iban = direct_debit.iban.replace(" ", "")
        bic = direct_debit.bic.replace(" ", "")

        payment = {
            "name": account.name,
            "IBAN": iban,
            "BIC": bic,
            "amount": pending_order.price,
            "type": "RCUR",
            "collection_date": batch.execution_date.date(),
            "mandate_id": direct_debit.mandate_id,
            "mandate_date": direct_debit.sign_date,
            "description": batch.description,
        }
        sepa.add_payment(payment)

    audit_service.audit_create(context, "sepa.batch.download.xml", object_id=batch_id)
    context.db_session.commit()

    body = sepa.export()
    return XMLResponse(content=body, media_type="text/xml")


class CSVResponse(Response):
    media_type = "text/csv"


@fapi_admin.get(
    "/sepa_batches/directdebit/{batch_id}.csv",
    response_class=Response,
    responses={
        "200": {"content": {"text/csv": {}}},
    },
)
def api_admin_direct_debit_csv(
    batch_id: int,
    context: UserRequestContext = Depends(admin_required),
) -> CSVResponse:
    batch = sepa_service.get_batch_by_id(context, batch_id)
    if batch.deleted:
        raise NotFoundException()

    #  Throws validation exception if not available.
    sepa_service.get_direct_debit_config_from_settings(context.db_session)

    str_io = io.StringIO()
    writer = csv.writer(str_io)
    writer.writerow(["Email", "Name", "IBAN", "Amount"])

    transactions = sepa_service.get_pending_orders_with_user_accounts(context, batch)

    for pending_order, account, user, direct_debit in transactions:
        if pending_order.deleted:
            continue

        if not direct_debit or not direct_debit.enabled:
            raise ValidationException("incomplete_account_data")

        if not direct_debit.iban or not direct_debit.bic:
            #  This cannot happen as we have a check constraint on the database,
            #  but we need to make types happy.
            raise ValidationException("direct_debit_incomplete")

        email = f"{account.name}"
        if user:
            email += f" <{user.username}>"
        price = pending_order.price / 100

        iban = format_iban(direct_debit.iban)

        writer.writerow([email, account.name, iban, f"€{price:.2f}"])

    audit_service.audit_create(context, "sepa.batch.download.csv", object_id=batch_id)
    context.db_session.commit()

    return CSVResponse(str_io.getvalue())


class DirectDebitConfigRequest(BaseModel):
    creditorName: str = Field(min_length=3, max_length=64)
    creditorIban: str = Field(min_length=3, max_length=64)
    creditorBic: str = Field(min_length=3, max_length=64)
    creditorIdentifier: str = Field(min_length=3, max_length=64)


class DirectDebitConfig(BaseModel):
    creditorName: str | None
    creditorIban: str | None
    creditorBic: str | None
    creditorIdentifier: str | None


@fapi_admin.get("/direct_debit/config/", response_model=DirectDebitConfig)
def api_admin_direct_debit_config(
    context: UserRequestContext = Depends(admin_required),
) -> DirectDebitConfig:
    db_settings = setting_service.get_db_settings(context.db_session)

    return DirectDebitConfig(
        creditorName=db_settings.direct_debit_creditor_name,
        creditorIban=db_settings.direct_debit_creditor_iban,
        creditorBic=db_settings.direct_debit_creditor_bic,
        creditorIdentifier=db_settings.direct_debit_creditor_identifier,
    )


@fapi_admin.put(
    "/direct_debit/config/",
    response_class=Response,
    status_code=HTTPStatus.NO_CONTENT,
)
def api_admin_direct_debit_config_put(
    request: DirectDebitConfigRequest,
    context: UserRequestContext = Depends(admin_required),
) -> None:
    db_settings = setting_service.get_db_settings(context.db_session)
    audit_service.audit_create(context, "direct_debit.config.update")

    db_settings.direct_debit_creditor_name = request.creditorName
    db_settings.direct_debit_creditor_iban = request.creditorIban
    db_settings.direct_debit_creditor_bic = request.creditorBic
    db_settings.direct_debit_creditor_identifier = request.creditorIdentifier
    setting_service.save_db_settings(context.db_session, db_settings)


@fapi_admin.delete(
    "/sepa_batches/{batch_id}/accounts/{account_id}/",
    response_class=Response,
    status_code=HTTPStatus.NO_CONTENT,
)
def api_admin_sepa_batch_order_delete(
    batch_id: int,
    account_id: int,
    context: UserRequestContext = Depends(admin_required),
) -> None:
    batch = sepa_service.get_batch_by_id(context, batch_id)
    if batch.deleted:
        raise NotFoundException()
    if batch.success:
        raise ValidationException("batch_already_executed")

    sepa_service.delete_pending_order(context, batch, account_id)
