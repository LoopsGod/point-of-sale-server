from http import HTTPStatus
from fastapi import Depends, Query
from sqlalchemy.orm import Session

from app.dependencies import UserRequestContext, admin_required
from app.dependencies import get_db
from app.models.category import CategoryData, NewCategoryData
from app.views.api_admin import fapi_admin
from app.views.exceptions import ResourceConflictException
from app.repository import categories as category_repo
from app.repository.util.pagination import (
    PageParameters,
    PaginatedResponse,
    SearchParameters,
    SortParameters,
    page_sort_parameters,
)
from app.repository.util.filters import LiteralEqualsFilter, NullFilter
from app.service import category_service


@fapi_admin.get("/categories/", response_model=PaginatedResponse[CategoryData])
def api_admin_categories_search(
    page_params: PageParameters = Depends(),
    search_params: SearchParameters = Depends(),
    sort_params: SortParameters = Depends(
        page_sort_parameters(
            "name",
            "position",
            "deleted_at",
            default_sort="-deleted_at,-position,name",
        )
    ),
    id_filter: LiteralEqualsFilter | None = Query(None, alias="idFilter"),
    deleted_filter: NullFilter | None = Query(None, alias="deletedFilter"),
    db_session: Session = Depends(get_db),
) -> PaginatedResponse[CategoryData]:
    return category_repo.get_all(
        db_session,
        page_params,
        search_params,
        sort_params,
        id_filter,
        deleted_filter,
    )


@fapi_admin.put("/categories/{category_id}/", response_model=CategoryData)
def api_admin_categories_put(
    category_id: int,
    updated_category: CategoryData,
    context: UserRequestContext = Depends(admin_required),
) -> CategoryData:
    c = category_service.update_category(context, updated_category)
    if category_id != c.id:
        raise ResourceConflictException("URL id not equal to object id.")

    context.db_session.commit()
    return c


@fapi_admin.post(
    "/categories/", response_model=CategoryData, status_code=HTTPStatus.CREATED
)
def api_admin_categories_post(
    new_category: NewCategoryData,
    context: UserRequestContext = Depends(admin_required),
) -> CategoryData:
    c = category_service.create_category(context, new_category)
    context.db_session.commit()
    return c
