import json
from http import HTTPStatus

import pydantic
from fastapi import Depends, Response
from pydantic import BaseModel, Field

from app.dependencies import UserRequestContext, admin_required
from app.service import setting_service, mail_service
from app.util.util import PinStr
from app.views.api_admin import fapi_admin
from app.views.exceptions import ValidationException


class ApiAdminSettings(BaseModel):
    notificationSepaDestination: str
    notificationAccountRequestDestination: str
    notificationProductQuantityDestination: str
    notificationSender: str = Field(min_length=3, max_length=64)


@fapi_admin.get("/settings/notification/")
def api_admin_settings_get(
    context: UserRequestContext = Depends(admin_required),
) -> ApiAdminSettings:
    settings = setting_service.get_db_settings(context.db_session)
    return ApiAdminSettings(
        notificationSepaDestination=settings.notification_sepa_destination,
        notificationAccountRequestDestination=settings.notification_account_request_destination,
        notificationProductQuantityDestination=settings.notification_product_quantity_destination,
        notificationSender=settings.notification_sender,
    )


@fapi_admin.post(
    "/settings/notification/",
    response_class=Response,
    status_code=HTTPStatus.NO_CONTENT,
)
def api_admin_settings_post(
    request: ApiAdminSettings,
    context: UserRequestContext = Depends(admin_required),
) -> None:
    db_settings = setting_service.get_db_settings(context.db_session)

    valid_notification_senderoptions = mail_service.get_treasurer_aliases()

    if request.notificationSender not in valid_notification_senderoptions:
        raise ValidationException("invalid_notification_sender")

    db_settings.notification_sender = request.notificationSender

    db_settings.notification_sepa_destination = request.notificationSepaDestination
    db_settings.notification_account_request_destination = (
        request.notificationAccountRequestDestination
    )
    db_settings.notification_product_quantity_destination = (
        request.notificationProductQuantityDestination
    )

    setting_service.save_db_settings(context.db_session, db_settings)


class SenderOptionResponse(BaseModel):
    options: list[str]


@fapi_admin.get("/settings/notification/sender_options/")
def api_admin_settings_notification_sender_options() -> SenderOptionResponse:
    return SenderOptionResponse(options=mail_service.get_treasurer_aliases())


class ExactOldDivisionResponse(BaseModel):
    oldDivision: int | None


@fapi_admin.get(
    "/settings/exact/old_division/", response_model=ExactOldDivisionResponse
)
def api_admin_exact_get_old_division(
    context: UserRequestContext = Depends(admin_required),
) -> ExactOldDivisionResponse:
    old_division = setting_service.get_db_settings(
        context.db_session
    ).exact_old_division
    return ExactOldDivisionResponse(oldDivision=old_division)


class AdminCheckoutReason(pydantic.BaseModel):
    name: str
    pin: PinStr | None


class AdminCheckoutReasonJson(pydantic.BaseModel):
    reasons: list[AdminCheckoutReason]


@fapi_admin.post(
    "/checkout/reasons/", response_class=Response, status_code=HTTPStatus.NO_CONTENT
)
def api_admin_checkout_reasons(
    request_json: AdminCheckoutReasonJson,
    context: UserRequestContext = Depends(admin_required),
) -> None:
    value = json.dumps([r.dict() for r in request_json.reasons])
    setting_service.update_settings(context, {"CHECKOUT_REASONS": value})
