from http import HTTPStatus

from fastapi import Depends, Query

from app.dependencies import UserRequestContext, admin_required
from app.models import User
from app.models.user import UserData
from app.repository import users
from app.models.user_direct_debit import UserDirectDebitData
from app.repository.util.filters import BooleanFilter, NullFilter, LiteralEqualsFilter
from app.repository.util.pagination import (
    PageParameters,
    SearchParameters,
    SortParameters,
    page_sort_parameters,
    PaginatedResponse,
)
from app.service import audit_service, user_service
from app.service.user_service import (
    UserDirectDebitUpdateRequest,
    UserCreateUpdateRequest,
)
from app.views.api_admin import fapi_admin
from app.views.exceptions import ValidationException, ResourceConflictException


@fapi_admin.put("/users/{user_id}/", response_model=UserData)
def api_update_admin_user(
    user_request: UserCreateUpdateRequest,
    user_id: int,
    context: UserRequestContext = Depends(admin_required),
) -> UserData:
    return user_service.update_user(context, user_request, user_id).to_pydantic()


@fapi_admin.get("/users/{user_id}/", response_model=UserData)
def api_admin_user_get(
    user_id: int,
    context: UserRequestContext = Depends(admin_required),
) -> UserData:
    user = user_service.get_by_id(context, user_id)
    return user.to_pydantic()


@fapi_admin.delete("/users/{user_id}/", response_model=UserData)
def api_admin_user_delete(
    user_id: int,
    context: UserRequestContext = Depends(admin_required),
) -> UserData:
    user = user_service.get_by_id(context, user_id)

    if user_id == context.user.id:
        raise ValidationException("cant_remove_self")

    if user.viaduct_user_id:
        raise ResourceConflictException("cant_remove_viaduct_user")

    #  Deleting an account implicitly disables direct debit. We cannot do this when
    #  there is still a DD batch pending
    user_service.check_disable_direct_debit(context, False, user.account_id)
    users.delete_user_direct_debit(context.db_session, user_id)
    user_service.update_account_allow_negative_balance(
        context, None, user.account_id, None
    )

    user.account_id = None
    user.deleted = True

    audit_service.audit_create(context, "user.delete", object_id=user_id)

    context.db_session.commit()
    return user.to_pydantic()


@fapi_admin.get("/users/", response_model=PaginatedResponse[UserData])
def api_admin_get_users(
    page_params: PageParameters = Depends(),
    search_params: SearchParameters = Depends(),
    sort_params: SortParameters = Depends(
        page_sort_parameters(
            "deleted",
            "username",
            "name",
            "register",
            default_sort="-deleted,username,name",
        )
    ),
    id_filter: LiteralEqualsFilter | None = Query(None, alias="idFilter"),
    admin_filter: BooleanFilter | None = Query(None, alias="adminFilter"),
    viaduct_filter: NullFilter | None = Query(None, alias="viaductFilter"),
    context: UserRequestContext = Depends(admin_required),
) -> PaginatedResponse[UserData]:
    return users.paginated_search(
        context.db_session,
        page_params,
        search_params,
        sort_params,
        id_filter,
        admin_filter,
        viaduct_filter,
    )


@fapi_admin.post("/users/", response_model=UserData, status_code=HTTPStatus.CREATED)
def api_admin_post_user(
    request_json: UserCreateUpdateRequest,
    context: UserRequestContext = Depends(admin_required),
) -> UserData:
    user = User.register_user(
        db_session=context.db_session,
        username=request_json.username,
        password=request_json.password,
        is_admin=request_json.isAdmin,
        viaduct_user_id=request_json.viaductUserId,
    )
    user.name = request_json.name
    user.account_id = request_json.accountId
    user.long_login = request_json.longLogin
    user.register_id = request_json.registerId

    audit_service.audit_create(context, "user.create", object_id=user.id)

    context.db_session.commit()

    return user.to_pydantic()


@fapi_admin.put("/users/{user_id}/direct_debit/", response_model=UserDirectDebitData)
def api_admin_users_update_direct_debit(
    request_json: UserDirectDebitUpdateRequest,
    user_id: int,
    context: UserRequestContext = Depends(admin_required),
) -> UserDirectDebitData:
    result = user_service.update_user_direct_debit(context, user_id, request_json)
    audit_service.audit_create(context, "user.update.direct_debit", object_id=user_id)
    context.db_session.commit()
    return result
