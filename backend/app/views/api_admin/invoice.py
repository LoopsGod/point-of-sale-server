from http import HTTPStatus

import pydantic
from fastapi import Depends, Response, File, UploadFile

from app.dependencies import admin_required, UserRequestContext
from app.models.invoice import InvoiceData
from app.repository import (
    invoices as invoices_repo,
)
from app.repository.util.pagination import (
    PageParameters,
    PaginatedResponse,
    SearchParameters,
    SortParameters,
    page_sort_parameters,
)
from app.service import invoice_service
from app.service.invoice.interpreter import Invoice
from app.service.invoice_service import (
    MatcherUpdate,
    ProductInventoryUpdate,
)
from app.views.api_admin import fapi_admin


class ApiMarkInvoiceImportedRequest(pydantic.BaseModel):
    invoice_id: int


class ApiInvoiceImportedResponse(pydantic.BaseModel):
    pass


class ApiInvoiceFileUrl(pydantic.BaseModel):
    url: str


class ApiCreateInvoiceResponse(pydantic.BaseModel):
    invoice: Invoice


class MatcherUpdateRequest(pydantic.BaseModel):
    updates: list[MatcherUpdate]


class ProductInventoryRequest(pydantic.BaseModel):
    updates: list[ProductInventoryUpdate]


@fapi_admin.get("/invoice/invoice_file/{invoice_file_id}")
def get_invoice_file_link(
    invoice_file_id: str,
    _: UserRequestContext = Depends(admin_required),
) -> ApiInvoiceFileUrl:
    file_url = invoice_service.get_invoice_url(invoice_file_id)

    return ApiInvoiceFileUrl(url=file_url)


@fapi_admin.post("/invoice/mark_imported")
def mark_imported(
    request: ApiMarkInvoiceImportedRequest,
    context: UserRequestContext = Depends(admin_required),
) -> ApiInvoiceImportedResponse:
    invoice_service.mark_invoice_imported(context.db_session, request.invoice_id)

    return ApiInvoiceImportedResponse()


@fapi_admin.post("/invoice/process")
def process_invoice(
    file: UploadFile = File(...),
    context: UserRequestContext = Depends(admin_required),
) -> ApiCreateInvoiceResponse:
    invoice = invoice_service.process_invoice(
        context.db_session, file.file, file.filename
    )

    return ApiCreateInvoiceResponse(invoice=invoice)


@fapi_admin.post(
    "/invoice/matcher/update",
    response_class=Response,
    status_code=HTTPStatus.NO_CONTENT,
)
def update_matcher(
    request: MatcherUpdateRequest,
    context: UserRequestContext = Depends(admin_required),
) -> None:
    invoice_service.update_matcher(context.db_session, request.updates)


@fapi_admin.post(
    "/invoice/quantity/add", response_class=Response, status_code=HTTPStatus.NO_CONTENT
)
def add_quantities_to_products(
    request: ProductInventoryRequest,
    context: UserRequestContext = Depends(admin_required),
) -> None:
    invoice_service.add_quantities(context.db_session, request.updates)


@fapi_admin.get("/invoice/", response_model=PaginatedResponse[InvoiceData])
def api_admin_invoices(
    context: UserRequestContext = Depends(admin_required),
    page_params: PageParameters = Depends(),
    search_params: SearchParameters = Depends(),
    sort_params: SortParameters = Depends(
        page_sort_parameters(
            "invoice_id",
            default_sort="-created_at",
        )
    ),
) -> PaginatedResponse[InvoiceData]:
    return invoices_repo.get_all(
        context.db_session,
        page_params,
        search_params,
        sort_params,
    )
