import pydantic
from fastapi import Depends
from sqlalchemy import func, and_, text, case

from app.dependencies import UserRequestContext, get_date_range, admin_required
from app.models import Product, Order, Transaction
from app.util.util import DateRangeParameters
from app.views.api_admin import fapi_admin


class ApiAdminLedgerTransactionResponseItem(pydantic.BaseModel):
    productName: str
    count: int
    totalPrice: int


class ApiAdminLedgerTransactionResponse(pydantic.BaseModel):
    products: list[ApiAdminLedgerTransactionResponseItem]


@fapi_admin.get("/ledgers/{ledger_id}/transactions/count/")
def api_admin_ledgers_transactions_count(
    ledger_id: int,
    date_range: DateRangeParameters = Depends(get_date_range),
    context: UserRequestContext = Depends(admin_required),
) -> ApiAdminLedgerTransactionResponse:
    # In this query we filter Transaction is none since we aren't interested in
    # bookkeeping. Just the number of products actually bought

    # When order is deleted we subtract 1, as there are 2 orders for a deleted purchase
    query = (
        context.db_session.query(
            Product.name,
            func.sum(case((Order.deleted_at.isnot(None), -1), else_=1)).label("c"),
            func.sum(Order.price),
        )
        .select_from(Order)
        .filter(and_(Order.created_at.between(date_range.start, date_range.end)))
        .join(Transaction, Transaction.id == Order.transaction_id)
        .join(
            Product,
            and_(Product.id == Order.product_id, Product.ledger_id == ledger_id),
        )
        .group_by(Product.name, Order.product_id)
        .order_by(text("c DESC"))
    )

    return ApiAdminLedgerTransactionResponse(
        products=[
            ApiAdminLedgerTransactionResponseItem(
                productName=e[0],
                count=e[1],
                totalPrice=e[2],
            )
            for e in query.all()
        ]
    )
