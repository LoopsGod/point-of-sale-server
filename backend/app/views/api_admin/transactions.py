from fastapi import Depends, Query
from pydantic import BaseModel

from app.dependencies import UserRequestContext, admin_required
from app.models.transaction import TransactionData
from app.repository.util.filters import DateTimeFilter, LiteralEqualsFilter, NullFilter
from app.repository.util.pagination import (
    PageParameters,
    PaginatedResponse,
    SearchParameters,
    SortParameters,
    page_sort_parameters,
)
from app.service import transaction_service
from app.service.transaction_service import ListTransactionParameters
from app.views.api_admin import fapi_admin


class ApiAdminTransactionDeleteResponse(BaseModel):
    transaction: TransactionData


@fapi_admin.delete("/transactions/{transaction_id}/")
def api_admin_transactions_delete(
    transaction_id: int,
    context: UserRequestContext = Depends(admin_required),
) -> ApiAdminTransactionDeleteResponse:
    transaction = transaction_service.delete_by_id(context, transaction_id)

    return ApiAdminTransactionDeleteResponse(transaction=transaction.to_pydantic())


@fapi_admin.get("/transactions/", response_model=PaginatedResponse[TransactionData])
def api_admin_transactions(
    params: ListTransactionParameters = Depends(),
    page_params: PageParameters = Depends(),
    context: UserRequestContext = Depends(admin_required),
) -> PaginatedResponse[TransactionData]:
    return transaction_service.paginated_search_all(context, params, page_params)


@fapi_admin.get("/v2/transactions/", response_model=PaginatedResponse[TransactionData])
def api_admin_transactions_v2(
    page_params: PageParameters = Depends(),
    search_params: SearchParameters = Depends(),
    sort_params: SortParameters = Depends(
        page_sort_parameters(
            "created_at",
            default_sort="-created_at",
        )
    ),
    id_filter: LiteralEqualsFilter | None = Query(None, alias="idFilter"),
    deleted_filter: NullFilter | None = Query(None, alias="deletedFilter"),
    transaction_type_filter: LiteralEqualsFilter
    | None = Query(None, alias="transactionTypeFilter"),
    product_filter: LiteralEqualsFilter | None = Query(None, alias="productFilter"),
    account_filter: LiteralEqualsFilter | None = Query(None, alias="accountFilter"),
    created_at_filter: DateTimeFilter | None = Query(None, alias="createdAtFilter"),
    method_filter: LiteralEqualsFilter | None = Query(None, alias="methodFilter"),
    sepa_batch_filter: LiteralEqualsFilter
    | None = Query(None, alias="sepaBatchFilter"),
    context: UserRequestContext = Depends(admin_required),
) -> PaginatedResponse[TransactionData]:
    return transaction_service.paginated_search_all_v2(
        context.db_session,
        page_params,
        search_params,
        sort_params,
        id_filter,
        deleted_filter,
        transaction_type_filter,
        product_filter,
        account_filter,
        created_at_filter,
        method_filter,
        sepa_batch_filter,
    )
