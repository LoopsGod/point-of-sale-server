from datetime import date
from http import HTTPStatus
from typing import Any

from fastapi import Depends, Query, Response
from sqlalchemy import func, and_, text, or_
from sqlalchemy.orm import joinedload, Session
from starlette.responses import JSONResponse

from app.dependencies import UserRequestContext, get_db, admin_required, get_date_range
from app.models import (
    Transaction,
    Order,
    Account,
    Product,
    ProductData,
    Category,
)
from app.models.account_type import AccountType
from app.models.product import NewProductData
from app.repository.util.filters import (
    BooleanFilter,
    ForeignKeyFilter,
    LiteralEqualsFilter,
    NumberFilter,
    NullFilter,
)
from app.repository.util.pagination import (
    PageParameters,
    SortParameters,
    PaginatedResponse,
    SearchParameters,
    page_sort_parameters,
)
from app.service import product_service
from app.util.util import DatetimeRangeParameters
from app.views.api_admin import fapi_admin


@fapi_admin.get("/products/{product_id}/count_per_day/")
def api_admin_products_count_per_day(
    product_id: int,
    year: int | None,
    context: UserRequestContext = Depends(admin_required),
) -> product_service.ProductCountComparedPerDay:
    if year is None:
        year = date.today().year

    return product_service.count_per_year(context, product_id, year)


@fapi_admin.get("/products/{product_id}/count/")
def api_admin_products_count(
    product_id: int,
    date_range: DatetimeRangeParameters = Depends(get_date_range),
    context: UserRequestContext = Depends(admin_required),
) -> list[dict[str, Any]]:
    q1 = (
        context.db_session.query(Transaction.id, func.count(Transaction.id).label("c"))
        .select_from(Transaction)
        .join(Order)
        .filter(
            and_(
                Order.product_id == product_id,
                Transaction.deleted_at.is_(None),
                Order.created_at.between(date_range.start, date_range.end),
            )
        )
        .group_by(Transaction.id)
    ).cte("tmp")
    # This would be 'with' in SQL, the second query will join on this

    q2 = (
        context.db_session.query(Account.name, func.sum(q1.c.c).label("s"))
        .select_from(Order)
        .join(Account)
        .join(q1, q1.c.id == Order.transaction_id)
        .filter(
            or_(
                Account.type == AccountType.USER.value,
                Account.type == AccountType.EVENT.value,
            )
        )
        .group_by(Account.name)
        .order_by(text("s desc"))
        .limit(10)
    )

    return [{"accountName": e[0], "count": int(e[1])} for e in q2.all()]


#  FIXME: remove this when frontend no longer uses it.
@fapi_admin.get(
    "/products/all/",
    deprecated=True,
    response_class=Response,
    status_code=HTTPStatus.NO_CONTENT,
)
def api_admin_products(
    db_session: Session = Depends(get_db),
) -> JSONResponse:
    models = {
        "products": (
            db_session.query(Product)
            .order_by(Product.name)
            .options(joinedload(Product.categories))
        ),
        "categories": db_session.query(Category).order_by(Category.position),
    }

    serialized: dict[str, Any] = {}
    for name, model in models.items():
        serialized[name] = {
            "data": [x.serialize() for x in model.all()],
        }
    return JSONResponse(serialized)


@fapi_admin.get("/products/", response_model=PaginatedResponse[ProductData])
def api_admin_products_search(
    page_params: PageParameters = Depends(),
    search_params: SearchParameters = Depends(),
    sort_params: SortParameters = Depends(
        page_sort_parameters(
            "name",
            "price",
            "quantity",
            "ledger",
            "deposit",
            "deleted_at",
            default_sort="-deleted_at,name",
        )
    ),
    price_filter: NumberFilter | None = Query(None, alias="priceFilter"),
    deposit_filter: BooleanFilter | None = Query(None, alias="depositFilter"),
    quantity_filter: NumberFilter | None = Query(None, alias="quantityFilter"),
    category_filter: ForeignKeyFilter | None = Query(None, alias="categoryFilter"),
    ledger_filter: ForeignKeyFilter | None = Query(None, alias="ledgerFilter"),
    slide_filter: BooleanFilter | None = Query(None, alias="slideFilter"),
    deleted_filter: NullFilter | None = Query(None, alias="deletedFilter"),
    id_filter: LiteralEqualsFilter | None = Query(None, alias="idFilter"),
    db_session: Session = Depends(get_db),
) -> PaginatedResponse[ProductData]:
    return product_service.paginated_search_all(
        db_session,
        page_params,
        search_params,
        sort_params,
        price_filter,
        deposit_filter,
        quantity_filter,
        category_filter,
        ledger_filter,
        slide_filter,
        deleted_filter,
        id_filter,
    )


@fapi_admin.put("/products/{product_id}/", response_model=ProductData)
def api_admin_products_put(
    product_id: int,
    updated_product: ProductData,
    context: UserRequestContext = Depends(admin_required),
) -> ProductData:
    p = product_service.update_product(context, updated_product)
    context.db_session.commit()
    return p


@fapi_admin.post(
    "/products/", response_model=ProductData, status_code=HTTPStatus.CREATED
)
def api_admin_products_post(
    new_product: NewProductData,
    context: UserRequestContext = Depends(admin_required),
) -> ProductData:
    p = product_service.create_product(context, new_product)
    context.db_session.commit()
    return p


@fapi_admin.delete(
    "/products/{product_id}/",
    status_code=HTTPStatus.NO_CONTENT,
    response_class=Response,
)
def api_admin_products_delete(
    product_id: int,
    context: UserRequestContext = Depends(admin_required),
) -> None:
    product_service.delete_product(context, product_id)
    context.db_session.commit()
