from datetime import datetime, date
from http import HTTPStatus
from typing import Literal

from fastapi import Depends, Response, Query
from pydantic import BaseModel, validator, Field
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

from app import dependencies
from app.dependencies import UserRequestContext, get_db, admin_required
from app.domains.account.account_ledger_router import router as accounts_ledger_router
from app.models.account import (
    Account,
    AccountRequestStatus,
    AccountData,
    DirectDebitAccountData,
    SerializedAccountRequest,
)
from app.models.account_type import AccountType
from app.models.checkout import CheckoutMethod
from app.repository import accounts
from app.repository.util.filters import (
    BooleanFilter,
    NullFilter,
    NumberFilter,
    LiteralEqualsFilter,
    LastUsageFilter,
)
from app.repository.util.pagination import (
    PageParameters,
    PaginatedResponse,
    SearchParameters,
    SortParameters,
    page_sort_parameters,
)
from app.service import account_service, sepa_service, checkout_service
from app.service import audit_service
from app.service import register_service
from app.service.account_service import BalanceAtDate
from app.service.checkout_service import CheckoutDirectRequest
from app.views.api_admin import fapi_admin
from app.views.exceptions import (
    ResourceConflictException,
    NotFoundException,
    ValidationException,
)

fapi_admin.include_router(
    accounts_ledger_router, prefix="/accounts/ledgers", tags=["Accounts", "Ledgers"]
)


class ApiAdminAccountResponse(BaseModel):
    accounts: list[AccountData]


@fapi_admin.get("/accounts/", response_model=PaginatedResponse[AccountData])
def api_admin_accounts(
    page_params: PageParameters = Depends(),
    search_params: SearchParameters = Depends(),
    sort_params: SortParameters = Depends(
        page_sort_parameters(
            "name",
            "deleted",
            "type",
            "allowNegative",
            "balance",
            default_sort="-deleted,type,name",
        )
    ),
    id_filter: LiteralEqualsFilter | None = Query(None, alias="idFilter"),
    type_filter: LiteralEqualsFilter | None = Query(None, alias="typeFilter"),
    allow_negative_filter: BooleanFilter
    | None = Query(None, alias="allowNegativeFilter"),
    deleted_filter: NullFilter | None = Query(None, alias="deletedFilter"),
    balance_filter: NumberFilter | None = Query(None, alias="balanceFilter"),
    last_usage_filter: LastUsageFilter | None = Query(None, alias="lastUsageFilter"),
    context: UserRequestContext = Depends(dependencies.admin_required),
) -> PaginatedResponse[AccountData]:
    return accounts.paginated_search(
        context.db_session,
        page_params,
        search_params,
        sort_params,
        id_filter,
        type_filter,
        allow_negative_filter,
        deleted_filter,
        balance_filter,
        last_usage_filter,
    )


class ApiAccountsSepaResponseItem(BaseModel):
    account: DirectDebitAccountData


class ApiAccountsSepaResponse(BaseModel):
    accounts: list[ApiAccountsSepaResponseItem]


@fapi_admin.get("/accounts/sepa/", response_model=ApiAccountsSepaResponse)
def api_admin_accounts_sepa(
    context: UserRequestContext = Depends(dependencies.admin_required),
) -> ApiAccountsSepaResponse:
    accounts_with_direct_debit = (
        account_service.get_all_eligible_for_direct_debit_batch(context)
    )

    # This API response is a subset of the data in
    # /api/admin/sepa_batches/<int:batch_id>/accounts/
    # to be able to share the view logic to share view
    # logic for creating and viewing batches.
    accounts = [
        ApiAccountsSepaResponseItem(account=acc) for acc in accounts_with_direct_debit
    ]

    return ApiAccountsSepaResponse(accounts=accounts)


@fapi_admin.delete(
    "/accounts/{account_id}/",
    response_class=Response,
    status_code=HTTPStatus.NO_CONTENT,
)
def api_admin_account_delete(
    account_id: int, context: UserRequestContext = Depends(dependencies.admin_required)
) -> None:
    account = context.db_session.get(Account, account_id)
    if not account:
        raise NotFoundException()

    if not account.deleted:
        if (
            account.type == AccountType.INCOME_LEDGER
            or account.type == AccountType.PRODUCT_LEDGER
        ):
            raise ResourceConflictException("account_is_ledger")
        elif account.balance != 0:
            raise ResourceConflictException("account_has_balance")

        account.deleted = True

        audit_service.audit_create(
            context,
            "account.archive",
            object_id=account_id,
        )
    else:
        account.deleted = False

        audit_service.audit_create(
            context,
            "account.unarchive",
            object_id=account_id,
        )

    context.db_session.add(account)
    context.db_session.commit()


class AccountUpdateRequest(BaseModel):
    name: str | None = Field(None, min_length=3, max_length=64)
    description: str | None = Field(None, max_length=128)
    pin: str | None = Field(None, max_length=4)

    allowNegativeBalance: bool | None

    def partial_populate_account(self, account: Account) -> None:
        update_data = self.dict(exclude_unset=True)

        account.name = update_data["name"] if "name" in update_data else account.name
        account.description = (
            update_data["description"]
            if "description" in update_data
            else account.description
        )

        if "allowNegativeBalance" in update_data and account.type == AccountType.EVENT:
            #  Only allowed to update allow negative when this is an event account
            account.allow_negative_balance = update_data["allowNegativeBalance"]

        if "pin" in update_data:
            account_service.set_account_pin(account, update_data["pin"])


class AccountCreateRequest(AccountUpdateRequest):
    name: str = Field(min_length=3, max_length=64)
    type: AccountType = Field(AccountType.USER)

    @validator("type")
    def validate_type(
        cls, v: AccountType, values: dict[str, str | bool | date | None]
    ) -> AccountType:
        if v == AccountType.USER or v == AccountType.EVENT:
            return v

        if v == AccountType.PRODUCT_LEDGER:
            assert values.get("pin") is None, "Pin cannot be set for ledger account."
            return v

        raise ValueError("Cannot create this type ledger account")

    def populate_account(self, account: Account) -> None:
        account.name = self.name
        account.description = self.description
        account.type = self.type

        if self.allowNegativeBalance is not None and account.type == AccountType.EVENT:
            #  Only allowed to update allow negative when this is an event account
            account.allow_negative_balance = self.allowNegativeBalance

        if self.pin:
            account_service.set_account_pin(account, self.pin)
        else:
            self.pin = None


@fapi_admin.put("/accounts/{account_id}/", response_model=AccountData)
def api_admin_account_put(
    account_id: int,
    request_json: AccountCreateRequest,
    context: UserRequestContext = Depends(dependencies.admin_required),
) -> AccountData:
    account: Account = account_service.get_by_id(context, account_id)

    request_json.populate_account(account)

    audit_service.audit_create(
        context,
        "account.edit",
        object_id=account_id,
    )
    context.db_session.commit()

    return account.to_pydantic()


class AccountUpgradeRequest(BaseModel):
    method: Literal["cash", "card", "transfer", "other"]
    nonce: str
    amount: int
    reason: str | None = Field(None, max_length=60)


class AccountUpgradeResponse(BaseModel):
    transactionId: int
    account: AccountData


@fapi_admin.post(
    "/accounts/{account_id}/upgrade/", response_model=AccountUpgradeResponse
)
def api_admin_account_upgrade(
    req_json: AccountUpgradeRequest,
    account_id: int,
    context: UserRequestContext = Depends(admin_required),
) -> AccountUpgradeResponse:
    account = account_service.get_by_id(context, account_id)

    if not (account.type == AccountType.USER or account.type == AccountType.EVENT):
        raise ValidationException("ledger_cannot_upgrade")

    req = CheckoutDirectRequest(
        method=CheckoutMethod(req_json.method),
        nonce=req_json.nonce,
        cart_items=[],
        upgrade_account_id=account.id,
        upgrade_amount=req_json.amount,
        reason=req_json.reason,
    )

    register = register_service.get_register_for_user(context.db_session, context.user)
    transaction_id = checkout_service.checkout_other(context, req, register)

    account = account_service.get_by_id(context, account_id)

    return AccountUpgradeResponse(
        transactionId=transaction_id, account=account.to_pydantic()
    )


@fapi_admin.patch(
    "/accounts/{account_id}/", status_code=200, response_model=AccountData
)
def api_admin_accounts_patch(
    request_json: AccountUpdateRequest,
    account_id: int,
    context: UserRequestContext = Depends(dependencies.admin_required),
) -> AccountData:
    account = account_service.get_by_id(context, account_id)
    request_json.partial_populate_account(account)

    if request_json.allowNegativeBalance and not account.allow_negative_balance:
        #  We did not set allow negative balance to the requested value,
        #  this means it was not allowed.
        raise ValidationException("cant_enable_negative_balance_for_non_event_account")

    try:
        context.db_session.add(account)
        context.db_session.flush()

        audit_service.audit_create(
            context,
            "account.edit",
            object_id=account_id,
        )
        context.db_session.commit()
    except IntegrityError as e:
        raise ResourceConflictException("duplicate_name") from e

    return account.to_pydantic()


@fapi_admin.post("/accounts/", status_code=201, response_model=AccountData)
def api_admin_accounts_post(
    request_json: AccountCreateRequest,
    context: UserRequestContext = Depends(dependencies.admin_required),
) -> AccountData:
    account = Account()

    request_json.populate_account(account)

    try:
        context.db_session.add(account)
        context.db_session.flush()

        audit_service.audit_create(context, "account.create", object_id=account.id)
        context.db_session.commit()
    except IntegrityError as e:
        raise ResourceConflictException("duplicate_name") from e

    return account.to_pydantic()


class ApiAdminAccountRequestsResponse(BaseModel):
    account_requests: list[SerializedAccountRequest]


@fapi_admin.get(
    "/account_requests/", response_model=PaginatedResponse[SerializedAccountRequest]
)
def api_admin_products_search(
    page_params: PageParameters = Depends(),
    search_params: SearchParameters = Depends(),
    sort_params: SortParameters = Depends(
        page_sort_parameters(
            "name", "createdAt", "status", default_sort="status,-createdAt"
        )
    ),
    db_session: Session = Depends(get_db),
) -> PaginatedResponse[SerializedAccountRequest]:
    return account_service.paginated_search_requests(
        db_session, page_params, search_params, sort_params
    )


class AccountRequestStatusJson(BaseModel):
    status: Literal["approve", "decline"]


@fapi_admin.post(
    "/account_requests/{account_request_id}/status",
    response_model=SerializedAccountRequest,
)
def api_admin_account_requests_status(
    request_json: AccountRequestStatusJson,
    account_request_id: int,
    context: UserRequestContext = Depends(dependencies.admin_required),
) -> SerializedAccountRequest:
    status = {
        "approve": AccountRequestStatus.APPROVED,
        "decline": AccountRequestStatus.DECLINED,
    }[request_json.status]

    account_request = account_service.set_account_request_status(
        context, account_request_id, status
    )

    return account_request.to_pydantic()


class DateParameter(BaseModel):
    date: date


class ApiAdminAccountBalanceResponse(BaseModel):
    balanceAt: BalanceAtDate


@fapi_admin.get("/accounts/balance/")
def api_admin_accounts_balance(
    at_date: date = Query(None, alias="date"),
    context: UserRequestContext = Depends(dependencies.admin_required),
) -> ApiAdminAccountBalanceResponse:
    if at_date:
        at_datetime = datetime.combine(at_date, datetime.min.time())
    else:
        at_datetime = datetime.now()

    balance_at = account_service.get_total_account_balance_at_date(context, at_datetime)

    return ApiAdminAccountBalanceResponse(balanceAt=balance_at)


class AccountSepaInfo(BaseModel):
    amount: int
    exec_date: date


class AccountDataWithSepa(AccountData):
    sepa: AccountSepaInfo | None = None


@fapi_admin.get("/accounts/{account_id}/", response_model=AccountDataWithSepa)
def api_admin_account_get(
    account_id: int, context: UserRequestContext = Depends(dependencies.admin_required)
) -> AccountDataWithSepa:
    account: Account = account_service.get_by_id(context, account_id)

    account_json = AccountDataWithSepa(**account.to_pydantic().dict())

    batch, order = sepa_service.get_pending_batch_for_account(context, account_id)

    if batch and order:
        account_json.sepa = AccountSepaInfo(
            amount=order.price, exec_date=batch.execution_date
        )

    return account_json
