import inspect


class ApplicationException(Exception):
    code = 500
    error = "internal_error"

    def __init__(self, error: str = None) -> None:
        super().__init__()

        if error is not None:
            self.error = error

    def __repr__(self) -> str:
        args = ", ".join(f"{k}={v}" for k, v in self.__dict__.items())
        return f'{self.__class__.__name__}("{args}")'

    def __reduce__(self):
        sig = inspect.signature(self.__init__)
        return type(self), tuple(sig.parameters.keys())

    def __str__(self) -> str:
        return self.error


class ValidationException(ApplicationException):
    code = 400
    error = "bad_request"


class AuthorizationException(ApplicationException):
    code = 401


class NotFoundException(ApplicationException):
    code = 404
    error = "not_found"


class NotAcceptableException(ApplicationException):
    code = 406
    error = "not_acceptable"


class ResourceConflictException(ApplicationException):
    code = 409


class UnprocessableEntityException(ApplicationException):
    code = 422


class NotImplementedException(ApplicationException):
    code = 501


class ServiceUnavailableException(ApplicationException):
    code = 503
    error = "service_unavailable"
