from app.dependencies import UserRequestContext
from app.models import Category
from app.models.category import CategoryData, NewCategoryData
from app.repository import categories as category_repo
from app.service import audit_service
from app.views.exceptions import NotFoundException


def update_category(
    context: UserRequestContext, category: CategoryData
) -> CategoryData:
    updated_category = category_repo.get_by_id(context.db_session, category.id)
    if not updated_category:
        raise NotFoundException("category_not_found")

    updated_category.name = category.name
    updated_category.position = category.position
    updated_category.color = category.color
    updated_category.deleted = category.deleted

    context.db_session.add(updated_category)
    context.db_session.flush()

    audit_service.audit_create(
        context, "category.update", object_id=updated_category.id
    )

    return updated_category.to_pydantic()


def create_category(
    context: UserRequestContext, category: NewCategoryData
) -> CategoryData:
    new_category = Category()
    new_category.name = category.name
    new_category.position = category.position
    new_category.color = category.color
    new_category.deleted = category.deleted

    # We need to flush to be able to create audit log.
    context.db_session.add(new_category)
    context.db_session.flush()

    audit_service.audit_create(context, "category.create", object_id=new_category.id)
    return new_category.to_pydantic()
