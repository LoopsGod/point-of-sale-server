from app.dependencies import UserRequestContext
from app.models import UserCondition
from app.repository import user_condition

CURRENT_CONDITION_VERSION = 1


def verify_accepted_condition(context: UserRequestContext) -> bool:
    user_accepted_condition = user_condition.find_accepted_condition_by_id(context)

    if user_accepted_condition is not None:
        return (
            user_accepted_condition.accepted_cond_version == CURRENT_CONDITION_VERSION
        )
    else:
        return False


def update_condition(context: UserRequestContext) -> UserCondition:
    user_cond = user_condition.find_accepted_condition_by_id(context)

    if user_cond is None:
        user_cond = UserCondition(
            user_id=context.user.id, accepted_cond_version=CURRENT_CONDITION_VERSION
        )
    else:
        user_cond.accepted_cond_version = CURRENT_CONDITION_VERSION

    user_condition.update_conditions(context.db_session, user_cond)
    return user_cond
