from app.dependencies import UserRequestContext, RequestContext
from app.models.audit_trail import AuditTrail, AuditTrailData
from app.repository import audit
from app.repository.util.pagination import (
    PageParameters,
    PaginatedResponse,
    SearchParameters,
    SortParameters,
)
from sqlalchemy.orm.session import Session

from app.repository.util.filters import DateTimeFilter, LiteralEqualsFilter


def audit_get_logs(
    db_session: Session,
    page_params: PageParameters,
    search_params: SearchParameters,
    sort_params: SortParameters,
    id_filter: LiteralEqualsFilter = None,
    created_at_filter: DateTimeFilter = None,
    user_filter: LiteralEqualsFilter = None,
) -> PaginatedResponse[AuditTrailData]:
    return audit.audit_get_logs(
        db_session,
        page_params,
        search_params,
        sort_params,
        id_filter,
        created_at_filter,
        user_filter,
    )


def audit_create(
    context: RequestContext, event: str, object_id: int = None, comment: str = None
) -> None:
    """Creates an audit log entry.

    Resource is deducted from event. Gets user from current session,
    if there is no current session this function will do nothing.

    The caller is responsible for committing the created entry to the database
    """
    idx = event.find(".")

    if idx == -1:
        raise ValueError("Event parameter does not specify resource.")

    resource = event[:idx]

    trail = AuditTrail(
        resource=resource,
        event=event,
        comment=comment,
        object_id=object_id,
    )
    if isinstance(context, UserRequestContext):
        trail.user_id = context.user.id

    context.db_session.add(trail)
