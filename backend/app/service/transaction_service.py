from sqlalchemy.orm.session import Session

import pydantic

from app.dependencies import UserRequestContext, RequestContext
from app.models import Transaction
from app.models.order import OrderType, Order
from app.models.transaction import TransactionType, TransactionData
from app.repository import transactions, orders
from app.repository.util.pagination import (
    PageParameters,
    PaginatedResponse,
    SearchParameters,
    SortParameters,
)
from app.repository.util.filters import DateTimeFilter, NullFilter, LiteralEqualsFilter
from app.service import order_service, audit_service
from app.views.exceptions import NotFoundException, ValidationException


def get_by_id(db_session: Session, transaction_id: int) -> Transaction:
    t = transactions.get_transaction_by_id(db_session, transaction_id)
    if not t:
        raise NotFoundException("transaction_not_found")
    return t


def delete_by_id(context: UserRequestContext, transaction_id: int) -> Transaction:
    """
    Deletes a transaction

    For every order in a transaction, this function creates a reversed order.
    This is a permanent action. When the delete needs to be undone, a new
    transaction (and orders) should be made instead.
    """
    transaction = transactions.get_transaction_by_id(context.db_session, transaction_id)

    if not transaction:
        raise NotFoundException("transaction_not_found")

    if transaction.deleted:
        raise ValidationException("transaction_already_deleted")

    transaction.deleted = True

    order_list = orders.get_by_transaction_id(context.db_session, transaction_id)
    order_service.delete_orders(context, order_list, do_commit=False)

    audit_service.audit_create(
        context,
        "transaction.delete",
        object_id=transaction.id,
    )

    context.db_session.commit()

    return transaction


def determine_transaction_type(transaction: Transaction) -> TransactionType:
    return determine_transaction_type_by_orders(transaction.orders)


def determine_transaction_type_by_orders(order_list: list[Order]) -> TransactionType:
    types = {o.type for o in order_list}

    if OrderType.INVALID in types:
        return TransactionType.INVALID

    if types == {OrderType.DIRECT_PAYMENT, OrderType.ACCOUNT_TOP_UP_OR_ACCOUNT_PAYMENT}:
        return TransactionType.TOP_UP

    if types == {
        OrderType.DIRECT_PAYMENT,
        OrderType.ACCOUNT_TOP_UP_OR_ACCOUNT_PAYMENT,
        OrderType.PRODUCT_PURCHASE,
    }:
        return TransactionType.TOP_UP_AND_PURCHASE

    if types == {OrderType.DIRECT_PAYMENT, OrderType.PRODUCT_PURCHASE}:
        return TransactionType.DIRECT_PURCHASE

    if types == {
        OrderType.ACCOUNT_TOP_UP_OR_ACCOUNT_PAYMENT,
        OrderType.PRODUCT_PURCHASE,
    }:
        return TransactionType.ACCOUNT_PURCHASE

    return TransactionType.INVALID


def save_and_set_type(
    context: RequestContext, transaction: Transaction, do_commit: bool = True
) -> None:
    # TODO hybrid_properties do not type
    transaction.type = determine_transaction_type(transaction)  # type: ignore[assignment] # noqa: E501

    transactions.save(context.db_session, transaction, do_commit)


class ListTransactionParameters(pydantic.BaseModel):
    accountId: int | None
    sepaBatchId: int | None
    deleted: bool | None


def paginated_search_all(
    context: UserRequestContext,
    params: ListTransactionParameters,
    page_params: PageParameters,
) -> PaginatedResponse[TransactionData]:
    return transactions.paginated_search_all(context.db_session, params, page_params)


def consolidate_orders_in_transactions(
    transactions: list[TransactionData],
) -> list[TransactionData]:
    for t in transactions:
        t.orders = order_service.consolidate_orders(t.orders)
    return transactions


def paginated_search_all_v2(
    db_session: Session,
    page_params: PageParameters,
    search_params: SearchParameters,
    sort_params: SortParameters,
    id_filter: LiteralEqualsFilter = None,
    deleted_filter: NullFilter = None,
    transaction_type_filter: LiteralEqualsFilter = None,
    product_filter: LiteralEqualsFilter = None,
    account_filter: LiteralEqualsFilter = None,
    created_at_filter: DateTimeFilter = None,
    method_filter: LiteralEqualsFilter = None,
    sepa_batch_filter: LiteralEqualsFilter = None,
) -> PaginatedResponse[TransactionData]:
    result = transactions.paginated_search_all_v2(
        db_session,
        page_params,
        search_params,
        sort_params,
        id_filter,
        deleted_filter,
        transaction_type_filter,
        product_filter,
        account_filter,
        created_at_filter,
        method_filter,
        sepa_batch_filter,
    )
    result.items = consolidate_orders_in_transactions(result.items)
    return result
