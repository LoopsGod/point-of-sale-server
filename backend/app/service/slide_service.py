from datetime import datetime, timedelta

import pydantic
import pytz
from sqlalchemy.orm import Session

from app.dependencies import UserRequestContext
from app.models import SlideProduct
from app.models.slide import NewSlideProductData, SlideProductData
from app.repository import slides
from app.service import audit_service
from app.views.exceptions import NotFoundException, ValidationException

MAX_TREND_ITEMS = 5
SECONDS_TO_AVG_BPM = 60 * 10
# At least this "weight" has to be sold the week before,
# before the trend comparison uses that day. Otherwise,
# it'll take the week before that.
MINIMUM_SOLD_ITEM_WEIGHT_TO_COMPARE = 100.0
LOCAL_TIMEZONE = pytz.timezone("Europe/Amsterdam")


class Trend(pydantic.BaseModel):
    name: str
    sales: int
    trend: float | None


def _get_event_start_tz_aware_converted_to_utc() -> datetime:
    # return datetime.now().replace(year=2022, month=10, day=13, hour=16)
    # Convert the local timezone aware 17:00 to utc, and make it a naive datetime again.
    return (
        datetime.now(LOCAL_TIMEZONE)
        .replace(hour=17, minute=0, second=0, microsecond=0)
        .astimezone(pytz.utc)
        .replace(tzinfo=None)
    )


def _get_now_tz_aware_converted_to_utc() -> datetime:
    # return datetime.now().replace(year=2022, month=10, day=13, hour=19)
    # Convert the local timezone aware time to utc, and make it a naive datetime again.
    return datetime.now(LOCAL_TIMEZONE).astimezone(pytz.utc).replace(tzinfo=None)


def get_slide_products(db: Session) -> list[SlideProduct]:
    return slides.get_slide_products(db)


def get_bpm(db: Session) -> float:
    end = _get_now_tz_aware_converted_to_utc()
    start = end - timedelta(seconds=SECONDS_TO_AVG_BPM)

    total_value = slides.get_slide_products_sold_weighted(db, start, end)

    return total_value / ((end - start).total_seconds() / 60.0)


def get_active_accounts(db: Session) -> int:
    end = _get_now_tz_aware_converted_to_utc()
    start = _get_event_start_tz_aware_converted_to_utc()

    return slides.get_active_accounts_since(db, start, end)


def get_trends(db: Session) -> list[Trend]:
    end = _get_now_tz_aware_converted_to_utc()
    start = _get_event_start_tz_aware_converted_to_utc()

    sales_today = slides.get_trends_sorted(db, start, end)

    offset = timedelta(days=7)
    sold_previously = slides.get_slide_products_sold_weighted(
        db, start - offset, end - offset
    )

    # Do not compare days when nothing was sold.
    if sold_previously < MINIMUM_SOLD_ITEM_WEIGHT_TO_COMPARE:
        offset = timedelta(days=14)
    sales_previously = slides.get_trends_sorted(db, start - offset, end - offset)

    trends = []
    for _, sale_name, sale_sum in sales_today.values():
        trend = None
        if sale_name in sales_previously:
            prev_id, prev_name, prev_sum = sales_previously[sale_name]

            trend = (sale_sum - prev_sum) / prev_sum

        trends.append(Trend(name=sale_name, sales=sale_sum, trend=trend))
        if len(trends) >= MAX_TREND_ITEMS:
            break

    return trends


def create_slide_product(
    context: UserRequestContext, slide_product: NewSlideProductData
) -> SlideProductData:
    new_slide_product = SlideProduct()
    new_slide_product.name = slide_product.name
    new_slide_product.weight = slide_product.weight
    new_slide_product.order = slide_product.order
    new_slide_product.show_slide = slide_product.show
    new_slide_product.show_trends = slide_product.trends
    new_slide_product.add_spacing = slide_product.spacing
    new_slide_product.product_id = slide_product.product.id

    # We need to flush to be able to create audit log.
    context.db_session.add(new_slide_product)
    context.db_session.flush()

    audit_service.audit_create(
        context, "slide_product.create", object_id=new_slide_product.id
    )
    return new_slide_product.to_pydantic()


def update_slide_product(
    context: UserRequestContext, slide_product: SlideProductData
) -> SlideProductData:
    updated_slide = slides.get_by_id(context.db_session, slide_product.id)
    if not updated_slide:
        raise NotFoundException("slide_product_not_found")

    if updated_slide.deleted:
        raise ValidationException("slide_product_deleted")

    updated_slide.name = slide_product.name
    updated_slide.weight = slide_product.weight
    updated_slide.order = slide_product.order
    updated_slide.show_slide = slide_product.show
    updated_slide.show_trends = slide_product.trends
    updated_slide.add_spacing = slide_product.spacing
    updated_slide.product_id = slide_product.product.id
    context.db_session.add(updated_slide)
    context.db_session.flush()

    audit_service.audit_create(
        context, "slide_product.update", object_id=updated_slide.id
    )

    return updated_slide.to_pydantic()


def delete_slide_product(context: UserRequestContext, slide_product_id: int) -> None:
    slide_product = slides.get_by_id(context.db_session, slide_product_id)

    if not slide_product:
        raise NotFoundException("slide_product_not_found")

    #  We don't care about soft delete for these.
    slides.delete_by_id(context.db_session, slide_product_id)
    audit_service.audit_create(
        context, "slide_product.delete", object_id=slide_product_id
    )
