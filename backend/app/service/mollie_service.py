from urllib.parse import urljoin

from mollie.api.client import Client as MollieClient
from mollie.api.objects.payment import Payment
from starlette.requests import Request

import config
from app.dependencies import UserRequestContext, RequestContext
from app.models import IdealPayment
from app.models.checkout import CheckoutMethod
from app.repository import mollie_repo
from app.service import checkout_service
from app.service.checkout_service import UpgradeRequest

mollie_client = MollieClient()


def create_ideal_payment(
    context: UserRequestContext,
    request: Request,
    account_id: int,
    amount_in_euros: float,
) -> IdealPayment:
    mollie_client.set_api_key(config.MOLLIE_API_KEY)
    if getattr(config, "DEBUG", False):
        webhook = urljoin(
            config.NGROK_HOST,
            request.scope["router"].url_path_for("mollie_callback"),
        )
    else:
        webhook = request.url_for("mollie_callback")

    ideal_payment = IdealPayment(account_id=account_id)
    mollie_repo.flush(context.db_session, ideal_payment)

    mollie_data = mollie_client.payments.create(
        {
            "amount": {
                "currency": "EUR",
                "value": f"{amount_in_euros:.2f}",
            },
            "metadata": {"accountId": account_id, "name": context.user.name},
            "description": "Upgrade via account",
            "redirectUrl": urljoin(
                str(request.base_url), f"/account?cb={ideal_payment.id}"
            ),
            "webhookUrl": webhook,
        }
    )

    ideal_payment.mollie_id = mollie_data["id"]
    mollie_repo.save(context.db_session, ideal_payment)

    return ideal_payment


def update_ideal_payment(
    context: RequestContext, payment: IdealPayment
) -> tuple[IdealPayment, Payment]:
    mollie_data = get_mollie_data(payment)

    if mollie_data.is_paid() and payment.status != "paid":
        # Up the balance of this user
        amount = int(float(mollie_data["amount"]["value"]) * 100)
        req = UpgradeRequest(
            payment.account_id,
            f"ideal-nonce-{payment.id}",
            CheckoutMethod.IDEAL,
            amount,
        )
        payment.transaction_id = checkout_service.upgrade(context, req, do_commit=False)

    payment.status = mollie_data["status"]
    mollie_repo.save(context.db_session, payment)

    return payment, mollie_data


def get_mollie_data(payment: IdealPayment) -> Payment:
    mollie_client.set_api_key(config.MOLLIE_API_KEY)

    return mollie_client.payments.get(payment.mollie_id)
