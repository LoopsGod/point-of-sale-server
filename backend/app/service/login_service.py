import typing
from base64 import urlsafe_b64encode
from datetime import timedelta, datetime
from os import urandom

from sqlalchemy.orm import Session

from app.models.login_token import LoginToken
from app.repository import login_tokens

if typing.TYPE_CHECKING:
    from app.dependencies import RequestContext

TOKEN_VALID_DAYS = 1
TOKEN_LONG_LOGIN_VALID_DAYS = 100


def generate_login_token(
    context: "RequestContext", user_id: int, tfa_enabled: bool = False
) -> LoginToken:
    token_str = urlsafe_b64encode(urandom(24)).decode("utf-8")

    if login_tokens.find_by_token(context.db_session, token_str) is not None:
        return generate_login_token(context, user_id)

    token = LoginToken(user_id=user_id, token=token_str, tfa_enabled=tfa_enabled)
    login_tokens.create(context.db_session, token)

    return token


def is_token_valid(token: LoginToken) -> bool:
    time_valid = timedelta(days=TOKEN_VALID_DAYS)
    if token.user.long_login:
        time_valid = timedelta(days=TOKEN_LONG_LOGIN_VALID_DAYS)

    return token.created_at + time_valid > datetime.now()


def get_token(db_session: Session, token_str: str | None) -> LoginToken | None:
    if not token_str:
        return None

    token = login_tokens.find_by_token(db_session, token_str)

    if not token:
        return None

    if not is_token_valid(token):
        login_tokens.delete(db_session, token)
        return None

    return token
