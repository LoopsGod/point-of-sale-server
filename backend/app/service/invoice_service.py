import difflib
import logging
from typing import IO

import pydantic
from sqlalchemy.orm import Session

from app.models import ProductClassification, Product
from app.repository import invoices, products
from app.service.invoice.interpreter import process_and_interpret_invoice, Invoice
from app.service.invoice.s3 import get_temporary_public_file_url
from app.views.exceptions import NotFoundException

_logger = logging.getLogger(__name__)


class MatcherUpdate(pydantic.BaseModel):
    item: str
    multipack: int
    productId: int


class ProductInventoryUpdate(pydantic.BaseModel):
    productId: int
    diff: int


def get_invoice_url(invoice_file_id: str) -> str:
    return get_temporary_public_file_url(invoice_file_id)


def mark_invoice_imported(db_session: Session, invoice_id: int) -> None:
    invoices.mark_as_imported(db_session, invoice_id)


def process_invoice(db_session: Session, file: IO, filename: str) -> Invoice:
    interpreted_invoice, s3_file_id = process_and_interpret_invoice(file, filename)

    all_products = db_session.query(Product).filter_by(deleted_at=None).all()
    all_products_name_to_product = {p.name: p for p in all_products}

    for line_item in interpreted_invoice.line_items:
        classification = invoices.find_classification_by_name(
            db_session, line_item.item
        )

        if classification and classification.product_id:
            line_item.productId = classification.product_id
            line_item.multipack = classification.multiplier or 1
            line_item.productPrice = classification.product.price or 100
        else:
            matching = difflib.get_close_matches(
                line_item.item, all_products_name_to_product.keys(), cutoff=0.5
            )
            if matching:
                product = all_products_name_to_product[matching[0]]
                line_item.productId = product.id
                line_item.productPrice = product.price
                line_item.matchWasGuessed = True

    created_db_invoice = invoices.create_invoice(
        db_session,
        interpreted_invoice.invoice_id,
        interpreted_invoice.vendor,
        s3_file_id,
        0,
    )

    interpreted_invoice.id = created_db_invoice.id

    return interpreted_invoice


def update_matcher(
    db_session: Session,
    updates: list[MatcherUpdate],
) -> None:
    """
    Update all classifications if there already is one otherwise create a new
    product classification.
    """
    classifications = []

    for update in updates:
        classification = None
        if update.productId:
            classification = invoices.find_classification_by_name(
                db_session, update.item
            )

        if classification is None:
            classification = ProductClassification()

        classification.description = update.item
        classification.multiplier = update.multipack
        classification.product_id = update.productId

        classifications.append(classification)

    invoices.update_classifications(db_session, classifications)


def add_quantities(db_session: Session, updates: list[ProductInventoryUpdate]) -> None:
    """
    Based on the products found in the invoice and the classifications add the
    new quantities to the current quantities.

    :param updates: List of updates for the products
    """
    for update in updates:
        if update.productId:
            product = products.get_by_id(db_session, update.productId)
            if not product:
                raise NotFoundException("product_not_found")

            product.quantity = product.quantity + update.diff

    db_session.commit()
