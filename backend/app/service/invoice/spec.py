from pydantic import BaseModel


class LineItemsRuleSpec(BaseModel):
    itemRegex: str | None = None
    multipackRegex: str | None = None
    truncateItemToFirstLine: bool = False
    replaceNewlinesWithSpaces: bool = False
    ignoreRegex: str | None = None
    replace: dict | None = None


class InvoiceSpec(BaseModel):
    ignorePages: list[int] = []
    invoiceIdHeuristic: str | None = None


class RuleSpec(BaseModel):
    vendor: str
    nameHeuristic: str
    invoice: InvoiceSpec = InvoiceSpec()
    lineItems: LineItemsRuleSpec = LineItemsRuleSpec()


class Rules(BaseModel):
    name: str
    spec: RuleSpec
