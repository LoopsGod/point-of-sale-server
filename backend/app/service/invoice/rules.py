from app.service.invoice.spec import Rules, RuleSpec, InvoiceSpec, LineItemsRuleSpec

INVOICE_RULES = [
    Rules(
        name="AH",
        spec=RuleSpec(
            vendor="Albert Heijn",
            nameHeuristic="Provincialeweg 11",
            invoice=InvoiceSpec(ignorePages=[1, 2]),
            lineItems=LineItemsRuleSpec(
                ignoreRegex="(Plastic tasje)|(AH Klapkrat)|"
                "(\\+ Statiegeld)|(Bezorgkosten)",
                multipackRegex="(?:(\\d*)-pack)|(?:(\\d*) zakjes)",
            ),
        ),
    ),
    Rules(
        name="BrouwerijtIJ",
        spec=RuleSpec(
            vendor="BrouwerijtIJ",
            nameHeuristic="Brouwerij 't IJ",
            lineItems=LineItemsRuleSpec(
                ignoreRegex="(.*Statiegeld.*)|(Transport)",
                multipackRegex="(?:(\\d*)x33cl)",
                replace={" I ": " | "},
                replaceNewlinesWithSpaces=True,
            ),
        ),
    ),
    Rules(
        name="Dorstlust",
        spec=RuleSpec(
            vendor="Dorstlust",
            nameHeuristic="Dorstlust.*",
            lineItems=LineItemsRuleSpec(
                ignoreRegex="(.*Statiegeld.*)|(Transport)",
                multipackRegex="(?:(\\d*)X500ml)",
                replace={" I ": " | "},
                replaceNewlinesWithSpaces=True,
            ),
        ),
    ),
    Rules(
        name="Klinkers",
        spec=RuleSpec(
            vendor="Klinkers",
            nameHeuristic="klinkers zoetwaren",
            lineItems=LineItemsRuleSpec(
                ignoreRegex="(Verzending & Afhandeling)|(Betaalkosten)",
                multipackRegex="(?:(?:([\\d\\.]+)x)?([\\d\\.]+)x[\\d\\.]*[gr]*)|(?:(?:x(\\d*))$)",
                truncateItemToFirstLine=True,
            ),
        ),
    ),
    Rules(
        name="MKW trading",
        spec=RuleSpec(
            vendor="Goedkoop blikjes",
            nameHeuristic="MKW Trading",
            invoice=InvoiceSpec(invoiceIdHeuristic="Factuurnummer"),
            lineItems=LineItemsRuleSpec(
                multipackRegex="(?:(\\d+)x)?(\\d+)x\\d*[cl]*",
            ),
        ),
    ),
    Rules(
        name="Compliment",
        spec=RuleSpec(
            vendor="Compliment",
            nameHeuristic="Compliment.nl",
            invoice=InvoiceSpec(invoiceIdHeuristic="bestelnummer"),
            lineItems=LineItemsRuleSpec(
                itemRegex="^(.+)\n.*$",
                multipackRegex=r"\((\d+)x?.*\)",
            ),
        ),
    ),
]
