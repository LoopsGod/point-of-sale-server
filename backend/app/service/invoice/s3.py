from os.path import basename
from tempfile import NamedTemporaryFile
from typing import IO

import boto3
from botocore.client import BaseClient
from pydantic import BaseSettings
from time import time, sleep

TEXTRACT_MAX_DURATION_SECONDS = 60
TEXTRACT_STATUS_POLL_INTERVAL_SECONDS = 5


class S3Config(BaseSettings):
    s3_region = "eu-west-1"
    s3_bucket = "via-pos-develop"
    s3_bucket_path = "invoices"


config = S3Config()


def get_s3_client() -> BaseClient:
    return boto3.client("s3", region_name=config.s3_region)


def get_temporary_public_file_url(invoice_file_id: str) -> str:
    s3_client = get_s3_client()

    object_name = f"{config.s3_bucket_path}/{invoice_file_id}"

    response = s3_client.generate_presigned_url(
        "get_object",
        Params={"Bucket": config.s3_bucket, "Key": object_name},
        ExpiresIn=3600,
    )

    return response


def upload_and_analyse_with_textract(file: IO, filename: str) -> tuple[dict, str]:
    s3_object = _upload_file_to_bucket(file, filename)

    textract_client = boto3.client("textract", region_name=config.s3_region)

    document_analysis = textract_client.start_expense_analysis(
        DocumentLocation={
            "S3Object": {
                "Bucket": config.s3_bucket,
                "Name": f"{config.s3_bucket_path}/{s3_object}",
            }
        }
    )

    tries = int(TEXTRACT_MAX_DURATION_SECONDS / TEXTRACT_STATUS_POLL_INTERVAL_SECONDS)
    document_analysis_result = None
    while tries > 0:
        document_analysis_result = textract_client.get_expense_analysis(
            JobId=document_analysis["JobId"]
        )
        if document_analysis_result["JobStatus"] == "IN_PROGRESS":
            tries -= 1
            sleep(TEXTRACT_STATUS_POLL_INTERVAL_SECONDS)
            continue
        break

    if (
        not document_analysis_result
        or document_analysis_result["JobStatus"] == "IN_PROGRESS"
    ):
        raise ValueError(
            f"Analysing document took to long (>{TEXTRACT_MAX_DURATION_SECONDS}s)"
        )

    return document_analysis_result, s3_object


def _upload_file_to_bucket(file: IO, filename: str) -> str:
    s3_client = get_s3_client()

    upload_name = "".join(
        [c for c in basename(filename) if c.isalpha() or c.isdigit() or c == "."]
    ).rstrip()
    object_name = f"{config.s3_bucket_path}/{str(int(time()))}-{upload_name}"

    with NamedTemporaryFile() as f:
        f.write(file.read())
        s3_client.upload_file(f.name, config.s3_bucket, object_name)

    return object_name[len(config.s3_bucket_path) + 1 :]
