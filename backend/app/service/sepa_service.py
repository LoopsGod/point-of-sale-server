from calendar import monthrange
from datetime import datetime
from typing import Any

from sqlalchemy.orm import Session

from app.dependencies import UserRequestContext, RequestContext
from app.models import SepaBatch, SepaBatchOrder
from app.models.checkout import CheckoutMethod
from app.repository import sepa_repo, accounts as account_repo
from app.repository.sepa_repo import SepaUpgradeDetails
from app.service import checkout_service, audit_service
from app.service.checkout_service import UpgradeRequest
from app.views.exceptions import ValidationException, NotFoundException
from app.service import setting_service


def get_of_current_month(db_session: Session) -> list[SepaBatch]:
    today = datetime.today()
    month_start = today.replace(day=1)
    month_end = today.replace(day=monthrange(today.year, today.month)[1])
    return sepa_repo.get_in_date_range(db_session, month_start, month_end)


def get_unexecuted_before_today(db_session: Session) -> list[SepaBatch]:
    return sepa_repo.get_in_date_range(
        db_session, None, datetime.today(), executed=False
    )


def create_sepa_batch(
    context: UserRequestContext,
    description: str,
    execution_date: datetime,
    accounts: list[int],
) -> SepaBatch:
    if sepa_repo.get_in_date_range(context.db_session, None, None, executed=False):
        raise ValidationException("pending_batch_exists")

    batch = SepaBatch()
    batch.description = description
    batch.execution_date = execution_date

    batch.success = False
    sepa_repo.flush(context.db_session, batch)

    for account in account_repo.get_by_ids(context.db_session, accounts):
        batch.pending_orders.append(
            SepaBatchOrder(account_id=account.id, price=abs(account.balance))
        )

    audit_service.audit_create(context, "sepa.batch.create", object_id=batch.id)

    sepa_repo.save(context.db_session, batch)

    return batch


def execute_sepa_batch(context: UserRequestContext, batch: SepaBatch) -> None:
    for pending_order in batch.pending_orders.all():
        if pending_order.deleted:
            continue

        upgrade_req = UpgradeRequest(
            account_id=pending_order.account_id,
            nonce=f"sepa-batch-{batch.id}-account-{pending_order.account_id}",
            method=CheckoutMethod.SEPA,
            amount=pending_order.price,
            sepa_batch_id=batch.id,
        )
        checkout_service.upgrade(context, upgrade_req, do_commit=False)

    batch.success = True

    audit_service.audit_create(context, "sepa.batch.execute", object_id=batch.id)

    # Also commits audit and upgrades
    sepa_repo.save(context.db_session, batch)


def get_pending_orders_with_user_accounts(
    context: RequestContext, batch: SepaBatch
) -> list[SepaUpgradeDetails]:
    return sepa_repo.get_pending_orders_with_user_accounts(context.db_session, batch)


def delete_batch(context: UserRequestContext, batch: SepaBatch) -> None:
    batch.deleted = True

    for pending_order in batch.pending_orders.all():
        pending_order.deleted = True

    audit_service.audit_create(context, "sepa.batch.delete", object_id=batch.id)

    sepa_repo.save(context.db_session, batch)


def delete_pending_order(
    context: UserRequestContext, batch: SepaBatch, account_id: int
) -> None:
    order = sepa_repo.find_pending_order(context.db_session, batch, account_id)
    if not order:
        raise NotFoundException()

    order.deleted = True

    audit_service.audit_create(context, "sepa.batch.order.delete", object_id=order.id)

    sepa_repo.save(context.db_session, order)


def get_pending_batch_for_account(
    context: UserRequestContext,
    account_id: int,
) -> tuple[SepaBatch | None, SepaBatchOrder | None]:
    return sepa_repo.get_pending_batch_for_account(context.db_session, account_id)


def get_total_batch_amount(db_session: Session) -> int:
    return sepa_repo.get_total_debt_for_sepa_batch(db_session)


def get_batch_by_id(context: UserRequestContext, batch_id: int) -> SepaBatch:
    b = sepa_repo.find_batch_by_id(context.db_session, batch_id)
    if not b:
        raise NotFoundException()
    return b


def get_direct_debit_config_from_settings(db_session: Session) -> dict[str, Any]:
    """Returned dict as required by SepaDD library."""
    db_settings = setting_service.get_db_settings(db_session)

    if (
        db_settings.direct_debit_creditor_name is None
        or db_settings.direct_debit_creditor_iban is None
        or db_settings.direct_debit_creditor_bic is None
        or db_settings.direct_debit_creditor_identifier is None
    ):
        raise ValidationException("no_configuration")

    return {
        "name": db_settings.direct_debit_creditor_name,
        "IBAN": db_settings.direct_debit_creditor_iban,
        "BIC": db_settings.direct_debit_creditor_bic,
        "batch": True,
        "creditor_id": db_settings.direct_debit_creditor_identifier,
        "currency": "EUR",
    }
