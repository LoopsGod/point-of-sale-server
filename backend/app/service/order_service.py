from app.dependencies import UserRequestContext, RequestContext
from app.models import Order, Product
from app.models.account import AccountType
from app.models.checkout import CheckoutMethod
from app.models.order import OrderData, OrderType
from app.repository import orders
from app.views.exceptions import ValidationException


def delete_orders(
    context: UserRequestContext, order_list: list[Order], do_commit: bool = True
) -> None:
    """
    Deletes orders by creating the reverse of the given orders.

    The deleted property on an order indicates if THAT order deletes a
    different order. A deletion order cannot be deleted itself. I.e.
    they do not stack.

    This also credits the account with the balance they lost
    :param order_list:
    :param do_commit: indicates if db session will commit
    :return:
    """
    new_orders = []

    for o in order_list:
        if o.deleted:
            raise ValidationException("order_is_deletion_order")
        if o.method == CheckoutMethod.ACCOUNT:
            if o.account is None:
                raise ValidationException("missing_account_on_account_order")

            if (
                o.account.type == AccountType.USER
                or o.account.type == AccountType.EVENT
            ):
                o.account.balance += o.price

        new = create_reverse_order(o)
        new_orders.append(new)

    orders.save_multiple(context.db_session, new_orders, do_commit=do_commit)


def create_reverse_order(original: Order) -> Order:
    # Simply copy most of the properties
    new = Order(
        account_id=original.account_id,
        product_id=original.product_id,
        transaction_id=original.transaction_id,
        sepa_batch_id=original.sepa_batch_id,
    )

    # This uses a setter so can't have it as kwarg
    new.type = original.type
    new.method = original.method

    # Revert the price
    new.price = -original.price

    # Set deleted to true so we know this is a deletion order.
    new.deleted = True

    return new


def create_order(
    product: Product,
    transaction_id: int,
    method: CheckoutMethod,
    sepa_batch_id: int = None,
) -> Order:
    o = Order(
        product_id=product.id,
        transaction_id=transaction_id,
        price=-product.price,
        account_id=product.ledger_id,
    )
    # This uses a setter so can't have it as kwarg
    o.method = method

    if sepa_batch_id is not None:
        o.sepa_batch_id = sepa_batch_id

    return o


def determine_order_type(order: Order) -> OrderType:
    if order.product_id is None and order.method != CheckoutMethod.ACCOUNT:
        return OrderType.DIRECT_PAYMENT

    if (
        order.product_id is None
        and order.method == CheckoutMethod.ACCOUNT
        and order.account_id is not None
    ):
        return OrderType.ACCOUNT_TOP_UP_OR_ACCOUNT_PAYMENT

    if order.product_id is not None and order.account_id is not None:
        return OrderType.PRODUCT_PURCHASE

    return OrderType.INVALID


def save_multiple_and_set_type(
    context: RequestContext, order_list: list[Order], do_commit: bool = True
) -> None:
    for o in order_list:
        o.type = determine_order_type(o)  # type: ignore

    orders.save_multiple(context.db_session, order_list, do_commit)


def consolidate_orders(orders: list[OrderData]) -> list[OrderData]:
    orders_per_product_id: dict[int, OrderData] = {}
    consolidated_orders = []
    orders_are_deleted = False

    for o in orders:
        if o.deleted:
            #  Ignore deleted orders (the transaction will indicate that all
            #  orders are deleted)
            orders_are_deleted = True
            continue

        if not o.product:
            consolidated_orders.append(o)
            continue

        product_id = o.product.id

        if product_id in orders_per_product_id:
            orders_per_product_id[product_id].count += 1
            orders_per_product_id[product_id].price += o.price
        else:
            orders_per_product_id[product_id] = o

    for o in orders_per_product_id.values():
        if o.count > 1 and o.product:
            o.product.name = f"{o.count}x {o.product.name}"
        consolidated_orders.append(o)

    if orders_are_deleted:
        #  Mark all orders as deleted
        for o in consolidated_orders:
            o.deleted = True

    return sorted(consolidated_orders, key=lambda o: o.price)
