import logging
from typing import NamedTuple

import pydantic
from sqlalchemy.orm import Session

from app.dependencies import UserRequestContext, RequestContext
from app.models import Register, Order, Account
from app.models.checkout import CheckoutMethod
from app.models.checkout_card import CheckoutCard
from app.repository import (
    products,
    transactions as transaction_repo,
    accounts as account_repo,
    checkout_repo,
)
from app.repository.settings import get_db_settings
from app.service import (
    order_service,
    audit_service,
    account_service,
    transaction_service,
    product_service,
    setting_service,
)
from app.views.exceptions import (
    AuthorizationException,
    ValidationException,
    NotFoundException,
    UnprocessableEntityException,
    NotAcceptableException,
    ResourceConflictException,
)

_logger = logging.getLogger(__name__)

MAX_CHECKOUT_AMOUNT = 50
MAX_CHECKOUT_AMOUNT_ADMIN = 99999


class CartItemAmountStr(pydantic.ConstrainedInt):
    gt = 0
    lt = MAX_CHECKOUT_AMOUNT_ADMIN


class CartItem(pydantic.BaseModel):
    id: int
    amount: CartItemAmountStr


class CheckoutDirectRequest(NamedTuple):
    method: CheckoutMethod
    nonce: str
    cart_items: list[CartItem]
    upgrade_account_id: int | None
    upgrade_amount: int | None
    reason: str | None


class CheckoutAccountRequest(NamedTuple):
    account_id: int
    nonce: str
    cart_items: list[CartItem]
    pin_code: str | None


class CheckoutAccountResponse(NamedTuple):
    balance_before: int
    balance_after: int
    transaction_id: int


class UpgradeRequest(NamedTuple):
    account_id: int
    nonce: str
    method: CheckoutMethod
    amount: int
    reason: str | None = None
    sepa_batch_id: int | None = None


def upgrade(
    context: RequestContext, req: UpgradeRequest, do_commit: bool = True
) -> int:
    """
    Upgrades an account.

    The context here is not a UserRequestContext, as upgrades can be initiated using
    a mollie webhook. Those do not have a user.

    Creates a transaction, two orders and updates the account's balance"""
    transaction = transaction_repo.create_empty_transaction(
        context.db_session, req.nonce
    )

    if req.reason:
        transaction.reason = req.reason

    audit_service.audit_create(
        context,
        "transaction.upgrade." + req.method.name.lower(),
        object_id=transaction.id,
    )

    upgrade_order = _process_upgrade(
        context,
        transaction.id,
        req.account_id,
        req.amount,
        req.sepa_batch_id,
    )

    compensation = Order(
        account_id=get_account_for_checkout_method(context.db_session, req.method),
        transaction_id=transaction.id,
        price=req.amount,
    )
    compensation.method = req.method
    if req.sepa_batch_id is not None:
        compensation.sepa_batch_id = req.sepa_batch_id

    order_service.save_multiple_and_set_type(
        context, [upgrade_order, compensation], do_commit=False
    )

    transaction_service.save_and_set_type(context, transaction, do_commit=do_commit)

    return transaction.id


def checkout_account(
    context: UserRequestContext,
    req: CheckoutAccountRequest,
    register: Register,
    check_pin: bool,
    max_checkout_amount: int = 50,
) -> CheckoutAccountResponse:
    """Handles purchasing products from an account.

    Can be used for self checkout, checking out with pin code and for
    checking out without pin."""
    account = account_repo.get_by_id(context.db_session, req.account_id)

    if not account or account.deleted:
        raise NotFoundException("invalid_account")

    # Check the PIN code if the user can't checkout on other accounts without
    # authorization.
    if check_pin and (
        not req.pin_code or not account_service.check_pin(account, req.pin_code)
    ):
        raise UnprocessableEntityException("invalid_account_pin")

    transaction = transaction_repo.create_empty_transaction(
        context.db_session, req.nonce
    )

    try:
        orders, total = _process_cart_items(
            context,
            req.cart_items,
            transaction.id,
            CheckoutMethod.ACCOUNT,
            register,
            max_checkout_amount,
        )
    except ValueError as e:
        raise ValidationException("invalid_product_in_cart") from e

    if not account.allow_negative_balance and total > account.balance:
        raise NotAcceptableException("insufficient_credits")

    product_service.update_quantity_after_buy(context, orders)

    compensation = Order(
        account_id=account.id, transaction_id=transaction.id, price=total
    )
    compensation.method = CheckoutMethod.ACCOUNT

    orders.append(compensation)
    order_service.save_multiple_and_set_type(context, orders, do_commit=False)

    balance_before = account.balance
    account.balance -= total

    audit_service.audit_create(
        context,
        "transaction.checkout.account",
        object_id=transaction.id,
    )

    transaction_service.save_and_set_type(context, transaction)

    return CheckoutAccountResponse(balance_before, account.balance, transaction.id)


def checkout_other(
    context: UserRequestContext,
    req: CheckoutDirectRequest,
    register: Register,
    max_checkout_amount: int = 50,
) -> int:
    """Handles purchasing products and upgrading with cash/card/other."""
    if req.method in (CheckoutMethod.OTHER, CheckoutMethod.TRANSFER) and not req.reason:
        raise ValidationException("reason_expected")

    transaction = transaction_repo.create_empty_transaction(
        context.db_session, req.nonce
    )
    transaction.reason = req.reason

    try:
        orders, total = _process_cart_items(
            context,
            req.cart_items,
            transaction.id,
            req.method,
            register,
            max_checkout_amount,
        )
    except ValueError as e:
        raise ValidationException("invalid_product_in_cart") from e

    if req.upgrade_account_id is not None and req.upgrade_amount is not None:
        if req.upgrade_amount < 0 and not req.reason:
            raise ValidationException("reason_expected")

        audit_service.audit_create(
            context,
            "transaction.upgrade." + req.method.name.lower(),
            object_id=transaction.id,
        )

        upgrade_order = _process_upgrade(
            context, transaction.id, req.upgrade_account_id, req.upgrade_amount
        )
        orders.append(upgrade_order)
        total += req.upgrade_amount

    product_service.update_quantity_after_buy(context, orders)

    compensation = Order(
        account_id=get_account_for_checkout_method(context.db_session, req.method),
        transaction_id=transaction.id,
        price=total,
    )
    compensation.method = req.method

    orders.append(compensation)

    order_service.save_multiple_and_set_type(context, orders, do_commit=False)

    audit_service.audit_create(
        context,
        "transaction.checkout." + req.method.name.lower(),
        object_id=transaction.id,
    )

    transaction_service.save_and_set_type(context, transaction)

    return transaction.id


def _process_upgrade(
    context: RequestContext,
    transaction_id: int,
    account_id: int,
    amount: int,
    sepa_batch_id: int = None,
) -> Order:
    acc = account_repo.get_by_id(context.db_session, account_id)

    if acc is None or acc.deleted:
        raise NotFoundException("account_not_found")

    order = Order(account_id=account_id, transaction_id=transaction_id, price=-amount)
    order.method = CheckoutMethod.ACCOUNT

    if sepa_batch_id is not None:
        order.sepa_batch_id = sepa_batch_id

    acc.balance += amount

    return order


def _process_cart_items(
    context: UserRequestContext,
    cart_items: list[CartItem],
    transaction_id: int,
    method: CheckoutMethod,
    register: Register,
    max_checkout_amount: int = 50,
) -> tuple[list[Order], int]:
    register_cat_ids = [c.id for c in register.categories]

    orders, total = [], 0
    for cart_item in cart_items:
        if cart_item.amount > max_checkout_amount:
            raise ValueError()

        product = products.get_by_id(context.db_session, cart_item.id)
        if product is None or product.deleted:
            raise NotFoundException("product_deleted")

        # Check if the product's category is part of the register
        product_cat_ids = [c.id for c in product.categories]
        if not set(product_cat_ids) & set(register_cat_ids):
            raise AuthorizationException("invalid_product_category")

        total += product.price * cart_item.amount
        for _ in range(cart_item.amount):
            order = order_service.create_order(product, transaction_id, method)
            orders.append(order)

    return orders, total


def _validate_card_secret(db_session: Session, card_secret: str) -> str:
    reader_secret = setting_service.get_db_settings(db_session).checkout_reader_secret

    if reader_secret is None:
        _logger.warning(
            "Checkout using reader not enabled, configure 'CHECKOUT_READER_SECRET'."
        )
        raise ResourceConflictException("reader_disabled")

    if not card_secret.startswith(reader_secret):
        raise ValidationException("invalid_card_secret")
    card_secret = card_secret.removeprefix(reader_secret)
    if not card_secret:
        raise ValidationException("invalid_card_secret")
    return card_secret


def get_account_for_card_secret(db_session: Session, card_secret: str) -> Account:
    card_secret = _validate_card_secret(db_session, card_secret)

    user = checkout_repo.get_user_for_card_secret(db_session, card_secret)

    if not user:
        raise NotFoundException("unknown_card_secret")

    if not user.account:
        raise NotFoundException("no_linked_account")

    return user.account


def register_card_for_account(
    context: UserRequestContext, account_id: int, card_secret: str, pin: str
) -> None:
    account = account_service.get_by_id(context, account_id)
    if not account_service.check_pin(account, pin):
        raise UnprocessableEntityException("invalid_account_pin")

    card_secret = _validate_card_secret(context.db_session, card_secret)

    if checkout_repo.find_card_by_secret(context.db_session, card_secret):
        raise ResourceConflictException("card_secret_exists")

    if not account.user:
        raise NotFoundException("no_user_for_account")

    context.db_session.add(
        CheckoutCard(card_secret=card_secret, user_id=account.user.id)
    )
    context.db_session.commit()


def get_account_for_checkout_method(db_session: Session, method: CheckoutMethod) -> int:
    account_id = get_db_settings(db_session).get_setting_for_checkout_method(method)

    if account_id is None:
        raise Exception(f"{method.value}_checkout_method_has_no_linked_account")

    return int(account_id)
