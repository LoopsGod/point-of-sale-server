import sqlalchemy as sa
from datetime import datetime
import app


class Order(app.migrations.util.Base):
    __tablename__ = "order"
    id = sa.Column(sa.Integer, primary_key=True)
    created_at = sa.Column(sa.DateTime, default=datetime.now)
    updated_at = sa.Column(sa.DateTime, default=datetime.now, onupdate=datetime.now)
    deleted_at = sa.Column(sa.DateTime, default=None)
    account_id = sa.Column(sa.Integer, sa.ForeignKey("account.id"))
    product_id = sa.Column(sa.Integer, sa.ForeignKey("product.id"))
    transaction_id = sa.Column(sa.Integer, sa.ForeignKey("transaction.id"))
    sepa_batch_id = sa.Column(sa.Integer, sa.ForeignKey("sepa_batch.id"))

    method = sa.Column(
        sa.Enum(
            "cash",
            "card",
            "account",
            "transfer",
            "ideal",
            "sepa",
            "other",
            name="payment_methods",
        )
    )

    price = sa.Column(sa.Integer)
