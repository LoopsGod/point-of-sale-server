"""Move account_id and method to Order

Revision ID: d286caefb56e
Revises: 5773636acf0a
Create Date: 2018-10-15 10:26:45.036281

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.orm import Session, load_only
from sqlalchemy.sql import text
from tqdm import tqdm

from app.migrations.util.order import Order

# revision identifiers, used by Alembic.
revision = "d286caefb56e"
down_revision = "5773636acf0a"
branch_labels = ()
depends_on = None


def upgrade():
    bind = op.get_bind()
    session = Session(bind=bind)

    # Create "order"."method"
    method_enum = sa.Enum(
        "cash",
        "card",
        "account",
        "transfer",
        "ideal",
        "sepa",
        "other",
        name="payment_methods",
    )
    op.add_column("order", sa.Column("method", method_enum, nullable=True))

    # Create "order"."account_id"
    op.add_column("order", sa.Column("account_id", sa.Integer(), nullable=True))
    op.create_foreign_key(None, "order", "account", ["account_id"], ["id"])

    # Move "transaction"."method" and ."account_id" back to "order"
    orders = (
        session.query(Order).options(load_only(Order.id, Order.transaction_id)).all()
    )

    for order in tqdm(orders):
        if order.transaction_id is not None:
            query = """
                SELECT "account_id", "method" FROM "transaction"
                WHERE "id"=:id
            """
            order.account_id, order.method = bind.execute(
                text(query), id=order.transaction_id
            ).first()
            session.add(order)

    session.commit()

    op.drop_constraint("transaction_account_id_fkey", "transaction", type_="foreignkey")
    op.drop_column("transaction", "method")
    op.drop_column("transaction", "account_id")


def downgrade():
    bind = op.get_bind()
    session = Session(bind=bind)

    # Create "transaction"."method"
    method_enum = sa.Enum(
        "cash",
        "card",
        "account",
        "transfer",
        "ideal",
        "sepa",
        "other",
        name="payment_methods",
    )
    op.add_column(
        "transaction",
        sa.Column("method", method_enum, autoincrement=False, nullable=True),
    )

    # Create "transaction"."account_id"
    op.add_column(
        "transaction",
        sa.Column("account_id", sa.INTEGER(), autoincrement=False, nullable=True),
    )
    op.create_foreign_key(
        "transaction_account_id_fkey", "transaction", "account", ["account_id"], ["id"]
    )

    # Move "method" and "account_id" back to "transaction"
    orders = (
        session.query(Order).options(load_only(Order.id, Order.transaction_id)).all()
    )

    for order in tqdm(orders):
        if order.transaction_id is not None:
            query = """
                UPDATE "transaction"
                SET "account_id"=:account_id, "method"=:method
                WHERE "id"=:transaction_id
            """

            args = {
                "account_id": order.account_id,
                "method": order.method,
                "transaction_id": order.transaction_id,
            }
            session.execute(query, args)
    session.commit()

    op.drop_constraint("order_account_id_fkey", "order", type_="foreignkey")
    op.drop_column("order", "account_id")
    op.drop_column("order", "method")
