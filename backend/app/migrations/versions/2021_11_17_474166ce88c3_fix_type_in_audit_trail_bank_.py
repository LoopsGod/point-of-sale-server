"""Fix type in audit trail bank_transaction.upload

Revision ID: 474166ce88c3
Revises: b0980d805d7d
Create Date: 2021-11-17 15:26:06.584691

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "474166ce88c3"
down_revision = "b0980d805d7d"
branch_labels = ()
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.execute(
        "UPDATE audit_trail set resource = 'bank_transaction' where resource = 'back_transaction'"
    )
    op.execute(
        "UPDATE audit_trail set event = 'bank_transaction.upload' where event = 'back_transaction.upload'"
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
