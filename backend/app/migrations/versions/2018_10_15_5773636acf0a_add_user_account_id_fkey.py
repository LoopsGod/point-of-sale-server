"""Add user account_id_fkey

Revision ID: 5773636acf0a
Revises: 43496dd9024c
Create Date: 2018-10-15 09:39:39.599558

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.orm.session import Session
from sqlalchemy.dialects import postgresql

from app.models.transaction import Transaction
from app.models.order import Order

# revision identifiers, used by Alembic.
revision = "5773636acf0a"
down_revision = "43496dd9024c"
branch_labels = ()
depends_on = None


def upgrade():
    op.create_foreign_key(
        "user_account_id_fkey", "user", "account", ["account_id"], ["id"]
    )


def downgrade():
    op.drop_constraint("user_account_id_fkey", "user", type_="foreignkey")
