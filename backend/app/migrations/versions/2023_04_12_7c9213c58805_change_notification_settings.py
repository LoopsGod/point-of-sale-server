"""change notification settings

Revision ID: 7c9213c58805
Revises: 7e9bc3f19f2f
Create Date: 2023-04-12 20:17:05.477233

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "7c9213c58805"
down_revision = "7e9bc3f19f2f"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
UPDATE setting
SET key = 'NOTIFICATION_SEPA_DESTINATION'
WHERE key = 'NOTIFICATION_DESTINATION';
    """
    )

    op.execute(
        """
UPDATE setting
SET key = 'NOTIFICATION_PRODUCT_QUANTITY_DESTINATION'
WHERE key = 'PRODUCT_QUANTITY_NOTIFICATION_DESTINATION';
    """
    )


def downgrade():
    op.execute(
        """
UPDATE setting
SET key = 'NOTIFICATION_DESTINATION'
WHERE key = 'NOTIFICATION_SEPA_DESTINATION';
    """
    )

    op.execute(
        """
UPDATE setting
SET key = 'PRODUCT_QUANTITY_NOTIFICATION_DESTINATION'
WHERE key = 'NOTIFICATION_PRODUCT_QUANTITY_DESTINATION';
    """
    )
