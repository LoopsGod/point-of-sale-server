"""merge heads

Revision ID: 3bdec049920e
Revises: 53ce0af12d69, 4819211ae7cf
Create Date: 2019-11-28 16:24:33.502969

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "3bdec049920e"
down_revision = ("53ce0af12d69", "4819211ae7cf")
branch_labels = ()
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
