"""Create on_bar_slide column on Product

Revision ID: bac1b7a7d9c4
Revises: 4faf5fbdb4e6
Create Date: 2022-05-14 14:36:23.175136

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "bac1b7a7d9c4"
down_revision = "08f1150d235b"
branch_labels = ()
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "product",
        sa.Column("on_bar_slide", sa.Boolean(), nullable=False, server_default="false"),
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("product", "on_bar_slide")
    # ### end Alembic commands ###
