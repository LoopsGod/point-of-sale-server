"""merge heads

Revision ID: 9bcb8c78fa17
Revises: cf05e387addb, 26c7a8c4c8df
Create Date: 2019-03-20 18:21:32.854572

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "9bcb8c78fa17"
down_revision = ("cf05e387addb", "26c7a8c4c8df")
branch_labels = ()
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
