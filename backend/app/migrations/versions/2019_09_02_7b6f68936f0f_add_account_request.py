"""Add account request

Revision ID: 7b6f68936f0f
Revises: baecfe64c7ee
Create Date: 2019-09-02 16:16:09.631721

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "7b6f68936f0f"
down_revision = "baecfe64c7ee"
branch_labels = ()
depends_on = None


def upgrade():
    op.create_table(
        "account_request",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("updated_at", sa.DateTime(), nullable=True),
        sa.Column("deleted_at", sa.DateTime(), nullable=True),
        sa.Column("user_id", sa.Integer(), nullable=True),
        sa.Column("status", sa.Integer(), nullable=False, server_default="0"),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["user.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
    )


def downgrade():
    op.drop_table("account_request")
