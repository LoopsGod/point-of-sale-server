"""Move sepa_batch_id to order

Revision ID: cfc077ad20fc
Revises: d286caefb56e
Create Date: 2018-10-15 14:08:56.112626

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.orm import Session
from tqdm import tqdm

from app.migrations.util.order import Order

# revision identifiers, used by Alembic.
revision = "cfc077ad20fc"
down_revision = "d286caefb56e"
branch_labels = ()
depends_on = None


def upgrade():
    session = Session(bind=op.get_bind())

    op.add_column("order", sa.Column("sepa_batch_id", sa.Integer(), nullable=True))
    op.create_foreign_key(
        "order_sepa_batch_id_fkey", "order", "sepa_batch", ["sepa_batch_id"], ["id"]
    )

    query = """SELECT "transaction"."sepa_batch_id" FROM "transaction"
               WHERE "id"=:transaction_id"""
    for order in tqdm(session.query(Order).all()):
        args = {"transaction_id": order.transaction_id}
        (order.sepa_batch_id,) = session.execute(query, args).first()

    session.commit()

    op.drop_constraint("fk_sepa_batch_id", "transaction", type_="foreignkey")
    op.drop_column("transaction", "sepa_batch_id")


def downgrade():
    session = Session(bind=op.get_bind())

    op.add_column(
        "transaction",
        sa.Column("sepa_batch_id", sa.INTEGER(), autoincrement=False, nullable=True),
    )
    op.create_foreign_key(
        "fk_sepa_batch_id", "transaction", "sepa_batch", ["sepa_batch_id"], ["id"]
    )

    query = """UPDATE "transaction"
               SET "sepa_batch_id"=:sepa_batch_id
               WHERE "id"=:transaction_id"""
    for order in tqdm(session.query(Order).all()):
        if order.method == "sepa" and order.sepa_batch_id is not None:
            args = {
                "transaction_id": order.transaction_id,
                "sepa_batch_id": order.sepa_batch_id,
            }
            session.execute(query, args)

    session.commit()

    op.drop_constraint("order_sepa_batch_id_fkey", "order", type_="foreignkey")
    op.drop_column("order", "sepa_batch_id")
