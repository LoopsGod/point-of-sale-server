"""Add missing sepa_batch_id on orders

Revision ID: 237e483c8f31
Revises: bac1b7a7d9c4
Create Date: 2022-09-22 10:58:55.774239

"""
from alembic import op
from sqlalchemy import Column, Integer, ForeignKey, text
from sqlalchemy.orm import Session, declarative_base, relationship
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "237e483c8f31"
down_revision = "a15d7e50ef43"
branch_labels = ()
depends_on = None

Base = declarative_base()


class Transaction(Base):
    __tablename__ = "transaction"

    id = sa.Column(sa.Integer, primary_key=True)
    orders = relationship("Order", backref="transaction", order_by="Order.price")


class Order(Base):
    __tablename__ = "order"
    id = sa.Column(sa.Integer, primary_key=True)
    transaction_id = Column(Integer, ForeignKey("transaction.id"))
    sepa_batch_id = Column(Integer)


def upgrade():
    bind = op.get_bind()
    session = Session(bind=bind)
    query = text(
        """
update "order" o1
set sepa_batch_id = sq.sepa_batch_id
from (select distinct o2.sepa_batch_id, o2.transaction_id
      from "order" o2
      where o2.sepa_batch_id is not null) as sq
where o1.transaction_id = sq.transaction_id;
""".strip()
    )
    count = session.execute(query).rowcount
    print(f"Updated {count} orders with their respective sepa_batch_id")
    session.commit()


def downgrade():
    pass
