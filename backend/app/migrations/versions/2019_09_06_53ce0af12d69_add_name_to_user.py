"""Add name to User

Revision ID: 53ce0af12d69
Revises: 7b6f68936f0f
Create Date: 2019-09-06 20:15:59.203348

"""
from datetime import datetime

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
from sqlalchemy.orm import Session, declarative_base
from tqdm import tqdm

revision = "53ce0af12d69"
down_revision = "7b6f68936f0f"
branch_labels = ()
depends_on = None

Base = declarative_base()


class User(Base):
    __tablename__ = "user"

    id = sa.Column(sa.Integer, primary_key=True)
    created_at = sa.Column(sa.DateTime, default=datetime.now)
    updated_at = sa.Column(sa.DateTime, default=datetime.now, onupdate=datetime.now)
    deleted_at = sa.Column(sa.DateTime, default=None)
    username = sa.Column(sa.String(64), unique=True)
    password = sa.Column(sa.String(64))
    is_admin = sa.Column(sa.Boolean, default=False)
    is_bar = sa.Column(sa.Boolean, default=False)
    long_login = sa.Column(sa.Boolean, default=False)
    # account_id = sa.Column(sa.Integer, sa.ForeignKey("account.id"))
    viaduct_user_id = sa.Column(sa.Integer)
    # register_id = sa.Column(sa.Integer, sa.ForeignKey("register.id"), nullable=True)
    name = sa.Column(sa.String(512), nullable=False, default="")


def upgrade():
    op.add_column(
        "user",
        sa.Column("name", sa.String(length=512), nullable=False, server_default=""),
    )

    session = Session(bind=op.get_bind())

    users = session.query(User).all()

    for user in tqdm(users):
        user.name = user.username
        session.add(user)

    session.commit()


def downgrade():
    op.drop_column("user", "name")
