"""registers

Revision ID: 414cfd0d94e8
Revises: 5bba86141754
Create Date: 2019-04-22 21:36:54.335852

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "414cfd0d94e8"
down_revision = "5bba86141754"
branch_labels = ()
depends_on = None


def upgrade():
    op.create_table(
        "register",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("updated_at", sa.DateTime(), nullable=True),
        sa.Column("deleted_at", sa.DateTime(), nullable=True),
        sa.Column("name", sa.String(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )

    op.create_table(
        "register_category",
        sa.Column("register_id", sa.Integer(), nullable=False),
        sa.Column("category_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["category_id"],
            ["category.id"],
        ),
        sa.ForeignKeyConstraint(
            ["register_id"],
            ["register.id"],
        ),
        sa.PrimaryKeyConstraint("register_id", "category_id"),
    )


def downgrade():
    op.drop_table("register_category")
    op.drop_table("register")
