"""Add start_balance to account.

Revision ID: 16c15401d10b
Revises: ce44c02b3672
Create Date: 2021-01-11 16:03:27.450468

"""
from datetime import datetime

import sqlalchemy as sa
from alembic import op
from sqlalchemy import func, case, and_
from sqlalchemy.orm import Session, relationship, declarative_base

# revision identifiers, used by Alembic.

revision = "16c15401d10b"
down_revision = "26a40532e0fd"
branch_labels = ()
depends_on = None

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection, autoflush=False, autocommit=False)
    db.session = session


class Account(db.Model):
    __tablename__ = "account"

    id = sa.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.now, onupdate=datetime.now)
    deleted_at = db.Column(db.DateTime, default=None)

    name = db.Column(db.String(64), unique=True)
    pin = db.Column(db.String(64))
    balance = db.Column(db.Integer, default=0)
    event_account = db.Column(db.Boolean, default=False)

    iban = db.Column(db.String(48))
    bic = db.Column(db.String(16))
    direct_debit = db.Column(db.Boolean)
    mandate_id = db.Column(db.String(64))
    sign_date = db.Column(db.DateTime)

    ledger_account = db.Column(db.Boolean, default=False)
    ledger_exact_code = db.Column(db.String(64))
    description = db.Column(db.String(128))

    orders = db.relationship("Order", backref="account")


class Order(db.Model):
    __tablename__ = "order"

    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.now, onupdate=datetime.now)
    deleted_at = db.Column(db.DateTime, default=None)

    account_id = db.Column(db.Integer, db.ForeignKey("account.id"))
    transaction_id = db.Column(
        db.Integer, db.ForeignKey("transaction.id", ondelete="cascade")
    )

    method = db.Column(
        db.Enum(
            "cash",
            "card",
            "account",
            "transfer",
            "ideal",
            "sepa",
            "other",
            name="payment_methods",
        ),
        name="method",
    )

    price = db.Column(db.Integer)

    type = db.Column(
        db.Enum(
            "invalid",
            "direct_payment",
            "account_top_up_or_account_payment",
            "product_purchase",
            name="order_types",
        ),
        name="type",
        nullable=False,
    )


class Transaction(db.Model):
    __tablename__ = "transaction"

    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.now, onupdate=datetime.now)
    deleted_at = db.Column(db.DateTime, default=None)

    success = db.Column(db.Boolean, default=False, name="success")
    nonce = db.Column(db.String, unique=True)
    reason = db.Column(db.String(60))

    type = db.Column(
        db.Enum(
            "invalid",
            "top_up",
            "top_up_and_purchase",
            "direct_purchase",
            "account_purchase",
            name="transaction_types",
        ),
        name="type",
        nullable=False,
    )

    orders = relationship("Order", backref="transaction", passive_deletes=True)


def upgrade():
    create_session()

    start_date = datetime(2018, 8, 1, 0, 0, 0)

    # Taken from accounts.get_account_balances_at_date
    for account_id, balance, transactions_sum in (
        db.session.query(
            Account.id,
            Account.balance,
            func.sum(case((Transaction.id.isnot(None), Order.price), else_=0)),
        )
        .select_from(Account)
        .filter(Account.ledger_account.isnot(True))
        .outerjoin(Order, and_(Order.account_id == Account.id))
        .outerjoin(
            Transaction,
            and_(Transaction.id == Order.transaction_id, Transaction.success == True),
        )
        .group_by(Account.id, Account.balance)
        .order_by(Account.id.asc())
        .all()
    ):

        if not transactions_sum:
            transactions_sum = 0

        balance_at_start = balance + transactions_sum

        if balance_at_start != 0:
            t = Transaction(
                created_at=start_date,
                success=True,
                nonce=f"START_BALANCE_{account_id}",
                reason="Starting balance from old PoS",
                type="top_up",
            )
            db.session.add(t)
            db.session.flush()

            o1 = Order(
                created_at=start_date,
                transaction_id=t.id,
                method="account",
                price=-balance_at_start,
                account_id=account_id,
                type="account_top_up_or_account_payment",
            )
            o2 = Order(
                created_at=start_date,
                transaction_id=t.id,
                method="other",
                price=balance_at_start,
                type="direct_payment",
            )
            db.session.add(o1)
            db.session.add(o2)
            db.session.flush()

    db.session.commit()


def downgrade():
    create_session()

    transactions = db.session.query(Transaction).filter(
        Transaction.nonce.ilike("START_BALANCE_%")
    )
    for transaction in transactions.all():
        for order in (
            db.session.query(Order).filter_by(transaction_id=transaction.id).all()
        ):
            db.session.delete(order)

    for transaction in transactions.all():
        db.session.delete(transaction)

    db.session.commit()
