"""register default

Revision ID: 511a3b0ad3a1
Revises: 824d5013a8f7
Create Date: 2019-04-24 14:22:23.207765

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "511a3b0ad3a1"
down_revision = "824d5013a8f7"
branch_labels = ()
depends_on = None


def upgrade():
    op.add_column(
        "register",
        sa.Column(
            "default", sa.Boolean(), nullable=False, server_default="0", default=False
        ),
    )


def downgrade():
    op.drop_column("register", "default")
