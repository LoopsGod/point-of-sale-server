"""Proper delete bookkeeping

Revision ID: 1551bda1521e
Revises: 9bcb8c78fa17
Create Date: 2019-03-20 18:28:37.746428

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm import Session, make_transient, declarative_base
from datetime import datetime
from sqlalchemy import orm

Base = declarative_base()


# revision identifiers, used by Alembic.
revision = "1551bda1521e"
down_revision = "9bcb8c78fa17"
branch_labels = ()
depends_on = None


class Transaction(Base):
    __tablename__ = "transaction"

    id = sa.Column(sa.Integer, primary_key=True)
    created_at = sa.Column(sa.DateTime, default=datetime.now)
    updated_at = sa.Column(sa.DateTime, default=datetime.now, onupdate=datetime.now)
    deleted_at = sa.Column(sa.DateTime, default=None)

    success = sa.Column(sa.Boolean, default=False)

    orders = orm.relationship("Order", backref="transaction", order_by="Order.price")
    nonce = sa.Column(sa.String, unique=True)


class Order(Base):
    __tablename__ = "order"
    id = sa.Column(sa.Integer, primary_key=True)
    created_at = sa.Column(sa.DateTime, default=datetime.now)
    updated_at = sa.Column(sa.DateTime, default=datetime.now, onupdate=datetime.now)
    deleted_at = sa.Column(sa.DateTime, default=None)
    account_id = sa.Column(sa.Integer)
    product_id = sa.Column(sa.Integer)
    transaction_id = sa.Column(sa.Integer, sa.ForeignKey("transaction.id"))
    sepa_batch_id = sa.Column(sa.Integer)

    method = sa.Column(
        sa.Enum(
            "cash",
            "card",
            "account",
            "transfer",
            "ideal",
            "sepa",
            "other",
            name="payment_methods",
        )
    )

    price = sa.Column(sa.Integer)

    @staticmethod
    def create_reverse_order(original: "Order") -> "Order":
        # Simply copy most of the properties
        new = Order(
            account_id=original.account_id,
            product_id=original.product_id,
            transaction_id=original.transaction_id,
            sepa_batch_id=original.sepa_batch_id,
            method=original.method,
        )

        # Revert the price
        new.price = -original.price

        # Set deleted to the original date
        new.deleted_at = original.created_at

        return new


def upgrade():
    session = Session(bind=op.get_bind())
    transactions = (
        session.query(Transaction).filter(Transaction.deleted_at.isnot(None)).all()
    )

    for t in transactions:
        for o in t.orders:
            new = Order.create_reverse_order(o)
            session.add(new)

            # Set deleted_at to None, since in the new system deleted_at shows
            # that those orders are reversed orders. But these are actually the
            # original orders.
            o.deleted_at = None
            session.add(o)

    session.commit()


def downgrade():
    session = Session(bind=op.get_bind())
    transactions = (
        session.query(Transaction).filter(Transaction.deleted_at.isnot(None)).all()
    )

    for t in transactions:
        for o in t.orders:
            if o.deleted_at is not None:
                session.delete(o)

    session.commit()
