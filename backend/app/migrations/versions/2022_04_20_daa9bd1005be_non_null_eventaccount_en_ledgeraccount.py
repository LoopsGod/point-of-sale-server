"""non-null eventAccount en ledgerAccount

Revision ID: daa9bd1005be
Revises: 205c4cf666f4
Create Date: 2022-04-20 15:20:43.413040

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "daa9bd1005be"
down_revision = "205c4cf666f4"
branch_labels = ()
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.execute('UPDATE "account" SET event_account = false WHERE event_account is null')
    op.execute(
        'UPDATE "account" SET ledger_account = false WHERE ledger_account is null'
    )
    op.alter_column(
        "account", "event_account", existing_type=sa.BOOLEAN(), nullable=False
    )
    op.alter_column(
        "account", "ledger_account", existing_type=sa.BOOLEAN(), nullable=False
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column(
        "account", "ledger_account", existing_type=sa.BOOLEAN(), nullable=True
    )
    op.alter_column(
        "account", "event_account", existing_type=sa.BOOLEAN(), nullable=True
    )
    # ### end Alembic commands ###
