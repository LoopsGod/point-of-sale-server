"""merge heads

Revision ID: ce44c02b3672
Revises: 57eca3c5af52, e0dce9c9e5d1
Create Date: 2020-05-26 23:20:21.275242

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "ce44c02b3672"
down_revision = ("57eca3c5af52", "e0dce9c9e5d1")
branch_labels = ()
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
