"""Insert ledger accounts for checkout methods

Revision ID: bb6650cf5294
Revises: 7e9bc3f19f2f
Create Date: 2023-06-07 19:25:14.030669

"""
from datetime import datetime
from enum import Enum
from typing import Optional

from alembic import op
import sqlalchemy as sa
from sqlalchemy import orm, Column, String, Boolean, Integer, ForeignKey, DateTime
from sqlalchemy.orm import declarative_base, Mapped, relationship


class CheckoutMethod(Enum):
    CASH = "cash"
    CARD = "card"
    OTHER = "other"
    ACCOUNT = "account"
    IDEAL = "ideal"
    SEPA = "sepa"
    TRANSFER = "transfer"


CheckoutMethodSQL = sa.Enum(
    "cash",
    "card",
    "account",
    "transfer",
    "ideal",
    "sepa",
    "other",
    name="payment_methods",
)

base = declarative_base()


class Account(base):
    """
    Tab with its owner's name, hashed PIN, balance and banking information, if
    any.
    """

    __tablename__ = "account"

    created_at: Mapped[datetime] = Column(
        DateTime,
        # We use lambda here such that we can freezegun this time in tests.
        default=lambda: datetime.now(),
        nullable=False,
    )
    updated_at: Mapped[datetime] = Column(
        DateTime,
        # We use lambda here such that we can freezegun this time in tests.
        default=lambda: datetime.now(),
        onupdate=lambda: datetime.now(),
        nullable=False,
    )
    deleted_at: Mapped[datetime | None] = Column(DateTime, nullable=True, default=None)

    id = sa.Column(sa.Integer, primary_key=True)
    name: Mapped[str] = Column(String(64), unique=True)
    balance: Mapped[int] = Column(Integer, default=0)

    event_account: Mapped[bool] = Column(Boolean, default=False, nullable=False)
    ledger_account: Mapped[bool] = Column(Boolean, default=False, nullable=False)
    income_ledger: Mapped[bool] = Column(Boolean, default=False, nullable=False)

    allow_negative_balance: Mapped[bool] = Column(
        Boolean, default=False, nullable=False
    )

    exact_ledger: Mapped[Optional["AccountExactLedger"]] = relationship(
        "AccountExactLedger", uselist=False, viewonly=True
    )
    description: Mapped[str | None] = Column(String(128), nullable=True)


class Setting(base):
    __tablename__ = "setting"

    id = sa.Column(sa.Integer, primary_key=True)
    key: Mapped[str] = Column(String(128), unique=True, nullable=False)
    value: Mapped[str] = Column(String(4096), nullable=False)

    created_at: Mapped[datetime] = Column(
        DateTime,
        # We use lambda here such that we can freezegun this time in tests.
        default=lambda: datetime.now(),
        nullable=False,
    )
    updated_at: Mapped[datetime] = Column(
        DateTime,
        # We use lambda here such that we can freezegun this time in tests.
        default=lambda: datetime.now(),
        onupdate=lambda: datetime.now(),
        nullable=False,
    )
    deleted_at: Mapped[datetime | None] = Column(DateTime, nullable=True, default=None)


class AccountExactLedger(base):
    __tablename__ = "account_exact_ledger"

    id = sa.Column(sa.Integer, primary_key=True)
    account_id: Mapped[int] = Column(Integer, ForeignKey("account.id"), nullable=False)
    exact_ledger_code: Mapped[str] = Column(String(64), nullable=False)

    created_at: Mapped[datetime] = Column(
        DateTime,
        # We use lambda here such that we can freezegun this time in tests.
        default=lambda: datetime.now(),
        nullable=False,
    )
    updated_at: Mapped[datetime] = Column(
        DateTime,
        # We use lambda here such that we can freezegun this time in tests.
        default=lambda: datetime.now(),
        onupdate=lambda: datetime.now(),
        nullable=False,
    )
    deleted_at: Mapped[datetime | None] = Column(DateTime, nullable=True, default=None)
    associated_checkout_method: Mapped[CheckoutMethod | None] = Column(
        CheckoutMethodSQL, nullable=True
    )


# revision identifiers, used by Alembic.
revision = "bb6650cf5294"
down_revision = "7c9213c58805"
branch_labels = None
depends_on = None
methods = list(CheckoutMethod)


def upgrade():
    bind = op.get_bind()
    session = orm.Session(bind=bind)

    op.drop_constraint("account_name_key", "account", type_="unique")
    op.add_column(
        "account",
        sa.Column("income_ledger", sa.Boolean(), server_default="f", nullable=False),
    )
    op.alter_column(
        "account_exact_ledger",
        "exact_cost_center_code",
        existing_type=sa.VARCHAR(length=64),
        nullable=True,
    )
    op.alter_column(
        "account_exact_ledger",
        "exact_vat_code",
        existing_type=sa.VARCHAR(length=64),
        nullable=True,
    )
    op.add_column(
        "account_exact_ledger",
        sa.Column("associated_checkout_method", CheckoutMethodSQL, nullable=True),
    )

    # Insert accounts with names:
    for method in methods:
        if method == CheckoutMethod.ACCOUNT:
            continue

        acc = Account(
            name="income_ledger_" + method.value,
            balance=0,
            event_account=False,
            ledger_account=True,
            income_ledger=True,
        )

        session.add(acc)
        session.flush()

        print(f"Mapping method '{method.value}' to account '{acc.name}' ({acc.id})")
        setting = Setting(
            key=(f"{method.value}_CHECKOUT_METHOD_ACCOUNT_ID").upper(),
            value=str(acc.id),
        )
        session.add(setting)

        original_setting = (
            session.query(Setting)
            .filter(Setting.key == f"EXACT_ACCOUNT_POS_INCOME_{method.value}".upper())
            .first()
        )

        if original_setting:
            print(
                f"Original setting {original_setting.key} = {original_setting.value}."
            )
            session.add(
                AccountExactLedger(
                    account_id=acc.id,
                    exact_ledger_code=original_setting.value,
                    associated_checkout_method=method.value,
                )
            )
        else:
            print("Original setting is NONE.")

        op.execute(
            sa.text(
                f"UPDATE \"order\" SET account_id = {acc.id} WHERE method = '{method.value}' and account_id IS NULL"
            )
        )

    op.alter_column("order", "account_id", nullable=False)
    session.commit()


def downgrade():
    bind = op.get_bind()
    session = orm.Session(bind=bind)
    op.alter_column("order", "account_id", nullable=True)

    for method in methods:
        if method == CheckoutMethod.ACCOUNT:
            continue

        acc_id: Setting | None = (
            session.query(Setting)
            .filter(
                Setting.key == (f"{method.value}_CHECKOUT_METHOD_ACCOUNT_ID").upper()
            )
            .first()
        )

        if acc_id:
            # Update all old order
            op.execute(
                sa.text(
                    "UPDATE \"order\" SET account_id = NULL WHERE method != 'account' and account_id = :account_id"
                ).bindparams(account_id=acc_id.value)
            )

            exact_ledger: AccountExactLedger | None = (
                session.query(AccountExactLedger)
                .filter(AccountExactLedger.account_id == acc_id.value)
                .first()
            )

            if exact_ledger:
                # Restore the old EXACT_ACCOUNT_POS_INCOME_* settings
                ledger_setting = (
                    session.query(Setting)
                    .filter(
                        Setting.key
                        == (f"EXACT_ACCOUNT_POS_INCOME_{method.value}").upper()
                    )
                    .first()
                )

                if not ledger_setting:
                    ledger_setting = Setting(
                        key=(f"EXACT_ACCOUNT_POS_INCOME_{method.value}").upper(),
                        value=str(exact_ledger.exact_ledger_code),
                    )

                session.add(ledger_setting)

            session.delete(exact_ledger)

            print("Deleting account: " + str(acc_id.value))
            acc = session.query(Account).filter(Account.id == acc_id.value).first()
            session.delete(acc)

        checkout_setting = (
            session.query(Setting)
            .filter(
                Setting.key == (f"{method.value}_CHECKOUT_METHOD_ACCOUNT_ID").upper()
            )
            .first()
        )

        if checkout_setting:
            session.delete(checkout_setting)

    op.create_unique_constraint("account_name_key", "account", ["name"])
    op.drop_column("account", "income_ledger")
    op.drop_column("account_exact_ledger", "associated_checkout_method")
    op.alter_column(
        "account_exact_ledger",
        "exact_vat_code",
        existing_type=sa.VARCHAR(length=64),
        nullable=False,
    )
    op.alter_column(
        "account_exact_ledger",
        "exact_cost_center_code",
        existing_type=sa.VARCHAR(length=64),
        nullable=False,
    )
    session.commit()
