"""Convert upgrades to double-entry transactions

Revision ID: eb9c9d7dae91
Revises: cfc077ad20fc
Create Date: 2018-10-16 08:05:48.908822

"""
from alembic import op
from sqlalchemy.orm import Session
from tqdm import tqdm
from datetime import timedelta
from app.migrations.util.transaction import Transaction

# revision identifiers, used by Alembic.
revision = "eb9c9d7dae91"
down_revision = "cfc077ad20fc"
branch_labels = ()
depends_on = None


def upgrade():
    session = Session(bind=op.get_bind())

    transactions = (
        session.query(Transaction).order_by(Transaction.created_at.desc()).all()
    )

    for i, transaction in enumerate(tqdm(transactions)):
        if transaction.deleted:
            continue

        if sum(o.price for o in transaction.orders) == 0:
            # Already a double-entry transaction or not an upgrade
            continue

        if not all(o.product is None for o in transaction.orders):
            continue

        try:
            next = transactions[i + 1]
        except IndexError:
            continue

        if next.created_at - transaction.created_at > timedelta(milliseconds=500):
            # Not an upgrade
            continue

        if (
            (
                sum(o.price for o in transaction.orders) < 0
                and sum(o.price for o in next.orders) < 0
            )
            or transaction.orders[0].method == "sepa"
            or next.orders[0].method == "sepa"
        ):
            # Probably a SEPA transaction, leave alone
            continue

        # Add the second transaction to the current
        transaction.orders.extend(next.orders)
        next.deleted = True
        session.add(next)

        total = sum(o.price for o in transaction.orders)
        assert total == 0, f"{transaction.serialize()}\n{next.serialize()}\n{total}"

        session.add(transaction)
    session.commit()


def downgrade():
    pass
