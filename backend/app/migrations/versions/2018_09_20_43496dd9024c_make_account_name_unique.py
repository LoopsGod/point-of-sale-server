"""Make account name unique

Revision ID: 43496dd9024c
Revises: 7479aebc413d
Create Date: 2018-09-20 12:38:59.452531

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "43496dd9024c"
down_revision = "7479aebc413d"
branch_labels = ()
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_unique_constraint(None, "account", ["name"])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint("account_name_key", "account", type_="unique")
    # ### end Alembic commands ###
