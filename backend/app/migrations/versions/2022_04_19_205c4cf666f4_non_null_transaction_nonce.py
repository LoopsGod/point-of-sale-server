"""Non-null transaction nonce

Revision ID: 205c4cf666f4
Revises: 1aa53475d40b
Create Date: 2022-04-19 16:07:39.971096

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "205c4cf666f4"
down_revision = "1aa53475d40b"
branch_labels = ()
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.execute(
        "UPDATE transaction "
        "set nonce = 'pre-nonce-transaction-' || transaction.id "
        "where nonce is null;"
    )
    op.alter_column("transaction", "nonce", existing_type=sa.VARCHAR(), nullable=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column("transaction", "nonce", existing_type=sa.VARCHAR(), nullable=True)
    # ### end Alembic commands ###
