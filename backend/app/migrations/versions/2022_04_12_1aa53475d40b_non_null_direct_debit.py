"""Non-null direct_debit

Revision ID: 1aa53475d40b
Revises: d5f278e645ae
Create Date: 2022-04-12 11:25:34.567769

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "1aa53475d40b"
down_revision = "d5f278e645ae"
branch_labels = ()
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.execute('UPDATE "account" set direct_debit = false where direct_debit is null;')
    op.alter_column(
        "account", "direct_debit", existing_type=sa.BOOLEAN(), nullable=False
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column(
        "account", "direct_debit", existing_type=sa.BOOLEAN(), nullable=True
    )
    # ### end Alembic commands ###
