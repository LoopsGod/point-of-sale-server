"""Add default register

Revision ID: c1f05c300432
Revises: ea4bcb13cf96
Create Date: 2019-06-08 17:42:20.542795

"""
from alembic import op
import sqlalchemy as sa
from datetime import datetime

from sqlalchemy import orm

# revision identifiers, used by Alembic.
from sqlalchemy.orm import declarative_base

revision = "c1f05c300432"
down_revision = "ea4bcb13cf96"
branch_labels = ()
depends_on = None

Base = declarative_base()


class Register(Base):
    __tablename__ = "register"

    """Represents a register, some point of sale"""
    id = sa.Column(sa.Integer, primary_key=True)
    created_at = sa.Column(sa.DateTime, default=datetime.now)
    updated_at = sa.Column(sa.DateTime, default=datetime.now, onupdate=datetime.now)
    deleted_at = sa.Column(sa.DateTime, default=None)

    name = sa.Column(sa.String, nullable=False)

    capabilities = sa.Column(sa.Integer, nullable=False, default=0)
    default = sa.Column(sa.Boolean, nullable=False, default=False)


def upgrade():
    bind = op.get_bind()
    session = orm.Session(bind=bind)

    register = Register()
    register.name = "Default register"
    register.capabilities = 0
    register.default = True

    session.add(register)
    session.commit()


def downgrade():
    pass
