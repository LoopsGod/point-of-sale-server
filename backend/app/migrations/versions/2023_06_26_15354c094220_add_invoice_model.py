"""Add invoice model

Revision ID: 15354c094220
Revises: 7c9213c58805
Create Date: 2023-06-26 13:42:37.618585

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = "15354c094220"
down_revision = "bb6650cf5294"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "invoice",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=False),
        sa.Column("updated_at", sa.DateTime(), nullable=False),
        sa.Column("deleted_at", sa.DateTime(), nullable=True),
        sa.Column("invoice_id", sa.String(length=128), nullable=False),
        sa.Column("vendor", sa.String(length=128), nullable=False),
        sa.Column("uploaded_file_id", sa.String(length=128), nullable=False),
        sa.Column("imported", sa.Boolean(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("vendor", "invoice_id", name="_vendor_invoice_id_uc"),
    )


def downgrade():
    op.drop_table("invoice")
