from sqlalchemy.orm import Session

from app.models import LoginToken


def find_by_token(db_session: Session, token: str) -> LoginToken | None:
    return db_session.query(LoginToken).filter_by(token=token).one_or_none()


def create(db_session: Session, token: LoginToken) -> LoginToken:
    db_session.add(token)
    db_session.commit()

    return token


def delete(db_session: Session, token: LoginToken) -> None:
    db_session.delete(token)
    db_session.commit()
