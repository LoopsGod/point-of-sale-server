from datetime import datetime
from typing import cast

from sqlalchemy import delete, func, and_, distinct, text, select
from sqlalchemy.orm import Session, contains_eager

from app.models import SlideProduct, Product, Order, Transaction
from app.repository.util.pagination import (
    PageParameters,
    PaginatedResponse,
    PaginatedSearch,
    SearchParameters,
    SortParameters,
)
from app.repository.util.filters import LiteralEqualsFilter, BooleanFilter
from app.models.slide import SlideProductData


def get_all(
    db_session: Session,
    page_params: PageParameters,
    search_params: SearchParameters,
    sort_params: SortParameters,
    id_filter: LiteralEqualsFilter = None,
    product_filter: LiteralEqualsFilter = None,
    show_filter: BooleanFilter = None,
    trends_filter: BooleanFilter = None,
    spacing_filter: BooleanFilter = None,
) -> PaginatedResponse[SlideProductData]:
    query = (
        db_session.query(SlideProduct)
        .join(Product)
        .options(contains_eager(SlideProduct.product))
    )

    return (
        PaginatedSearch[SlideProduct, SlideProductData](query, page_params)
        .enable_searching(search_params, [SlideProduct.name])
        .enable_sorting(
            sort_params,
            sorting_column_definition={
                "name": SlideProduct.name,
                "order": SlideProduct.order,
                "show": SlideProduct.show_slide,
                "trends": SlideProduct.show_trends,
                "spacing:": SlideProduct.add_spacing,
            },
        )
        .enable_filtering(
            {
                SlideProduct.id: id_filter,
                SlideProduct.product_id: product_filter,
                SlideProduct.show_slide: show_filter,
                SlideProduct.show_trends: trends_filter,
                SlideProduct.add_spacing: spacing_filter,
            }
        )
        .execute()
    )


def get_slide_products(db: Session) -> list[SlideProduct]:
    return db.scalars(
        select(SlideProduct)
        .select_from(SlideProduct)
        .join(Product)
        .filter(SlideProduct.show_slide.is_(True))
        .order_by(SlideProduct.order.asc())
    ).all()


def get_slide_products_sold_weighted(
    db: Session, start: datetime, end: datetime
) -> float:
    q = (
        select(
            func.sum(SlideProduct.weight),
        )
        .select_from(SlideProduct)
        .outerjoin(Order, and_(Order.product_id == SlideProduct.product_id))
        .join(Transaction)
        .filter(
            and_(
                Order.deleted_at.is_(None),
                Transaction.deleted_at.is_(None),
                Transaction.created_at.between(start, end),
            )
        )
    )

    return db.execute(q).scalar() or 0.0


def get_active_accounts_since(db: Session, start: datetime, end: datetime) -> int:
    q = (
        select(
            func.count(distinct(Order.account_id)),
        )
        .select_from(Order)
        .join(Transaction)
        .filter(
            and_(
                Order.deleted_at.is_(None),
                Order.account_id.isnot(None),
                Order._type == "account_top_up_or_account_payment",
                Order.price > 0,
                Transaction.deleted_at.is_(None),
                Transaction.created_at.between(start, end),
            )
        )
    )
    return db.scalar(q) or 0


def get_trends_sorted(
    db: Session, start: datetime, end: datetime
) -> dict[str, tuple[int, str, int]]:
    q = (
        select(
            SlideProduct.id,
            SlideProduct.name,
            func.count(SlideProduct.id).label("s"),
        )
        .select_from(SlideProduct)
        .outerjoin(Order, and_(Order.product_id == SlideProduct.product_id))
        .join(Transaction)
        .filter(
            and_(
                SlideProduct.show_trends.is_(True),
                Order.deleted_at.is_(None),
                Transaction.deleted_at.is_(None),
                Transaction.created_at.between(start, end),
            )
        )
        .group_by(SlideProduct.id, SlideProduct.name)
        .order_by(text("s desc"))
    )
    trends = db.execute(q).all()

    return cast(dict[str, tuple[int, str, int]], {trend[1]: trend for trend in trends})


def get_by_id(db_session: Session, slide_product_id: int) -> SlideProduct | None:
    return db_session.get(SlideProduct, slide_product_id)


def delete_by_id(db_session: Session, slide_product_id: int) -> None:
    stmt = delete(SlideProduct).where(SlideProduct.id == slide_product_id)
    db_session.execute(stmt)
