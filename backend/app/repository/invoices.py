from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

from app.models.invoice import ProductClassification, InvoiceData, Invoice
from app.repository.util.pagination import (
    PageParameters,
    PaginatedResponse,
    PaginatedSearch,
    SearchParameters,
    SortParameters,
)
from app.views.exceptions import ResourceConflictException


def create_invoice(
    db_session: Session,
    invoice_id: str,
    vendor: str,
    uploaded_file_id: str,
    total_value: int,
) -> Invoice:
    try:
        invoice = Invoice.create_with_details(
            invoice_id, vendor, uploaded_file_id, total_value
        )
        db_session.add(invoice)
        db_session.commit()
    except IntegrityError as e:
        raise ResourceConflictException("duplicate_invoice") from e

    return invoice


def get_all(
    db_session: Session,
    page_params: PageParameters,
    search_params: SearchParameters,
    sort_params: SortParameters,
) -> PaginatedResponse[InvoiceData]:
    query = db_session.query(Invoice).filter(Invoice.imported.is_(True))

    return (
        PaginatedSearch[Invoice, InvoiceData](query, page_params)
        .enable_searching(search_params, [Invoice.invoice_id, Invoice.vendor])
        .enable_sorting(
            sort_params,
            sorting_column_definition={
                "invoiceId": Invoice.invoice_id,
            },
        )
    ).execute()


def find_classification_by_name(
    db_session: Session,
    description: str,
) -> ProductClassification | None:
    return (
        db_session.query(ProductClassification)
        .filter(ProductClassification.description == description)
        .one_or_none()
    )


def get_classification_by_id(
    db_session: Session, classification_id: int
) -> ProductClassification:
    return db_session.query(ProductClassification).filter_by(id=classification_id).one()


def update_classifications(
    db_session: Session, classifications: list[ProductClassification]
) -> None:
    db_session.add_all(classifications)
    db_session.commit()


def mark_as_imported(db_session: Session, invoice_id: int) -> None:
    invoice = db_session.query(Invoice).filter_by(id=invoice_id).one()
    invoice.imported = True
    db_session.add(invoice)
    db_session.commit()
