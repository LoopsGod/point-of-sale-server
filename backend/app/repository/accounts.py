from collections.abc import Iterable
from datetime import datetime
from typing import cast

from sqlalchemy import and_, func, select, String, or_
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

from app.dependencies import UserRequestContext, RequestContext
from app.models import Order, User, UserDirectDebit
from app.models.account import (
    AccountRequest,
    AccountRequestStatus,
    Account,
    SerializedAccountRequest,
    AccountExactLedger,
    AccountData,
)
from app.models.account_type import AccountType
from app.repository.util.filters import (
    BooleanFilter,
    LiteralEqualsFilter,
    NullFilter,
    NumberFilter,
    LastUsageFilter,
)
from app.repository.util.pagination import (
    PageParameters,
    SortParameters,
    PaginatedResponse,
    PaginatedSearch,
    SearchParameters,
)
from app.util import util
from app.views.exceptions import ResourceConflictException, ValidationException


def create_account_and_link_user(
    context: UserRequestContext, account_request: AccountRequest
) -> Account:
    # Undelete existing account if exists.
    user: User = account_request.user
    if user.account_id:
        existing_account = (
            context.db_session.query(Account).filter_by(id=user.account_id).one()
        )
        existing_account.deleted = False
        context.db_session.add(existing_account)
        context.db_session.commit()
        return existing_account

    # Otherwise create an account and link the user.
    try:
        account = Account()
        account.name = user.name
        # Note: Backwards compatibility for old account requests that have empty
        # string in the PIN column. NULL is used for accounts without pin.
        account.pin = account_request.pin if account_request.pin else None
        context.db_session.add(account)
        user.account = account
        context.db_session.add(user)
        context.db_session.commit()
        return account
    except IntegrityError as e:
        raise ResourceConflictException("duplicate_name") from e


def get_by_id(db_session: Session, acc_id: int) -> Account | None:
    return db_session.execute(
        select(Account).filter(Account.id == acc_id)
    ).scalar_one_or_none()


def get_by_ids(db_session: Session, acc_ids: list[int]) -> list[Account]:
    return db_session.query(Account).filter(Account.id.in_(acc_ids)).all()


def paginated_search(
    db_session: Session,
    page_params: PageParameters,
    search_params: SearchParameters,
    sort_params: SortParameters,
    id_filter: LiteralEqualsFilter = None,
    type_filter: LiteralEqualsFilter = None,
    allow_negative_filter: BooleanFilter = None,
    deleted_filter: NullFilter = None,
    balance_filter: NumberFilter = None,
    last_usage_filter: LastUsageFilter = None,
) -> PaginatedResponse[AccountData]:
    q = db_session.query(Account)
    return (
        PaginatedSearch[Account, AccountData](q, page_params)
        .enable_searching(search_params, [Account.name])
        .enable_sorting(
            sort_params,
            sorting_column_definition={
                "deleted": Account.deleted_at,
                "name": func.lower(Account.name),
                "type": func.cast(Account.type, String),
                "balance": Account.balance,
                "allowNegative": Account.allow_negative_balance,
            },
        )
        .enable_filtering(
            {
                Account.id: id_filter,
                Account.type: type_filter,
                Account.allow_negative_balance: allow_negative_filter,
                Account.deleted_at: deleted_filter,
                Account.balance: balance_filter,
                Account.updated_at: last_usage_filter,
            }
        )
        .execute()
    )


def paginated_search_requests(
    db_session: Session,
    page_params: PageParameters,
    search_params: SearchParameters,
    sort_params: SortParameters,
) -> PaginatedResponse[SerializedAccountRequest]:
    q = db_session.query(AccountRequest)
    return (
        PaginatedSearch[AccountRequest, SerializedAccountRequest](q, page_params)
        .enable_searching(search_params, [(User.name, User), (User.username, User)])
        .enable_sorting(
            sort_params,
            sorting_column_definition={
                "name": (User.name, User),
                "createdAt": AccountRequest.created_at,
            },
        )
        .execute()
    )


def find_latest_account_request(
    context: UserRequestContext,
) -> AccountRequest | None:
    return context.db_session.scalar(
        select(AccountRequest)
        .filter_by(user_id=context.user.id)
        .order_by(AccountRequest.created_at)
        .limit(1)
    )


def find_account_request_by_id(
    context: UserRequestContext, request_id: int
) -> AccountRequest | None:
    return (
        context.db_session.query(AccountRequest).filter_by(id=request_id).one_or_none()
    )


def create_account_request(context: UserRequestContext, pin: str) -> AccountRequest:
    request = AccountRequest.create_with_user_and_pin(context.user, pin)
    context.db_session.add(request)
    context.db_session.commit()
    return request


def set_account_request_status(
    context: UserRequestContext,
    account_request: AccountRequest,
    status: AccountRequestStatus,
) -> None:
    account_request.status = status.value
    context.db_session.add(account_request)
    context.db_session.commit()


def get_account_balances_at_date(
    context: RequestContext, date: datetime
) -> Iterable[tuple[str, int, int]]:
    """Returns the balance of all accounts on a given date"""
    # We only want to count the orders AFTER the date
    date = util.get_last_moment_of_day(date)

    # We use outerjoin so that accounts that don't have any orders in
    # the period are in the result.
    return (
        context.db_session.query(
            Account.name,
            Account.balance,
            func.sum(Order.price),
        )
        .select_from(Account)
        .filter(
            or_(
                Account.type == AccountType.USER.value,
                Account.type == AccountType.EVENT.value,
            )
        )
        .outerjoin(Order, and_(Order.account_id == Account.id, Order.created_at > date))
        .group_by(Account.name, Account.balance)
        .order_by(Account.name.asc())
        .all()
    )


def set_account_exact_ledger(
    db_session: Session,
    account: Account,
    ledger_code: str,
    cost_center_code: str | None,
    vat_code: str | None,
) -> AccountExactLedger:
    if account.type == AccountType.INCOME_LEDGER:
        if not (cost_center_code is None and vat_code is None):
            raise ValidationException(
                "income_ledger_cannot_have_vat_or_cost_center_and_reversed"
            )
    elif cost_center_code is None or vat_code is None:
        raise ValidationException("cost_center_and_vat_code_required")

    ledger: AccountExactLedger = db_session.scalar(
        select(AccountExactLedger).filter(AccountExactLedger.account_id == account.id)
    )
    if not ledger:
        ledger = AccountExactLedger(
            account=account,
            exact_ledger_code=ledger_code,
            exact_cost_center_code=cost_center_code,
            exact_vat_code=vat_code,
        )
        db_session.add(ledger)
    else:
        ledger.exact_ledger_code = ledger_code
        ledger.exact_cost_center_code = cost_center_code
        ledger.exact_vat_code = vat_code
    db_session.commit()
    return ledger


def find_unset_exact_codes(db_session: Session) -> list[Account]:
    return (
        db_session.query(Account)
        .outerjoin(AccountExactLedger)
        .filter(
            and_(
                or_(
                    Account.type == AccountType.INCOME_LEDGER.value,
                    Account.type == AccountType.PRODUCT_LEDGER.value,
                    Account.type == AccountType.PENDING_LEDGER.value,
                ),
                AccountExactLedger.id.is_(None),
            )
        )
        .all()
    )


def get_all_distinct_exact_codes(db_session: Session) -> list[str]:
    return db_session.scalars(
        select(AccountExactLedger.exact_ledger_code).distinct()
    ).all()


def get_all(db_session: Session, ledger: bool, with_pin: bool) -> list[Account]:
    stmt = (
        select(Account)
        .filter(Account.deleted_at.is_(None))
        .order_by((Account.type == AccountType.EVENT.value).desc(), Account.name.asc())
    )
    if not ledger:
        stmt = stmt.filter(
            or_(
                Account.type == AccountType.USER.value,
                Account.type == AccountType.EVENT.value,
            )
        )
    if with_pin:
        stmt = stmt.filter(Account.pin.isnot(None))
    return db_session.scalars(stmt).all()


def get_all_with_joined_user_direct_debit(
    db_session: Session,
) -> Iterable[tuple[Account, UserDirectDebit | None]]:
    stmt = (
        select(Account, UserDirectDebit)
        .filter(Account.deleted_at.is_(None))
        .filter(
            or_(
                Account.type == AccountType.USER.value,
                Account.type == AccountType.EVENT.value,
            )
        )
        .join(User, User.account_id == Account.id, isouter=True)
        .join(
            UserDirectDebit,
            UserDirectDebit.user_id == User.id,
            isouter=True,
        )
        .order_by(Account.name.asc())
    )

    # FIXME No clue how to type this properly
    return cast(
        Iterable[tuple[Account, UserDirectDebit | None]], db_session.execute(stmt).all()
    )


def get_all_eligible_for_direct_debit_batch(
    db_session: Session,
) -> Iterable[tuple[Account, UserDirectDebit]]:
    stmt = (
        select(Account, UserDirectDebit)
        .filter(Account.deleted_at.is_(None))
        .filter(Account.type == AccountType.USER.value)
        .filter(Account.balance < 0)
        .join(User, User.account_id == Account.id)
        .join(
            UserDirectDebit,
            UserDirectDebit.user_id == User.id,
        )
        .order_by(Account.name.asc())
    )

    # FIXME No clue how to type this properly
    return cast(
        Iterable[tuple[Account, UserDirectDebit]], db_session.execute(stmt).all()
    )
