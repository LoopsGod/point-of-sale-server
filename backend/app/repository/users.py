from sqlalchemy import delete, func, select
from sqlalchemy.orm import Session, contains_eager, joinedload

from app.models import User, Register, UserDirectDebit
from app.models.user import UserData
from app.repository.util.filters import BooleanFilter, NullFilter, LiteralEqualsFilter
from app.repository.util.pagination import (
    PageParameters,
    SearchParameters,
    SortParameters,
    PaginatedSearch,
    PaginatedResponse,
)


def find_user_by_username(db_session: Session, username: str) -> User | None:
    return (
        db_session.query(User)
        .filter(
            func.lower(User.username) == username.lower(),
            User.viaduct_user_id.is_(None),
        )
        .one_or_none()
    )


def get_user_direct_debit(db_session: Session, user_id: int) -> UserDirectDebit | None:
    stmt = select(UserDirectDebit).filter(UserDirectDebit.user_id == user_id)
    return db_session.scalars(stmt).one_or_none()


def delete_user_direct_debit(db_session: Session, user_id: int) -> None:
    stmt = delete(UserDirectDebit).where(UserDirectDebit.user_id == user_id)
    db_session.execute(stmt)


def find_user_by_viaduct_id(db_session: Session, viaduct_user_id: int) -> User | None:
    return (
        db_session.query(User)
        .filter(User.viaduct_user_id == viaduct_user_id, User.password.is_(None))
        .one_or_none()
    )


def create_or_update_user(db_session: Session, user: User) -> User:
    db_session.add(user)
    db_session.commit()
    return user


def get_by_id(db_session: Session, user_id: int) -> User | None:
    stmt = (
        select(User).filter(User.id == user_id).options(joinedload(User.direct_debit))
    )

    return db_session.scalars(stmt).one_or_none()


def find_user_by_account_id(db_session: Session, account_id: int) -> User | None:
    return db_session.execute(
        select(User).filter(User.account_id == account_id)
    ).scalar_one_or_none()


def get_all(db_session: Session) -> list[User]:
    return db_session.scalars(
        select(User).filter(User.deleted_at.is_(None)).order_by(User.username)
    ).all()


def paginated_search(
    db_session: Session,
    page_params: PageParameters,
    search_params: SearchParameters,
    sort_params: SortParameters,
    id_filter: LiteralEqualsFilter = None,
    admin_filter: BooleanFilter = None,
    viaduct_filter: NullFilter = None,
) -> PaginatedResponse[UserData]:
    q = (
        db_session.query(User)
        .filter(User.deleted_at.is_(None))
        .outerjoin(User.register)
        .options(
            contains_eager(User.register),
        )
    )

    return (
        PaginatedSearch[User, UserData](q, page_params)
        .enable_searching(search_params, [User.username, User.name])
        .enable_sorting(
            sort_params,
            sorting_column_definition={
                "deleted": User.deleted_at,
                "name": User.name,
                "username": User.username,
                "register": Register.name,
            },
        )
        .enable_filtering(
            {
                User.id: id_filter,
                User.is_admin: admin_filter,
                User.viaduct_user_id: viaduct_filter,
            }
        )
        .execute()
    )
