from sqlalchemy.orm.session import Session

from app.models import Category
from app.models.category import CategoryData
from app.repository.util.filters import LiteralEqualsFilter, NullFilter
from app.repository.util.pagination import (
    PageParameters,
    PaginatedResponse,
    PaginatedSearch,
    SearchParameters,
    SortParameters,
)
from app.views.exceptions import NotFoundException


def get_all_with_ids(db_session: Session, category_ids: list[int]) -> list[Category]:
    return db_session.query(Category).filter(Category.id.in_(category_ids)).all()


def get_by_id(db_session: Session, category_id: int) -> Category:
    category = db_session.get(Category, category_id)

    if category is None:
        raise NotFoundException("Category not found.")
    return category


def get_all(
    db_session: Session,
    page_params: PageParameters,
    search_params: SearchParameters,
    sort_params: SortParameters,
    id_filter: LiteralEqualsFilter = None,
    deleted_filter: NullFilter = None,
) -> PaginatedResponse[CategoryData]:
    q = db_session.query(Category)

    return (
        PaginatedSearch[Category, CategoryData](q, page_params)
        .enable_searching(search_params, [Category.name])
        .enable_sorting(sort_params)
        .enable_filtering(
            {
                Category.id: id_filter,
                Category.deleted_at: deleted_filter,
            }
        )
        .execute()
    )
