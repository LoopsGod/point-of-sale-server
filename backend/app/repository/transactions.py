from typing import cast

from sqlalchemy.exc import IntegrityError
from datetime import datetime, timedelta

from sqlalchemy import select, distinct, func
from sqlalchemy.orm import contains_eager, Session, joinedload, aliased

from app.models import Transaction, Order, Product, Category
from app.models.category import ProductCategory
from app.models.transaction import TransactionType, TransactionData
from app.repository.util.pagination import (
    FilterAndJoinCondition,
    PageParameters,
    PaginatedSearch,
    PaginatedResponse,
    SearchParameters,
    SortParameters,
)
from app.repository.util.filters import DateTimeFilter, NullFilter, LiteralEqualsFilter
from app.service.transaction_service import ListTransactionParameters
from app.views.exceptions import ResourceConflictException


def get_transaction_by_id(
    db_session: Session, transaction_id: int
) -> Transaction | None:
    return db_session.query(Transaction).filter_by(id=transaction_id).one_or_none()


def get_all_between_datetime_joinedload(
    db_session: Session, start: datetime, end: datetime
) -> list[Transaction]:
    # contains_eager tells SQLAlchemy that we have already joined on orders
    return (
        db_session.query(Transaction)
        .join(Order, Order.transaction_id == Transaction.id)
        .filter(Order.created_at.between(start, end))
        .options(contains_eager(Transaction.orders))
        .order_by(Transaction.created_at.asc())
        .all()
    )


def save(db_session: Session, transaction: Transaction, do_commit: bool = True) -> None:
    db_session.add(transaction)

    if do_commit:
        db_session.commit()


def create_empty_transaction(db_session: Session, nonce: str) -> Transaction:
    try:
        transaction = Transaction(nonce=nonce)
        # We need to flush to get an id but we can't calculate type yet
        # TODO hybrid_property doesn't type yet.
        transaction.type = TransactionType.INVALID
        db_session.add(transaction)
        db_session.flush()

        return transaction

    except IntegrityError as e:
        raise ResourceConflictException("duplicate_transaction") from e


def paginated_search_all(
    db_session: Session,
    params: ListTransactionParameters,
    page_params: PageParameters,
) -> PaginatedResponse[TransactionData]:
    q = (
        db_session.query(Transaction)
        .order_by(Transaction.created_at.desc())
        .options(
            joinedload(Transaction.orders),
            joinedload(Transaction.orders, Order.account),
            joinedload(Transaction.orders, Order.product),
        )
    )
    if params.accountId is not None or params.sepaBatchId is not None:
        q = q.join(Order)

        if params.accountId is not None:
            q = q.filter(Order.account_id == params.accountId)

        if params.sepaBatchId is not None:
            q = q.filter(Order.sepa_batch_id == params.sepaBatchId)

    if params.deleted is not None and not params.deleted:
        q = q.filter(Transaction.deleted_at.is_(None))
    return PaginatedSearch[Transaction, TransactionData](q, page_params).execute()


def paginated_search_all_v2(
    db_session: Session,
    page_params: PageParameters,
    search_params: SearchParameters,
    sort_params: SortParameters,
    id_filter: LiteralEqualsFilter = None,
    deleted_filter: NullFilter = None,
    transaction_type_filter: LiteralEqualsFilter = None,
    product_filter: LiteralEqualsFilter = None,
    account_filter: LiteralEqualsFilter = None,
    created_at_filter: DateTimeFilter = None,
    method_filter: LiteralEqualsFilter = None,
    sepa_batch_filter: LiteralEqualsFilter = None,
) -> PaginatedResponse[TransactionData]:
    o1 = aliased(Order)
    o2 = aliased(Order)
    o3 = aliased(Order)

    main_query = db_session.query(distinct(Transaction.id), Transaction)
    # We won't execute this pagination, we will just use it as subquery
    in_query_pagination = (
        PaginatedSearch[Transaction, TransactionData](main_query, page_params)
        .enable_searching(search_params, [Transaction.reason])
        .enable_sorting(sort_params)
        .enable_filtering(
            {
                Transaction.id: id_filter,
                Transaction.deleted_at: deleted_filter,
                Transaction._type: transaction_type_filter,
                Transaction.created_at: created_at_filter,
                o1.product_id: FilterAndJoinCondition(
                    product_filter, o1, o1.transaction_id == Transaction.id
                ),
                o2.account_id: FilterAndJoinCondition(
                    account_filter, o2, o2.transaction_id == Transaction.id
                ),
                o3._method: FilterAndJoinCondition(
                    method_filter, o3, o3.transaction_id == Transaction.id
                ),
                o1.sepa_batch_id: FilterAndJoinCondition(
                    sepa_batch_filter, o1, o1.transaction_id == Transaction.id
                ),
            },
        )
    )
    # We need a second nested sub query because we cannot sort by (e.g.) `created_At``
    # when we are using DISTINCT id.
    in_sub_query = (
        #  Apply limit/offset manually, this is normally done by pagination.execute
        in_query_pagination.query.limit(page_params.perPage).offset(
            (page_params.page - 1) * page_params.perPage
        )
    ).subquery()

    # This sub query throws away all other columns from Transaction besides id.
    transaction_from_subquery = aliased(Transaction, in_sub_query)
    in_query = select(transaction_from_subquery.id)

    main_query = (
        db_session.query(Transaction)
        .options(
            joinedload(Transaction.orders),
            joinedload(Transaction.orders, Order.account),
            joinedload(Transaction.orders, Order.product),
        )
        .filter(Transaction.id.in_(in_query))
    )

    return (
        PaginatedSearch[Transaction, TransactionData](main_query, page_params)
        .enable_sorting(sort_params)
        .set_total_items(in_query_pagination.query.count())
        .execute_without_count()
    )


ProductSales = list[tuple[int, str, int]]


def get_transaction_product_count(
    db_session: Session, categories: list[int]
) -> ProductSales:
    current_date = datetime.now()

    product_stmt = (
        select(Product.id)
        .join(ProductCategory)
        .join(Category)
        .filter(
            Category.id.in_(categories),
        )
    )

    product_count = func.count(Order.product_id).label("product_count")

    stmt = (
        select(
            product_count,
            Product.name,
            Product.quantity,
        )
        .select_from(Order)
        .join(Transaction)
        .join(Product)
        .filter(
            Transaction.deleted_at.is_(None),
            Order._type == "product_purchase",
            Order.created_at.between(current_date - timedelta(days=7), current_date),
            Order.product_id.in_(product_stmt),
        )
        .group_by(Order.product_id, Product.name, Product.quantity)
        .order_by(product_count.desc(), Product.name)
    )
    return cast(ProductSales, db_session.execute(stmt).all())
