from sqlalchemy.orm import Session

from app.dependencies import UserRequestContext
from app.models import UserCondition


def update_conditions(db_session: Session, user_condition: UserCondition) -> None:
    db_session.add(user_condition)
    db_session.commit()


def find_accepted_condition_by_id(
    context: UserRequestContext,
) -> UserCondition | None:
    return (
        context.db_session.query(UserCondition)
        .filter(UserCondition.user_id == context.user.id)
        .one_or_none()
    )
