from datetime import date
from typing import Sequence
from sqlalchemy import delete


from app.domains.exact.comparison.bank_transaction import BankTransaction
from app.service import audit_service
from app.dependencies import UserRequestContext


def replace_bank_transactions(
    context: UserRequestContext,
    first_date: date,
    last_date: date,
    transactions: Sequence[BankTransaction],
) -> None:
    # Delete all so we can add without unique constraint trouble
    # FIXME: refactor this to use a proper upsert
    context.db_session.execute(
        delete(BankTransaction).filter(
            BankTransaction.date.between(first_date, last_date)
        ),
        execution_options={"synchronize_session": False},
    )

    context.db_session.add_all(transactions)

    audit_service.audit_create(context, "bank_transaction.upload")
    context.db_session.commit()
