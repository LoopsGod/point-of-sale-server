# The code in this file was adopted from:
# https://github.com/opensafely-core/databuilder/blob/4a49fc7c72fdf1d06ae75ad01ec91455ad22749c/databuilder/legacy_sqlalchemy_utils.py#L62
# LICENSE GNU GPL v3

from collections.abc import Iterable

from sqlalchemy import Table
from sqlalchemy.orm import Query
from sqlalchemy.sql.selectable import Join, Selectable
from sqlalchemy.sql.util import join_condition

from app.models._base import Base


def get_joined_tables(select_query: Selectable) -> list[Table]:
    """
    Given a SELECT query object return a list of all tables in its FROM clause
    """
    tables = []
    from_exprs = list(select_query.get_final_froms())  # type: ignore[attr-defined]
    while from_exprs:
        next_expr = from_exprs.pop()
        if isinstance(next_expr, Join):
            from_exprs.extend([next_expr.left, next_expr.right])
        else:
            tables.append(next_expr)
    # The above algorithm produces tables in right to left order, but it makes
    # more sense to return them as left to right
    tables.reverse()
    return tables


def get_primary_table(select_query: Selectable) -> Table | None:
    """
    Return the left-most table referenced in the SELECT query
    """
    tables = get_joined_tables(select_query)

    if not len(tables):
        return None

    return tables[0]


def include_joined_tables(select_query: Query, tables: Iterable[type[Base]]) -> Query:
    """
    Ensure that each table in `tables` is included in the join conditions for
    `select_query`
    """
    current_tables = get_joined_tables(select_query.selectable)
    for table in tables:
        if table.__table__ in current_tables:
            continue

        primary_table = get_primary_table(select_query.selectable)

        if primary_table is None:
            continue

        select_query = select_query.join(table, join_condition(primary_table, table))
        current_tables.append(table.__table__)
    return select_query
