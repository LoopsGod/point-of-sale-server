from collections.abc import Callable
from math import ceil
from typing import (
    Any,
    Generic,
    NamedTuple,
    TypeVar,
    cast,
)

from fastapi import Query as FapiQuery
from pydantic import BaseModel
from pydantic.fields import ModelField
from pydantic.generics import GenericModel
from pydantic.types import ConstrainedInt
from sqlalchemy import Column, or_, String, func
from sqlalchemy.orm import Query
from sqlalchemy.exc import ProgrammingError
from sqlalchemy.sql.operators import ColumnOperators
from sqlalchemy.sql.selectable import CompoundSelect, Select
from sqlalchemy.sql.elements import ClauseElement

from app.models._base import Base
from app.repository.util.filters import BaseFilter
from app.repository.util.sqlalchemy import include_joined_tables


class PageNumberInt(ConstrainedInt):
    gt = 0


class PageLimitInt(ConstrainedInt):
    gt = 0
    le = 50


class SortField(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def __modify_schema__(cls, field_schema: dict[str, Any], field: ModelField) -> None:
        allowed_columns = field.field_info.extra.get("allowed_columns", "")
        field_schema.update(
            title="Sort",
            allowed_columns=allowed_columns,
        )

    @property
    def columns(self) -> list[tuple[str, bool]]:
        if not self:
            return []

        result = []
        sort_list = self.split(",")

        for column in sort_list:
            column = column.strip()
            ascending = True

            if column.startswith("-"):
                column = column[1:]
                ascending = False

            result.append((column, ascending))

        return result

    @classmethod
    def validate(cls, sort: Any, field: ModelField) -> str:
        """Validates that the value is a comma-seperated list of columns.

        E.g. input can be 'price,-quantity' to sort by price, then quantity
        (descending)."""

        if not sort:
            return cls(field.field_info.default)

        if not isinstance(sort, str):
            raise TypeError("string required")

        allowed_sortable_columns = field.field_info.extra.get("allowed_columns", "")
        sort_list = sort.split(",")

        for column in sort_list:
            column = column.strip()

            if column.startswith("-"):
                column = column[1:]

            if column not in allowed_sortable_columns:
                raise ValueError(f"Invalid sorting key '{column}'.")

        return cls(sort)

    def __repr__(self):
        return f"SortField({super().__repr__()})"


def page_sort_parameters(
    *allowed_sorting_columns: str, default_sort: str = None
) -> Callable:
    if not len(allowed_sorting_columns):
        raise IndexError("'allowed_sorting_columns' must contain items. ")

    if not default_sort:
        default_sort = allowed_sorting_columns[0]

    def _page_sort_parameters(
        sort: SortField = FapiQuery(
            SortField(default_sort),  # noqa: B008
            allowed_columns=allowed_sorting_columns,
        )
    ) -> SortParameters:
        #  `construct` to skip validation, since FastAPI already does that for us.
        return SortParameters.construct(sort=sort)

    return _page_sort_parameters


class FilterAndJoinCondition(NamedTuple):
    filter: BaseFilter | None
    join_table: type[Base]
    join_condition: ClauseElement | None


class PageParameters(BaseModel):
    page: PageNumberInt = PageNumberInt(1)
    perPage: PageLimitInt = PageLimitInt(15)


class SearchParameters(BaseModel):
    search = ""


class SortParameters(BaseModel):
    sort: SortField


ItemT = TypeVar("ItemT", bound=BaseModel)


class PaginatedResponse(GenericModel, Generic[ItemT]):
    page: int
    perPage: int
    pageCount: int
    items: list[ItemT]


ModelT = TypeVar("ModelT")
ResponseT = TypeVar("ResponseT", bound=BaseModel)

_PaginatedSearchSelf = TypeVar("_PaginatedSearchSelf", bound="PaginatedSearch")
_DoSortGeneric = TypeVar("_DoSortGeneric", Query, Select, CompoundSelect)

ColumnUnion = Column | ColumnOperators


class PaginatedSearch(Generic[ModelT, ResponseT]):
    query: Query

    page = 0
    total = 0
    per_page = 0
    page_count = 0
    items: list[ModelT]

    # TODO(Wilco): _to_join is currently not supported for sub queries.
    # Meaning columns need to be joined manually.
    _tables_to_join: set[type[Base]]

    sort_params: SortParameters
    sorting_column_definition: dict[str, ColumnUnion | tuple[ColumnUnion, type[Base]]]

    def __init__(self, query: Query, page_params: PageParameters) -> None:
        # TODO Can we type this better? The query can return many different froms?
        # Perhaps only with SQLA 2.0 with full types?
        self._mapper: Callable[..., ResponseT] = self._base_model_mapper
        self.query = query

        self.per_page = page_params.perPage
        self.page = page_params.page

        self.items = []
        self._tables_to_join = set()

    def _base_model_mapper(self, x: Any) -> ResponseT:
        if isinstance(x, Base):
            return x.to_pydantic()  # type: ignore[return-value]
        raise TypeError(
            f"Expected query result to be a subclass of {Base}. "
            f"Use `{PaginatedSearch.set_result_mapper.__name__}` to define a mapper."
        )

    def enable_searching(
        self: _PaginatedSearchSelf,
        search_params: SearchParameters,
        searchable_columns: list[ColumnUnion | tuple[ColumnUnion, type[Base]]] = None,
    ) -> "_PaginatedSearchSelf":
        """Enables sorting.

        If only sorting on columns in the current table, a simple list of
        columns can be passed, if a table needs to be join tuples can be
        given.
            .enable_searching(search_params, [Account.name])
            #  Or when a join on `User` is required:
            .enable_searching(search_params, [(User.name, User)])
        """
        if search_params.search == "":
            return self

        if not searchable_columns or not len(searchable_columns):
            return self

        filters = []
        for searchable_column in searchable_columns:
            if type(searchable_column) is tuple:
                column, to_join = searchable_column
                self._tables_to_join.add(to_join)
            else:
                column = cast(Column, searchable_column)

            filters.append(column.ilike(f"%{search_params.search}%"))

        self.query = self.query.filter(or_(*filters))

        return self

    def enable_filtering(
        self: _PaginatedSearchSelf,
        filterable_columns: dict[
            ColumnUnion,
            BaseFilter | FilterAndJoinCondition | None,
        ],
    ) -> _PaginatedSearchSelf:
        """Enables filtering on this paginated search query.

        Filters can be defined as a parameter on a route by assigning
        a field that is a child of BaseFilter. E.g.
            #  Optional
            price_filter: Optional[NumberFilter] = Query(None), alias="priceFilter")
            #  Or
            price_filter: NumberFilter = Query(..., alias="priceFilter")

        Next, the filter needs to be applied on the PaginatedSearch object by
        setting the filterable_columns argument. This tells the query which
        column we want to filter. E.g.
            filterable_columns = {
                Product.price: price_filter
                #  if a column needs to be joined:
                User.name: (some_filter, User)
            }
        """
        for (
            filterable_column,
            filter_or_filter_and_join,
        ) in filterable_columns.items():
            if filter_or_filter_and_join is None:
                continue

            if isinstance(filter_or_filter_and_join, tuple):
                filter, join_table, join_condition = filter_or_filter_and_join

                if not filter:
                    continue

                if join_condition is None:
                    self._tables_to_join.add(join_table)
                else:
                    self.query = self.query.join(join_table, join_condition)
            else:
                filter = filter_or_filter_and_join

                if filter is None:
                    continue

            self.query = filter.filter(self.query, cast(Column, filterable_column))

        return self

    def enable_sorting(
        self: _PaginatedSearchSelf,
        sort_params: SortParameters,
        sorting_column_definition: dict[
            str, ColumnUnion | tuple[ColumnUnion, type[Base]]
        ] = None,
    ) -> _PaginatedSearchSelf:
        """Enables sorting on this paginated search query.

        Columns that are allowed to be sorted are automatically inferred from
        `sort_params`. Additionally, `sorting_column_definition` can be used
        to provide custom sorting for specific keys. E.g
            sorting_column_definition = {
                "name": Product.name,
                "price": Product.price,
                "quantity": Product.quantity,

                #  if a column needs to be joined:
                "username": (User.name, User)
            }
        """
        self.sort_params = sort_params

        if sorting_column_definition:
            self.sorting_column_definition = sorting_column_definition
        else:
            self.sorting_column_definition = {}

        self.query = self._do_sort(self.query)

        return self

    def _do_sort(self, query: _DoSortGeneric) -> _DoSortGeneric:
        if not self.sort_params.sort:
            return query

        sorting_columns = self.sort_params.sort.columns
        model_columns = self.query.statement.selected_columns

        # If we use db.Query(<Not a model>), (i.e. select_from is used) the above
        # selected_columns won't work.
        froms = self.query.selectable.get_final_froms()  # type: ignore
        if froms and len(froms):
            model_colunns_alternative = froms[0].columns
        else:
            model_colunns_alternative = {}

        for column, ascending in sorting_columns:
            #  First check if a custom definition for sorting this column has been given
            column_definition = self.sorting_column_definition.get(column)
            model_column = None

            if type(column_definition) is tuple:
                model_column, to_join = column_definition
                self._tables_to_join.add(to_join)
            elif column_definition is not None:
                model_column = cast(ColumnUnion, column_definition)

            if model_column is None:
                #  If no definition is given, infer it from the model
                model_column = model_columns.get(column)

            if model_column is None:
                model_column = model_colunns_alternative.get(column)

            if model_column is None:
                raise ValueError(
                    f"Cannot sort by '{column}', it is not a property of the model."
                )

            if not ascending:
                model_column = model_column.desc()

            if isinstance(model_column.type, String):  # type: ignore
                model_column = func.lower(model_column)

            query = query.order_by(model_column)

        return query

    def set_result_mapper(
        self: _PaginatedSearchSelf, mapper: Callable[..., ResponseT]
    ) -> _PaginatedSearchSelf:
        self._mapper = mapper
        return self

    def _empty_response(self) -> PaginatedResponse[ResponseT]:
        return PaginatedResponse[ResponseT](
            page=1,
            perPage=self.per_page,
            pageCount=0,
            items=[],
        )

    def set_total_items(
        self: _PaginatedSearchSelf, total_items: int
    ) -> _PaginatedSearchSelf:
        self.page_count = int(ceil(total_items / float(self.per_page)))
        return self

    def execute_without_count(self) -> PaginatedResponse[ResponseT]:
        self.query = include_joined_tables(self.query, self._tables_to_join)

        try:
            self.items = self.query.all()
        except ProgrammingError as e:
            raise ValueError("Programming Error during filtering: ", e.orig) from e

        return PaginatedResponse[ResponseT](
            page=self.page,
            perPage=self.per_page,
            pageCount=self.page_count,
            items=[self._mapper(item) for item in self.items],
        )

    def execute(self) -> PaginatedResponse[ResponseT]:
        try:
            self.total = self.query.order_by(None).count()
        except ProgrammingError as e:
            raise ValueError("Programming Error during filtering: ", e.orig) from e

        self.page_count = int(ceil(self.total / float(self.per_page)))

        self.query = include_joined_tables(self.query, self._tables_to_join)

        if self.page < 1:
            return self._empty_response()

        try:
            self.items = (
                self.query.limit(self.per_page)
                .offset((self.page - 1) * self.per_page)
                .all()
            )
        except ProgrammingError as e:
            raise ValueError("Programming Error during filtering: ", e.orig) from e

        return PaginatedResponse[ResponseT](
            page=self.page,
            perPage=self.per_page,
            pageCount=self.page_count,
            items=[self._mapper(item) for item in self.items],
        )
