import json
import re
from abc import ABC, abstractmethod
from collections.abc import Callable, Generator
from datetime import date, datetime, timedelta, timezone
from operator import ge, le, eq
from re import Pattern
from typing import Any, TypeVar

from dateutil.parser import parse as parse_iso8601
from sqlalchemy import Column, Date, and_, cast, Text, Time
from sqlalchemy.orm import Query
from sqlalchemy.sql.util import join_condition
from sqlalchemy.sql.selectable import Select
from sqlalchemy.sql.elements import ColumnElement

from app.repository.util.sqlalchemy import get_primary_table

FilterGeneric = TypeVar("FilterGeneric", Query, Select)


class BaseFilter(ABC):
    examples: None | str | list[str] = None

    @abstractmethod
    def filter(
        self, query: FilterGeneric, column: Column | ColumnElement
    ) -> FilterGeneric:
        pass

    @classmethod
    def __get_validators__(cls) -> Generator[Callable[..., Any], None, None]:
        yield cls.validate

    @classmethod
    def validate(cls, v: Any) -> Any:
        return v

    @classmethod
    def __modify_schema__(cls, field_schema: dict[str, Any]) -> None:
        field_schema.update(title=f"{cls.__name__}", examples=cls.examples)

    def __repr__(self):
        return self.__class__.__name__


class BooleanFilter(BaseFilter, str):
    valid_true_values = ["true", "yes", "1"]
    valid_false_values = ["false", "no", "0"]

    examples = valid_true_values + valid_false_values

    def filter(
        self, query: FilterGeneric, column: Column | ColumnElement
    ) -> FilterGeneric:
        return query.filter(column.is_(self.as_bool))

    @property
    def as_bool(self) -> bool:
        if self in BooleanFilter.valid_true_values:
            return True

        return False

    @classmethod
    def __get_validators__(cls) -> Generator[Callable[..., Any], None, None]:
        yield cls.validate

    @classmethod
    def __modify_schema__(cls, field_schema: dict[str, Any]) -> None:
        super().__modify_schema__(field_schema)

        field_schema.update(type="boolean")

    @classmethod
    def validate(cls, v):
        if not isinstance(v, str):
            raise TypeError("String required.")

        normalized = v.strip().lower()

        if normalized not in (cls.valid_true_values + cls.valid_false_values):
            raise TypeError("Invalid boolean value.")

        return cls(normalized)


class NullFilter(BooleanFilter):
    def filter(
        self, query: FilterGeneric, column: Column | ColumnElement
    ) -> FilterGeneric:
        if self.as_bool is True:
            return query.filter(column.isnot(None))
        else:
            return query.filter(column.is_(None))


class NumberFilter(BaseFilter, str):
    examples = ["=102.3", "=100", ">=100", "<=100", ">=50, <=100"]
    regex: Pattern[str] = re.compile(r"^([><]=|=)(-?\d+\.?\d*)$")

    _ops: dict[str, Callable] = {
        "=": eq,
        ">=": ge,
        "<=": le,
    }

    def filter(
        self, query: FilterGeneric, column: Column | ColumnElement
    ) -> FilterGeneric:
        for op, num in self.expressions:
            query = query.filter(op(column, num))

        return query

    @property
    def expressions(self) -> list[tuple[Callable, float]]:
        result: list[tuple[Callable, float]] = []
        for expr in self.split(","):
            m = NumberFilter.regex.match(expr.strip())

            if not m:
                #  This should not happen, as we already validated.
                raise ValueError(f"Invalid expression '{expr}' for NumberFilter.")

            op_str, num = m.groups()

            op = self._ops.get(op_str, None)
            if not op:
                raise ValueError(f"Invalid expression '{expr}' for NumberFilter.")

            result.append((op, float(num)))

        return result

    @classmethod
    def validate(cls, v):
        if not isinstance(v, str):
            raise TypeError("string required")

        expressions = v.split(",")

        for expr in expressions:
            m = cls.regex.match(expr.strip())

            if not m:
                raise ValueError(f"Invalid expression '{expr}' for NumberFilter.")

        return cls(v)


class LastUsageFilter(NumberFilter):
    examples = ["=100", ">=100", "<=100", ">=50, <=100"]
    _ops: dict[str, Callable] = {
        "=": eq,
        ">=": le,
        "<=": ge,
    }

    def filter(
        self, query: FilterGeneric, column: Column | ColumnElement
    ) -> FilterGeneric:
        if not issubclass(column.type.python_type, datetime):
            raise TypeError("Last usage filter must be used on date-like column")
        now = datetime.now(timezone.utc)
        for op, num in self.expressions:
            query = query.filter(op(column, now - timedelta(days=int(num))))
        return query


class DateTimeFilter(BaseFilter):
    examples = [
        "2022-10-01T13:37:28.101Z",
        "2022-10-01T13:37:28.101Z~2022-10-01T14:37:28.101Z",
        "13:37:28.101Z~14:37:28.101Z",
    ]

    _default = datetime(1, 1, 1)

    def __init__(self, start: datetime, end: datetime = None) -> None:
        super().__init__()
        self.start = start
        self.end = end

    @classmethod
    def validate(cls, v):
        if not isinstance(v, str):
            raise TypeError("string required")

        if "~" in v:
            start, end = v.split("~")

            start_datetime = parse_iso8601(start, default=cls._default)
            end_datetime = parse_iso8601(end, default=cls._default)
            return cls(start_datetime, end_datetime)

        start_datetime = parse_iso8601(v, default=cls._default)
        return cls(start_datetime)

    def filter(
        self, query: FilterGeneric, column: Column | ColumnElement
    ) -> FilterGeneric:
        if not issubclass(column.type.python_type, date):
            raise TypeError("DateTimeFilter must be used on date-like column")

        date_only = not issubclass(column.type.python_type, datetime)
        time_only = self.start.date() == self._default.date()

        if self.end:
            time_only = time_only and self.end.date() == self._default.date()

        if date_only and time_only:
            raise TypeError(
                "DateTimeFilter cannot filter on time for a column of type date "
                "(datetime required)."
            )

        if date_only:
            query = query.filter(cast(column, Date) >= self.start.date())
            if self.end:
                query = query.filter(cast(column, Date) <= self.end.date())

            return query

        if time_only:
            query = query.filter(cast(column, Time) >= self.start.time())
            if self.end:
                query = query.filter(cast(column, Time) <= self.end.time())

            return query

        query = query.filter(column >= self.start)
        if self.end:
            query = query.filter(column <= self.end)

        return query


class ForeignKeyFilter(BaseFilter, str):
    examples = ["5", "5,6", "none"]

    def filter(
        self, query: FilterGeneric, column: Column | ColumnElement
    ) -> FilterGeneric:
        from_table = get_primary_table(query.selectable)

        if from_table is None:
            raise TypeError("Could not find primary table for ForeignKeyFilter.")

        join_table = column.parent.persist_selectable
        join_cond = join_condition(join_table, from_table)

        if len(self.foreign_ids):
            return query.join(
                join_table,
                and_(
                    join_cond,
                    column.in_(self.foreign_ids),
                ),
            )
        else:
            #  No ids means we are searching for rows without relationship.
            return query.outerjoin(join_table, join_cond).where(column.is_(None))

    @property
    def foreign_ids(self) -> list[int]:
        if self.lower().strip() == "none":
            return []

        return [int(foreign_id) for foreign_id in self.split(",")]

    @classmethod
    def validate(cls, v):
        if not isinstance(v, str):
            raise TypeError("string required")

        if v.lower().strip() == "none":
            return cls(v)

        foreign_ids = v.split(",")

        if not len(foreign_ids):
            raise ValueError("Expected at least one id or 'none'.")

        for foreign_id in foreign_ids:
            try:
                int(foreign_id)
            except ValueError as e:
                raise ValueError(
                    f"Non-int foreign key ids are not supported (id='{foreign_id}'."
                ) from e

        return cls(v)


class LiteralEqualsFilter(BaseFilter):
    max_length = 1000
    examples = ["5", "True", "'some string'", "None", "5.5"]

    def __init__(self, v: type[None] | int | float | str | bool | list) -> None:
        super().__init__()
        self.value = v

    def filter(
        self, query: FilterGeneric, column: Column | ColumnElement
    ) -> FilterGeneric:
        if self.value is None:
            return query.filter(column.is_(None))
        elif type(self.value) is str:
            #  Be lenient with string compares, this allows to compare to e.g. an enum
            return query.filter(cast(column, Text) == self.value)
        elif type(self.value) is list and type(self.value[0]) is str:
            #  Do the same for list of strings
            return query.filter(cast(column, Text).in_(self.value))
        elif type(self.value) is list:
            return query.filter(column.in_(self.value))
        else:
            return query.filter(column == self.value)

    @classmethod
    def validate(cls, v):
        if not isinstance(v, str):
            raise TypeError("String required.")

        if len(v) > cls.max_length:
            raise TypeError("String too long.")

        try:
            parsed_literal = json.loads(v)
        except SyntaxError as e:
            raise TypeError("Could not parse literal.") from e

        if type(parsed_literal) is list and not len(parsed_literal):
            raise TypeError("List literal cannot be empty")

        return cls(parsed_literal)
