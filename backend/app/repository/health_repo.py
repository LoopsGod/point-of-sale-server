import logging

from sqlalchemy import true, select
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import Session

_logger = logging.getLogger(__name__)


def get_database_status(db_session: Session) -> bool:
    try:
        return db_session.scalar(select(true()))
    except SQLAlchemyError as e:
        _logger.exception("Database health check failed: %s", e)
        return False
