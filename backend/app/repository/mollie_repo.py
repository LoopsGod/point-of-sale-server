from sqlalchemy.orm import Session

from app.models import IdealPayment
from app.models.ideal_payment import IdealPaymentData
from app.repository.util.filters import (
    LiteralEqualsFilter,
    DateTimeFilter,
)
from app.repository.util.pagination import (
    PageParameters,
    PaginatedSearch,
    PaginatedResponse,
    SearchParameters,
    SortParameters,
)
from app.views.exceptions import NotFoundException


def flush(db_session: Session, payment: IdealPayment) -> None:
    db_session.add(payment)
    db_session.flush()


def save(db_session: Session, payment: IdealPayment) -> None:
    db_session.add(payment)
    db_session.commit()


def get_by_mollie_id(db_session: Session, mollie_id: str) -> IdealPayment | None:
    return (
        db_session.query(IdealPayment)
        .filter(IdealPayment.mollie_id == mollie_id)
        .one_or_none()
    )


def get_by_id(db_session: Session, payment_id: int) -> IdealPayment:
    payment = (
        db_session.query(IdealPayment)
        .filter(IdealPayment.id == payment_id)
        .one_or_none()
    )
    if not payment:
        raise NotFoundException("payment_unknown")
    return payment


def paginated_search_all(
    db_session: Session,
    page_params: PageParameters,
    search_params: SearchParameters,
    sort_params: SortParameters,
    id_filter: LiteralEqualsFilter = None,
    created_at_filter: DateTimeFilter = None,
    updated_at_filter: DateTimeFilter = None,
    account_filter: LiteralEqualsFilter = None,
) -> PaginatedResponse[IdealPaymentData]:
    q = db_session.query(IdealPayment)

    return (
        PaginatedSearch[IdealPayment, IdealPaymentData](q, page_params)
        .enable_searching(search_params, [IdealPayment.mollie_id, IdealPayment.status])
        .enable_sorting(
            sort_params,
            sorting_column_definition={
                "createdAt": IdealPayment.created_at,
                "updatedAt": IdealPayment.updated_at,
                "status": IdealPayment.status,
            },
        )
        .enable_filtering(
            {
                IdealPayment.id: id_filter,
                IdealPayment.created_at: created_at_filter,
                IdealPayment.updated_at: updated_at_filter,
                IdealPayment.account_id: account_filter,
            }
        )
        .execute()
    )
