from datetime import date
from typing import NamedTuple, cast

from sqlalchemy import and_, case, func, select, Date, text
from sqlalchemy.orm.session import Session
from sqlalchemy.sql.selectable import CTE

from app.models import Account, Product, ProductData
from app.models.category import ProductCategory
from app.repository.util.filters import (
    BooleanFilter,
    ForeignKeyFilter,
    LiteralEqualsFilter,
    NullFilter,
    NumberFilter,
)
from app.repository.util.pagination import (
    PageParameters,
    SortParameters,
    PaginatedResponse,
    PaginatedSearch,
    SearchParameters,
)
from app.models.order import Order


def get_by_id(db_session: Session, product_id: int) -> Product | None:
    return db_session.query(Product).filter_by(id=product_id).one_or_none()


class ProductFilters:
    priceFilter: NumberFilter | None
    depositFilter: BooleanFilter | None


def paginated_search_all(
    db_session: Session,
    page_params: PageParameters,
    search_params: SearchParameters,
    sort_params: SortParameters,
    price_filter: NumberFilter = None,
    deposit_filter: BooleanFilter = None,
    quantity_filter: NumberFilter = None,
    category_filter: ForeignKeyFilter = None,
    ledger_filter: ForeignKeyFilter = None,
    slide_filter: BooleanFilter = None,
    deleted_filter: NullFilter = None,
    id_filter: LiteralEqualsFilter = None,
) -> PaginatedResponse[ProductData]:
    q = db_session.query(Product)

    return (
        PaginatedSearch[Product, ProductData](q, page_params)
        .enable_searching(search_params, [Product.name])
        .enable_sorting(sort_params, {"ledger": (Account.name, Account)})
        .enable_filtering(
            {
                Product.price: price_filter,
                Product.deposit: deposit_filter,
                Product.quantity: quantity_filter,
                ProductCategory.category_id: category_filter,
                Account.id: ledger_filter,
                Product.deleted_at: deleted_filter,
                Product.id: id_filter,
            }
        )
        .execute()
    )


class ProductCountAtDate(NamedTuple):
    date: date
    purchases: int
    cum_purchases: int
    comparison_purchases: int
    comparison_cum_purchases: int


def _sum_of_purchases_per_day_in_year(product_id: int, year: int) -> CTE:
    first_day_of_year = date(year, 1, 1)

    date_col = func.cast(Order.created_at, Date).label("date")
    return (
        select(
            date_col,
            #  Only count purchases that are not deleted
            func.sum(case((Order.deleted_at.is_(None), 1), else_=-1)).label(
                "purchases"
            ),
        )
        .where(
            and_(
                Order.product_id == product_id,
                #  Simply checks if the purchase happened in the given year
                func.date_trunc("year", Order.created_at) == first_day_of_year,
            )
        )
        .group_by(Order.product_id, date_col)
        .order_by(date_col.desc())
    ).cte()


def count_per_year(
    db_session: Session,
    product_id: int,
    year: int,
    comparison_year: int | None,
) -> list[ProductCountAtDate]:
    #  The first CTE in this function simply selects all days in 2004. Two other CTEs
    #  sums all purchases of the given product per day. Then the
    #  queries are joined and a window funcion is used for the cumulative sum.

    if comparison_year is None:
        comparison_year = year - 1

    #  All dates from 01/01/2004 to 31/12/2004 (incl. 29/02 since it's a leap year)
    leap_year = 2004
    date_list = select(
        func.cast(
            func.generate_series(
                date(leap_year, 1, 1), date(leap_year, 12, 31), "1 day"
            ),
            Date,
        ).label("date")
    ).cte()

    year_difference_for_given_year = text(f"'{year - leap_year} years'::interval")
    purchases_per_day_given_year = _sum_of_purchases_per_day_in_year(product_id, year)

    year_difference_for_comparison_year = text(
        f"'{comparison_year - leap_year} years'::interval"
    )
    purchases_per_day_comparison_year = _sum_of_purchases_per_day_in_year(
        product_id, comparison_year
    )

    joined_stmt = (
        select(
            date_list.c.date,
            func.coalesce(purchases_per_day_given_year.c.purchases, 0).label(
                "purchases"
            ),
            func.coalesce(purchases_per_day_comparison_year.c.purchases, 0).label(
                "comparison_purchases"
            ),
            func.sum(func.coalesce(purchases_per_day_given_year.c.purchases, 0))
            .over(order_by=date_list.c.date.asc())
            .label("cum_purchases"),
            func.sum(func.coalesce(purchases_per_day_comparison_year.c.purchases, 0))
            .over(order_by=date_list.c.date.asc())
            .label("comparison_cum_purchases"),
        )
        .join(
            purchases_per_day_given_year,
            date_list.c.date + year_difference_for_given_year
            == purchases_per_day_given_year.c.date,
            isouter=True,
        )
        .join(
            purchases_per_day_comparison_year,
            date_list.c.date + year_difference_for_comparison_year
            == purchases_per_day_comparison_year.c.date,
            isouter=True,
        )
    )

    return cast(list[ProductCountAtDate], db_session.execute(joined_stmt).all())
