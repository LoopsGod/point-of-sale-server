from sqlalchemy import select
from sqlalchemy.orm import Session, joinedload

from app.models import User
from app.models.checkout_card import CheckoutCard


def get_user_for_card_secret(db_session: Session, card_secret: str) -> User | None:
    return db_session.scalar(
        select(User)
        .select_from(CheckoutCard)
        .join(User)
        .filter(CheckoutCard.card_secret == card_secret)
        .options(joinedload(User.account))
    )


def find_card_by_secret(db_session: Session, card_secret: str) -> CheckoutCard | None:
    return db_session.scalar(
        select(CheckoutCard).filter(CheckoutCard.card_secret == card_secret)
    )
