from datetime import datetime

from sqlalchemy import func, and_, select
from sqlalchemy.orm import Session

from app.models import (
    SepaBatch,
    SepaBatchOrder,
    Account,
    User,
    UserDirectDebit,
)
from app.models.account_type import AccountType
from app.models.sepa_batch import SepaBatchData
from app.repository.util.filters import BooleanFilter, LiteralEqualsFilter
from app.repository.util.pagination import (
    PageParameters,
    SearchParameters,
    SortParameters,
    PaginatedSearch,
    PaginatedResponse,
)

# Convenience type
SepaUpgradeDetails = tuple[SepaBatchOrder, Account, User, UserDirectDebit]


def get_pending_orders_with_user_accounts(
    db_session: Session, batch: SepaBatch
) -> list[SepaUpgradeDetails]:
    return (
        db_session.query(SepaBatchOrder, Account, User, UserDirectDebit)
        .join(Account, SepaBatchOrder.account_id == Account.id)
        .join(User, Account.id == User.account_id)
        .join(UserDirectDebit, UserDirectDebit.user_id == User.id)
        .filter(
            SepaBatchOrder.sepa_batch_id == batch.id,
        )
        .all()
    )


def get_in_date_range(
    db_session: Session,
    date_start: datetime | None,
    date_end: datetime | None,
    executed: bool = None,
) -> list[SepaBatch]:
    q = db_session.query(SepaBatch).filter(SepaBatch.deleted_at.is_(None))
    if date_start:
        q = q.filter(SepaBatch.execution_date >= date_start)
    if date_end:
        q = q.filter(SepaBatch.execution_date <= date_end)
    if executed is not None:
        q = q.filter(SepaBatch.success == executed)
    return q.all()


def flush(db_session: Session, batch: SepaBatch) -> SepaBatch:
    db_session.add(batch)
    db_session.flush()

    return batch


def save(db_session: Session, batch: SepaBatch | SepaBatchOrder) -> None:
    db_session.add(batch)
    db_session.commit()


def get_pending_batch_for_account(
    db_session: Session,
    account_id: int,
) -> tuple[SepaBatch | None, SepaBatchOrder | None]:
    query = (
        db_session.query(SepaBatch, SepaBatchOrder)
        .join(SepaBatchOrder)
        .filter(
            SepaBatch.success.is_(False),
            SepaBatch.deleted_at.is_(None),
            SepaBatchOrder.account_id == account_id,
        )
        .first()
    )

    if query is not None:
        batch, order = query
        return batch, order
    else:
        return None, None


def get_total_debt_for_sepa_batch(db_session: Session) -> int:
    query = (
        select(func.coalesce(func.sum(Account.balance), 0))
        .select_from(Account)
        .filter(
            and_(
                Account.balance < 0,
                Account.type == AccountType.USER.value,
            )
        )
    )
    return db_session.execute(query).scalar_one()


def find_pending_order(
    db_session: Session, batch: SepaBatch, account_id: int
) -> SepaBatchOrder | None:
    return (
        db_session.query(SepaBatchOrder)
        .filter(
            SepaBatchOrder.sepa_batch_id == batch.id,
            SepaBatchOrder.deleted_at.is_(None),
            SepaBatchOrder.account_id == account_id,
        )
        .one_or_none()
    )


def get_batches(db_session: Session) -> list[SepaBatch]:
    return db_session.scalars(
        select(SepaBatch).order_by(
            SepaBatch.execution_date.desc(), SepaBatch.description
        )
    ).all()


def find_batch_by_id(db_session: Session, batch_id: int) -> SepaBatch | None:
    return db_session.get(SepaBatch, batch_id)


def paginated_search(
    db_session: Session,
    page_params: PageParameters,
    search_params: SearchParameters,
    sort_params: SortParameters,
    id_filter: LiteralEqualsFilter = None,
    success_filter: BooleanFilter = None,
) -> PaginatedResponse[SepaBatchData]:
    q = db_session.query(SepaBatch)
    return (
        PaginatedSearch[SepaBatch, SepaBatchData](q, page_params)
        .enable_searching(search_params, [SepaBatch.description])
        .enable_sorting(
            sort_params,
            sorting_column_definition={
                "execution_date": SepaBatch.execution_date,
                "deleted": SepaBatch.deleted_at,
                "description": func.lower(SepaBatch.description),
            },
        )
        .enable_filtering(
            {
                SepaBatch.id: id_filter,
                SepaBatch.success: success_filter,
            }
        )
        .execute()
    )
