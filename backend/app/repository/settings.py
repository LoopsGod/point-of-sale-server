import json
from typing import Any

from sqlalchemy import delete, select
from sqlalchemy.orm import Session
from sqlalchemy.dialects.postgresql import insert

from app.models.setting import Setting, DatabaseSettings


def create_setting() -> Setting:
    return Setting()


def find_by_key(db_session: Session, key: str) -> Setting | None:
    return db_session.query(Setting).filter_by(key=key.upper()).one_or_none()


def update_settings(db_session: Session, values: dict[str, Any]) -> None:
    for key, value in values.items():
        setting = find_by_key(db_session, key)
        if setting is not None:
            setting.value = str(value)
        else:
            setting = Setting()
            setting.key = key.upper()
            setting.value = str(value)
            db_session.add(setting)
    db_session.commit()


def get_db_settings(db_session: Session) -> DatabaseSettings:
    settings_dict = {
        s.key.lower(): s.value for s in db_session.scalars(select(Setting))
    }
    return DatabaseSettings(**settings_dict)


def save_db_settings(
    db_session: Session, database_settings: DatabaseSettings
) -> DatabaseSettings:
    insert_values: list[dict[str, Any]] = []
    delete_keys: list[str] = []

    for key, value in database_settings.dict().items():
        if value is None:
            delete_keys.append(key.upper())
            continue

        if isinstance(value, list) or isinstance(value, dict):
            value = json.dumps(value)

        insert_values.append({"key": key.upper(), "value": value})

    insert_stmt = insert(Setting)
    db_session.execute(
        insert_stmt.on_conflict_do_update(
            constraint="setting_key_key", set_={"value": insert_stmt.excluded.value}
        ),
        insert_values,
    )

    if delete_keys:
        delete_stmt = delete(Setting).where(Setting.key.in_(delete_keys))
        db_session.execute(delete_stmt)

    db_session.commit()

    return database_settings
