import logging
from calendar import monthrange
from datetime import datetime, date

import pydantic
from pydantic import BaseModel

_logger = logging.getLogger(__name__)


class DateRangeParameters(BaseModel):
    start: date
    end: date


class DatetimeRangeParameters(BaseModel):
    start: datetime
    end: datetime


def get_last_moment_of_day(dt: datetime) -> datetime:
    return datetime.combine(dt.date(), datetime.max.time())


def get_last_day_of_month(year: int, month: int) -> int:
    return monthrange(year, month)[1]


def format_iban(iban: str) -> str:
    iban = iban.replace(" ", "")
    return " ".join([iban[i : i + 4] for i in range(0, len(iban), 4)])


class PinStr(pydantic.ConstrainedStr):
    min_length = 4
    max_length = 4
