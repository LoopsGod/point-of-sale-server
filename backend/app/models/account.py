"""
Tab with its owner's name, hashed PIN, balance and banking information, if any.
"""
import enum
from datetime import datetime
from typing import Any, Literal, Optional

import pydantic
from sqlalchemy import (
    Integer,
    String,
    ForeignKey,
    CheckConstraint,
    Column,
    Boolean,
)
from sqlalchemy.orm import Mapped, relationship

from app.models.order import Order
from app.models.user import User
from app.models.checkout import CheckoutMethod, CheckoutMethodSQL
from ._base import Base
from .account_type import AccountType, AccountTypeSQL
from .database import Model


class AccountData(pydantic.BaseModel):
    id: int
    createdAt: datetime
    updatedAt: datetime
    name: str
    description: str | None
    balance: int
    type: str
    allowNegativeBalance: bool
    pinSet: bool
    deleted: bool


# Exists to keep backward compatibility with APIs that are used in the front-end
class DirectDebitAccountData(AccountData):
    directDebit: bool
    iban: str | None
    bic: str | None
    mandateId: str | None
    signDate: str | None


class Account(Model, Base):
    """
    Tab with its owner's name, hashed PIN, balance and banking information, if
    any.
    """

    __tablename__ = "account"

    name: Mapped[str] = Column(String(64))  # type: ignore[assignment]
    pin: Mapped[str | None] = Column(String(64), nullable=True)  # type: ignore[assignment]  # noqa: E501
    balance: Mapped[int] = Column(Integer, default=0)  # type: ignore[assignment]  # noqa: E501

    type: Mapped[AccountType] = Column(  # type: ignore[assignment]
        AccountTypeSQL,
        nullable=False,
        server_default=AccountType.USER.value,
    )

    allow_negative_balance: Mapped[bool] = Column(
        Boolean, default=False, nullable=False
    )  # type: ignore[assignment]

    exact_ledger: Mapped[Optional["AccountExactLedger"]] = relationship(  # type: ignore[assignment]  # noqa: E501
        "AccountExactLedger", uselist=False, viewonly=True
    )
    description: Mapped[str | None] = Column(String(128), nullable=True)  # type: ignore[assignment]  # noqa: E501

    orders: Mapped[list[Order]] = relationship("Order")  # type: ignore[assignment]  # noqa: E501
    user: Mapped[User | None] = relationship(  # type: ignore[assignment]
        "User",
        back_populates="account",
        uselist=False,
    )

    def to_pydantic(self) -> AccountData:
        return AccountData(
            id=self.id,
            createdAt=self.created_at.astimezone(),
            updatedAt=self.updated_at.astimezone(),
            name=self.name,
            description=self.description,
            balance=self.balance,
            type=self.type.value,
            allowNegativeBalance=self.allow_negative_balance,
            pinSet=self.pin is not None,
            deleted=self.deleted,
        )

    def serialize(self) -> dict[str, Any]:
        return {
            "id": self.id,
            "name": self.name,
            "description": self.description,
            "balance": self.balance,
            "type": self.type,
            "allowNegativeBalance": self.allow_negative_balance,
            "pinSet": self.pin is not None,
            "deleted": self.deleted,
        }


class AccountExactLedger(Model, Base):
    __tablename__ = "account_exact_ledger"

    __table_args__ = (
        CheckConstraint("exact_ledger_code <> ''", name="ledger_not_empty_string"),
        CheckConstraint(
            "exact_cost_center_code <> ''", name="cost_center_not_empty_string"
        ),
        CheckConstraint("exact_vat_code <> ''", name="vat_code_not_empty_string"),
    )
    account_id: Mapped[int] = Column(
        Integer, ForeignKey("account.id"), nullable=False
    )  # type: ignore[assignment]
    account: Mapped[Account] = relationship("Account")  # type: ignore[assignment]  # noqa: E501

    exact_ledger_code: Mapped[str] = Column(String(64), nullable=False)  # type: ignore[assignment]  # noqa: E501
    exact_cost_center_code: Mapped[str | None] = Column(
        String(64),
        nullable=True,
    )  # type: ignore[assignment]
    exact_vat_code: Mapped[str | None] = Column(String(64), nullable=True)  # type: ignore[assignment]  # noqa: E501
    associated_checkout_method: Mapped[CheckoutMethod | None] = Column(
        CheckoutMethodSQL, nullable=True
    )  # type: ignore[assignment]


class AccountRequestStatus(enum.IntEnum):
    PENDING = 0
    APPROVED = 1
    DECLINED = 2


AccountRequestStatusType = Literal["pending", "approved", "declined"]


class SerializedAccountRequest(pydantic.BaseModel):
    id: int
    createdAt: datetime
    user_id: int
    name: str
    status: AccountRequestStatusType


class AccountRequest(Model, Base):
    __tablename__ = "account_request"

    user_id: Mapped[int] = Column(Integer, ForeignKey("user.id"))  # type: ignore[assignment]  # noqa: E501
    user: Mapped[User] = relationship("User")  # type: ignore[assignment]

    pin: Mapped[str] = Column(String(64), nullable=False)  # type: ignore[assignment]  # noqa: E501
    status: Mapped[int] = Column(Integer, nullable=False, default=0)  # type: ignore[assignment]  # noqa: E501

    @classmethod
    def create_with_user_and_pin(cls, user: User, pin: str) -> "AccountRequest":
        r = cls()
        r.user_id = user.id
        r.pin = pin
        r.status = AccountRequestStatus.PENDING.value
        return r

    def to_pydantic(self) -> SerializedAccountRequest:
        return SerializedAccountRequest(
            id=self.id,
            createdAt=self.created_at.astimezone(),
            user_id=self.user_id,
            name=self.user.name,
            status=AccountRequestStatus(self.status).name.lower(),
        )

    def serialize(self) -> dict[str, Any]:
        return {
            "id": self.id,
            "createdAt": self.created_at.astimezone().isoformat(),
            "user_id": self.user_id,
            "name": self.user.name,
            "status": AccountRequestStatus(self.status).name.lower(),
        }
