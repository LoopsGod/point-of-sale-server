from datetime import datetime
from typing import Any

import pydantic
from pydantic import BaseModel
from sqlalchemy import String, Column, Boolean, Integer, ForeignKey, UniqueConstraint
from sqlalchemy.orm import Mapped, relationship

from ._base import Base
from .database import Model
from .product import Product


class InvoiceData(BaseModel):
    createdAt: datetime
    invoiceId: str
    vendor: str
    uploadedFileId: str


class Invoice(Model, Base):
    __tablename__ = "invoice"
    invoice_id: Mapped[str | None] = Column(
        String(128), nullable=False
    )  # type: ignore[assignment]
    vendor: Mapped[str] = Column(
        String(128), nullable=False
    )  # type: ignore[assignment]
    uploaded_file_id: Mapped[str | None] = Column(
        String(128), nullable=False
    )  # type: ignore[assignment]
    imported: Mapped[bool] = Column(
        Boolean, default=False, nullable=False
    )  # type: ignore[assignment]

    __table_args__ = (
        UniqueConstraint("vendor", "invoice_id", name="_vendor_invoice_id_uc"),
    )

    @classmethod
    def create_with_details(
        cls,
        invoice_id: str,
        vendor: str,
        uploaded_file_id: str,
        total_value: int,
    ) -> "Invoice":
        r = cls()
        r.invoice_id = invoice_id
        r.vendor = vendor
        r.uploaded_file_id = uploaded_file_id
        return r

    def to_pydantic(self) -> InvoiceData:
        return InvoiceData(
            createdAt=self.created_at.astimezone(),
            invoiceId=self.invoice_id,
            vendor=self.vendor,
            uploadedFileId=self.uploaded_file_id,
        )


class ProductClassificationData(pydantic.BaseModel):
    id: int
    description: str
    product_id: int | None
    multiplier: int | None


class ProductClassification(Model, Base):
    __tablename__ = "product_classification"
    description: Mapped[str] = Column(
        String, nullable=False, unique=True
    )  # type: ignore[assignment]
    multiplier: Mapped[int | None] = Column(
        Integer, nullable=True
    )  # type: ignore[assignment]
    product_id: Mapped[int | None] = Column(
        Integer, ForeignKey("product.id"), nullable=True
    )  # type: ignore[assignment]
    product: Mapped[Product] = relationship("Product")  # type: ignore[assignment]

    def to_pydantic(self) -> ProductClassificationData:
        return ProductClassificationData(
            id=self.id,
            description=self.description,
            product_id=self.product_id,
            multiplier=self.multiplier,
        )

    def serialize(self) -> dict[str, Any]:
        return {
            "id": self.id,
            "description": self.description,
            "product_id": self.product_id,
            "multiplier": self.multiplier,
        }

    def deserialize(self, obj: dict[str, Any]) -> None:
        self.id = obj.get("id", self.id)
        self.description = obj.get("description", self.description)
        self.multiplier = obj.get("multiplier", self.multiplier)
        self.product_id = obj.get("product_id", self.product_id)
