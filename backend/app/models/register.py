from enum import unique, IntFlag
from typing import Any, Literal, cast

import pydantic
from sqlalchemy import Column, ForeignKey, Integer, String, Boolean
from sqlalchemy.orm import relationship, Mapped

from . import Category
from ._base import Base
from .database import Model


class RegisterCategory(Model):
    __tablename__ = "register_category"

    register_id: Mapped[int] = Column(  # type: ignore[assignment]
        Integer, ForeignKey("register.id"), primary_key=True, nullable=False
    )
    category_id: Mapped[int] = Column(  # type: ignore[assignment]
        Integer, ForeignKey("category.id"), primary_key=True, nullable=False
    )


RegisterCapStr = Literal[
    "PAYMENT_CASH",
    "PAYMENT_CARD",
    "PAYMENT_OTHER",
    "PAYMENT_ACCOUNT_AUTHED",
    "PAYMENT_ACCOUNT_OTHER",
    "PAYMENT_ACCOUNT_SELF",
    "PAYMENT_UNDO",
]


# Don't rename these values without also changing the client code.
# These are bitwise flags, each new value doubles the previous one,
# effectively setting a single bit. This way the caps can be stored in the
# database using a single int.
@unique
class RegisterCap(IntFlag):
    # Payment capabilities:
    PAYMENT_CASH = 1
    PAYMENT_CARD = 2
    PAYMENT_OTHER = 4

    # Payments on all accounts with code
    PAYMENT_ACCOUNT_AUTHED = 8

    # Payments on all accounts without code (bar)
    PAYMENT_ACCOUNT_OTHER = 16

    # Payments on your own account
    PAYMENT_ACCOUNT_SELF = 32

    # Can undo purchases
    PAYMENT_UNDO = 64

    @classmethod
    def __get_validators__(cls):
        cls.lookup = {v.name: v for v in list(cls)}
        yield cls.validate

    @classmethod
    def validate(cls, v):
        try:
            return cls.lookup[v]
        except KeyError as e:
            raise ValueError("invalid value") from e


CONFLICTING_CAPABILITIES = {
    RegisterCap.PAYMENT_ACCOUNT_SELF: [
        RegisterCap.PAYMENT_ACCOUNT_OTHER,
        RegisterCap.PAYMENT_ACCOUNT_AUTHED,
        RegisterCap.PAYMENT_CASH,
        RegisterCap.PAYMENT_CARD,
        RegisterCap.PAYMENT_OTHER,
    ],
    RegisterCap.PAYMENT_ACCOUNT_OTHER: [
        RegisterCap.PAYMENT_ACCOUNT_AUTHED,
        RegisterCap.PAYMENT_ACCOUNT_SELF,
    ],
    RegisterCap.PAYMENT_ACCOUNT_AUTHED: [
        RegisterCap.PAYMENT_ACCOUNT_OTHER,
        RegisterCap.PAYMENT_ACCOUNT_SELF,
    ],
    RegisterCap.PAYMENT_CASH: [RegisterCap.PAYMENT_ACCOUNT_SELF],
    RegisterCap.PAYMENT_CARD: [RegisterCap.PAYMENT_ACCOUNT_SELF],
    RegisterCap.PAYMENT_OTHER: [RegisterCap.PAYMENT_ACCOUNT_SELF],
}


class RegisterData(pydantic.BaseModel):
    class RegisterCategoryData(pydantic.BaseModel):
        id: int
        name: str
        color: str

    id: int
    name: str
    isDefault: bool
    capabilities: list[RegisterCapStr]
    categories: list[RegisterCategoryData] | None


class Register(Model, Base):
    """Represents a register, some point of sale"""

    __tablename__ = "register"

    name: Mapped[str] = Column(String, nullable=False)  # type: ignore[assignment]  # noqa: E501

    capabilities: Mapped[int] = Column(Integer, nullable=False, default=0)  # type: ignore[assignment]  # noqa: E501
    default: Mapped[bool] = Column(Boolean, nullable=False, default=False)  # type: ignore[assignment]  # noqa: E501

    categories: Mapped[list[Category]] = relationship(  # type: ignore[assignment]  # noqa: E501
        "Category",
        secondary="register_category",
        order_by="Category.position, Category.name",
        back_populates="registers",
    )

    def _get_caps(self) -> list[RegisterCapStr]:
        cap_names = []
        for cap in RegisterCap:
            if (int(cap) & self.capabilities) != 0 and cap.name is not None:
                cap_names.append(cast(RegisterCapStr, cap.name))

        return cap_names

    def to_pydantic(self, include_categories: bool = True) -> RegisterData:
        return RegisterData(
            id=self.id,
            name=self.name,
            isDefault=self.default,
            capabilities=self._get_caps(),
            categories=[c.to_pydantic() for c in self.categories]
            if include_categories
            else None,
        )

    def serialize(self) -> dict[str, Any]:
        """Serialize the instance into a dict."""

        return {
            "id": self.id,
            "name": self.name,
            "capabilities": self._get_caps(),
            "categoryIds": [c.id for c in self.categories],
            "isDefault": self.default,
        }
