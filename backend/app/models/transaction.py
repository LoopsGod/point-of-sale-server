import enum
from datetime import datetime
from typing import Any

from pydantic import BaseModel
from sqlalchemy import Index, Column, String, Enum
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship, Mapped

from ._base import Base
from .database import Model
from .order import Order, OrderData


class TransactionType(enum.Enum):
    INVALID = 0
    TOP_UP = 1
    TOP_UP_AND_PURCHASE = 2
    DIRECT_PURCHASE = 3
    ACCOUNT_PURCHASE = 4


class TransactionData(BaseModel):
    id: int
    createdAt: datetime
    deletedAt: datetime | None
    orders: list[OrderData]
    deleted: bool
    type: str
    reason: str | None


class Transaction(Model, Base):
    __tablename__ = "transaction"

    nonce: Mapped[str] = Column(String, unique=True, nullable=False)  # type: ignore[assignment]  # noqa: E501
    reason: Mapped[str | None] = Column(String(60), nullable=True)  # type: ignore[assignment]  # noqa: E501

    orders: Mapped[list[Order]] = relationship(  # type: ignore[assignment]  # noqa: E501
        "Order",
        uselist=True,
        back_populates="transaction",
        order_by=Order.price.asc(),
    )

    _type: Mapped[str] = Column(  # type: ignore[assignment]
        Enum(
            "invalid",
            "top_up",
            "top_up_and_purchase",
            "direct_purchase",
            "account_purchase",
            name="transaction_types",
        ),
        name="type",
        nullable=False,
    )

    __table_args__ = (Index("transaction_created_at_idx", "created_at"),)

    @hybrid_property
    def type(self) -> TransactionType | None:
        if self._type is not None:
            return TransactionType[self._type.upper()]

        return None

    # TODO hybrid_properties do not type
    @type.setter  # type: ignore[no-redef]
    def type(self, value: TransactionType):
        self._type = value.name.lower()

    # TODO hybrid_properties do not type
    @type.expression  # type: ignore[no-redef]
    def type(self):
        return self._type

    def to_pydantic(self) -> TransactionData:
        return TransactionData(
            id=self.id,
            createdAt=self.created_at.astimezone(),
            deletedAt=self.deleted_at.astimezone() if self.deleted_at else None,
            orders=[order.to_pydantic() for order in self.orders],
            deleted=self.deleted_at is not None,
            # TODO Hybrid properties do not type yet.
            type=self.type.name,  # type: ignore[attr-defined]
            reason=self.reason,
        )

    def serialize(self) -> dict[str, Any]:
        return {
            "id": self.id,
            "createdAt": self.created_at.astimezone().isoformat(),
            "deletedAt": self.deleted_at.astimezone().isoformat()
            if self.deleted_at
            else None,
            "orders": [order.serialize() for order in self.orders],
            "deleted": self.deleted_at is not None,
            # TODO Hybrid properties do not type yet.
            "type": self.type.name,  # type: ignore[attr-defined]
            "reason": self.reason,
        }
