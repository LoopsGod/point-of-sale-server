from datetime import datetime

from pydantic import BaseModel
from sqlalchemy import Column, Integer, ForeignKey, String
from sqlalchemy.orm import relationship, Mapped

from . import Account
from ._base import Base
from .database import Model


class IdealPaymentData(BaseModel):
    class AccountData(BaseModel):
        id: int
        name: str

    id: int
    createdAt: datetime
    updatedAt: datetime
    deletedAt: datetime | None
    account: AccountData | None
    status: str
    mollieId: str
    transactionId: int | None


class IdealPayment(Model, Base):
    __tablename__ = "ideal_payment"

    account_id: Mapped[int] = Column(Integer, ForeignKey("account.id"), nullable=False)  # type: ignore[assignment]  # noqa: E501
    account: Mapped[Account] = relationship("Account")  # type: ignore[assignment]  # noqa: E501
    status: Mapped[str] = Column(String(128), default="open", nullable=False)  # type: ignore[assignment]  # noqa: E501
    # TODO Check, should this be nullable?
    mollie_id: Mapped[str] = Column(String(128))  # type: ignore[assignment]  # noqa: E501
    transaction_id: Mapped[int | None] = Column(  # type: ignore[assignment]  # noqa: E501
        Integer, ForeignKey("transaction.id"), unique=True, nullable=True
    )

    def to_pydantic(self) -> IdealPaymentData:
        return IdealPaymentData(
            id=self.id,
            createdAt=self.created_at.astimezone(),
            updatedAt=self.updated_at.astimezone(),
            deletedAt=self.deleted_at.astimezone() if self.deleted_at else None,
            account=IdealPaymentData.AccountData(
                id=self.account_id, name=self.account.name
            )
            if self.account
            else None,
            status=self.status,
            mollieId=self.mollie_id,
            transactionId=self.transaction_id,
        )

    def serialize(self):
        return {
            "id": self.id,
            "createdAt": self.created_at.astimezone().isoformat(),
            "updatedAt": self.updated_at.astimezone().isoformat(),
            "deletedAt": self.deleted_at.astimezone().isoformat()
            if self.deleted_at
            else None,
            "account": {
                "id": self.account_id,
                "name": self.account.name,
            }
            if self.account
            else None,
            "status": self.status,
            "mollieId": self.mollie_id,
            "transactionId": self.transaction_id,
        }
