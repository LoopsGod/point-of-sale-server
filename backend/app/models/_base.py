from datetime import datetime
from typing import Any

from pydantic import BaseModel
from sqlalchemy import Table, DateTime, Column, Integer
from sqlalchemy.orm import Mapped


class BaseResponse(BaseModel):
    id: int


class Base:
    """Base class for database models."""

    # TODO SQLAlchemy typing won't work yet, since Model is a Run-time evaluated
    #  base class, which is not supported by mypy.
    #  https://github.com/python/mypy/wiki/Unsupported-Python-Features
    #  For now, we just declare properties we know exist on the DeclarativeBase here.
    #  Sqlalchemy 2.0 (in development) has better typing support

    __table__: Table  # NOTE Also required to inherit Model.

    id: Mapped[int] = Column(Integer, primary_key=True)  # type: ignore[assignment]  # noqa: E501
    created_at: Mapped[datetime] = Column(  # type: ignore[assignment]
        DateTime,
        # We use lambda here such that we can freezegun this time in tests.
        default=lambda: datetime.now(),
        nullable=False,
    )
    updated_at: Mapped[datetime] = Column(  # type: ignore[assignment]
        DateTime,
        # We use lambda here such that we can freezegun this time in tests.
        default=lambda: datetime.now(),
        onupdate=lambda: datetime.now(),
        nullable=False,
    )
    deleted_at: Mapped[datetime | None] = Column(DateTime, nullable=True, default=None)  # type: ignore[assignment]  # noqa: E501

    def serialize(self) -> dict[str, Any]:
        """This should never be called."""
        #  FIXME: At some point we can remove serialize() in favour of to_pydantic.
        return {
            "id": self.id,
        }

    def to_pydantic(self) -> BaseModel:
        """This should never be called."""
        return BaseResponse(id=self.id)

    @property
    def deleted(self) -> bool:
        """True if the row was soft deleted."""
        return self.deleted_at is not None

    @deleted.setter
    def deleted(self, value: bool) -> None:
        self.deleted_at = datetime.now() if value else None
