import typing
from typing import Any

from pydantic import BaseModel, ConstrainedStr, ConstrainedInt
from sqlalchemy import Column, String, ForeignKey, Integer, Boolean
from sqlalchemy.orm import relationship, Mapped

from ._base import Base
from .database import Model

if typing.TYPE_CHECKING:
    from app.models.order import Order
    from app.models.category import Category


class PriceDecimal(ConstrainedInt):
    strict = True
    ge = 0


class NonEmptyString(ConstrainedStr):
    min_length = 1


class NewProductData(BaseModel):
    id: int | None
    name = NonEmptyString()
    price = PriceDecimal()
    ledgerId: int
    deposit: bool = False
    categoryIds: list[int]
    quantity: int = 0
    deleted: bool = False


class ProductData(NewProductData):
    id: int


class Product(Model, Base):
    """Represent a product."""

    __tablename__ = "product"
    name: Mapped[str] = Column(String, nullable=False)  # type: ignore[assignment]  # noqa: E501
    price: Mapped[int] = Column(Integer, nullable=False)  # type: ignore[assignment]  # noqa: E501
    ledger_id: Mapped[int] = Column(Integer, ForeignKey("account.id"), nullable=False)  # type: ignore[assignment]  # noqa: E501
    deposit: Mapped[bool] = Column(Boolean, nullable=False)  # type: ignore[assignment]  # noqa: E501
    quantity: Mapped[int] = Column(Integer, nullable=False, server_default="0")  # type: ignore[assignment]  # noqa: E501
    orders: Mapped[list["Order"]] = relationship("Order", back_populates="product")  # type: ignore[assignment]  # noqa: E501

    categories: Mapped[list["Category"]] = relationship(  # type: ignore[assignment]  # noqa: E501
        "Category",
        secondary="product_category",
        order_by="Category.position, Category.name",
        back_populates="products",
    )

    @property
    def price_str(self) -> str:
        """String representation of the price, in the form of 1.23."""
        return f"{self.price / 100:.2f}"

    @price_str.setter
    def price_str(self, value: str) -> None:
        self.price = int(float(value) * 100)

    def serialize(self) -> dict[str, Any]:
        """Serialize the instance into a dict."""
        return {
            "id": self.id,
            "name": self.name,
            "price": self.price,
            "ledgerId": self.ledger_id,
            "deposit": self.deposit,
            "deleted": self.deleted,
            "categoryIds": [category.id for category in self.categories],
            "quantity": self.quantity,
        }

    def to_pydantic(self) -> ProductData:
        return ProductData(
            id=self.id,
            name=self.name,
            price=self.price,
            ledgerId=self.ledger_id,
            deposit=self.deposit,
            deleted=self.deleted,
            categoryIds=[category.id for category in self.categories],
            quantity=self.quantity,
        )

    def deserialize(self, obj: dict[str, Any]) -> None:
        """Deserialize a dict into the instance."""
        self.id = obj.get("id", self.id)
        self.name = obj.get("name", self.name)
        self.price = obj.get("price", self.price)
        self.ledger_id = obj.get("ledgerId", self.ledger_id)
        self.deposit = obj.get("deposit", self.deposit)
        self.quantity = obj.get("quantity", self.quantity)
