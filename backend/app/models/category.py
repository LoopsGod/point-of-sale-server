"""Represent a category of Products."""
import typing
from typing import Any

import pydantic
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship, Mapped

from . import Product
from ._base import Base
from .database import Model

if typing.TYPE_CHECKING:
    from app.models.register import Register


class ProductCategory(Model):
    __tablename__ = "product_category"

    product_id: Mapped[int] = Column(  # type: ignore[assignment]
        Integer, ForeignKey("product.id"), primary_key=True, nullable=False
    )
    category_id: Mapped[int] = Column(  # type: ignore[assignment]
        Integer, ForeignKey("category.id"), primary_key=True, nullable=False
    )


class NewCategoryData(pydantic.BaseModel):
    name: str = pydantic.Field(max_length=64)
    position: int = pydantic.Field(lt=42069, gt=-42069)
    color: int
    deleted: bool


class CategoryData(NewCategoryData):
    id: int


class Category(Model, Base):
    """Represent a category of Products."""

    __tablename__ = "category"

    name: Mapped[str] = Column(String(64), nullable=False)  # type: ignore[assignment]  # noqa: E501
    position: Mapped[int] = Column(Integer, nullable=False)  # type: ignore[assignment]  # noqa: E501
    color: Mapped[int] = Column(Integer, nullable=False)  # type: ignore[assignment]  # noqa: E501

    products: Mapped[list[Product]] = relationship(  # type: ignore[assignment]  # noqa: E501
        "Product",
        secondary="product_category",
        order_by="Product.name",
        back_populates="categories",
    )

    registers: Mapped[list["Register"]] = relationship(  # type: ignore[assignment]  # noqa: E501
        "Register",
        secondary="register_category",
        order_by="Register.name",
        back_populates="categories",
    )

    def __str__(self) -> str:
        return self.name

    def serialize(self) -> dict[str, Any]:
        """Serializes the instance into a dict."""
        return {
            "id": self.id,
            "name": self.name,
            "position": self.position,
            "color": self.color,
            "deleted": self.deleted,
        }

    def to_pydantic(self) -> CategoryData:
        return CategoryData(
            id=self.id,
            name=self.name,
            position=self.position,
            color=self.color,
            deleted=self.deleted,
        )

    def deserialize(self, obj: dict[str, Any]) -> None:
        """Deserializes a dict into the instance."""
        self.id = obj.get("id", self.id)
        self.name = obj.get("name", self.name)
        self.position = obj.get("position", self.position)
        self.color = obj.get("color", self.color)
