import json
from typing import Any
from pydantic import BaseModel, BaseSettings, Extra, validator
from sqlalchemy import Column, String
from sqlalchemy.orm import Mapped

from ._base import Base
from .checkout import CheckoutMethod
from .database import Model


class Setting(Model, Base):
    __tablename__ = "setting"

    key: Mapped[str] = Column(String(128), unique=True, nullable=False)  # type: ignore[assignment]  # noqa: E501
    value: Mapped[str] = Column(String(4096), nullable=False)  # type: ignore[assignment]  # noqa: E501

    @property
    def as_bool(self) -> bool:
        return self.value.lower() == "true"

    def __repr__(self) -> str:
        return f"Setting({self.key}={self.value})"


class CheckoutReason(BaseModel):
    name: str
    pin: str | None = None


class DatabaseSettings(BaseSettings):
    summary_included_categories: list[int] = []

    notification_sender: str = "pos@svia.nl"
    notification_sepa_destination: str = "penningmeester@svia.nl"
    notification_account_request_destination: str = "secretaris@svia.nl"
    notification_product_quantity_destination: str = "kelder@svia.nl"

    exact_storage_refresh_token: str | None = None
    exact_storage_division: str | None = None
    exact_storage_client_secret: str | None = None
    exact_storage_client_id: str | None = None
    exact_storage_base_url: str = "https://pos.svia.nl"

    exact_setup_finished: bool = False
    exact_old_division: str | None = None
    exact_journal_pos_description: str | None = None
    exact_journal_pos_code: str | None = None

    checkout_reasons: list[CheckoutReason] = []
    checkout_reader_secret: str | None = None

    april_fools_enabled: bool = False

    direct_debit_creditor_name: str | None = None
    direct_debit_creditor_iban: str | None = None
    direct_debit_creditor_bic: str | None = None
    direct_debit_creditor_identifier: str | None = None

    cash_checkout_method_account_id: str | None = None
    card_checkout_method_account_id: str | None = None
    transfer_checkout_method_account_id: str | None = None
    ideal_checkout_method_account_id: str | None = None
    sepa_checkout_method_account_id: str | None = None
    other_checkout_method_account_id: str | None = None

    def get_setting_for_checkout_method(self, method: CheckoutMethod) -> str | None:
        mapping = {
            CheckoutMethod.CASH: self.cash_checkout_method_account_id,
            CheckoutMethod.CARD: self.card_checkout_method_account_id,
            CheckoutMethod.TRANSFER: self.transfer_checkout_method_account_id,
            CheckoutMethod.IDEAL: self.ideal_checkout_method_account_id,
            CheckoutMethod.SEPA: self.sepa_checkout_method_account_id,
            CheckoutMethod.OTHER: self.other_checkout_method_account_id,
            CheckoutMethod.ACCOUNT: None,
        }
        return mapping.get(method)

    def set_setting_for_checkout_method(
        self, method: CheckoutMethod, value: str
    ) -> None:
        if method == CheckoutMethod.CASH:
            self.cash_checkout_method_account_id = value
        elif method == CheckoutMethod.CARD:
            self.card_checkout_method_account_id = value
        elif method == CheckoutMethod.TRANSFER:
            self.transfer_checkout_method_account_id = value
        elif method == CheckoutMethod.IDEAL:
            self.ideal_checkout_method_account_id = value
        elif method == CheckoutMethod.SEPA:
            self.sepa_checkout_method_account_id = value
        elif method == CheckoutMethod.OTHER:
            self.other_checkout_method_account_id = value
        else:
            raise ValueError(f"Invalid checkout method: {method}")

    @validator("summary_included_categories", "checkout_reasons", pre=True)
    def load_json(cls, v: Any) -> Any:
        if isinstance(v, str):
            return json.loads(v)
        return v

    class Config:
        extra = Extra.allow
