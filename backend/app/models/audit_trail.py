from datetime import datetime
from typing import Any

import pydantic
from sqlalchemy import Column, String, Integer, ForeignKey
from sqlalchemy.orm import relationship, Mapped

from . import User
from ._base import Base
from .database import Model


class AuditTrailUser(pydantic.BaseModel):
    id: int
    name: str


class AuditTrailData(pydantic.BaseModel):
    createdAt: datetime
    resource: str
    event: str
    comment: str | None
    user: AuditTrailUser | None
    objectId: int | None


class AuditTrail(Model, Base):
    __tablename__ = "audit_trail"

    resource: Mapped[str] = Column(String(128), nullable=False)  # type: ignore[assignment]  # noqa: E501
    event: Mapped[str] = Column(String(128), nullable=False)  # type: ignore[assignment]  # noqa: E501
    # TODO Issue Consider deleting comment, it is not used.
    comment: Mapped[str | None] = Column(String(128), nullable=True)  # type: ignore[assignment]  # noqa: E501

    # TODO Issue for event:"transaction.upgrade.ideal" has no user_id?
    user_id: Mapped[int | None] = Column(Integer, ForeignKey("user.id"), nullable=True)  # type: ignore[assignment]  # noqa: E501
    user: Mapped[User | None] = relationship("User", uselist=False)  # type: ignore[assignment]  # noqa: E501

    # TODO Issue for event:"order.update.account_id" has no object_id.
    object_id: Mapped[int | None] = Column(Integer, nullable=True)  # type: ignore[assignment]  # noqa: E501

    def to_pydantic(self) -> AuditTrailData:
        return AuditTrailData(
            createdAt=self.created_at.astimezone(),
            resource=self.resource,
            event=self.event,
            comment=self.comment,
            user=AuditTrailUser(id=self.user_id, name=self.user.username)
            if self.user
            else None,
            objectId=self.object_id,
        )

    def serialize(self) -> dict[str, Any]:
        return {
            "createdAt": self.created_at.astimezone().isoformat(),
            "resource": self.resource,
            "event": self.event,
            "comment": self.comment,
            "user": {
                "id": self.user_id,
                "name": self.user.username,
            }
            if self.user
            else None,
            "objectId": self.object_id,
        }
