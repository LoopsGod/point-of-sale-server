import os

import sqlalchemy
from redis import Redis
from sqlalchemy.orm import sessionmaker, registry, DeclarativeMeta

mapper_registry = registry()


class Model(metaclass=DeclarativeMeta):
    __abstract__ = True

    registry = mapper_registry
    metadata = mapper_registry.metadata

    __init__ = mapper_registry.constructor


engine = sqlalchemy.create_engine(
    url=os.environ["SQLALCHEMY_DATABASE_URI"],
    future=True,
)

SessionLocal = sessionmaker(bind=engine, future=True)


class RedisKey:
    # TODO In the future it might be nice to do validate keys do not overlap.
    #  It can use __init_subclass__.
    pass


broker = Redis.from_url(os.environ["BROKER_URL"])
