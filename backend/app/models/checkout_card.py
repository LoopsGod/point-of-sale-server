import typing

from sqlalchemy import Column, String, Integer, ForeignKey
from sqlalchemy.orm import Mapped, relationship

from ._base import Base
from .database import Model

if typing.TYPE_CHECKING:
    from app.models.user import User


class CheckoutCard(Model, Base):
    __tablename__ = "checkout_card"
    card_secret: Mapped[str] = Column(String(64), unique=True, nullable=False)  # type: ignore[assignment]  # noqa: E501
    user_id: Mapped[int] = Column(Integer, ForeignKey("user.id"), nullable=False)  # type: ignore[assignment]  # noqa: E501
    user: Mapped["User"] = relationship("User")  # type: ignore[assignment]  # noqa: E501
