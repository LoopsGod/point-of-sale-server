import typing
from pydantic import BaseModel, Field

from sqlalchemy import Column, Integer, ForeignKey, Float, String, Boolean
from sqlalchemy.orm import Mapped, relationship

from ._base import Base
from .database import Model

if typing.TYPE_CHECKING:
    from app.models.product import Product


class NewSlideProductData(BaseModel):
    class NewProduct(BaseModel):
        id: int

    name: str = Field(min_length=3, max_length=64)
    product: NewProduct
    weight: float
    order: int
    show: bool
    trends: bool
    spacing: bool


class SlideProductData(NewSlideProductData):
    class Product(NewSlideProductData.NewProduct):
        name: str

    id: int
    product: Product


class SlideProduct(Model, Base):
    __tablename__ = "slide_product"
    product_id: Mapped[int] = Column(
        Integer, ForeignKey("product.id"), nullable=False
    )  # type: ignore[assignment]
    product: Mapped["Product"] = relationship("Product")  # type: ignore[assignment]  # noqa: E501
    # The weight attribute is for calculating the "bpm" (beers per minute) stat.
    # This flexibility is needed for some items that account for more (e.g. a
    # pitcher) and some items we want to be accounted for, while strictly not being
    # beer (soft drinks), and some we want to exclude.
    weight: Mapped[float] = Column(Float, nullable=False, default=1.0)  # type: ignore[assignment]  # noqa: E501
    name: Mapped[str] = Column(String(64), nullable=False)  # type: ignore[assignment]  # noqa: E501
    order: Mapped[int] = Column(Integer, nullable=False)  # type: ignore[assignment]  # noqa: E501
    show_slide: Mapped[bool] = Column(Boolean, default=True, nullable=False)  # type: ignore[assignment]  # noqa: E501
    show_trends: Mapped[bool] = Column(Boolean, default=True, nullable=False)  # type: ignore[assignment]  # noqa: E501
    add_spacing: Mapped[bool] = Column(Boolean, default=False, nullable=False)  # type: ignore[assignment]  # noqa: E501

    def to_pydantic(self) -> SlideProductData:
        return SlideProductData(
            id=self.id,
            name=self.name,
            product=SlideProductData.Product(
                id=self.product.id,
                name=self.product.name,
            ),
            weight=self.weight,
            order=self.order,
            show=self.show_slide,
            trends=self.show_trends,
            spacing=self.add_spacing,
        )
