from datetime import datetime
from typing import Any

from pydantic import BaseModel
from sqlalchemy import Column, Integer, ForeignKey, String, DateTime, Boolean
from sqlalchemy.orm import relationship, Mapped, AppenderQuery
from sqlalchemy.schema import UniqueConstraint

from . import Order
from ._base import Base
from .database import Model
from app.models.account import Account


class SepaBatchData(BaseModel):
    id: int
    description: str
    executionDate: datetime | None
    mailDate: datetime | None
    success: bool
    deletedAt: datetime | None


class SepaBatch(Model, Base):
    __tablename__ = "sepa_batch"

    description: Mapped[str] = Column(String(64), nullable=False)  # type: ignore[assignment]  # noqa: E501
    execution_date: Mapped[datetime] = Column(DateTime, nullable=False)  # type: ignore[assignment]  # noqa: E501
    success: Mapped[bool | None] = Column(Boolean, nullable=True, default=False)  # type: ignore[assignment]  # noqa: E501
    mail_date: Mapped[datetime | None] = Column(DateTime(), nullable=True)  # type: ignore[assignment]  # noqa: E501

    orders: Mapped[list[Order]] = relationship("Order", back_populates="sepa_batch")  # type: ignore[assignment]  # noqa: E501
    pending_orders: Mapped[AppenderQuery] = relationship("SepaBatchOrder", lazy="dynamic")  # type: ignore[assignment]  # noqa: E501

    def to_pydantic(self) -> SepaBatchData:
        return SepaBatchData(
            id=self.id,
            description=self.description,
            executionDate=self.execution_date.astimezone()
            if self.execution_date
            else None,
            mailDate=self.mail_date.astimezone()
            if self.mail_date
            else None
            if self.mail_date
            else None,
            success=self.success or False,
            deletedAt=self.deleted_at.astimezone() if self.deleted_at else None,
        )

    def serialize(self) -> dict[str, Any]:
        return {
            "id": self.id,
            "description": self.description,
            "executionDate": self.execution_date.astimezone().isoformat(),
            "mailDate": self.mail_date.astimezone().isoformat()
            if self.mail_date
            else None,
            "success": self.success,
            "deleted": self.deleted,
        }

    def deserialize(self, obj: dict[str, Any]) -> None:
        self.id = obj.get("id", self.id)
        self.description = obj.get("description", self.description)
        self.execution_date = obj.get("executionDate", self.execution_date)
        self.success = obj.get("success", self.success)


class SepaBatchOrderData(BaseModel):
    sepaBatchId: int
    accountId: int
    price: int
    deleted: bool


class SepaBatchOrder(Model, Base):
    __tablename__ = "sepa_batch_order"

    sepa_batch_id: Mapped[int] = Column(Integer, ForeignKey("sepa_batch.id"), nullable=False)  # type: ignore[assignment]  # noqa: E501
    account_id: Mapped[int] = Column(Integer, ForeignKey("account.id"), nullable=False)  # type: ignore[assignment]  # noqa: E501
    account: Mapped[Account] = relationship("Account")  # type: ignore[assignment]  # noqa: E501

    price: Mapped[int] = Column(Integer, nullable=False)  # type: ignore[assignment]  # noqa: E501

    __table_args__ = (UniqueConstraint(sepa_batch_id, account_id),)

    def to_pydantic(self) -> SepaBatchOrderData:
        return SepaBatchOrderData(
            sepaBatchId=self.sepa_batch_id,
            accountId=self.account_id,
            price=self.price,
            deleted=self.deleted,
        )

    def serialize(self) -> dict[str, Any]:
        return {
            "sepaBatchId": self.sepa_batch_id,
            "accountId": self.account_id,
            "price": self.price,
            "deleted": self.deleted,
        }
