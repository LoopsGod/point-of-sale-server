import dataclasses
from collections.abc import Generator
from datetime import date, datetime

from fastapi import Depends
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from sqlalchemy.orm import Session

from app.models import LoginToken, User, RegisterCap, Account
from app.models.database import SessionLocal
from app.service import login_service

# Use HTTPBearer instead of Header to include security model in openapi spec.
from app.util.util import DatetimeRangeParameters
from app.views.exceptions import AuthorizationException, NotFoundException

oauth_scheme = HTTPBearer(auto_error=False)
oauth_scheme_required = HTTPBearer()


@dataclasses.dataclass
class RequestContext:
    db_session: Session


@dataclasses.dataclass
class UserRequestContext(RequestContext):
    """
    Convenience class for passing generally needed dependencies to services.

    With FastAPI all dependencies are pass to services are arguments, not globally.
    Almost all services create audit logs and therefor require the user and database.
    """

    user: User


@dataclasses.dataclass
class AccountRequestContext(UserRequestContext):
    """
    UserRequestContext with user that has an account.
    """

    account: Account


def find_auth_token(
    authorization: HTTPAuthorizationCredentials = Depends(oauth_scheme),
) -> str | None:
    if authorization is not None:
        return authorization.credentials
    return None


def get_db() -> Generator[Session, None, None]:
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def find_login_token(
    access_token: str | None = Depends(find_auth_token),
    db_session: Session = Depends(get_db),
) -> LoginToken | None:
    return login_service.get_token(db_session, access_token)


def login_required(
    authorization: HTTPAuthorizationCredentials = Depends(oauth_scheme_required),
    db_session: Session = Depends(get_db),
) -> UserRequestContext:
    token = login_service.get_token(db_session, authorization.credentials)
    if not token:
        raise AuthorizationException("invalid_access_token")
    return UserRequestContext(db_session=db_session, user=token.user)


def account_required(
    context: UserRequestContext = Depends(login_required),
) -> AccountRequestContext:
    if not context.user.account or context.user.account.deleted:
        raise NotFoundException("no_linked_account")
    return AccountRequestContext(
        db_session=context.db_session, user=context.user, account=context.user.account
    )


def admin_required(
    authorization: HTTPAuthorizationCredentials = Depends(oauth_scheme_required),
    db_session: Session = Depends(get_db),
) -> UserRequestContext:
    token = login_service.get_token(db_session, authorization.credentials)
    if not token:
        raise AuthorizationException("invalid_access_token")

    user = token.user

    if not user.is_admin:
        raise AuthorizationException("insufficient_permissions")

    if not token.tfa_enabled:
        raise AuthorizationException("insufficient_permissions")

    return UserRequestContext(db_session=db_session, user=token.user)


def get_date_range(start: date = None, end: date = None) -> DatetimeRangeParameters:
    if start is None:
        start = datetime(1970, 1, 1)
    if end is None:
        end = datetime.now()

    start = datetime.combine(start, datetime.min.time())
    end = datetime.combine(end, datetime.max.time())

    return DatetimeRangeParameters(start=start, end=end)


class RegisterCapabilities:
    """
    Dependency injection with register capability checking.
    """

    def __init__(self, require_any_of: list[RegisterCap] = None) -> None:
        super().__init__()
        self.require_any_of: list[RegisterCap] = require_any_of or []

    def __call__(
        self, context: UserRequestContext = Depends(login_required)
    ) -> list[RegisterCap]:
        # We need to do a local import here, since register_service imports
        # the UserRequestContext from above.
        from app.service import register_service

        register = register_service.get_register_for_user(
            context.db_session, context.user
        )

        if not context.user.is_admin:
            if not any(
                (cap.value & register.capabilities) != 0 for cap in self.require_any_of
            ):
                raise AuthorizationException("insufficient_permissions")

        return register_service.get_capabilities_for_register(register)
