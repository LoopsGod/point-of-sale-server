import click
from sqlalchemy import select, text

from . import models
from .models.database import SessionLocal


@click.group()
def cli():
    """VIA Point of Sale"""


@cli.command()
@click.option("--username", prompt=True)
@click.option("--password", prompt=True, hide_input=True, confirmation_prompt=True)
@click.option("--admin/--no-admin", default=False)
def create_user(username: str, password: str, admin: bool) -> None:
    """Create a new local user with username and password."""
    with SessionLocal.begin() as db_session:
        models.user.User.register_user(
            db_session=db_session, username=username, password=password, is_admin=admin
        )


@cli.command()
@click.option("--username", prompt=True)
def make_admin(username: str) -> None:
    """Set the User.is_admin flag to true for a user."""
    User = models.user.User
    with SessionLocal.begin() as db_session:
        user = db_session.scalar(select(User).filter(User.username == username))

        if user:
            user.is_admin = True
            user.long_login = True
            print(f"{username} is now admin.")
        else:
            print(f"Did not find user '{username}'.")


@cli.command()
@click.option("--username", prompt=True)
def enable_tfa(username: str) -> None:
    """Set the LoginToken.tfa_enbled flag to true for all tokens of the user."""
    LoginToken = models.login_token.LoginToken
    User = models.login_token.User
    with SessionLocal.begin() as db_session:
        login_tokens = db_session.scalars(
            select(LoginToken).join(User).filter(User.username == username)
        )
        for login_token in login_tokens:
            login_token.tfa_enabled = True
    print(f"Enabled tfa for all tokens  of {username}")


@cli.command()
def make_fixtures() -> None:
    """Loads all fixtures into the database as test data."""
    from tests.fixtures.account import (
        ledger_accounts,
        event_account,
        normal_account,
        sepa_account,
        pin,
        account_ids_checkout_method,
    )
    from tests.fixtures.products import products
    from tests.fixtures.categories import categories
    from tests.fixtures.registers import registers
    from tests.fixtures.sepa import sepa_batch, sepa_config
    from tests.fixtures.users import sale_points, users, password, sepa_user

    session = SessionLocal()

    # Clear all rows that can conflict
    session.execute(
        text(
            """DELETE FROM product_category;
DELETE FROM "order";
DELETE FROM transaction;
DELETE FROM register_category;
DELETE FROM category;
DELETE FROM login_token;
DELETE FROM user_direct_debit;
DELETE FROM "user" WHERE viaduct_user_id IS NULL;
DELETE FROM register;
DELETE FROM product;
DELETE FROM "account" WHERE name ILIKE '%User%';
DELETE FROM "setting" WHERE key LIKE '%_CHECKOUT_METHOD_ACCOUNT_ID';
DELETE FROM "account_exact_ledger";
DELETE FROM "account" WHERE TYPE!='user';
DELETE FROM sepa_batch;"""
        )
    )

    pin_dict = pin.__wrapped__()  # type: ignore[attr-defined]
    password_dict = password.__wrapped__()  # type: ignore[attr-defined]

    ledgers = ledger_accounts.__wrapped__(session)  # type: ignore[attr-defined]
    prods = products.__wrapped__(session, ledgers)  # type: ignore[attr-defined]
    cats = categories.__wrapped__(session, prods)  # type: ignore[attr-defined]
    regs = registers.__wrapped__(session, cats)  # type: ignore[attr-defined]

    account_ids_checkout_method.__wrapped__(session)  # type: ignore[attr-defined]

    sepa_u = sepa_user.__wrapped__(  # type: ignore[attr-defined]
        session, regs, password_dict
    )

    event_account.__wrapped__(session)  # type: ignore[attr-defined]
    normal_acc = normal_account.__wrapped__(  # type: ignore[attr-defined]
        session, pin_dict
    )
    sepa_account.__wrapped__(session, sepa_u, pin_dict)  # type: ignore[attr-defined]

    sepa_config.__wrapped__(session)  # type: ignore[attr-defined]
    sepa_batch.__wrapped__(session)  # type: ignore[attr-defined]

    sale_points.__wrapped__(session, regs, password_dict)  # type: ignore[attr-defined]
    users.__wrapped__(  # type: ignore[attr-defined]
        session, regs, normal_acc, password_dict
    )
    session.commit()
    session.close()

    print("Created database entries with 3 users: user1@gmail.com.")
    print("Passwords are 'password', pins are '1234'.")


if __name__ == "__main__":
    cli()
