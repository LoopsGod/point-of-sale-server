"""Initializes the webapp"""
import logging.config
import os
import pathlib

import sentry_sdk
from cachelib import RedisCache
from celery import Celery
from fastapi import FastAPI
from sentry_sdk.integrations.redis import RedisIntegration
from sentry_sdk.integrations.sqlalchemy import SqlalchemyIntegration
from uvicorn.middleware.proxy_headers import ProxyHeadersMiddleware
from werkzeug.urls import url_parse, BaseURL

import config

logging_conf_file = pathlib.Path(__file__).parent.parent / "logging.conf"
logging.config.fileConfig(logging_conf_file, disable_existing_loggers=False)

_logger = logging.getLogger(__name__)

worker = Celery("pos")

# Log SQL statements
# import logging
# logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

# Reminiscence of flask times, used in production.
environment = os.environ.get("ENVIRONMENT", "production")

# Sentry
sentry_sdk.init(
    dsn=getattr(config, "SENTRY_DSN", None),
    integrations=[SqlalchemyIntegration(), RedisIntegration()],
    environment=environment,
    traces_sample_rate=1.0,
)

redis_url: BaseURL = url_parse(config.BROKER_URL)
cache = RedisCache(
    host=redis_url.host,
    port=redis_url.port if redis_url.port else 6379,
)

worker.config_from_object(
    {
        "result_backend": "rpc://",
        "task_serializer": "pickle",
        "accept_content": ["pickle", "json"],
        "result_serializer": "pickle",
        "broker_url": config.BROKER_URL,
        "broker_transport_options": {"max_retries": 3},
    }
)

fastapi_app = FastAPI(
    title="VIA Point of Sale",
    description="""
The Point of Sale a product of Vereniging Informatiewetenschappen Amsterdam (V.I.A.),
 located at Science Park 900, Room L0.04, 1098XH Amsterdam. The Point of Sale is
 published under the GNU Affero General Public License Version 3.
 Source of this product is published on Gitlab.com.
""".strip(),
    version=environment.capitalize(),
    openapi_url="/api/openapi.json",
    servers=[
        {"url": "http://localhost:5000", "description": "Development"},
        {"url": "https://pos.svia.nl", "description": "Production environment"},
    ],
    contact={
        "name": "Studievereniging VIA",
        "url": "https://svia.nl",
        "email": "ict@svia.nl",
    },
    terms_of_service="https://pos.svia.nl/about",
    license_info={
        "name": "GNU Affero General Public License Version 3",
        "url": "https://gitlab.com/studieverenigingvia/point-of-sale-server/"
        "-/blob/master/LICENSE",
    },
)

fastapi_app.add_middleware(ProxyHeadersMiddleware, trusted_hosts="*")

from . import models, views, tasks  # noqa: F401, E402

_logger.info("Imported module %s", views)
_logger.info("Imported module %s", models)

fastapi_app.include_router(views.fapi)
fastapi_app.include_router(views.fapi_admin)
fastapi_app.include_router(views.fapi_auth)
