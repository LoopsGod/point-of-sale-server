variables:
  BUILD_ID: build-${CI_PIPELINE_IID}
  BUILD_IMAGE_FRONTEND: ${CI_REGISTRY_IMAGE}/frontend:${BUILD_ID}
  BUILD_IMAGE_BACKEND: ${CI_REGISTRY_IMAGE}/backend:${BUILD_ID}

  IMAGE_FRONTEND: ${CI_REGISTRY_IMAGE}/frontend:$CI_COMMIT_REF_SLUG
  IMAGE_BACKEND: ${CI_REGISTRY_IMAGE}/backend:$CI_COMMIT_REF_SLUG

stages:
- lint
- build
- test
- tag
- deploy
- health
- release

pre-commit:
  stage: lint
  needs: [ ]
  image: ${CI_REGISTRY_IMAGE}/lint:latest
  cache:
    paths:
      - "${CI_PROJECT_DIR}/.pre-commit"
  script:
    - pre-commit run --all-files


lint-frontend:
  stage: lint
  image: node:18-alpine
  before_script: []
  script:
    - cd frontend
    - npm ci --no-progress --omit=optional
    - npm run check
  #  Allow failure because the frontend still has a number of type errors.
  #  FIXME: Remove this when type errors have been fixed
  allow_failure: true

build-frontend:
  stage: build
  needs: [ ]
  services: []
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [ "" ]
  script:
    - >-
      /kaniko/executor
      --context "${CI_PROJECT_DIR}/frontend"
      --dockerfile "${CI_PROJECT_DIR}/frontend/docker/prod.Dockerfile"
      --cache=true
      --destination "${BUILD_IMAGE_FRONTEND}"
      --build-arg=FRONTEND_SENTRY_DSN=${FRONTEND_SENTRY_DSN}
      --build-arg=CI_COMMIT_SHORT_SHA=${CI_COMMIT_SHORT_SHA}

build-backend:
  stage: build
  needs: [ ]
  services: []
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [ "" ]
  script:
    - >-
      /kaniko/executor
      --context "${CI_PROJECT_DIR}/backend"
      --dockerfile "${CI_PROJECT_DIR}/backend/docker/Dockerfile"
      --cache=true
      --destination "${BUILD_IMAGE_BACKEND}"

tag:
  stage: tag
  image:
    name: gcr.io/go-containerregistry/crane:debug
    entrypoint: [ "" ]
  script:
    - crane auth login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - crane tag ${BUILD_IMAGE_FRONTEND} $CI_COMMIT_REF_SLUG
    - crane tag ${BUILD_IMAGE_BACKEND} $CI_COMMIT_REF_SLUG



pytest:
  services:
    - postgres
    - redis
  stage: test
  image: $BUILD_IMAGE_BACKEND
  variables:
    SQLALCHEMY_DATABASE_TEST_URI: "postgresql+psycopg2://runner:runner@postgres/test"
    POSTGRES_DB: test
    POSTGRES_USER: runner
    POSTGRES_PASSWORD: runner
    SQLALCHEMY_WARN_20: 1
  script:
    - cp backend/docker/gitlab-ci/test_config.py backend/config.py
    - cd backend && pytest --cov=app --cov-branch --cov-report html --cov-report xml --cov-report term-missing:skip-covered  --junitxml=report.xml
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: backend/coverage.xml
      junit: backend/report.xml
    paths:
      - backend/htmlcov/
    expire_in: 1 week
  coverage: /^TOTAL.*\s+(\d+\%)$/
  needs: ["build-backend"]

mypy:
  image: $BUILD_IMAGE_BACKEND
  stage: test
  needs: ["build-backend"]
  script:
    - cp backend/docker/gitlab-ci/test_config.py backend/config.py
    - cd backend && mypy app tests


deploy_develop:
  stage: deploy
  image: alpine:latest
  before_script:
  - apk update && apk add openssh-client sshpass
  script:
  - sshpass -p $DEPLOYMENT_SERVER_PASS ssh -o StrictHostKeyChecking=no -o PreferredAuthentications=password -o PubkeyAuthentication=no git@svia.nl "docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com && cd /opt/docker/containers/${CI_ENVIRONMENT_NAME}.pos.svia.nl && docker-compose pull backend frontend worker && docker-compose up -d backend frontend worker"
  environment:
    name: develop
    url: https://develop.pos.svia.nl
  only:
    refs:
    - master

health_check_develop:
  stage: health
  image: $IMAGE_BACKEND
  script:
    - python ./backend/docker/gitlab-ci/healthcheck.py
  environment:
    name: develop
    url: https://develop.pos.svia.nl
  only:
    refs:
      - master

release_production:
  stage: release
  image: alpine:latest
  before_script:
  - apk update && apk add openssh-client sshpass
  script:
  - sshpass -p $DEPLOYMENT_SERVER_PASS ssh -o StrictHostKeyChecking=no -o PreferredAuthentications=password -o PubkeyAuthentication=no git@svia.nl "docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com && cd /opt/docker/containers/pos.svia.nl && docker-compose pull backend frontend worker && docker-compose up -d backend frontend worker"
  environment:
    name: production
    url: https://pos.svia.nl
  only:
    refs:
    - master
  when: manual
  allow_failure: false


health_check_master:
  stage: .post
  image: $IMAGE_BACKEND
  script:
    - python ./backend/docker/gitlab-ci/healthcheck.py
  environment:
    name: production
    url: https://pos.svia.nl
  only:
    refs:
      - master
